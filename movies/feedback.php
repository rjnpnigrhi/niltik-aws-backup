<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
echo '<div class="container">';
    if(isset($_GET['insert'])){
        //add to database
        $name = $db->safe_data($_POST['name']);
        $email = $db->safe_data($_POST['email']);
        $mobile = $db->safe_data($_POST['mobile']);
        $feedback = $db->safe_data($_POST['feedback']);

        date_default_timezone_set('Asia/Kolkata');
        $date = date("m/d/Y");

        $confirm_code = random_code(8);

        $q = "INSERT INTO feedback (name, email, mobile, feedback, date) VALUES ('$name', '$email', '$mobile', '$feedback', '$date')";
        $r = $db->insert($q);

        echo '<p>'."Thank You for Posting Your Feedback.<br><br>The Feedback is forwarded to our Administrator.<br><br>If you can, then please share this App with your friends and people you know on Social media like Facebook, Twitter etc. <br><br>Thank you Once Again.".'</p>';
        echo '<a href="index.php" class="btn btn-sm btn-success">'."Back to Home Page".'</a>';
    }else{
        //show form
    ?>
    <div class="col-sm-6 col-sm-offset-3">        
    <h3>Fill the form below : </h3>
	   <form action="feedback.php?insert=true" method="post" class="form">
	       <div class="form-group">
	           <label for="name">Your name :</label>
	           <input type="text" name="name" class="form-control" placeholder="Type Your Name" required>
	       </div>
	       
	       <div class="form-group">
	           <label for="email">Your e-Mail ID :</label>
	           <input type="email" name="email" class="form-control" placeholder="Type active email ID" required>
	       </div>
	       
	       <div class="form-group">
	           <label for="mobile">Mobile Number :</label>
	           <input type="text" name="mobile" class="form-control" placeholder="Mobile Number">
	       </div>
	       
	       <div class="form-group">
	           <label for="feedback">Feedback :</label>
	           <textarea name="feedback" id="feedback" class="form-control" cols="30" rows="10" required></textarea>
	       </div>
	       
	       <div class="form-group">
	           <button class="btn btn-default btn-sm" type="submit">Submit</button>
	       </div>
	   </form>
    </div>
    
    <?PHP
    }
echo '</div>';
echo '<div class="clearfix"></div>';

    include("mods/footer.php");
?>