<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
    
?>
    <div class="container container-main">
        <h1>Trailor of Movies</h1>
        <div class="col-sm-6">         
        
<?PHP
    if(isset($_GET['id'])){
	
        //show the trailer of the same id
        $id = $db->safe_data($_GET['id']);
        $movie = get_value('movies', $id);
        
        echo '<h3>'.$movie['name'].'</h3>';
        echo $movie['trailer'];
        echo "<br><hr>";
        echo "Certificate : " . $movie['certificate'] . "&nbsp;&nbsp;";
        echo "Language : " . $movie['language']."<br>";
        echo "*ing : " . $movie['cast']."<br>";
        echo "Director : " . $movie['director']."<br>";
        echo "Music : " . $movie['music']."<br>";
        echo "Released on : " . $movie['released']."<br>";
        echo "Duration : " . $movie['duration'] . " Minutes<br><br>";        
    }else{
        //show the first trailor
        $q = "SELECT * FROM movies ORDER BY id DESC LIMIT 1";
        $r = $db->select($q);
        if(!$r){
            echo "There are no Movies on Our Database Now.<br>";
        }else{
        $movie = $r->fetch_array();
        
        echo '<h3>'.$movie['name'].'</h3>';
        echo $movie['trailer'];
        echo "<br><hr>";
        echo "Certificate : " . $movie['certificate'] . "&nbsp;&nbsp;";
        echo "Language : " . $movie['language']."<br>";
        echo "*ing : " . $movie['cast']."<br>";
        echo "Director : " . $movie['director']."<br>";
        echo "Music : " . $movie['music']."<br>";
        echo "Released on : " . $movie['released']."<br>";
        echo "Duration : " . $movie['duration'] . " Minutes<br><br>"; 
    }
    }
    
    echo "<hr>";
    
    //let us get all trailors here
?>
   </div>
   <div class="col-sm-6">
         
      <?PHP
       $movies = get_values('movies');
            foreach($movies as $movie):
        ?>
     
     <div class="col-xs-6">
         <?PHP
                echo '<h4>'.$movie['name'].'</h4>';
         echo '<p>'.$movie['language'] . ", " . $movie['certificate'].'</p>';
                echo '<a href="trailers/'.$movie['id'].'" class="btn btn-sm btn-success">'."Watch Now!".'</a>';
       ?>
     </div>
     <div class="col-xs-6">
               <?PHP
                echo '<span></span>';
                echo '<a href="trailers/'.$movie['id'].'"><img src="uploads/'.$movie['image'].'" class="img-responsive" width="100%"></a>';
        ?>
     </div>
     <div class="clearfix"></div>
     <hr><br>
     <?PHP endforeach; ?>
   </div>
    </div>
<?PHP

    include("mods/footer.php");
?>