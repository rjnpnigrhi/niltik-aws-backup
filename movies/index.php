<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
?>
    <br>
     <div class="container container-main">
       <div class="col-sm-8 col-sm-offset-2 col-xs-6 col-xs-offset-3">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation" class="active"><a href="#current" data-toggle="tab">Now Showing</a></li>
            <li role="presentation"><a href="#coming" data-toggle="tab">Coming Soon</a></li>
        
        </ul>
       </div>
       <div class="clearfix"></div> 
       <div class="tab-content clearfix">
          <div class="tab-pane active" id="current">
              <?PHP

              if(!isset($_SESSION['city_name'])){
                  //do nothing             
              }else{
                  if($_SESSION['city_name'] == 'Berhampur'){
                  $city_id = '2';
              }elseif($_SESSION['city_name'] == 'Muniguda'){
                  $city_id = '10';
              }elseif($_SESSION['city_name'] == 'Jeypore'){
                  $city_id = '11';
              }

                movies_now_playing($city_id);

              }              
              ?>
          </div>
          <div class="tab-pane" id="coming">
             <?PHP
                $status = 'coming soon';
                movies_now();
              ?>
          </div>
          <div class="tab-pane" id="events">
          <h1>Niltik Events</h1>
          <h3>COMING SOON</h3>
              <?PHP
                //$status = 'coming soon';
                //movies_now();
              ?>
          </div>    
       </div>
       
     </div>
     
     <div class="clearfix"></div>
     
<br>
<?PHP
    include("mods/footer.php");
?>