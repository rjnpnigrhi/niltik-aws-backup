<?PHP 
    session_start();

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
    
    $msg ='';

    if(isset($_GET['submit'])){
        $mobile = strip_tags($_POST['mobile']);
        $password = strip_tags($_POST['password']);
        
        $mobile = stripslashes($mobile);
        $password = stripslashes($password);
        
        $password = md5($password);
        
        $q = "SELECT * FROM admin WHERE mobile = '$mobile' LIMIT 1";
        $r = $db->select($q);
        
        $row = mysqli_fetch_assoc($r);
        $id = $row['id'];
        $db_password = $row['password'];
        
        if($password == $db_password){
            
            $_SESSION['aid'] = $id;
            $_SESSION['mobile'] = $row['mobile'];
            $_SESSION['f_name'] = $row['name'];
            
            $msg = "You are logged in !<br>";
            
            //let us update the last login value
            $date = date("m/d/Y/H/i/A");
            $q1 = "UPDATE admin SET last_login = '$date' WHERE id = '$id'";
            $r1 = $db->update($q1);
            
            header("Location:index.php");
            exit;
            
        }else{        	
            $msg = "Please insert correct login credentials.<br>";
        }
    }
?>
<!DOCTYPE html>
 <html>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <head>
     <title>Niltik Cinema Booking Site Administrators Dashboard</title>
     <!-- let us get the bootstrap css -->
     <link rel="stylesheet" href="../css/bootstrap.min.css">
       
     <!--let us get the font awesome css-->
     <link rel="stylesheet" href="../css/font-awesome.min.css">
       
     <!--We will create a custom css for this site-->
     <link rel="stylesheet" href="../css/admin.css">
     
     <!--Let us ger the favicon here--->
     <link rel="icon" type="image/png" href="../img/favicon.png">
     
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     
     <!-- Bootstrap Date-Picker Plugin -->
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
     
     <!-- TinyMCE for advanced text formatting-->
     <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
     <script type="text/javascript">
     tinymce.init({
     selector: '#textarea1',
     theme: 'modern',
     plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
     ],
     content_css: 'css/content.css',
     toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
     });
     </script>
     
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->     
</head>
   
<body>
 <div class="header">
     <div class="container text-center">        
         <h1>
         <img src="../img/favicon.png" style="margin-top:-5px;" alt="Site Logo" width="36px"> 
         <strong>Movies.Niltik.com : Dashboard</strong>
         </h1>
         <script type="text/javascript">
            tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
            tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

            function GetClock(){
            var d=new Date();
            var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear();
            if(nyear<1000) nyear+=1900;
            var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

            if(nhour==0){ap=" AM";nhour=12;}
            else if(nhour<12){ap=" AM";}
            else if(nhour==12){ap=" PM";}
            else if(nhour>12){ap=" PM";nhour-=12;}

            if(nmin<=9) nmin="0"+nmin;
            if(nsec<=9) nsec="0"+nsec;

            document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
            }

            window.onload=function(){
            GetClock();
            setInterval(GetClock,1000);
            }
            </script>
            <div id="clockbox"></div>
     </div>
 </div>
 
 <div class="container main_area">
     <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
         <div class="form-box">
             <form action="login.php?submit=true" method="post" class="login-form">
                 <div class="form-group">
                     <h1>Sign in Credentials</h1>
                     <br><br>
                     <?PHP
                        if(isset($msg)){
                            echo '<p style="color:RED;">'.$msg.'</p>';
                        }
                     ?>
                     <input type="text" name="mobile" class="form-control" placeholder="Mobile No" required>
                     
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>         
                <div class="form-group">
                    <label for="submit"></label>
                        <button class="btn btn-success" type="submit">Sign In !</button>
                        <a href="#" class="btn btn-danger">Forgot Password !</a>
                    </div>
            </form>
        </div>
    </div>
 </div>
 
 <div class="footer">
     <div class="container">
         All Rights &copy; Reserved, <a href="http://www.niltik.com" target="_blank">Niltik.com</a>, 2017
     </div>
 </div>
  
     
     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="../js/bootstrap.min.js"></script>  
     <!--Le us include a new script here-->     
     <script src="../js/script.js"></script>
</body>
    
</html>