<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //show form
?>
   <h3>Add a New City : </h3>
   <form action="city.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="name">Name of the City</label>
           <input type="text" name="name" class="form-control" placeholder="Name of the City" required>
           <p class="help-block">P.S. : Ensure the city is not in our list.</p>
       </div>
       
       <div class="form-group">
           <button class="btn btn-success" type="submit">Submit</button>
           <a href="city.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert data
        $name = $db->safe_data($_POST['name']);
        $date = date("m/d/Y");
        
        $tbl_name = "name";
        
        if(value_exists('city', $tbl_name, $name)>0){
            echo "City with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "INSERT INTO city (name, date) VALUES ('$name', '$date')";
            $r = $db->insert($q);
            
            echo "Successfully added " . $name . "To our Database .<br><br>";
            echo '<a href="city.php" class="btn btn-info">'."View City List".'</a>';
        }
    }elseif(isset($_GET['edit'])){
        //show form
        $id = $db->safe_data($_GET['edit']);
        $city = get_value('city', $id);
?>
   <h3>Edit City : </h3>
   <form action="city.php?update=true" method="post" class="form">
       <div class="form-group">
           <label for="name">Name of the City</label>
           <input type="text" name="name" class="form-control" value="<?PHP echo $city['name']; ?>" required>
           <p class="help-block">P.S. : Ensure the city is not in our list.</p>
       </div>
       
       <div class="form-group">
            <input type="hidden" name="id" value="<?PHP echo $id; ?>">
            <button class="btn btn-success" type="submit">Submit</button>
            <a href="city.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update'])){
        //update data
        $new_name = $db->safe_data($_POST['name']);
        $id = $db->safe_data($_POST['id']);
        
        $tbl_name = "name";
        
        if(value_exists('city', $tbl_name, $new_name)>0){
            echo "City with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "UPDATE city SET name = '$new_name' WHERE id = '$id'";
            $r = $db->update($q);
            
            echo "Successfully updated " . $new_name . " To our Database .<br><br>";
            echo '<a href="city.php" class="btn btn-info">'."View City List".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //check if there are halls
        //if yes do not delete
        $city_id = $db->safe_data($_GET['delete']);
        
        $q = "SELECT * FROM halls WHERE city_id = '$city_id'";
        $r = $db->select($q);
        
        if($r){
            echo "There are Movie halls in our database with this city name.<br>";
            echo "You must first delete the halls then delete the city.<br><br>";
            echo '<a href="city.php" class="btn btn-info">'."Go back to City List".'</a>';
        }else{
            delete_row('city', $city_id);
            
            echo "We have deleted the city from our database.<br><br>";
            echo '<a href="city.php" class="btn btn-success">'."Go back to City List".'</a>';
        }
    
    }else{
        //show all cities
    $cities = get_values('city');       
        
?>
   <h1>City List on Our Database : <a href="city.php?add=true" class="btn btn-primary pull-right">Add City</a></h1>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Halls</th>
            <th>Added On</th>
            <th>Action</th>
        </tr>
        <?PHP foreach($cities as $city): ?>
        <tr>
            <td><?PHP echo $city['id']; ?></td>
            <td><h1 class="text-success"><strong><?PHP echo $city['name']; ?></strong></h1></td>
            <td>
        <?PHP
            $city_id = $city['id'];
        
            $q = "SELECT * FROM halls WHERE city_id = '$city_id'";
            $r = $db->select($q);
            
            if(!$r){
                
            }else{
                while($halls = $r->fetch_array()){
                    echo '<a href="halls.php" class="btn btn-default">'.$halls['name'].'</a>'."<br><br>";
                }
            }
        ?>
            </td>
            <td><?PHP echo $city['date']; ?></td>
            <td>
                <a href="halls.php?add=true" class="btn btn-info">+ Add Hall</a>
                <a href="city.php?edit=<?PHP echo $city['id']; ?>" class="btn btn-primary">Edit</a>
                <a href="city.php?delete=<?PHP echo $city['id']; ?>" class="btn btn-danger">Delete</a>            
            </td>
        </tr>
        <?PHP endforeach; ?>
    </table>
<?PHP   
    }
    include("mods/footer.php");
?>
            