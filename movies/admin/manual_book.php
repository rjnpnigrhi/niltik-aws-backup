<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['insert'])){
        $email = $db->safe_data($_POST['email']);
        $phone = $db->safe_data($_POST['mobile']);
        $schedule_id = $db->safe_data($_POST['schedule_id']);
        $movie_id = $db->safe_data($_POST['movie_id']);
        $hall_id = $db->safe_data($_POST['hall_id']);
        $show_id = $db->safe_data($_POST['show_id']);
        $seat_names = $db->safe_data($_POST['seat_select']);
        $date = date("m/d/Y");  
        
        
        $seatcount = count(explode(',', $seat_names));
        
        $show = get_value('movie_schedule', $schedule_id);
        $ticket_price = $show['ticket'];
        $charge = $show['charges'];
        $show_time = $show['show_time'];
        $hall = get_value('halls', $hall_id);
        $theater = $hall['name'];
        $address = $hall['address'];
        $date = $show['date'];
$show_date = $show['date'];
        
        //let us get the movie details
        $movie = get_value('movies', $movie_id);
        $movie_name = $movie['name'];
        
        $total = $ticket_price*$seatcount;
        $extras = $seatcount*$charge;
        $amount = $total+$extras;
        
        //we will insert this data into database order_book
        $q = "INSERT INTO order_book (email, phone, txnid, seats, seatcount, amount, optradio, date, schedule_id, status) VALUES ('$email', '$phone', '$txnid', '$seat_names', '$seatcount', '$amount', '$optradio', '$show_date', '$schedule_id', 'success')";
        $r = $db->insert($q);
        
        //update seat chart with booked seats
        $seats = explode(',', $seat_names);

        foreach($seats as $seat){
            $q2 = "UPDATE seats SET status = 'booked' WHERE schedule_id = '$schedule_id' AND seat = '$seat'";
            $r2 = $db->update($q2);                
        }
        
        
        //API Details
        $msg = "Dear Customer, Booking ID $txnid for $seatcount Balcony seats ($seat_names) booked for $movie_name on $date at $show_time at $theater, Thank you. NILTIK";
        
        $encoded_msg= urlencode($msg);
        //Create API URL
        $fullapiurl="http://api.msg91.com/api/sendhttp.php?sender=NILTIK&route=4&mobiles=$phone&authkey=215426ARSXDZ9i5af98d91&country=91&message=$msg&campaign=MovieTicket";

        //Call API
        $ch = curl_init($fullapiurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); 
        //echo $result ; // For Report or Code Check
        curl_close($ch);
        
        //send the email
        $to = $email;

        $subject = 'Movie Ticket';

        $headers = "From: helpdesk@niltik.com\r\n";
        $headers .= "Reply-To: helpdesk@niltik.com\r\n";
        $headers .= "CC: sonubisoyi56@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message = '<html><body>';
        $message .= '<img src="http://www.niltik.com/movies/img/mnw_print.jpg" alt="Movies.niltik.com" width="480px" />';
        $message .= '<h1>Dear Customer, <br> Your Ticket(s) are confirmed!. </h1>';
        $message .= '<table rules="all" style="border:1px dashed #666666;" cellpadding="10" width="480px">';
        $message .= "<tr style='background: #eee;'><td><strong>eMail ID : </strong> </td><td>" . strip_tags($email) . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>Phone : </strong> </td><td>" . strip_tags($phone) . "</td></tr>";
        $message .= "<tr><td><strong>Movie:</strong> </td><td><h3>" . strip_tags($movie_name) . "</h3></td></tr>";
        $message .= "<tr><td><strong>Show Time:</strong> </td><td>" . strip_tags($show_time) . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>Tickets : </strong> </td><td>" . strip_tags($seat_names) . "</td></tr>";
        $message .= "<tr><td><strong>Date:</strong> </td><td>" . strip_tags($show_date) . "</td></tr>";
        $message .= "<tr><td><strong>Theater:</strong> </td><td>" . strip_tags($theater) . "</td></tr>";
        $message .= "<tr><td><strong>Address:</strong> </td><td>" . strip_tags($address) . "</td></tr>";
        $message .= "<tr><td><strong>Total Amount Paid:</strong> </td><td> Rs. " . strip_tags($amount) . ".00</td></tr>";
        $message .= "<tr><td colspan='2'>Thank You for Booking with us. <br><br> Helpdesk, <h4>Movies.niltik.com</h4>For Phone Booking call : 7008584789</td></tr>";
        $message .= "</table>";
        $message .= "</body></html>";
        
        
        
        
        $message  = '<div id="to-print">';
      	$message .= '<table class="table">';
        $message .= '<tr style="background-color:#333; color:white;">';
        $message .= '<td><img src="http://niltik.com/movies/img/mnw_print.jpg" width="240px" alt=""></td>';
        $message .= '<td align="right"><br>Carry this Ticket with you! <br></td></tr><tr>';
        $message .= '<td colspan="2">';
        $message .= 'Dear Customer,<br>';
        $message .= 'Your ticket(s) are Confirmed!';
        $message .= '<br></td></tr>';
        $message .= '<tr style="background-color:#f7f7f7;">';
        $message .= '<td>';
        
        
        $message .= '<h2>'. strip_tags($movie_name) .'</h2>';
        $message .= '<h4>'. strip_tags($theater) .'</h4>';
        $message .= '<p>'. strip_tags($address) .'</p>';
        $message .= '</td>';
        $message .= '<td align="right">';
        $message .= '<h2>Balcony</h2>';
        $message .= '<h4>Seats:'. strip_tags($seat_names) .'</h4>';
        $message .= '<p>('. strip_tags($show_date) . ') ' . strip_tags($show_time) .'</p>';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td colspan="2">';
        $message .= '<br><br>';
        $message .= 'ORDER SUMMARY';
        $message .= '</td></tr><tr>';
        $message .= '<td>';
        $message .= '<strong>Ticket Amount : (<?PHP echo $seatcount; ?> Tickets)</strong>';                       
        $message .= '</td>';
        $message .= '<td align="right">';
        $message .= 'Rs. '. $total .'.00';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td>Internet Charges : (Rs. '. $charge .'.00 / Ticket)</td>';
        $message .= '<td align="right">';
        $message .= 'Rs. '. $extras .'.00';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td><strong class="text-danger">Amount Paid :</strong></td>';
        $message .= '<td align="right">Rs . '. $amount .'.00</td>';
        
        
        
        $message .= '</tr>';
        $message .= '<tr></tr>';
        $message .= '<tr style="border-bottom:2px dotted #333;">';
        $message .= '<td colspan="2">';
        $message .= '<br><br>';
        $message .= '<strong>Important Instructions</strong>';
        $message .= '<br><br>';
        $message .= '<p>';                
        $message .= 'Tickets once booked cannot be exchanged, cancelled or refunded. <br>';
        $message .= 'In case of 3D Movie, a refundable amount of Rs. 30.00 must be paid for each 3D glass.<br>';
        $message .= '</p>';    
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td colspan="2" style="background-color:#333333; padding:20px; color:white; text-align:center;">';
        $message .= 'For Assistance or Phone Booking please contact <i class="fa fa-phone" aria-hidden="true"></i> : 7008584789 | mail us at <i class="fa fa-envelope-o" aria-hidden="true"></i> : niltikbam@gmail.com';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '</table>';
	$message .= '</div>';
        
        
        
        
        
        // Mailgun e-send API
        define('MAILGUN_URL', 'https://api.mailgun.net/v3/mg.niltik.com');
	define('MAILGUN_KEY', 'key-159b6adbe0a83226f1b2fce6b7aa6606');
	
	function sendmailbymailgun($to,$toname,$mailfromname,$mailfrom,$subject,$html,$text,$tag,$replyto){
	    $array_data = array(
			'from'=> $mailfromname .'<'.$mailfrom.'>',
			'to'=> $toname.'<'.$to.'>,Niltik Admin<sonubisoyi56@gmail.com>',
			'subject'=>$subject,
			'html'=>$html,
			'text'=>$text,
			'o:tracking'=>'yes',
			'o:tracking-clicks'=>'yes',
			'o:tracking-opens'=>'yes',
			'o:tag'=>$tag,
			'h:Reply-To'=>$replyto
	    );
	    
	    $session = curl_init(MAILGUN_URL.'/messages');
	    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
	    curl_setopt($session, CURLOPT_POST, true);
	    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
	    curl_setopt($session, CURLOPT_HEADER, false);
	    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
	    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
	    
	    $response = curl_exec($session);
	    curl_close($session);
	    $results = json_decode($response, true);
	    //print_r($results);
	    
	    	//if (mail($to, $subject, $message, $headers)) {
	        if ($results) {
	            echo "Congratulations. Booking Successful.<br>";
	            echo "We have sent confirmation by SMS and e-Mail.<br>";
	        } else {
	        	echo "Congratulations. Booking Successful.<br>";
	            echo "There was a problem sending the email.<br>";
	            echo "Save your Booking id : " . $txnid."<br>";
	            echo "Give a call to : 7008584789. Thank You.";
	        }
	    
	}
        
        
        sendmailbymailgun($to,'','Niltik.com','sales@niltik.com',$subject,$message,'','niltik','sales@niltik.com');
        
        
?>
      <div id="to-print">
      <button type="submit" class="btn btn-info pull-right" onclick="printDiv('to-print')">
         <i class="fa fa-print" aria-hidden="true"></i> Print
      </button>
      <div class="clearfix"></div>
      <br>
      <table class="table">
          <tr style="background-color:#333; color:white;">
              <td><img src="../img/mnw_print.jpg" width="240px" alt=""></td>
              <td align="right"><br>Carry this Ticket with you! <br></td>
          </tr>
          <tr>
              <td colspan="2">
                Dear Customer,<br>
                Your ticket(s) are Confirmed!
                <br>
              </td>
          </tr>
          <tr style="background-color:#f7f7f7;">
              <td>
                <h2><?PHP echo $movie_name; ?></h2>
                <h4><?PHP echo $theater; ?></h4>
                <p><?PHP echo $address; ?></p>
              </td>
              <td align="right">
                <h2>Balcony</h2>
                <h4><?PHP echo $seat_names; ?></h4>
                <p><?PHP echo $show_time; ?> | 
                <?PHP
                    list($month, $day, $year) = explode('/', $show_date);
                    //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
                    echo $m ." ". $day . ", ".  $year;
                ?>
                </p>    
              </td>
          </tr>
          <tr>
              <td colspan="2">
                 <br><br>
                  ORDER SUMMARY
              </td>
          </tr>
          <tr>
               <td>
                   <strong>Ticket Amount : (<?PHP echo $seatcount; ?> Tickets)</strong>                       
               </td>
               <td align="right">
                <?PHP                    
                    echo "Rs. ". $total .".00";
                ?>
               </td>
           </tr>
           <tr>
               <td>Internet Charges : (Rs. <?PHP echo $charge; ?>.00 / Ticket)</td>
               <td align="right">
               <?PHP
                    echo "Rs. ". $extras .".00";
                ?>    
               </td>
           </tr>
           <tr>
               <td><strong class="text-danger">Amount Paid :</strong></td>
               <td align="right">Rs . <?PHP echo $amount; ?>.00</td>
           </tr>
           <tr></tr>
           <tr style="border-bottom:2px dotted #333;">
               <td colspan="2">
               <br><br>
               <strong>Important Instructions</strong>
                <br><br>
                <p>                
                    Tickets once booked cannot be exchanged, cancelled or refunded. <br>
                    In case of 3D Movie, a refundable amount of Rs.40.00 must be paid for each 3D glass.<br>
                    Service Tax, Swachh Bharat Cess &amp; Krishi Kalyan Cess collected and paid to the department. <br>
                    For your proof of Identity at the hall counter, carry your Credit / Debit Card with you.
                </p>    
               </td>
           </tr>
           <tr>
               <td colspan="2">
                 <?PHP
                    if(!$optradio==''){
                        //$offer_id = $optradio;
$offer_id = '8';
                        $offer = get_value('offers', $offer_id);
                        echo '<p>'."Cut below this line and preserve for the Offer".'</p>';
                        echo "<hr>";
                        echo '<img src="uploads/'.$offer['image'].'" width="100%" alt="">';
                    }
                ?>
               </td>
           </tr>
           <tr>
               <td colspan="2" style="background-color:#333333; padding:20px; color:white; text-align:center;">
                   For Assistance or Phone Booking please contact <i class="fa fa-phone" aria-hidden="true"></i> : 7008584789 | mail us at <i class="fa fa-envelope-o" aria-hidden="true"></i> : helpdesk@niltik.com
               </td>
           </tr>
      </table>
          
       </div>
       <script type="text/javascript">
        function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();
        document.body.innerHTML = originalContents;
        }
    </script>
       
<?PHP
        
    }else{
    $id = $db->safe_data($_GET['id']);
    
    $book = get_value('movie_schedule', $id);
    
    $movie_id = $book['movie_id'];
    $movie = get_value('movies', $movie_id);

    echo '<h1>'.$movie['name'].'</h1>';

    $hall_id = $book['hall_id'];
    $hall = get_value('halls', $hall_id);
    $city_id = $hall['city_id'];
    $city = get_value('city', $city_id);

    echo '<h3>'.$hall['name']." | ".$city['name'].'</h3>';
    
    $date = $book['date'];
    list($month, $day, $year) = explode('/', $date);
    //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
    echo $m ." ". $day . ", ".  $year;    
    echo " | Show : " . $book['show_id'].'</p>';
        
    //getting free seats from our seats table
    $free_seats = available_seats($id, 'available');
    $free_seats = implode(',', $free_seats);
        
    //getting booked seats from our seats table
    $booked_seats = available_seats($id, 'booked');
    $booked_seats = implode(',', $booked_seats);
    
?>
    <h3>Select Seats for Manual Booking</h3>
    <br>
    
    <div class="col-sm-2">
        <div class="seat2 enabled" style="margin:0 20px 0 0; float:left;"></div> Available Seat 
    </div>
    <div class="col-sm-2">
        <div class="seat2 booked" style="margin:0 20px 0 0; float:left;"></div> Booked Seat 
    </div>
    <div class="col-sm-2">
        <div class="seat2 selected" style="margin:0 20px 0 0; float:left;"></div> Selected Seat
    </div>
    
    <div class="clearfix"></div>
    <br>
    
    <form action="manual_book.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="email">e-Mail ID</label>
           <input type="email" name="email" class="form-control" placeholder="eMail ID" required>
       </div>
       
       <div class="form-group">
           <label for="mobile">Mobile Number</label>
           <input type="text" name="mobile" class="form-control" placeholder="10 Digit Mobile Number" required>
       </div>
       
        <!-- TODO: To be saved into database-->
            <div class="form-group">
            <label for="Seat_select">Select Seats :</label>
            <input type="text" id="selected-seats" name="seat_select" class="form-control" required>
        </div>
        
        <div class="form-group hide">
            <label for="select_seats">Booked Seats</label>
            <input type="text" id="booked_seats" name="booked_seats" value="<?PHP echo $booked_seats; ?>" >
            <input type="text" id="available_seats" name="free_seats" value="<?PHP echo $free_seats; ?>" >
        </div>

        <!-- Hidden, JavaScript will get data from PHP -->
        <div class="form-group hide">
            <input type="text" id="seatmap-data" name="seatmap_data" class="form-control" value='<?PHP echo $hall["seatmap"]; ?>'>
        </div>

        <div class="form-group" id="seat-arrangement">
            <label for="showtime">Select Seats : (Click on the seat to Select / Deselect)</label>           
            <div class="enable-seats">
                <img src="../uploads/<?PHP echo $hall['image']; ?>">
            </div>
        </div>
        
        <div class="form-group">
           <input type="hidden" name="schedule_id" value="<?PHP echo $id; ?>">
           <input type="hidden" name="movie_id" value="<?PHP echo $movie_id; ?>">
           <input type="hidden" name="hall_id" value="<?PHP echo $hall_id; ?>">
           <input type="hidden" name="show_id" value="<?PHP echo $book['show_id']; ?>">
            <button class="btn btn-default">Submit</button>
        </div>
    </form>
<?PHP
    }
    include("mods/footer.php");
?>
            