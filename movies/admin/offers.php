<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //show form to add offer
?>
   <h3>Add a New Offer</h3>
   <form action="offers.php?insert=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="name">Name of Offer</label>
           <input type="text" name="name" class="form-control" placeholder="name of offer" required>
       </div>
       <div class="form-group">
           <label for="city">Chose City</label>
           <select name="city" id="city" class="form-control">
           <option value="all cities">Any City</option>
        <?PHP
            $cities = get_values('city');
            foreach($cities as $city):
        ?>
          <option value="<?PHP echo $city['name']; ?>"><?PHP echo $city['name']; ?></option>
        <?PHP
            endforeach;
        ?>
           </select>
       </div>
       <div class="form-group">
           <label for="image">Image</label>
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
           <img id="img" alt="your image" width="100%" />
       </div>
       
       <div class="form-group">
           <button class="btn btn-default btn-sm" type="submit">Submit</button>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert offer on database
        $name = $db->safe_data($_POST['name']);
        $city = $db->safe_data($_POST['city']);
        $date = date("m/d/Y");
        $image = $_FILES['image']['name'];
        
        $errors= array();
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

        $file_name = $_FILES['image']['name'];
        $file_size =$_FILES['image']['size'];
        $file_type=$_FILES['image']['type'];
        $ext=strtolower(getExtension($file_name));

        if(in_array($ext,$valid_formats)=== false){
         $errors[]="extension not allowed, please choose a JPEG or PNG or GIF file.";
        }

        if($file_size > 2097152){			
        $errors[]="File size exceeds the limit of 2 MB.";
        }

        $actual_image_name = time().".".$ext;
        $uploadedfile = $_FILES['image']['tmp_name'];				

        if(empty($errors)==true){
            if(move_uploaded_file($uploadedfile,$path.$actual_image_name)){

        $q = "INSERT INTO offers (name, city, image, date) VALUES ('$name', '$city', '$actual_image_name', '$date')";         
        $r = $db->insert($q);

        echo '<img src="../uploads/'.$actual_image_name.'" width="100%" class="img-responsive" />';
        echo "<br>";
        echo "Your Offer has been uploaded.";
        echo "<br><br>";
        echo '<a href="offers.php" class="btn btn-xs btn-default">'."Visit Offers Page".'</a>';
          }else{
        print_r($errors);
        echo "<br><br>";
        echo '<a href="offers.php?add=true" class="btn btn-xs btn-default">'."Go Back and Retry".'</a>';
            }
        }
    }elseif(isset($_GET['delete'])){
        //delete offer
        $id = $db->safe_data($_GET['delete']);
        
        delete_row('offers', $id);
        echo "Your Offer has been Deleted.";
        echo "<br><br>";
        echo '<a href="offers.php" class="btn btn-xs btn-default">'."Visit Offers Page".'</a>';
        
    }else{
        //show all offers
?>
   <h3>Offers Manager <a href="offers.php?add=true" class="btn btn-sm btn-primary pull-right">Add an Offer</a></h3>
   <table class="table">
       <tr>
           <th>ID</th>
           <th>City</th>
           <th>Name</th>
           <th>Image</th>
           <th>Action</th>
       </tr>
  
<?PHP
        $offers = get_values('offers');
        
        foreach($offers as $offer):
?>
     <tr>
         <td><?PHP echo $offer['id']; ?></td>
         <td><?PHP echo ucfirst($offer['city']); ?></td>
         <td><?PHP echo $offer['name']; ?></td>
         <td><img src="../uploads/<?PHP echo $offer['image']; ?>" alt="<?PHP echo $offer['name']; ?>" width="480px" class="img-responsive"></td>
         <td><a href="offers.php?delete=<?PHP echo $offer['id']; ?>" class="btn btn-sm btn-danger">Delete</a></td>
     </tr>  
<?PHP
        endforeach;
        echo '</table>';
    }

    include("mods/footer.php");
?>
            