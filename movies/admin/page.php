<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
    
    if(isset($_GET['add'])){
        //show form
?>
   <h3>Add a New Page : </h3>
   <form action="page.php?insert=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="name">Name of the Page</label>
           <input type="text" name="name" class="form-control" placeholder="Name of the Page" required>
           <p class="help-block">P.S. : Ensure the page is not in our list.</p>
       </div>
       
       <div class="form-group">
           <label for="contents">Contents of the Page</label>
           <textarea name="contents" id="textarea1" class="form-control" cols="30" rows="10"></textarea>
       </div>
       
       <div class="form-group">
           <button class="btn btn-primary" type="submit">Submit</button>
           <a href="page.php" class="btn btn-success">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert data
        $name = $db->safe_data($_POST['name']);
        $contents = $db->safe_data($_POST['contents']);
        
        $date = date("m/d/Y");
        
        $tbl_name = "name";
        
        if(value_exists('pages', $tbl_name, $name)>0){
            echo "Page with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "INSERT INTO pages (name, contents, date) VALUES ('$name', '$contents', '$date')";
            $r = $db->insert($q);
            
            echo "Successfully added " . $name . " Page to our Database .<br><br>";
            echo '<a href="page.php" class="btn btn-info">'."View Page List".'</a>';
        }
    }elseif(isset($_GET['edit'])){
        //show form
        $id = $db->safe_data($_GET['edit']);
        $page = get_value('pages', $id);
?>
   <h3>Edit "<?PHP echo $page['name']; ?>" Page : </h3>
   <form action="page.php?update=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="name">Name of the Page</label>
           <input type="text" name="name" class="form-control" value="<?PHP echo $page['name']; ?>" readonly required>
           <p class="help-block">P.S. : Ensure the page is not in our list.</p>
       </div>
       
       <div class="form-group">
           <label for="contents">Contents of the Page</label>
           <textarea name="contents" id="textarea1" class="form-control" cols="30" rows="10"><?PHP echo $page['contents']; ?></textarea>
       </div>
       
       <div class="form-group">
           <input type="hidden" name="id" value="<?PHP echo $id; ?>">
           <button class="btn btn-primary" type="submit">Submit</button>
           <a href="page.php" class="btn btn-success">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update'])){
        //update data
        $contents = $db->safe_data($_POST['contents']);
        $id = $db->safe_data($_POST['id']);
        
        $tbl_name = "name";
        

        $q = "UPDATE pages SET contents = '$contents' WHERE id = '$id'";
        $r = $db->update($q);
            
        echo "Successfully updated " . $name . " Page on our Database .<br><br>";
        echo '<a href="page.php" class="btn btn-info">'."View Page List".'</a>';

    }else{
        //show all pages
?>
    <h1>Page Manager <a href="page.php?add=true" class="btn btn-primary pull-right">Add a Page</a></h1>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Contents</th>
            <th>Edit</th>
        </tr>
        <?PHP
            $pages = get_values('pages');
            foreach($pages as $page):
        ?>
        <tr>
            <td><?PHP echo $page['id']; ?></td>
            <td><?PHP echo ucfirst($page['name']); ?></td>
            <td><?PHP echo nl2br($page['contents']); ?></td>
            <td><a href="page.php?edit=<?PHP echo $page['id']; ?>" class="btn btn-primary">Edit</a></td>
        </tr>
        <?PHP endforeach; ?>
    </table>
    
<?PHP
        
    }    

    include("mods/footer.php");
?>
            