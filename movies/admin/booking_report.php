<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
	
	if(isset($_GET['view'])){
		$schedule_id = $db->safe_data($_GET['view']);
		$show = get_value('movie_schedule', $schedule_id);		
		$hall = get_value('halls', $show['hall_id']);		
		$movie = get_value('movies', $show['movie_id']);
?>
	<h1><?PHP echo $movie['name']; ?> , <?PHP echo $hall['name']; ?> <a href="javascript:history.back()" class="btn btn-primary btn-sm pull-right">Go Back</a></h1>
	<p>Show : <?PHP echo $show['show_id']." ".$show['date']; ?> </p>
	<hr>
	<h3>Alloted (Available) seats </h3>
	
<?PHP
	$q = "SELECT * FROM seats WHERE schedule_id = '$schedule_id' AND status = 'available' ORDER BY id";
	$r = $db->select($q);
	
	if(!$r){
		echo "No seats alloted.<br>";
	}else{
		while($row = $r->fetch_array()){
			echo '<div class="text-center" style="border:1px solid #ccc; border-radius:4px; padding:5px; width:64px; height:64px; float:left; margin:10px 10px 0 0; background-color:green; color:white;">';
			echo '<h6>'.$row['seat'].'</h6>';					
			echo '<a href="booking_report.php?block='.$row['id'].'" class="btn btn-xs btn-default">BLOCK</a>';
			echo '</div>';
			
		}
	}
	
?>
<div class="clearfix"></div>
<h1>Add extra seats</h1>
<form method="post" action="booking_report.php?add=true" class="form">
<input name="schedule_id" value="<?PHP echo $schedule_id; ?>" type="hidden" />
<input name="date" value="<?PHP echo $show['date']; ?>" type="hidden" />
<input name="seats" type="text" class="form-control" style="text-transform:uppercase;" />
<button type="submit" class="btn btn-default btn-sm">Add New Seats</button>
</form>
<br>
<h1>Booked seats List</h1>
	<?PHP
	$booked_seats = booked_seats($schedule_id);
    	$booked_seats = implode(',', $booked_seats);
	$booked_seats = explode(',', $booked_seats);
	
if(count($booked_seats)<1){
echo "No Bookings<br>";
}else{

	foreach($booked_seats as $bseats){
		echo '<span style="float:left; margin:2px; background-color:red; color:white; width:36px; height:36px; text-align:center; padding:8px; font-weight:bold; border-radius:4px;">'.$bseats.'</span>';
		//echo $bseats . " , ";
	}
}
?>
<div class="clearfix"></div>
	<h1>Bookings for this show</h1>
	
<?PHP
	$status = "success";
	$qb = "SELECT * FROM order_book WHERE schedule_id = '$schedule_id' AND status = '$status' ORDER BY timestamp";
	$rb = $db->select($qb);
	
	if(!$rb){
		echo "There are no Bookings.<br>";
		
	}else{
		
?>
	<table class="table table-striped table-hover">
		<thead>
			<th>ID</th>
			<th>Customer</th>
			<th>Seats</th>
			<th>Payment</th>
			<th>Status</th>
			<th>Invoice</th>
		</thead>
	
<?PHP
	while($book = $rb->fetch_array()){
?>
	<tr>
		<td><?PHP echo $book['id']; ?></td>
		<td><?PHP echo $book['email']."<br>".$book['phone']."<br><br>".$book['timestamp']; ?></td>
		<td>
		<strong>
            <?PHP
                $seats = $book['seats']; 
                $array = explode(',', $seats);
                echo count($array) . " Seats <br> ";
            ?>
                </strong>
                <?PHP echo $seats; ?>
		</td>
		<td>Rs. <?PHP echo $book['amount']; ?>.00</td>
		<td>
		<?PHP 
		if($book['status'] == 'success'){
			echo '<a href="#" class="btn btn-sm btn-success disabled">'."COMPLETE".'</a>';
		}else{
			echo '<a href="#" class="btn btn-sm btn-danger disabled">'."INCOMPLETE".'</a>';
		}		
		?>
		</td>
		<td><a href="http://movies.niltik.com/admin/ticket.php?view=<?PHP echo $book['txnid']; ?>" class="btn btn-sm btn-primary" target="_blank">Invoice</a></td>
	</tr>
<?PHP
		}
		echo '</table>';
		}
		
		echo "<hr>";
		//we will add a failure table below this
		echo '<h2>'."Incomplete Transactions".'</h2>';
		
		$qf = "SELECT * FROM order_book WHERE schedule_id = '$schedule_id' AND status != 'success' ORDER BY timestamp";
	$rf = $db->select($qf);
	
	if(!$rf){
		echo "There are no Bookings.<br>";
		
	}else{
		
?>
	<table class="table table-striped table-hover">
		<thead>
			<th>ID</th>
			<th>Customer</th>
			<th>Seats</th>
			<th>Payment</th>
			<th>Status</th>
			<th>Invoice</th>
		</thead>
	
<?PHP
	while($bookf = $rf->fetch_array()){
?>
	<tr>
		<td><?PHP echo $bookf['id']; ?></td>
		<td><?PHP echo $bookf['email']."<br>".$bookf['phone']."<br><br>".$bookf['timestamp']; ?></td>
		<td>
		<strong>
            <?PHP
                $seatsf = $bookf['seats']; 
                $array = explode(',', $seatsf);
                echo count($array) . " Seats <br> ";
            ?>
                </strong>
                <?PHP echo $seatsf; ?>
		</td>
		<td>Rs. <?PHP echo $bookf['amount']; ?>.00</td>
		<td>
		<?PHP 
		if($bookf['status'] == 'success'){
			echo '<a href="#" class="btn btn-sm btn-success disabled">'."COMPLETE".'</a>';
		}else{
			echo '<a href="#" class="btn btn-sm btn-danger disabled">'."INCOMPLETE".'</a>';
		}		
		?>
		</td>
		<td><a href="http://movies.niltik.com/admin/ticket.php?view=<?PHP echo $bookf['txnid']; ?>" class="btn btn-sm btn-primary" target="_blank">Invoice</a></td>
	</tr>
<?PHP
		}
		echo '</table>';
	}
?>
	<a href="javascript:history.back()" class="btn btn-primary btn-sm pull-right">Go Back</a>
<?PHP
	}elseif(isset($_GET['block'])){
		//we will delete the seat from our chart
		$id = $db->safe_data($_GET['block']);
		
		delete_row('seats',$id);
		
		echo "Successfully deleted one seat from booking chart.<br>";
		echo '<a href="javascript:history.back()" class="btn btn-primary btn-sm pull-right">'."Go Back".'</a>';
	}elseif(isset($_GET['add'])){
		$seats = $db->safe_data(strtoupper($_POST['seats']));
		$schedule_id = $db->safe_data($_POST['schedule_id']);
		$show = get_value('movie_schedule', $schedule_id);
		$date = $show['date'];
		$status = "available";
		$seats = array_map('trim',array_filter(explode(',',$seats)));
		
		
		foreach($seats as $seat){
			$q = "SELECT * FROM seats WHERE seat = '$seat' AND schedule_id = '$schedule_id'";
			$r = $db->select($q);
			
			if($r){
				echo $seat ."already exists in seats list.<br>";
			}else{
				$q1 = "INSERT INTO seats (seat, schedule_id, date, status) VALUES ('$seat', '$schedule_id', '$date', '$status')";
				$r1 = $db->insert($q1);
				
				echo $seat . " added to seat chart successfully.<br>";
			}
		}
	
	}elseif(isset($_GET['unblock'])){
		//we will make the seat available
		
		$id = $db->safe_data($_GET['unblock']);
		
		$q = "UPDATE seats SET status = 'available' WHERE id = '$id'";
		$r = $db->update($q);
		
		echo "Successfully updated one seat for booking chart.<br>";
		echo '<a href="javascript:history.back()" class="btn btn-primary btn-sm pull-right">'."Go Back".'</a>';
	}else{
	
    $count = '6';

    for($i=0; $i<$count; $i++){

        echo '<div class="col-sm-2 col-xs-3 text-center">';
        $date = date('m/d/Y', strtotime('+'.$i.' day', strtotime(date("m/d/Y"))));
        //$date = explode('/', $date);
        list($month, $day, $year) = explode('/', $date);
        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
        echo '<h1 class="text-danger"><strong>'.$day.'</strong></h1>';
        echo '<h4>'.$m . " " . $year.'</h4>';
        echo '<a href="booking_report.php?date='.$date.'" class="btn btn-sm btn-success">'."View Report".'</a>';
        echo '</div>';
    }
    echo '<div class="clearfix"></div>';
        echo "<hr>";
	
	if(isset($_GET['date'])){
		$date = $db->safe_data($_GET['date']);
	}else{
		$date = date("m/d/Y");
	}

    $q = "SELECT * FROM movie_schedule WHERE date = '$date' ORDER BY movie_id";
	$r = $db->select($q);	
?>
	<h1>Reports for : 
	<?PHP
		list($month, $day, $year) = explode('/', $date);
        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
        echo $day. " " . $m . " " . $year;
	?>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	  $( function() {
	    $( "#datepicker" ).datepicker();
	  } );
	  </script>
	<p class="text-danger pull-right">Date: <input type="text" name="date" id="datepicker"></p>
	</h1>
	<table class="table table-striped table-hover">
	<thead>
		<th>Movie Name</th>
		<th>Hall Name</th>
		<th>Show</th>
		<th>Report</th>
	</thead>
	<tr>
	<?PHP
	while($row = $r->fetch_array()){
	
		$schedule_id = $row['id'];		
		$show = get_value('movie_schedule', $schedule_id);		
		$hall = get_value('halls', $row['hall_id']);		
		$movie = get_value('movies', $row['movie_id']);	
		
	?>
		<td>
		<?PHP echo '<h3>'.$movie['name'].'</h3>'; ?>
		</td>
		<td><?PHP echo $hall['name']; ?></td>
		<td><?PHP echo "Show " . $row['show_id']; ?></td>
		<td><?PHP echo '<a href="booking_report.php?view='.$row['id'].'" class="btn btn-success btn-sm">'."REPORT".'</a>'; ?></td>
	</tr>
	
	
<?PHP
	}
	echo '</table>';
	}
    include("mods/footer.php");
?>