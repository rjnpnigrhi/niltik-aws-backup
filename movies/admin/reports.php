<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
    
    if(isset($_GET['search'])){
        
        if(isset($_POST['movie_id'])){
            $movie_id = $db->safe_data($_POST['movie_id']);
        }else{
            $movie_id = $db->safe_data($_GET['movie_id']);
        }
        
        if(isset($_POST['hall_id'])){
            $hall_id = $db->safe_data($_POST['hall_id']);
        }else{
            $hall_id = $db->safe_data($_GET['hall_id']);
        }
        
        if(isset($_POST['show_id'])){
            $show_id = $db->safe_data($_POST['show_id']);
        }else{
            $show_id = $db->safe_data($_GET['show_id']);
        }
        
        if(isset($_POST['date'])){
            $date = $db->safe_data($_POST['date']);
        }else{
            $date = $db->safe_data($_GET['date']);
        }
        
        if(isset($_POST['from_date'])){
            $from_date = $db->safe_data($_POST['from_date']);
        }else{
            $from_date = $db->safe_data($_GET['from_date']);
        }
        
        if(isset($_POST['till_date'])){
            $till_date = $db->safe_data($_POST['till_date']);
        }else{
            $till_date = $db->safe_data($_GET['till_date']);
        }
        
        $q = "SELECT * FROM booking WHERE name !=''";
        
        if($movie_id ==''){
            
        }else{
            $q .= "AND movie_id = '$movie_id'";
        }
        
        if($hall_id == ''){
            
        }else{
            $q .= "AND hall_id = '$hall_id'";
        }
        
        if($show_id == ''){
            
        }else{
            $q .= "AND show_id = '$show_id'";
        }
        
        if($date == ''){
            
        }else{
            $q .= "AND date = '$date'";
        }
        
        if($from_date == ''||$till_date==''){
            
        }else{
            $q .= "AND date BETWEEN '$from_date' AND '$till_date'";
        }
        
        $q .= " ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, The system sent a zero resultset.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-sm btn-default">'."Go Back and Retry!".'</a>';
        }else{
            while($diaries = $r->fetch_array()){
                $result[] = $diaries;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 12);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
?>
     <table class="table">
        <tr>
            <th>ID</th>
            <th>Customer Details</th>
            <th class="text-center">Movie</th>
            <th class="text-center">Date &amp; Show</th>
            <th class="text-center">Tickets</th>
            <th class="text-center">Hall</th>
            <th class="text-center">Payment</th>
        </tr> 
        <?PHP                
            foreach($data as $book):
        ?>
        <tr>
                <td><?PHP echo $book['id']; ?></td>
                <td>
                    <i class="fa fa-user" aria-hidden="true"></i> : <strong><?PHP echo $book['name']; ?></strong> <br>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> : <?PHP echo $book['email']; ?> <br>
                    <i class="fa fa-phone" aria-hidden="true"></i> : <?PHP echo $book['mobile']; ?>                
                </td>
                <td align="center">
            <?PHP
                $movie_id = $book['movie_id'];
                $movie = get_value('movies', $movie_id);
                echo '<strong>'. $movie['name'] .'</strong>';
                echo ", ";
                echo $movie['language'];
            ?>
                </td>
                <td align="center">
                   Show : <?PHP echo $book['show_id']; ?>
                <br>
            <?PHP 
                $date = $book['date'];
                list($month, $day, $year) = explode('/', $date);
                $m = strftime("%B",mktime(0,0,0,$month));    
                echo $m ." ". $day .", ".$year; 
            ?>
                </td>
                <td align="center">
            <?PHP 
                $seats = $book['seats']; 
                $array = explode(',', $seats);
                echo count($array) . " Seats <br> " . $seats;
            ?>
                </td>
                <td align="center">
            <?PHP
                $hall_id = $book['hall_id'];
                $hall = get_value('halls', $hall_id);
                echo '<strong>'.$hall['name'].'</strong>'."<br>";
                $city_id = $hall['city_id'];
                $city = get_value('city', $city_id);
                echo $city['name'];
            ?>
                </td>                
                <td align="center">Rs. <?PHP echo $book['price']; ?>.00</td>
            </tr>
        <?PHP
            endforeach;
        ?>
        
    </table> 
    
         <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="reports.php?search=true&movie_id='.$movie_id.'&hall_id='.$hall_id.'&show_id='.$show_id.'&date='.$date.'&from_date='.$from_date.'&till_date='.$till_date.'&page='.$pp.'" aria-label="Previous"><< PREV</a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="reports.php?search=true&movie_id='.$movie_id.'&hall_id='.$hall_id.'&show_id='.$show_id.'&date='.$date.'&from_date='.$from_date.'&till_date='.$till_date.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="reports.php?search=true&movie_id='.$movie_id.'&hall_id='.$hall_id.'&show_id='.$show_id.'&date='.$date.'&from_date='.$from_date.'&till_date='.$till_date.'&page='.$np.'" aria-label="Next">NEXT >></a></li>';
                }
                
               ?>
              </ul>
            </nav>
        <div class="clearfix"></div>
        <a href="javascript:history.back()" class="btn btn-default">Go Back !</a>
<?PHP
            
            //pagination ends here
        }
        
    }else{
        //show the form
?>
    <h3>Search Site Database for Reports:</h3>
    <br>
    <form action="reports.php?search=true" method="post" class="form">
        <div class="form-group">
            <label for="movie">Search by Movie Name :</label>
            <select name="movie_id" id="movie_id" class="form-control">
                <option value="">All Movies</option>
                 <?PHP 
                    $movies = get_values('movies');
                    foreach($movies as $movie):
                ?>
                <option value="<?PHP echo $movie['id']; ?>"><?PHP echo ucfirst($movie['name'])." - ".$movie['language']; ?></option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="hall">Search by Hall Name :</label>
            <select name="hall_id" id="hall_id" class="form-control">
                <option value="">All Halls</option>
                 <?PHP 
                    $halls = get_values('halls');
                    foreach($halls as $hall):
                ?>
                <option value="<?PHP echo $hall['id']; ?>"><?PHP echo ucfirst($hall['name']); ?></option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="show">Search by Show :</label>
            <select name="show_id" id="show_id" class="form-control">
                <option value="">All Shows</option>
                <option value="1">Show 1</option>
                <option value="2">Show 2</option>
                <option value="3">Show 3</option>
                <option value="4">Show 4</option>
                <option value="5">Show 5</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="date">Search by Date :</label>
            <select name="date" id="date" class="form-control">
                <?PHP
                    $q = "SELECT date from booking GROUP BY date ORDER BY id DESC";
                    $r = $db->select($q);
        
                    if(!$r){
                        echo '<option value="">'."Data Unavailable at this moment".'</option>';
                    }else{
                        echo '<option value="">'."All Dates".'</option>';
                        while($row = $r->fetch_array()){
                           echo '<option value="'.$row['date'].'">'.$row['date'].'</option>'; 
                        }
                    }
                ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="date_from">Date Range From :</label>
            <select name="from_date" id="date" class="form-control">
                <?PHP
                    $q = "SELECT date from booking GROUP BY date";
                    $r = $db->select($q);
        
                    if(!$r){
                        echo '<option value="">'."Data Unavailable at this moment".'</option>';
                    }else{
                        echo '<option value="">'."Not Required".'</option>';
                        while($row = $r->fetch_array()){
                           echo '<option value="'.$row['date'].'">'.$row['date'].'</option>'; 
                        }
                    }
                ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="date_till">Date Range To :</label>
            <select name="till_date" id="date" class="form-control">
                <?PHP
                    $q = "SELECT date from booking GROUP BY date";
                    $r = $db->select($q);
        
                    if(!$r){
                        echo '<option value="">'."Data Unavailable at this moment".'</option>';
                    }else{
                        echo '<option value="">'."Not required".'</option>';
                        while($row = $r->fetch_array()){
                           echo '<option value="'.$row['date'].'">'.$row['date'].'</option>'; 
                        }
                    }
                ?>
            </select>
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-default">Search Site Database</button>
        </div>
    </form>
<?PHP
    }
    include("mods/footer.php");
?>
            