
    </div>
    <!-- The main area of action ends here -->
    
    <div class="container login_info">
       <?PHP
        $id = $_SESSION['aid'];
        
        $admin = get_value('admin', $id); 
        $last_login = $admin['last_login'];
        
        list($month, $day, $year, $hour, $minute, $am) = explode('/', $last_login);
        $m = strftime("%B",mktime(0,0,0,$month));
        
        echo "Last Login : " . $day ." ". $m . ", " . $year .", " . $hour . ":" . $minute ." " . $am;
        ?>
        <a href="logout.php" class="btn btn-sm pull-right btn-primary">Sign Out !</a>
    </div>
 
 <div class="footer">
     <div class="container">
         All Rights &copy; Reserved, <a href="http://www.niltik.com" target="_blank">Niltik.com</a>, 2017
     </div>
 </div>
  
     
</body>
    
</html>