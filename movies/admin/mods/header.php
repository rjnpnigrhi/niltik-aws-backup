<!DOCTYPE html>
 <html>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <head>
     <title>Niltik Cinema Booking Site Administrators Dashboard</title>
     <!-- let us get the bootstrap css -->
     <link rel="stylesheet" href="../css/bootstrap.min.css">
      
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
       
     <!--let us get the font awesome css-->
     <link rel="stylesheet" href="../css/font-awesome.min.css">
       
     <!--We will create a custom css for this site-->
     <link rel="stylesheet" href="../css/admin.css">
     
     <!--Let us ger the favicon here--->
     <link rel="icon" type="image/png" href="../img/favicon.png">
     
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Bootstrap Date-Picker Plugin -->
    <link rel="stylesheet" type="text/css" href="../css/datepicker.css"/>
    <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script>

      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="../js/bootstrap.min.js"></script>
      <!--Le us include a new script here-->
      <script src="../js/script.js"></script>   
     

     <!-- TinyMCE for advanced text formatting-->
     <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
     <script type="text/javascript">
     tinymce.init({
     selector: '#textarea1',
     theme: 'modern',
     plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
     ],
     content_css: 'css/content.css',
     toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
     });
     </script>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
     
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->     
</head>
   
<body>
 <div class="header">
     <div class="container text-center">        
         <h1>
         <img src="../img/favicon.png" style="margin-top:-5px;" alt="Site Logo" width="36px"> 
         <strong>Movies.Niltik.com : Dashboard</strong>
         </h1>
         <script type="text/javascript">
            tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
            tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

            function GetClock(){
            var d=new Date();
            var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear();
            if(nyear<1000) nyear+=1900;
            var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

            if(nhour==0){ap=" AM";nhour=12;}
            else if(nhour<12){ap=" AM";}
            else if(nhour==12){ap=" PM";}
            else if(nhour>12){ap=" PM";nhour-=12;}

            if(nmin<=9) nmin="0"+nmin;
            if(nsec<=9) nsec="0"+nsec;

            document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
            }

            window.onload=function(){
            GetClock();
            setInterval(GetClock,1000);
            }
            </script>
            <div id="clockbox"></div>
     </div>
 </div>
 
 <nav class="navbar navbar-default top_nav">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Home</a>             
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">            
            <li><a href="bookings.php">Bookings</a></li>
            <li><a href="movies.php">Movies</a></li>
            <li><a href="halls.php">Halls</a></li>
            <li><a href="schedule.php">Schedules</a></li>
            <li><a href="offers.php">Offers</a></li>
<li><a href="coupons.php">Coupons</a></li>
            <li><a href="booking_report.php">Reports</a></li>
            <li><a href="feedback.php">Feedback</a></li>
            <li><a href="unblock.php">Unblock</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">General Settings</a></li>
                <li><a href="city.php">City master</a></li>
                <li><a href="lang.php">Language Master</a></li>
                <li><a href="cert.php">Certificates Master</a></li>
                <li><a href="page.php">Pages Manager</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="logout.php">Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
 
    <!-- this is the main area of action -->
    <div class="container main_area">