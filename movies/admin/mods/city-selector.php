<div class="modal fade" id="city-selector-modal" tabindex="-1" role="dialog" data-backdrop="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Select City</h4>
      </div>
      <div class="modal-body">
        <div class="row">

          <div class="col-md-4 col-sm-12" style="padding: 5px; text-align: center;">
            <a href="javascript:" class="btn btn-default city-select">Berhampur</a>
          </div>

          <div class="col-md-4 col-sm-12" style="padding: 5px; text-align: center;">
            <a href="javascript:" class="btn btn-default city-select">Muniguda</a>
          </div>

          <div class="col-md-4 col-sm-12" style="padding: 5px; text-align: center;">
            <a href="javascript:" class="btn btn-default city-select">Jeypore</a>
          </div>

        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->