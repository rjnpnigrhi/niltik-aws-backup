<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
?>
   <div class="container info_panel text-center">
      <?PHP
       $date = date("m/d/Y");
        $q3 = "SELECT * FROM booking WHERE date = '$date' ORDER BY id DESC LIMIT 10";
        $r3 = $db->select($q3);
        
        if(!$r){
            $tickets = '0';
        }else{
            $tickets = $r3->num_rows;
        }
       ?>
       <h1><?PHP echo $tickets; ?></h1>
       <p>Bookings today</p>
   </div>
   
   <div class="container">
       <div class="col-sm-6">
           <h3>Latest Bookings</h3>
           <?PHP
            $date = date("m/d/Y");
                $q = "SELECT * FROM booking WHERE date = '$date' ORDER BY id DESC LIMIT 10";
                $r = $db->select($q);
           
                if(!$r){
                    echo "There are no bookings today.<br>";
                }else{
                    while($book = $r->fetch_array()){
                        echo '<strong>'.$book['name'].'</strong>'. " ( " . '<i class="fa fa-phone" aria-hidden="true"></i> '.  $book['mobile'] . " )<br><br>";
                        $movie_id = $book['movie_id'];
                        $movie = get_value('movies', $movie_id);
                        echo '<strong>'. $movie['name'] .'</strong>';
                        echo ", ";
                        echo $movie['language'];
                        echo "<br>";
                        echo "Show : " . $book['show_id']."<br>";
                        $hall_id = $book['hall_id'];
                        $hall = get_value('halls', $hall_id);
                        echo '<strong>'.$hall['name'].'</strong>'."<br>";
                        $city_id = $hall['city_id'];
                        $city = get_value('city', $city_id);
                        echo $city['name'];
                        echo "<br>";
                        $seats = $book['seats']; 
                        $array = explode(',', $seats);
                        echo " Tickets : " .count($array) ." ( " . $seats . " )<br>";
                        echo "Rs. ". $book['price']. ".00";
                        echo "<hr>";
                    }
                }
           ?>
           
       </div>
       <div class="col-sm-6">
           <h3>Movies on Screen</h3>
           <?PHP
            $date = date("m/d/Y");
           
            $q1 = "SELECT * FROM movie_schedule WHERE date = '$date' ORDER BY id DESC";
            $r1 = $db->select($q1);
           
            if(!$r1){
                echo "There are no Movies on screen today.<br>";
            }else{
                echo '<table class="table">';
                while($row = $r1->fetch_array()){
                    $movie_id = $row['movie_id'];
                    $movie = get_value('movies', $movie_id);
                    echo '<tr><td width="50%">';
                    echo '<img src="../uploads/'.$movie['image'].'" class="img-responsive" >';
                    echo '</td><td>';                    
                    echo '<h3>'. $movie['name'] .'</h3>';
                    echo $movie['language'] . " ( " . $movie['certificate'] . " )";
                    echo "<br><br>";
                    $hall_id = $row['hall_id'];
                    $hall = get_value("halls", $hall_id);
                    echo $hall['name'];
                    echo "<br>";
                    $city_id = $hall['city_id'];
                    $city = get_value('city', $city_id);
                    echo $city['name'];
                    echo "<br>";
                    echo "Show : " . $row['show_id'];
                    echo '</td></tr>';
                }
                echo '</table>';
            }
           ?>
       </div>
   </div>

<?PHP
    include("mods/footer.php");
?>
            