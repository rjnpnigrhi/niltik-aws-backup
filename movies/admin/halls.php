<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //show form to add a new hall
?>
   <h1>Add a New Hall</h1>
   <form action="halls.php?insert=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="name">Name of the Hall</label>
           <input type="text" name="name" class="form-control" placeholder="Name of the Movie Hall" required>
       </div>
       
       <div class="form-group">
           <label for="city">City of the Hall</label>
           <select name="city_id" class="form-control" id="city_id">
               <?PHP
                    $cities = get_values('city');
                    foreach($cities as $city){
                    
                    echo '<option value="'.$city['id'].'">'.$city['name'].'</option>';
                    }
                ?>                
           </select>
       </div>
       
       <div class="form-group">
           <label for="address">Address</label>
           <textarea name="address" id="address" class="form-control" cols="30" rows="10" required></textarea>
       </div>
       
       <div class="form-group">
           <label for="location">Location from Google Map</label>
           <textarea name="location" id="location" class="form-control" cols="30" rows="10"></textarea>
       </div>
       
       <div class="form-group">
           <label for="image">Image of the hall</label>
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])">
                <img id="img" alt="your image" width="100%" />
       </div>
       
       <div class="form-group">
           <button class="btn btn-default" type="submit">Submit</button>
           <a href="halls.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //add hall to database
        $name = $db->safe_data($_POST['name']);
        $city_id = $db->safe_data($_POST['city_id']);
        $address = $db->safe_data($_POST['address']);
        $location = $db->safe_data($_POST['location']);
        $date = date("m/d/Y");
        
        $image = $_FILES['image']['name'];
        
        if($image !== ''){
            //we will include the image and then add information
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 2097152){
            $errors[] = "File size must not be bigger than 512 KB.";
        }
        
        $actual_icon_name = time().$_SESSION['aid'].".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];
        
        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "INSERT INTO halls (name, city_id, address, location, image, date) VALUES ('$name', '$city_id', '$address', '$location', '$actual_icon_name', '$date')";
                $r = $db->insert($q);
                
                echo "Successfully added a new Hall.<br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'.nl2br(stripslashes($address)).'</p>';
                echo "<br>";
                echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
                }
            }
        }else{
            //we will add information without the image
            $q = "INSERT INTO halls (name, city_id, address, location, date) VALUES ('$name', '$city_id', '$address', '$location', '$date')";
                $r = $db->insert($q);
                
                echo "Successfully added a new Hall.<br>";
                echo '<h3>'.$name.'</h3>';
                echo '<p>'.nl2br(stripslashes($address)).'</p>';
                echo "<br>";
                echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
        }
    }elseif(isset($_GET['edit_seatmap'])){
        //show a form
        $hall_id = $db->safe_data($_GET['edit_seatmap']);
        $hall = get_value('halls', $hall_id);
        
        echo '<h1>'."Editing Seat Map of  " .$hall['name'].'</h1>';
?>
    <form action="halls.php?update_seatmap=true" method="post" enctype="multipart/form-data" class="form">
        <div class="form-group">
            <label for="seatmap">Seat Map</label>
            <textarea name="seatmap" id="seatmap" class="form-control" cols="30" rows="10" readonly><?PHP echo $hall['seatmap']; ?></textarea>
        </div>
        
        <div class="form-group">
            <label for="showtime">Add Seat :</label>
            <div class="row">
                <div class="col col-sm-2">
                    <input value="S1" id="seat-name" type="text" class="form-control" placeholder="Seat Name">
                </div>
                 <div class="col col-sm-2">
                    <select class="form-control" id="seat-type">
                        <option>Balcony</option>
                        <option>First Class</option>
                    </select>
                </div>
                <div class="col col-sm-2">
                    <button type="button" id="add-seat-name" class="btn btn-info">Add</button>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="showtime">Seat Height / Seat Width :</label>
            <div class="row">
                <div class="col-xs-1">
                    <input id="seat-height" type="number" class="form-control seat-elem-attribute" placeholder="Seat Height" value="25">
                </div>
                <div class="col-xs-1">
                    <input id="seat-width" type="number" class="form-control seat-elem-attribute" placeholder="Seat Width" value="25">
                </div>
            </div>
        </div>

        <div class="form-group" id="seat-arrangement">
            <label for="showtime">Create Seats :</label>
            <div class="enable-seats">
                <img src="../uploads/<?PHP echo $hall['image']; ?>">
            </div>

        </div>
        
        <div class="form-group">
           <input type="hidden" name="id" value="<?PHP echo $hall_id; ?>">
            <button class="btn btn-default" type="submit">Submit</button>
            <a href="halls.php" class="btn btn-success">Cancel</a>
        </div>
    </form>
<?PHP
    }elseif(isset($_GET['update_seatmap'])){
        //update database
        $seatmap = $db->safe_data($_POST['seatmap']);
        $id = $db->safe_data($_POST['id']);
        
        $q = "UPDATE halls SET seatmap = '$seatmap' WHERE id = '$id'";
        $r = $db->update($q);
                
                
        echo "Successfully updated Hall Seatmap.<br>";
        echo "<br>";
        echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
    }elseif(isset($_GET['edit'])){
        //show form to edit the hall data
        $hall_id = $db->safe_data($_GET['edit']);
        $hall = get_value('halls', $hall_id);
        
        echo '<h1>'."Editing " .$hall['name'].'</h1>';
?>
   <form action="halls.php?update=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="name">Name of the Hall</label>
           <input type="text" name="name" class="form-control" value="<?PHP echo $hall['name']; ?>" required>
       </div>
       
       <div class="form-group">
           <label for="city">City of the Hall</label>
           <select name="city_id" class="form-control" id="city_id">
               <?PHP
                    $cities = get_values('city');
                    foreach($cities as $city){
                    
                    echo '<option value="'.$city['id'].'">'.$city['name'].'</option>';
                    }
                ?>                
           </select>
       </div>
       
       <div class="form-group">
           <label for="address">Address</label>
           <textarea name="address" id="address" class="form-control" cols="30" rows="10" required><?PHP echo $hall['address']; ?></textarea>
       </div>
       
       <div class="form-group">
           <label for="location">Location from Google Map</label>
           <textarea name="location" id="location" class="form-control" cols="30" rows="10"><?PHP echo $hall['location']; ?></textarea>
       </div>
       
       <div class="form-group">
           <label for="seat-map">Seat Map of the hall</label>
           <textarea name="seatmap" id="seat-map" class="form-control" cols="30" rows="10"><?PHP echo $hall['seatmap']; ?></textarea>
       </div>
       
       <div class="form-group">
           <label for="seats">Seats</label>
           <textarea name="seats" id="seats" class="form-control" cols="30" rows="10" required><?PHP echo $hall['seats']; ?></textarea>
       </div>
       
       <div class="form-group">
           <label for="image">Image of the hall</label>
           <br>
           <img src="../uploads/<?PHP echo $hall['image']; ?>" alt="<?PHP echo $hall['image']; ?>" style="width:240px;">
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])">
                <img id="img" alt="your image" width="100%" />
                <p class="help-block">P.S. : Leave it as such if you do not wish to change the image</p>
       </div>
       
       <div class="form-group">
          <input type="hidden" name="id" value="<?PHP echo $hall_id; ?>">
          <input type="hidden" name="old_image" value="<?PHP echo $hall['image']; ?>">
           <button class="btn btn-default" type="submit">Submit</button>
           <a href="halls.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update'])){
        //update the hall data on website
        $name = $db->safe_data($_POST['name']);
        $city_id = $db->safe_data($_POST['city_id']);
        $address = $db->safe_data($_POST['address']);
        $location = $db->safe_data($_POST['location']);
        $seatmap = $db->safe_data($_POST['seatmap']);
        $seats = $db->safe_data($_POST['seats']);
        
        $id = $db->safe_data($_POST['id']);
        $old_image = $db->safe_data($_POST['old_image']);
        
        $image = $_FILES['image']['name'];
        
        if($image !== ''){
            //we will include the image and then add information
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 524288){
            $errors[] = "File size must not be bigger than 512 KB.";
        }
        
        $actual_icon_name = time().$_SESSION['aid'].".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];
        
        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "UPDATE halls SET name = '$name', city_id = '$city_id', address = '$address', location = '$location', seatmap = '$seatmap', seats = '$seats', image = '$actual_icon_name' WHERE id = '$id'";
                $r = $db->update($q);
                
                if($old_image !==''){
                    unlink('../uploads/'.$old_image);
                }
                
                echo "Successfully Updated the Hall Data.<br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'.nl2br(stripslashes($address)).'</p>';
                echo "<br>";
                echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
                }
            }
        }else{
            //we will add information without the image
            $q = "UPDATE halls SET name = '$name', city_id = '$city_id', address = '$address', location = '$location', seatmap = '$seatmap', seats = '$seats' WHERE id = '$id'";
                $r = $db->update($q);
                
                echo "Successfully Updated the Hall Data.<br>";
                echo '<img src="../uploads/'.$old_image.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'.nl2br(stripslashes($address)).'</p>';
                echo "<br>";
                echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //ask for confirmation
        $hall_id = $db->safe_data($_GET['delete']);
        $hall = get_value('halls', $hall_id);
        
        echo '<h1>'.$hall['name'].'</h1>';
        if($hall['image']!==''){
            echo '<img src="../uploads/'.$hall['image'].'" class="img-responsive" width="100%" />';
        }
        echo "<br><br>";
        echo '<p>'.nl2br($hall['address']).'</p>';        
        echo "<hr>";
        echo '<a href="halls.php?confirm_delete='.$hall_id.'" class="btn btn-danger">'."Yes, Delete it.".'</a>'." ".'<a href="halls.php" class="btn btn-success">'."Cancel".'</a>';
    }elseif(isset($_GET['confirm_delete'])){
        //delete the hall form list
        $hall_id = $db->safe_data($_GET['confirm_delete']);
        $hall = get_value('halls', $hall_id);
        
        $old_image = $hall['image'];
        
        if($old_image !==''){
            unlink('../uploads/'.$old_image);
        }
        
        delete_row('halls', $hall_id);
        
        echo "We have deleted the hall from our database.<br><br>";
        echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
    }elseif(isset($_GET['add_image'])){
        //show form to add image to hall
        $hall_id = $db->safe_data($_GET['add_image']);
        $hall = get_value('halls', $hall_id);
        
        echo '<h1>'.$hall['name'].'</h1>';
        if($hall['image']!==''){
            echo '<h3>'."Change this Current Image".'</h3>';
            echo '<img src="../uploads/'.$hall['image'].'" class="img-responsive" width="100%" />';
        }
        
        echo "<hr>";
?>
   <form action="halls.php?update_image=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="image">Chose Image</label>
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
           <img id="img" alt="your image" width="100%" />
       </div>
       
       <div class="form-group">
          <input type="hidden" name="id" value="<?PHP echo $hall_id; ?>">
          <input type="hidden" name="old_image" value="<?PHP echo $hall['image']; ?>">
           <button class="btn btn-default" type="submit">Submit</button>
           <a href="halls.php" class="btn btn-primary">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update_image'])){
        //update image of the hall
        $id = $db->safe_data($_POST['id']);
        $old_image = $db->safe_data($_POST['old_image']);
        
        $image = $_FILES['image']['name'];
        
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 524288){
            $errors[] = "File size must not be bigger than 512 KB.";
        }
        
        $actual_icon_name = time().$_SESSION['aid'].".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];
        
        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "UPDATE halls SET image = '$actual_icon_name' WHERE id = '$id'";
                $r = $db->update($q);
                
                if($old_image !==''){
                    unlink('../uploads/'.$old_image);
                }
                
                echo "Successfully updated Hall Image.<br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo "<br>";
                echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
                }
            }
    }elseif(isset($_GET['status'])){
        //change status
        $status = $db->safe_data($_GET['status']);
        $id = $db->safe_data($_GET['id']);
        
        $q = "UPDATE halls SET status = '$status' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "We have updated the status of the hall.<br>";
        echo "<br>";
        echo '<a href="halls.php" class="btn btn-success">'."Back to Halls Manager".'</a>';
        
    }else{
    ?>
    <h1>Halls Management <a href="halls.php?add=true" class="btn btn-primary pull-right">Add a Hall</a></h1>
    <table class="table">
         <tr>
             <th>ID</th>
             <th>Image</th>
             <th>Name</th>
             <th>City</th>
             <th>Seatmap</th>
             <th>Status</th>
             <th>Actions</th>
         </tr>
<?PHP
    $halls = get_values('halls');
        foreach($halls as $hall):
?>
     <tr>
         <td><?PHP echo $hall['id']; ?></td>
         <td>
             <?PHP
                if($hall['image']!==''){
                    echo '<img src="../uploads/'.$hall['image'].'" class="img-responsive" width="240px" />';
                    echo "<br>";
                    echo '<a href="halls.php?add_image='.$hall['id'].'" class="btn btn-sm btn-primary">'."Edit Image".'</a>';
                }else{
                    echo '<a href="halls.php?add_image='.$hall['id'].'" class="btn btn-sm btn-primary">'."Add Image".'</a>';
                }
            ?>
         </td>
         <td><?PHP echo $hall['name']; echo "<br><br>"; echo nl2br($hall['address']); ?></td>
         <td>
             <?PHP
                $city_id = $hall['city_id'];
                
                $city = get_value('city', $city_id);
        
                echo ucfirst($city['name']);
            ?>
         </td>
         <td><a href="halls.php?edit_seatmap=<?PHP echo $hall['id']; ?>" class="btn btn-sm btn-warning">View / Edit</a></td>
         <td>
             <?PHP
                if($hall['status']=='inactive'){
                    echo '<a href="halls.php?status=active&id='.$hall['id'].'" class="btn btn-sm btn-danger">'."Inactive".'</a>';
                }elseif($hall['status']=='active'){
                    echo '<a href="halls.php?status=inactive&id='.$hall['id'].'" class="btn btn-sm btn-success">'."Active".'</a>';
                }else{
                    echo '<a href="halls.php?status=active&id='.$hall['id'].'" class="btn btn-sm btn-success">'."Make Active".'</a>';
                }
            ?>
         </td>
         <td>
             <a href="halls.php?edit=<?PHP echo $hall['id']; ?>" class="btn btn-sm btn-primary">Edit</a>
             <a href="halls.php?delete=<?PHP echo $hall['id']; ?>" class="btn btn-sm btn-danger">Delete</a>
         </td>
     </tr>
      
<?PHP
        endforeach;
        
        echo '</table> ';
    }
    include("mods/footer.php");
?>
            