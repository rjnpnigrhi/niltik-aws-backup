<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //show a form to add a movie
?>
   <h3>Add a New Movie</h3>
    <form action="movies.php?insert=true" method="post" enctype="multipart/form-data" class="form">
        <div class="form-group">
            <label for="name">Name of the Movie</label>
            <input type="text" name="name" class="form-control" placeholder="Name of the Movie" required> 
        </div>
        
        <div class="form-group">
            <label for="language">Language</label>
            <select name="language" id="language" class="form-control">
                <?PHP 
                    $langs = get_values('languages');
                    foreach($langs as $lang):
                ?>
                <option value="<?PHP echo $lang['name']; ?>"> <?PHP echo $lang['name']; ?> </option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="certificate">Certificate</label>
            <select name="certificate" id="certificate" class="form-control">
                <?PHP 
                    $certs = get_values('certificates');
                    foreach($certs as $cert):
                ?>
                <option value="<?PHP echo $cert['name']; ?>"> [ <?PHP echo $cert['name']; ?> ] </option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="duration">Duration of Movie</label>
            <input type="text" name="duration" class="form-control" placeholder="Duration of the Movie" required>
        </div>
        
        <div class="form-group">
            <label for="dd">2D / 3D</label>
            <select name="dd" id="2d3d" class="form-control">
                <option value="2D">2D Movie</option>
                <option value="3D">3D Movie</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="cast">Cast &amp; Crew</label>
            <textarea name="cast" id="cast" class="form-control" cols="30" rows="10" required></textarea>
        </div>
        
        <div class="form-group">
            <label for="director">Director</label>
            <input type="text" name="director" class="form-control" placeholder="Director of the Movie" required>
        </div>
        
        <div class="form-group">
            <label for="music">Music Composer</label>
            <input type="text" name="music" class="form-control" placeholder="Music composer of the movie" required>
        </div>
        
        <div class="form-group">
            <label for="released">Release Date (MM/DD/YYYY)</label>
            <input class="form-control" id="date" name="released" placeholder="MM/DD/YYYY" type="text" required>
        </div>
        
        <div class="form-group">
            <label for="trailer">Teaser of the Movie (YouTube Embed Code)</label>
            <textarea name="trailer" id="trailer" class="form-control" cols="30" rows="10" placeholder="Collect embed code from youtube"></textarea>
        </div>
        
        <div class="form-group">
            <label for="image">Poster of the Movie (4 X 3 Size Only)</label>
            <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
                <img id="img" alt="your image" width="100%" />
        </div>
        
        <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" name="genre" class="form-control" placeholder="Comma Separated Terms" required>
        </div>
        
        <div class="form-group">
            <button class="btn btn-default" type="submit">Submit</button>
            <a href="movies.php" class="btn btn-success">Cancel</a>
        </div>
    </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert the data into database
        $name = $db->safe_data($_POST['name']);
        $language = $db->safe_data($_POST['language']);
        $certificate = $db->safe_data($_POST['certificate']);
        $duration = $db->safe_data($_POST['duration']);
        $dd = $db->safe_data($_POST['dd']);
        $cast = $db->safe_data($_POST['cast']);
        $director = $db->safe_data($_POST['director']);
        $music = $db->safe_data($_POST['music']);
        $trailer = $db->safe_data($_POST['trailer']);
        $released = $db->safe_data($_POST['released']);
        $genre = $db->safe_data($_POST['genre']);
        $status = "coming soon";
        
        $date = date("m/d/Y");
        $image = $_FILES['image']['name'];
        
        //$path = "/opt/bitnami/apache2/htdocs/movies/uploads/";
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 2097152){
            $errors[] = "File size must not be bigger than 2 MB.";
        }
        
        $actual_icon_name = time().".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];

        if(empty($errors)== TRUE){
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "INSERT INTO movies (name, language, certificate, duration, dd, cast, director, music, released, trailer, image, genre, date, status) VALUES ('$name', '$language', '$certificate', '$duration', '$dd', '$cast', '$director', '$music', '$released', '$trailer', '$actual_icon_name', '$genre', '$date', '$status')";
                $r = $db->insert($q);       
                
                echo "Successfully Added the Movie to Database.<br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'."Language :". $language.'</p>';
                echo '<p>'."Certificate : ". $certificate.'</p>';
                echo '<p>'."Release Date : ". $released.'</p>';
                echo '<p>'."Director : ". $director.'</p>';
                echo '<p>'."Music by : ". $music.'</p>';
                echo '<p>'."Cast &amp; Crew : ". $cast.'</p>';
                echo '<p>'."Genre : ". $genre.'</p>';
                echo "<br>";
                echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
            }else{
                print_r(error_get_last());
            }
        }else{
            print_r(json_encode($errors));
        }
    }elseif(isset($_GET['edit'])){
        //show form to edit the movie
        $id = $db->safe_data($_GET['edit']);
        $movie = get_value('movies', $id);
?>
   <h3>Edit <?PHP echo $movie['name']; ?></h3>
    <form action="movies.php?update=true" method="post" enctype="multipart/form-data" class="form">
        <div class="form-group">
            <label for="name">Name of the Movie</label>
            <input type="text" name="name" class="form-control" value="<?PHP echo $movie['name']; ?>" required> 
        </div>
        
        <div class="form-group">
            <label for="language">Language</label>
            <select name="language" id="language" class="form-control">
                <?PHP 
                    $langs = get_values('languages');
                    foreach($langs as $lang):
                ?>
                <option value="<?PHP echo $lang['name']; ?>"> <?PHP echo $lang['name']; ?> </option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="certificate">Certificate</label>
            <select name="certificate" id="certificate" class="form-control">
                <?PHP 
                    $certs = get_values('certificates');
                    foreach($certs as $cert):
                ?>
                <option value="<?PHP echo $cert['name']; ?>"> [ <?PHP echo $cert['name']; ?> ] </option>
                <?PHP endforeach; ?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="duration">Duration of Movie</label>
            <input type="text" name="duration" class="form-control" value="<?PHP echo $movie['duration']; ?>" required>
        </div>
        
        <div class="form-group">
            <label for="dd">2D / 3D</label>
            <select name="dd" id="2d3d" class="form-control">
                <option value="2D">2D Movie</option>
                <option value="3D">3D Movie</option>
            </select>
        </div>
        
        <div class="form-group">
            <label for="cast">Cast &amp; Crew</label>
            <textarea name="cast" id="cast" class="form-control" cols="30" rows="10" required><?PHP echo $movie['cast']; ?></textarea>
        </div>
        
        <div class="form-group">
            <label for="director">Director</label>
            <input type="text" name="director" class="form-control" value="<?PHP echo $movie['director']; ?>" required>
        </div>
        
        <div class="form-group">
            <label for="music">Music Composer</label>
            <input type="text" name="music" class="form-control" value="<?PHP echo $movie['music']; ?>" required>
        </div>
        
        <div class="form-group">
            <label for="released">Release Date (MM/DD/YYYY)</label>
            <input class="form-control" id="date" name="released"  value="<?PHP echo $movie['released']; ?>" type="text" required>
        </div>
        
        <div class="form-group">
            <label for="trailer">Teaser of the Movie (YouTube Embed Code)</label>
            <textarea name="trailer" id="trailer" class="form-control" cols="30" rows="10" placeholder="Collect embed code from youtube"><?PHP echo $movie['trailer']; ?></textarea>
        </div>
        
        <div class="form-group">
            <label for="image">Poster of the Movie (4 X 3 Size Only)</label>
            <br>
           <img src="../uploads/<?PHP echo $movie['image']; ?>" alt="<?PHP echo $movie['image']; ?>" style="width:240px;">
            <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])">
                <img id="img" alt="your image" width="100%" />
                <p class="help-block">P.S. : Leave it as such if you do not wish to change the image</p>
        </div>
        
        <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" name="genre" class="form-control" value="<?PHP echo $movie['genre']; ?>" required>
        </div>
        
        <div class="form-group">
            <input type="hidden" name="id" value="<?PHP echo $id; ?>">
            <input type="hidden" name="old_image" value="<?PHP echo $movie['image']; ?>">
            <button class="btn btn-default" type="submit">Submit</button>
            <a href="movies.php" class="btn btn-success">Cancel</a>
        </div>
    </form>
<?PHP
    }elseif(isset($_GET['update'])){
        //update movie data
        $name = $db->safe_data($_POST['name']);
        $language = $db->safe_data($_POST['language']);
        $certificate = $db->safe_data($_POST['certificate']);
        $duration = $db->safe_data($_POST['duration']);
        $dd = $db->safe_data($_POST['dd']);
        $cast = $db->safe_data($_POST['cast']);
        $director = $db->safe_data($_POST['director']);
        $music = $db->safe_data($_POST['music']);
        $trailer = $db->safe_data($_POST['trailer']);
        $released = $db->safe_data($_POST['released']);
        $genre = $db->safe_data($_POST['genre']);
        $id = $db->safe_data($_POST['id']);
        $old_image = $db->safe_data($_POST['old_image']);
        
        $image = $_FILES['image']['name'];
        
        if($image !== ''){
            //we will include the image and then add information
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 2097152){
            $errors[] = "File size must not be bigger than 2 MB.";
        }
        
        $actual_icon_name = time().$_SESSION['aid'].".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];
        
        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "UPDATE movies SET name = '$name', certificate = '$certificate', language = '$language', duration = '$duration', dd = '$dd', cast = '$cast', director = '$director', music = '$music', trailer = '$trailer', released = '$released', genre = '$genre', image = '$actual_icon_name' WHERE id = '$id'";
                $r = $db->update($q);
                
                if($old_image !==''){
                    unlink('../uploads/'.$old_image);
                }
                
                echo "Successfully Updated the Movie Data.<br><br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'."Language :". $language.'</p>';
                echo '<p>'."Certificate : ". $certificate.'</p>';
                echo '<p>'."Duration : ". $duration.'</p>';
                echo '<p>'."Release Date : ". $released.'</p>';
                echo '<p>'."Director : ". $director.'</p>';
                echo '<p>'."Music by : ". $music.'</p>';
                echo '<p>'."Cast &amp; Crew : ". $cast.'</p>';
                echo '<p>'."Genre : ". $genre.'</p>';
                echo "<br>";
                echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
                }
            }
        }else{
            //we will add information without the image
            $q = "UPDATE movies SET name = '$name', certificate = '$certificate', language = '$language', duration = '$duration', dd = '$dd', cast = '$cast', director = '$director', music = '$music', trailer = '$trailer', released = '$released', genre = '$genre' WHERE id = '$id'";
                $r = $db->update($q);
                
                echo "Successfully Updated the Movie Data.<br><br>";
                echo '<img src="../uploads/'.$old_image.'" class="img-responsive" width="100%" />';
                echo '<h3>'.$name.'</h3>';
                echo '<p>'."Language :". $language.'</p>';
                echo '<p>'."Certificate : ". $certificate.'</p>';
                echo '<p>'."Duration : ". $duration.'</p>';
                echo '<p>'."Release Date : ". $released.'</p>';
                echo '<p>'."Director : ". $director.'</p>';
                echo '<p>'."Music by : ". $music.'</p>';
                echo '<p>'."Cast &amp; Crew : ". $cast.'</p>';
                echo '<p>'."Genre : ". $genre.'</p>';
                echo "<br>";
                echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete the movie from database
        //remove the images if any attached to this
        $id = $db->safe_data($_GET['delete']);
        $movie = get_value('movies', $id);
        
        $old_image = $movie['image'];
        
        if($old_image !==''){
            unlink('../uploads/'.$old_image);
        }
        
        delete_row('movies', $id);
        
        echo "We have deleted the Movie from our database.<br><br>";
        echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
    }elseif(isset($_GET['view'])){
        //get the movie details and show it here
        $id = $db->safe_data($_GET['view']);
        $movie = get_value('movies', $id);
        
?>
     <div class="col-sm-6">
         <h1 class="text-info"><?PHP echo ucfirst($movie['name']); ?> <sup><?PHP echo $movie['certificate']; ?></sup> , <?PHP echo ucfirst($movie['language']); ?> </h1>
         <br>
         <p>Duration : <?PHP echo ucfirst($movie['duration']); ?></p>
         <sup>*</sup>ing : <br>
         <h4> <?PHP echo nl2br($movie['cast']); ?> </h4>
         <br>
         <p>Director : <?PHP echo ucfirst($movie['director']); ?> </p>
         <p>Music by : <?PHP echo ucfirst($movie['music']); ?></p>
         <p>Release Date : <?PHP echo ucfirst($movie['released']); ?></p>
         <p>Genre : <?PHP echo ucfirst($movie['genre']); ?></p>        
     </div>
     <div class="col-sm-6">
         <?PHP echo $movie['trailer']; ?>
     </div> 
     
     <div class="clearfix"></div>
      <img src="../uploads/<?PHP echo $movie['image']; ?>" alt="<?PHP echo ucfirst($movie['name']); ?>" class="img-responsive"> <br>
         <a href="movies.php" class="btn btn-default">Go Back !</a>
         <a href="movies.php?edit_image=<?PHP echo $movie['id']; ?>" class="btn btn-primary btn-sm">Change Poster</a>
<?PHP        
    }elseif(isset($_GET['edit_image'])){
        //show form to edit the image 
        $id = $db->safe_data($_GET['edit_image']);
        $movie = get_value('movies', $id);
        
        echo '<h1>'.$movie['name'].'</h1>';
        if($movie['image']!==''){
            echo '<h3>'."Change this Current Image".'</h3>';
            echo '<img src="../uploads/'.$movie['image'].'" class="img-responsive" width="100%" />';
        }
        
        echo "<hr>";
?>
   <form action="movies.php?update_image=true" method="post" enctype="multipart/form-data" class="form">
       <div class="form-group">
           <label for="image">Chose Image</label>
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('img').src = window.URL.createObjectURL(this.files[0])" required>
           <img id="img" alt="your image" width="100%" />
       </div>
       
       <div class="form-group">
          <input type="hidden" name="id" value="<?PHP echo $id; ?>">
          <input type="hidden" name="old_image" value="<?PHP echo $movie['image']; ?>">
           <button class="btn btn-default" type="submit">Submit</button>
           <a href="halls.php" class="btn btn-primary">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update_status'])){
        $id = $db->safe_data($_GET['update_status']);
        
        $q = "UPDATE movies SET status = 'now playing' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "Successfully updated Movie Status.<br>";
        echo "<br>";
        echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
                
    }elseif(isset($_GET['update_image'])){
        //update image of the hall
        $id = $db->safe_data($_POST['id']);
        $old_image = $db->safe_data($_POST['old_image']);
        
        $image = $_FILES['image']['name'];
        
        $path = "../uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");
        
        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];
        
        //file extension
        $ext=strtolower(getExtension($icon_name));
        
        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";
            
        }
        
        if($icon_size > 2097152){
            $errors[] = "File size must not be bigger than 2 MB.";
        }
        
        $actual_icon_name = time().$_SESSION['aid'].".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];
        
        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){
                $q = "UPDATE movies SET image = '$actual_icon_name' WHERE id = '$id'";
                $r = $db->update($q);
                
                if($old_image !==''){
                    unlink('../uploads/'.$old_image);
                }
                
                echo "Successfully updated Movie Banner Image.<br>";
                echo '<img src="../uploads/'.$actual_icon_name.'" class="img-responsive" width="100%" />';
                echo "<br>";
                echo '<a href="movies.php" class="btn btn-success">'."Back to Movies Manager".'</a>';
                }
            }
    }else{
        //show all movies in pagination view
?>
   <h1>Movies Manager <a href="movies.php?add=true" class="btn btn-primary pull-right">+ Add Movie</a></h1>
   
   <table class="table">
       <tr>
           <th>ID</th>
           <th>Image</th>
           <th>Name</th>
           <th>Cast</th>
           <th>Genre</th>
           <th>Status</th>
           <th>Action</th>
       </tr>
   
<?PHP
    $q = "SELECT * FROM movies ORDER BY id DESC";
    $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, there are no Movie entries.<br><br>";
            echo '<a href="movies.php?add=true" class="btn btn-sm btn-default">'."Add Movie !".'</a>';
        }else{
            while($diaries = $r->fetch_array()){
                $result[] = $diaries;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 6);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
                
            foreach($data as $movie):
        ?>
        <tr>
            <td><?PHP echo $movie['id']; ?></td>
            <td><a href="../uploads/<?PHP echo $movie['image']; ?>" target="_blank"><img src="../uploads/<?PHP echo $movie['image']; ?>" alt="<?PHP echo $movie['name']; ?>" width="240px" class="img-responsive"></a> <br> <a href="movies.php?edit_image=<?PHP echo $movie['id']; ?>" class="btn btn-primary btn-sm">Change Poster</a> </td>
            <td><strong><span class="text-danger"><?PHP echo $movie['name']; ?></span></strong> <br><br><?PHP echo $movie['certificate']."<br>".$movie['language']; ?></td>
            <td><?PHP echo nl2br($movie['cast']); ?></td>
            <td><?PHP echo $movie['genre']; ?></td>
            <td>
                <?PHP 
                    if($movie['status']=='coming soon'){
                        echo '<a href="movies.php?update_status='.$movie['id'].'" class="btn btn-sm btn-success ">'.ucfirst($movie['status']).'</a>';
                    }else{
                        echo '<a href="#" class="btn btn-sm btn-warning disabled">'.ucfirst($movie['status']).'</a>';
                    }
                ?>
            </td>
            <td><a href="movies.php?view=<?PHP echo $movie['id']; ?>" class="btn btn-sm btn-info"><i class="fa fa-search" aria-hidden="true"></i>
</a> <a href="movies.php?edit=<?PHP echo $movie['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a> <a href="movies.php?delete=<?PHP echo $movie['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>
</a></td>
        </tr>

        <?PHP
            endforeach;
        ?>
        </table>
        <div class="clearfix"></div>
        <hr>
    <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="movies.php?page='.$pp.'" aria-label="Previous"><< PREV</a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="movies.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="movies.php?page='.$np.'" aria-label="Next">NEXT >></a></li>';
                }
                
               ?>
              </ul>
            </nav>
<?PHP
    }
    }

    include("mods/footer.php");
?>
            