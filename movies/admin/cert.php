<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //add new certification
?>
   <h3>Add Certificate</h3>
   <form action="cert.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="name">Name of Certificate</label>
           <input type="text" name="name" class="form-control" placeholder="Name of the Certificate" required>
           <p class="help-block">P.S. : Ensure the Certificate is not in our list.</p>
       </div>
       
       <div class="form-group">
           <label for="details">Details about the Certificate</label>
           <textarea name="details" id="details" class="form-control" cols="30" rows="10"></textarea>
       </div>
       
       <div class="form-group">
           <button class="btn btn-success" type="submit">Submit</button>
           <a href="cert.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert data to database
        $name = $db->safe_data($_POST['name']);
        $details = $db->safe_data($_POST['details']);
        $date = date("m/d/Y");
        
        $tbl_name = "name";
        
        if(value_exists('certificates', $tbl_name, $name)>0){
            echo "Certificate with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "INSERT INTO certificates (name, details, date) VALUES ('$name', '$details', '$date')";
            $r = $db->insert($q);
            
            echo "Successfully added " . $name . " To our Database .<br><br>";
            echo '<a href="cert.php" class="btn btn-info">'."View Certificates List".'</a>';
        }
    }else{
        //show all certificates
?>
   <h1>Censorship Certification List <a href="cert.php?add=true" class="btn btn-primary pull-right">Add Certificate</a></h1>
   <table class="table">
       <tr>
           <th>ID</th>
           <th>Name</th>
           <th>Details</th>
       </tr>
       <?PHP
            $certs = get_values('certificates');
            foreach($certs as $cert):
        ?>
       <tr>
           <td><?PHP echo $cert['id']; ?></td>
           <td><a href="#" class="btn btn-primary disabled"><strong><?PHP echo $cert['name']; ?></strong></a></td>
           <td><?PHP echo nl2br($cert['details']); ?></td>
       </tr>
       <?PHP endforeach; ?>
   </table>
<?PHP
    }
    include("mods/footer.php");
?>
            