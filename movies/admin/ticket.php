<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
    
    $txnid = $db->safe_data($_GET['view']);

    $q = "SELECT * FROM order_book WHERE txnid = '$txnid' LIMIT 1";
    $r = $db->select($q);
    
    $info = $r->fetch_array();

    $email = $info['email'];
    $phone = $info['phone'];
    $schedule_id = $info['schedule_id'];
    $seat_names = $info['seats'];
    $seatcount = $info['seatcount'];
    $amount = $info['amount'];
    $date = $info['date'];

    $show = get_value('movie_schedule', $schedule_id);
    $ticket_price = $show['ticket'];
    $charges = $show['charges'];
    $movie_id = $show['movie_id'];
    $hall_id = $show['hall_id'];
    $show_id = $show['show_id'];
    $show_time = $show['show_time'];
$show_date = $show['date'];
    $hall = get_value('halls', $hall_id);
    $theater = $hall['name'];
    $address = $hall['address'];

    //let us get the movie details
    $movie = get_value('movies', $movie_id);
    $movie_name = $movie['name'];

    $total = $ticket_price*$seatcount;
    $extras = $seatcount*$charges;
    $amount = $total+$extras;

?>
  <br><br>
   <div id="to-print">
      <button type="submit" class="btn btn-info pull-right" onclick="printDiv('to-print')">
         <i class="fa fa-print" aria-hidden="true"></i> Print
     </button>
      <div class="clearfix"></div>
      <br>
      <table class="table">
          <tr style="background-color:#333; color:white;">
              <td><img src="../img/mnw_print.jpg" width="240px" alt=""></td>
              <td align="right"><br>Carry this Ticket with you! <br></td>
          </tr>
          <tr>
              <td>
                Dear Customer,<br>
                Your ticket(s) are Confirmed!
                <br>
              </td>
<td align="right">
<i class="fa fa-envelope-o" aria-hidden="true"></i> : <?PHP echo $email; ?><br>
<i class="fa fa-phone" aria-hidden="true"></i> : <?PHP echo $phone; ?><br>
</td>
          </tr>
          <tr style="background-color:#f7f7f7;">
              <td>
                <h2><?PHP echo $movie_name; ?></h2>
                <h4><?PHP echo ucfirst($theater); ?></h4>
                <p><?PHP echo ucfirst($address); ?></p>
              </td>
              <td align="right">
                <h2>Balcony</h2>
                <h4><?PHP echo $seat_names; ?></h4>
                <p><?PHP echo $show_time; ?> | 
                <?PHP
                    list($month, $day, $year) = explode('/', $show_date);
                    $m = strftime("%B",mktime(0,0,0,$month));
                    echo $m ." ". $day . ", ".  $year;
                ?>
                </p>    
              </td>
          </tr>
          <tr>
              <td colspan="2">
                 <br><br>
                  ORDER SUMMARY
              </td>
          </tr>
          <tr>
               <td>
                   <strong>Ticket Amount : (<?PHP echo $seatcount; ?> Tickets)</strong>                       
               </td>
               <td align="right">
                <?PHP                    
                    echo "Rs. ". $total .".00";
                ?>
               </td>
           </tr>
           <tr>
               <td>Internet Charges : (Rs. <?PHP echo $charges; ?>.00 / Ticket)</td>
               <td align="right">
               <?PHP
                    echo "Rs. ". $extras .".00";
                ?>    
               </td>
           </tr>
           <tr>
               <td><strong class="text-danger">Amount Paid :</strong></td>
               <td align="right">Rs . <?PHP echo $amount; ?>.00</td>
           </tr>
           <tr></tr>
           <tr style="border-bottom:2px dotted #333;">
               <td colspan="2">
               <strong>Important Instructions</strong>
                <br>
                <p>                
                    Tickets once booked cannot be exchanged, cancelled or refunded. <br>
                    Service Tax, Swachh Bharat Cess &amp; Krishi Kalyan Cess collected and paid to the department. <br>
                    For your proof of Identity at the hall counter, carry your Credit / Debit Card with you.
                </p>    
               </td>
           </tr>
           <tr>
               <td colspan="2">
                 <?PHP
                    //if(!$optradio==''){
                        //$offer_id = $optradio;
$offer_id = '8';
                        $offer = get_value('offers', $offer_id);
                        echo '<p>'."Cut below this line and preserve for the Offer".'</p>';
                        echo '<img src="../uploads/'.$offer['image'].'" width="100%" alt="">';
                    //}
                ?>
               </td>
           </tr>
           <tr>
               <td colspan="2" style="background-color:#333333; padding:20px; color:white; text-align:center;">
                   For Assistance or Phone Booking please contact <i class="fa fa-phone" aria-hidden="true"></i> : 7008584789 | mail us at <i class="fa fa-envelope-o" aria-hidden="true"></i> : helpdesk@niltik.com
               </td>
           </tr>
      </table>
          
       </div>
       <script type="text/javascript">
        function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();
        document.body.innerHTML = originalContents;
        }
    </script>
<?PHP
    include("mods/footer.php");
?>
            