<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');

    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();

	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['update'])){
        //show form for all cinema halls
        //on click of any show go to next form
        $date = $db->safe_data($_GET['update']);
        //$date = explode('/', $date);
        list($month, $day, $year) = explode('/', $date);
        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
?>
     <h3>Update Halls and Movies for <?PHP echo $day." ".$m.", ".$year; ?> <a href="schedule.php" class="btn btn-primary pull-right">Schedule Page</a></h3>
     <br><br>
     <table class="table">
         <tr>
             <th>Hall Name</th>
             <th class="text-center">Show 1</th>
             <th class="text-center">Show 2</th>
             <th class="text-center">Show 3</th>
             <th class="text-center">Show 4</th>
             <th class="text-center">Show 5</th>
         </tr>
         <?PHP
            $halls = get_values('halls');
            foreach($halls as $hall):
        ?>
        <tr>
            <td>
                <?PHP
                    echo '<h3>'.ucfirst($hall['name']).'</h3>';
                    $city_id = $hall['city_id'];
                    $city = get_value('city', $city_id);
                    echo $city['name'];
                ?>
            </td>
            <td align="center">
                <?PHP echo get_status($hall['id'], '1', $date) ?> <br><br>
                <a href="schedule.php?add_show=<?PHP echo $date; ?>&hall_id=<?PHP echo $hall['id']; ?>&show_id=1" class="btn btn-sm btn-info">View/Update</a>
            </td>
            <td align="center">
                <?PHP echo get_status($hall['id'], '2', $date) ?> <br><br>
                <a href="schedule.php?add_show=<?PHP echo $date; ?>&hall_id=<?PHP echo $hall['id']; ?>&show_id=2" class="btn btn-sm btn-danger">View/Update</a>
            </td>
            <td align="center">
                <?PHP echo get_status($hall['id'], '3', $date) ?> <br><br>
                <a href="schedule.php?add_show=<?PHP echo $date; ?>&hall_id=<?PHP echo $hall['id']; ?>&show_id=3" class="btn btn-sm btn-primary">View/Update</a>
            </td>
            <td align="center">
                <?PHP echo get_status($hall['id'], '4', $date) ?> <br><br>
                <a href="schedule.php?add_show=<?PHP echo $date; ?>&hall_id=<?PHP echo $hall['id']; ?>&show_id=4" class="btn btn-sm btn-success">View/Update</a>
            </td>
            <td align="center">
                <?PHP echo get_status($hall['id'], '5', $date) ?> <br><br>
                <a href="schedule.php?add_show=<?PHP echo $date; ?>&hall_id=<?PHP echo $hall['id']; ?>&show_id=5" class="btn btn-sm btn-warning">View/Update</a>
            </td>
        </tr>
        <?PHP endforeach; ?>

     </table>

<?PHP

    }elseif(isset($_GET['add_show'])){
        //show form for one hall and one show
        $date = $db->safe_data($_GET['add_show']);
        $hall_id = $db->safe_data($_GET['hall_id']);
        $show_id = $db->safe_data($_GET['show_id']);
$hall = get_value(halls, $hall_id);
        $city_id = $hall['city_id'];

        //$date = explode('/', $date);
        list($month, $day, $year) = explode('/', $date);
        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');

        $hall = get_value('halls', $hall_id);


?>
     <h3>Show <?PHP echo $show_id; ?>, <strong class="text-danger"><?PHP echo $hall['name']; ?></strong>, <?PHP echo $m . " " . $day . ", " . $year; ?> <a href="schedule.php?update=<?PHP echo $date; ?>" class="btn btn-primary pull-right">Cancel</a> </h3>

<?PHP
        $q = "SELECT * FROM movie_schedule WHERE hall_id = '$hall_id' AND show_id = '$show_id' AND date = '$date' LIMIT 1 ";
        $r = $db->select($q);

        if($r){
            //we show the form with the data collected for editing
            $row = $r->fetch_array();

            $id = $row['id'];
            $hall_id = $row['hall_id'];
            $hall = get_value('halls', $hall_id);
            $show_id = $row['show_id'];
            $movie_id = $row['movie_id'];
            $movie = get_value('movies', $movie_id);
            $available_seats = $row['available_seats'];
            $seatmap_data = $row['seatmap_data'];
            $date = $row['date'];
            $show_time = $row['show_time'];
?>
     <br><br>
     <form action="schedule.php?update_show=true" method="post" class="form">
         <div class="form-group">
             <label for="hall">Name of the Hall : </label>
             <input type="text" name="hall_name" class="form-control" value="<?PHP echo $hall['name']; ?>" readonly>
             <input type="hidden" name="hall_id" value="<?PHP echo $hall_id; ?>">
         </div>

         <div class="form-group">
             <label for="show">Show ID : </label>
             <input type="text" name="show_id" class="form-control" value="<?PHP echo $show_id; ?>" readonly>
         </div>

         <div class="form-group">
             <label for="date">Date of the Show : </label>
             <input type="text" name="date" class="form-control" value="<?PHP echo $date; ?>" readonly>
         </div>

         <div class="form-group">
             <label for="movie">Name of the Movie : </label>
             <input type="text" name="movie" class="form-control" value="<?PHP echo $movie['name']; ?>" readonly>
             <input type="hidden" name="old_movie_id" value="<?PHP echo $movie_id; ?>">
             <select name="movie_id" id="movie_id" class="form-control">
                <option value="">Do not Change</option>
                 <?PHP
                    $movies = get_values('movies');
                    foreach($movies as $movie):
                        ?>
                        <option value="<?PHP echo $movie['id']; ?>"><?PHP echo ucfirst($movie['name'])." - ".$movie['language']; ?></option>
                    <?PHP endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="showtime">Show Begins  :</label>
                <input type="text" name="show_time" class="form-control" value="<?PHP echo $row['show_time']; ?>" required>
            </div>

            <!-- TODO: To be saved into database-->
            <div class="form-group">
                <label for="available-seats">Available Seats :</label>
                <input type="text" id="available-seats" name="available_seats" class="form-control available-seats" value="<?PHP echo $row['available_seats']; ?>" required>
            </div>
            
            <div class="form-group">
                <label for="available-seats-type">Available Seats Type :</label>
                <input type="text" id="available-seats-type" name="available_seats_type" class="form-control available-seats-type" value="<?PHP echo $row['available_seats']; ?>" required>
            </div>
            
            <!-- Hidden, JavaScript will get data from PHP -->
            <div class="form-group hide">
                <textarea id="seatmap-data"><?PHP echo $hall['seatmap']; ?></textarea>
            </div>

            <div class="form-group" id="seat-arrangement">
                <label for="showtime">Enable Seats : (Click on the seat to enable / disable)</label>
                <div class="enable-seats">
                    <img src="../uploads/<?PHP echo $hall['image']; ?>">
                </div>
            </div>

            <div class="form-group">
                <input type="hidden" name="id" value="<?PHP echo $id; ?>">
                <button class="btn btn-default" type="submit">Submit</button>
                <a href="schedule.php?delete_show=<?PHP echo $id; ?>" class="btn btn-danger">Delete</a>
                <a href="schedule.php?update=<?PHP echo $date; ?>" class="btn btn-warning">Cancel</a>
            </div>
        </form>
        <?PHP
    }else{
        //show form
        ?>
        <br><br>
        <form action="schedule.php?insert_show=true" method="post" class="form">
            <div class="form-group">
                <label for="hall">Name of the Hall : </label>
                <input type="text" name="hall_name" class="form-control" value="<?PHP echo $hall['name']; ?>" readonly>
                <input type="hidden" name="hall_id" value="<?PHP echo $hall_id; ?>">
<input type="hidden" name="city_id" value="<?PHP echo $hall['city_id']; ?>"> 
            </div>

            <div class="form-group">
                <label for="show">Show ID : </label>
                <input type="text" name="show_id" class="form-control" value="<?PHP echo $show_id; ?>" readonly>
            </div>

            <div class="form-group">
                <label for="date">Date of the Show : </label>
                <input type="text" name="date" class="form-control" value="<?PHP echo $date; ?>" readonly>
            </div>

            <div class="form-group">
                <label for="movie">Name of the Movie : </label>
                <select name="movie_id" id="movie_id" class="form-control">
                    <?PHP
                    $movies = get_values('movies');
                    foreach($movies as $movie):
                        ?>
                        <option value="<?PHP echo $movie['id']; ?>"><?PHP echo ucfirst($movie['name'])." - ".$movie['language']; ?></option>
                    <?PHP endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="showtime">Show Begins at :</label>
                <input type="text" name="show_time" class="form-control" placeholder="00:00 AM/PM" required>
            </div>
            
            <div class="form-group">
                <label for="price">Price per Ticket</label>
                <input type="text" name="ticket" class="form-control" placeholder="Price per Ticket" required>
            </div>

	    <div class="form-group">
                <label for="charges">Service Charges</label>
                <input type="text" name="charges" class="form-control" placeholder="Service charge per ticket" required>
            </div>
            
            <!-- TODO: To be saved into database-->
            <div class="form-group">
                <label for="available-seats">Available Seats :</label>
                <input type="text" id="available-seats" name="available_seats" class="form-control" placeholder="Available Seats" required>
            </div>

            <!-- Hidden, JavaScript will get data from PHP -->
            <div class="form-group hide">
                <input type="text" id="seatmap-data" name="seatmap_data" class="form-control" value='<?PHP echo $hall['seatmap']; ?>'>
            </div>

            <div class="form-group" id="seat-arrangement">
                <label for="showtime">Enable Seats : (Click on the seat to enable / disable)</label>
                <div class="enable-seats">
                    <img src="../uploads/<?PHP echo $hall['image']; ?>" class="img-responsive">
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-default" type="submit">Submit</button>
                <a href="schedule.php?update=<?PHP echo $date; ?>" class="btn btn-warning">Cancel</a>
            </div>
        </form>
        <?PHP
    }

}elseif(isset($_GET['insert_show'])){
    //update the show, movie, hall, seats
    $hall_id = $db->safe_data($_POST['hall_id']);
    $city_id = $db->safe_data($_POST['city_id']);
    $show_id = $db->safe_data($_POST['show_id']);
    $movie_id = $db->safe_data($_POST['movie_id']);
    $show_time = $db->safe_data($_POST['show_time']);
    $ticket = $db->safe_data($_POST['ticket']);
    $charges = $db->safe_data($_POST['charges']);
    $available_seats = $db->safe_data($_POST['available_seats']);
    $date = $db->safe_data($_POST['date']);


    $q = "INSERT INTO movie_schedule (hall_id, city_id, show_id, movie_id, show_time, available_seats, ticket, charges, date) VALUES ('$hall_id', '$city_id', '$show_id', '$movie_id', '$show_time', '$available_seats', '$ticket', '$charges', '$date')";
    $r = $db->insert($q);

    $last_id = $r;

    //let us run the seat query here
    $array = explode(',', $available_seats);

    foreach($array as $seat){
        insert_seat($seat, $last_id, $date);
    }

    echo "You have successfully added a Movie Schedule <br><br>";
    echo "We have created the seat chart for you.<br><br>";
    echo '<a href="schedule.php?update='.$date.'" class="btn btn-warning">Back to Schedule</a>';

}elseif(isset($_GET['update_show'])){
    //update the show, movie, hall, seats
    $hall_id = $db->safe_data($_POST['hall_id']);
    $show_id = $db->safe_data($_POST['show_id']);
    $date = $db->safe_data($_POST['date']);
    $old_movie_id = $db->safe_data($_POST['old_movie_id']);
    $movie_id = $db->safe_data($_POST['movie_id']);
    $show_time = $db->safe_data($_POST['show_time']);
    $id = $db->safe_data($_POST['id']);

    if($movie_id !==''){
        $movie = $movie_id;
    }else{
        $movie = $old_movie_id;
    }
    $q = "UPDATE movie_schedule SET movie_id = '$movie', show_time = '$show_time' WHERE id = '$id'";
    $r = $db->update($q);

    //we need to change the seats too
    $q1 = "UPDATE seats SET movie_id = '$movie' WHERE hall_id = '$hall_id' AND show_id = '$show_id' AND date = '$date'";
    $r1 = $db->update($q1);

    echo "You have successfully Updated the Movie Schedule <br><br>";
    echo '<a href="schedule.php?add_show='.$date.'&hall_id='.$hall_id.'&show_id='.$show_id.'" class="btn btn-default">'."View Your Entry".'</a>';

}elseif(isset($_GET['delete_show'])){
    //delete hall/show box, remove all seats from table
    $id = $db->safe_data($_GET['delete_show']);
    $show = get_value('movie_schedule', $id);

    $hall_id = $show['hall_id'];
    $show_id = $show['show_id'];
    $date = $show['date'];

    $q = "SELECT * FROM booking WHERE movie_schedule_id = '$id'";
    $r = $db->select($q);

    if(!$r){
        //let us delete the show.
        $q1 = "DELETE FROM movie_schedule WHERE id = '$id'";
        $r1 = $db->delete($q1);

        //let us delete the seats
        $q2 = "DELETE FROM seats WHERE schedule_id = '$id'";
        $r2 = $db->delete($q2);

        echo "We have deleted the show from our database.<br><br>";
        echo '<a href="schedule.php?update='.$date.'" class="btn btn-success">'."Back to Movie Schedule Manager".'</a>';

    }else{
        echo "Bookings have been done for the show.<br>";
        echo "We can not delete the show.<br><br>";

        $count = $r->num_rows;
        echo $count . " Bookings for the show confirmed.<br><hr>";

        while($row = $r->fetch_array()):

            echo '<h2>'.$row['name'].'</h2>';
            echo '<p>'."eMail : ".$row['email']. " ";
            echo  "| Mobile : ".$row['mobile'].'</p>';
            $seats = $row['seats'];
            $array = explode(',', $seats);
            $seat_count = count($array);

            echo '<p>'."Seats : ".$seat_count." ";
            echo "| Seat Numbers : ".$seats.'</p>';
            echo '<p>'."Payment : Rs. ".$row['price'].".00".'</p>';
            echo "<hr>";

            endwhile;

            echo "If you wish you can BLOCK the booking for the show.<br><br>";
            echo '<a href="schedule.php?block_show='.$id.'&hall_id='.$hall_id.'&show_id='.$show_id.'&date='.$date.'" class="btn btn-warning">'."Block Booking for the Show".'</a>';
        }
    }elseif(isset($_GET['block_show'])){
        $id = $db->safe_data($_GET['block_show']);
        $show = get_value('movie_schedule', $id);
        $hall_id = $show['hall_id'];
        $show_id = $show['show_id'];
        $date = $show['date'];

        $q = "UPDATE movie_schedule SET status = 'blocked' WHERE id = '$id'";
        $r = $db->update($q);

        $q1 = "UPDATE seats SET status = 'blocked' WHERE hall_id = '$hall_id' AND show_id = '$show_id' AND date = '$date' AND status = 'available'";
        $r1 = $db->update($q1);

        echo "The scheduled movie show has been BLOCKED.<br><br>";
        echo '<a href="schedule.php?update='.$date.'" class="btn btn-default">'."Back to Movie Schedule Page".'</a>';
    }elseif(isset($_GET['unblock_show'])){
        $id = $db->safe_data($_GET['unblock_show']);
        $show = get_value('movie_schedule', $id);
        $hall_id = $show['hall_id'];
        $show_id = $show['show_id'];
        $date = $show['date'];

        $q = "UPDATE movie_schedule SET status = 'on' WHERE id = '$id'";
        $r = $db->update($q);

        $q1 = "UPDATE seats SET status = 'available' WHERE hall_id = '$hall_id' AND show_id = '$show_id' AND date = '$date' AND status = 'blocked'";
        $r1 = $db->update($q1);

        echo "The scheduled movie show has been UNBLOCKED.<br><br>";
        echo '<a href="schedule.php?update='.$date.'" class="btn btn-default">'."Back to Movie Schedule Page".'</a>';
    }else{

    echo '<h1 class="text-center text-primary"><strong>'."Schedule for the next One Week".'</strong></h1>';
    echo "<hr>";
    $date = date("m/d/Y");

    $count = '8';

    for($i=0; $i<$count; $i++){

        echo '<div class="col-sm-3 col-xs-6 text-center">';
        $date = date('m/d/Y', strtotime('+'.$i.' day', strtotime(date("m/d/Y"))));
        //$date = explode('/', $date);
        list($month, $day, $year) = explode('/', $date);
        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
        echo '<h1 class="text-danger"><strong>'.$day.'</strong></h1>';
        echo '<h4>'.$m . " " . $year.'</h4>';
        echo '<a href="schedule.php?update='.$date.'" class="btn btn-sm btn-success">'."View / Update".'</a>';
        echo '</div>';
    }
    echo '<div class="clearfix"></div>';
        echo "<hr>";
    }
    include("mods/footer.php");
?>
