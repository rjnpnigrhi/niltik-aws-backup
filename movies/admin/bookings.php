<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");
//before we show all bookings why not delete the empty records
$qe = "DELETE FROM order_book WHERE email = '' AND phone = '' AND txnid = ''";
$re = $db->delete($qe);

    //let us get all Bookings in a pagination

    $q = "SELECT * FROM order_book ORDER BY timestamp DESC";
    $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, there are no Movie Booking entries.<br><br>";
            echo '<a href="index.php" class="btn btn-sm btn-default">'."DashBoard !".'</a>';
        }else{
    ?>
        <h1><strong>Movie Booking Diary</strong></h1>
        <table class="table table-striped table-hover">
            <tr>
                <th>ID</th>
                <th>Customer Details</th>
                <th>Movie</th>
                <th>Date &amp; Show</th>
                <th>Tickets</th>
                <th>Hall</th>
                <th>Payment</th>
                <th>Status</th>
                <th>Invoice</th>
            </tr>  
    <?PHP
            while($diaries = $r->fetch_array()){
                $result[] = $diaries;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 100);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
                
            foreach($data as $book):
        ?>
            <tr>
                <td><?PHP echo $book['id']; ?></td>
                <td>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> : <?PHP echo $book['email']; ?> <br>
                    <i class="fa fa-phone" aria-hidden="true"></i> : <?PHP echo $book['phone']; ?>   
				<br><br>
				Transaction ID : <?PHP echo $book['txnid']; ?><br>
				Booking : <?PHP echo $book['timestamp']; ?>
                </td>
                <td>
            <?PHP
                $schedule_id = $book['schedule_id'];
				echo $schedule_id;
                $show = get_value('movie_schedule', $schedule_id);
                $movie_id = $show['movie_id'];
                $movie = get_value('movies', $movie_id);
                echo '<strong>'. $movie['name'] .'</strong>';
                echo "<br>";
                echo '<p class="small text-info">'.$movie['certificate'] . " | " . $movie['language'].'</p>';
            ?>
                </td>
                <td>
                <strong>
            <?PHP
                $show_name = $show['show_id'];
                
                if($show_name == '1'){
                    echo "Noon Show";
                }elseif($show_name == '2'){
                    echo "Matinee Show";
                }elseif($show_name == '3'){
                    echo "Evening Show";
                }elseif($show_name == '4'){
                    echo "Night Show";
                }
            ?>
               </strong>
                <br>
            <?PHP 
                $date = $show['date'];
                list($month, $day, $year) = explode('/', $date);
                //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
                echo $m ." ". $day .", ".$year; 
            ?>
                </td>
                <td>
                <strong>
            <?PHP
                $seats = $book['seats']; 
                $array = explode(',', $seats);
                echo count($array) . " Seats <br> ";
            ?>
                </strong>
                <?PHP echo $seats; ?>
                </td>
                <td>
            <?PHP
                $hall_id = $show['hall_id'];
                $hall = get_value('halls', $hall_id);
                echo '<strong>'.$hall['name'].'</strong>'."<br>";
                $city_id = $hall['city_id'];
                $city = get_value('city', $city_id);
                echo $city['name'];
            ?>
                </td>                
                <td><strong class="text-primary">Rs. <?PHP echo $book['amount']; ?>.00</strong></td>
                <td>
                <?PHP 
                if($book['status']=='success'){
                    echo '<span style="background-color:green; color:white; padding:5px;">'."COMPLETED".'</span>';
                }else{
                    echo '<span style="background-color:red; color:white; padding:5px;">'."INCOMPLETE".'</span>';                    
                }
                ?>                
                </td>
                <td>
                    <a href="ticket.php?view=<?PHP echo $book['txnid']; ?>" class="btn btn-sm btn-primary" target="_blank">Invoice</a>
                </td>
            </tr>
        <?PHP
            endforeach;
        ?>
        </table>
        <div class="clearfix"></div>
        <hr>
    <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="bookings.php?page='.$pp.'" aria-label="Previous"><< PREV</a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="bookings.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="bookings.php?page='.$np.'" aria-label="Next">NEXT >></a></li>';
                }
                
               ?>
              </ul>
            </nav>
<?PHP
        }
    include("mods/footer.php");
?>
            