<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['add'])){
        //show form to add a language
?>
   <h3>Add a New Language</h3>
   <form action="lang.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="name">Name of the Language</label>
           <input type="text" name="name" class="form-control" placeholder="Name of the Language" required>
       </div>
       
       <div class="form-group">
           <button class="btn btn-success" type="submit">Submit</button>
           <a href="lang.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['insert'])){
        //insert language to database
        $name = $db->safe_data($_POST['name']);
        $date = date("m/d/Y");
        
        $tbl_name = "name";
        
        if(value_exists('languages', $tbl_name, $name)>0){
            echo "Language with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "INSERT INTO languages (name, date) VALUES ('$name', '$date')";
            $r = $db->insert($q);
            
            echo "Successfully added " . $name . " To our Database .<br><br>";
            echo '<a href="lang.php" class="btn btn-info">'."View Language List".'</a>';
        }
    }elseif(isset($_GET['edit'])){
        //show form
        $id = $db->safe_data($_GET['edit']);
        $lang = get_value('languages', $id);
?>
   <h3>Edit Language : </h3>
   <form action="lang.php?update=true" method="post" class="form">
       <div class="form-group">
           <label for="name">Name of the Language</label>
           <input type="text" name="name" class="form-control" value="<?PHP echo $lang['name']; ?>" required>
           <p class="help-block">P.S. : Ensure the Language is not in our list.</p>
       </div>
       
       <div class="form-group">
            <input type="hidden" name="id" value="<?PHP echo $id; ?>">
            <button class="btn btn-success" type="submit">Submit</button>
            <a href="city.php" class="btn btn-info">Cancel</a>
       </div>
   </form>
<?PHP
    }elseif(isset($_GET['update'])){
        //update information on database
        $new_name = $db->safe_data($_POST['name']);
        $id = $db->safe_data($_POST['id']);
        
        $tbl_name = "name";
        
        if(value_exists('languages', $tbl_name, $new_name)>0){
            echo "Language with same name exists in our database.<br><br>";
            echo '<a href="javascript:history.back()" class="btn btn-primary">'."Go Back and Retry with other name.".'</a>';
        }else{
            $q = "UPDATE languages SET name = '$new_name' WHERE id = '$id'";
            $r = $db->update($q);
            
            echo "Successfully updated " . $new_name . " To our Database .<br><br>";
            echo '<a href="lang.php" class="btn btn-info">'."View Language List".'</a>';
        }
    }else{
        //show all languages
?>
   <h1>Language Master <a href="lang.php?add=true" class="btn btn-primary pull-right">Add New Language</a></h1>
   <table class="table">
       <tr>
           <th>ID</th>
           <th>Name</th>
           <th>Action</th>
       </tr>
       <?PHP
        $langs = get_values('languages');
        
        foreach($langs as $lang):
        ?>
        <tr>
            <td><?PHP echo $lang['id']; ?></td>
            <td><?PHP echo $lang['name']; ?></td>
            <td><a href="lang.php?edit=<?PHP echo $lang['id']; ?>" class="btn btn-sm btn-primary">Edit</a></td>
        </tr>
        <?PHP endforeach; ?>
   </table>
<?PHP
    }
    include("mods/footer.php");
?>
            