<?PHP
    session_start();

    if(!isset($_SESSION['aid'])){
        header("Location:login.php");
        exit;
    }

    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    if(isset($_GET['confirm'])){
        //we will update status
        $id = $db->safe_data($_GET['confirm']);
        
        $q = "UPDATE feedback SET status = 'read' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "Feedback Marked Read.<br><br>";
        echo '<a href="javascript:history.back()" class="btn btn-sm btn-info">'."Feedback Page".'</a>';
    }else{
        $q = "SELECT * FROM feedback ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, there are no entries.<br><br>";
            echo '<a href="index.php" class="btn btn-sm btn-default">'."Dashboard !".'</a>';
        }else{
            while($diaries = $r->fetch_array()){
                $result[] = $diaries;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 6);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);               
            
        ?>
        <h3>Feedback from Viewers</h3>
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Person</th>
                <th>Feedback</th>
                <th>Action</th>
            </tr>
            <?PHP foreach($data as $s): ?>
            <tr>
                <td><?PHP echo $s['id'];  ?></td>
                <td><i class="fa fa-calendar" aria-hidden="true"></i> : <?PHP echo $s['date'];  ?></td>
                <td><i class="fa fa-user" aria-hidden="true"></i> : <?PHP echo $s['name']."<br>".'<i class="fa fa-envelope" aria-hidden="true"></i>'." : ".$s['email']."<br>".'<i class="fa fa-phone-square" aria-hidden="true"></i>'." : ".$s['mobile'];  ?></td>
                <td><?PHP echo nl2br($s['feedback']);  ?></td>
                <td>
                <?PHP 
                    if($s['status']==''){
                        echo '<a href="feedback.php?confirm='.$s['id'].'" class="btn btn-xs btn-warning">'."Mark as Read".'</a>';
                    }else{
                        echo '<a href="#" class="btn btn-xs btn-success disabled">'."READ".'</a>';
                    }
                ?>                
                </td>
            </tr>
            <?PHP endforeach; ?>
        </table>
        
        <hr>
    <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="feedback.php?page='.$pp.'" aria-label="Previous"><< PREV</a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="feedback.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="feedback.php?page='.$np.'" aria-label="Next">NEXT >></a></li>';
                }
                
               ?>
              </ul>
            </nav>
   
<?PHP
        }
    }

    include("mods/footer.php");
?>
            