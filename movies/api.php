<?PHP

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    
    if(isset($_GET['coupon-code'])){
        $coupon_code = $_GET['coupon-code'];
        $coupons = get_coupon_code($coupon_code);
        $coupons_json = json_encode($coupons);
        echo $coupons_json;
    }


    if(isset($_GET['user-phone'])){
        $user_phone = $_GET['user-phone'];

        $q = "SELECT *, (SELECT show_time FROM movie_schedule WHERE id=schedule_id) AS show_time, (SELECT (SELECT name FROM movies WHERE id=movie_id) FROM movie_schedule WHERE id=schedule_id) AS movie_name, (SELECT (SELECT name FROM halls WHERE id=hall_id) FROM movie_schedule WHERE id=schedule_id) AS hall_name, (SELECT (SELECT name FROM city WHERE id=city_id) FROM movie_schedule WHERE id=schedule_id) AS city_name FROM order_book WHERE phone = ".$user_phone." ORDER BY timestamp DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo json_encode([]);
        }else{
            while($orders = $r->fetch_array()){
                $result[] = $orders;
            }

            $res = json_encode($result);
            echo $res;
        }
    }

    
?>