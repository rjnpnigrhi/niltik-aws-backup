<?PHP
	//this is db connection file
	
	class connection {
		
		private $host = DB_HOST;
		private $user = DB_USER;
		private $pass = DB_PASS;
		private $db_name = DB_NAME;
		
		public $link;
		
		public function __construct(){
			$this->connect();
		}
		
		private function connect(){
			$this->link = new mysqli($this->host, $this->user, $this->pass, $this->db_name);
		}
		
        public function real_escape_string($string) {
            // todo: make sure your connection is active etc.
            return $this->link->real_escape_string($string);
        }
        
		public function select($query){
			$result = $this->link->query($query);
			
			if($result->num_rows > 0){
				return $result;
			}else{
				return false;
			}
		}
        
        public function update($query){
            $result = $this->link->query($query);
            
            if($result){
               
            }else{
                echo "Data Could not be updated.<br />";
                echo '<a href="javascript:history.back()">'."Go back and Try again".'</a>';
            }
        }
        
        public function insert($query){
            $result = $this->link->query($query);
            
            if($result){
                $last_id = $this->link->insert_id;
                return $last_id;
            }else{
                echo "Data Could not be Inserted.<br />";
                echo '<a href="javascript:history.back()">'."Go back and Try again".'</a>';
            }
        }
        
        public function delete($query){
            $result = $this->link->query($query);
            
            if($result){
                
            }else{
                echo "Data Could not be deleted.<br />";
                echo '<a href="javascript:history.back()">'."Go back and Try again".'</a>';
            }
        }
        
        public function safe_data($data){
            $data = $this->link->real_escape_string($data);
            return $data;
        }
        
	}
?>