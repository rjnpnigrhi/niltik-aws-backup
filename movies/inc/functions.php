<?PHP
    //here we add all functions of the site
    $db = new connection();
    
    date_default_timezone_set('Asia/Kolkata'); 

	function get_db_to_array($result){
		global $db;
		
		$res_array = array();
		
		for($count=0; $row=mysqli_fetch_array($result); $count++){
			$res_array[$count] = $row;
		}
		
		return $res_array;
	}

    //let us get all the values from any table with $name
    function get_values($name){
        global $db;
        
        $q = "SELECT * FROM $name ORDER BY id DESC";
        $r = $db->select($q);
        
        $value = get_db_to_array($r);
        return $value;
    }

    //we can get a single row from any table with a name and id
    function get_value($name, $id){
        global $db;
        
        $q = "SELECT * FROM $name WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

    //we can find if an element exists in a table 
    function value_exists($table, $name, $value){
        global $db;
        
        $q = "SELECT * FROM $table WHERE $name LIKE '%$value%' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }

    //we can delete a row from any table with an id 
    function delete_row($table, $id){
        global $db;
        
        $q = "DELETE FROM $table WHERE id = '$id'";
        $r = $db->delete($q);
    }

    function getExtension($str){
        $i = strrpos($str, ".");
        if(!$i){
            return "";
        }
        
        $l = strlen($str) - $i;
        $ext = substr($str, $i+1, $l);
        return $ext;
    }

    function get_movie($hall_id, $show_id){
        global $db;
        
        $q = "SELECT * FROM movies WHERE hall_id = '$hall_id' AND show_id = '$show_id' LIMIT 1";
        $r = $db->select($q);
        
        if(!$r){
            $movie = '';
            $time = '';
            $price = '';
        }else{        
            $row = $r->fetch_array();
            $movie = $row['movie'];
            $time = $row['time'];
            $price = $row['price'];
        }
        
        return $row;
        
    }

    function random_code($num){
        global $db;
        
        $random_code = '';
        
        $count = 0;
        
        while($count < $num){
            $random_digit = mt_rand(0, 9);
            
            $random_code .= $random_digit;
            $count++;
        }
        
        return $random_code;
    }

    function get_status($hall_id, $show_id, $date){
        global $db;
        
        $q = "SELECT * FROM movie_schedule WHERE hall_id = '$hall_id' AND show_id = '$show_id' AND date = '$date' LIMIT 1";
        $r = $db->select($q);
        
        if(!$r){
            echo "No Schedule";
        }else{
            $row = $r->fetch_array();
            $id = $row['id'];
            $movie_id = $row['movie_id'];
            $time = $row['show_time'];
            $movie = get_value('movies', $movie_id);
            $status = $row['status'];
            
            $q1 = "SELECT * FROM seats WHERE schedule_id = '$id' AND status = 'available'";
            $r1 = $db->select($q1);
            
            if($r1){
                $row_cnt = $r1->num_rows;
            }else{
                $row_cnt = '0';
            }
            
            if($status == 'blocked'){
                echo '<span class="text-danger">'.$movie['name'].'</span>'." [ BLOCKED ]<br>";
                echo '<a href="schedule.php?unblock_show='.$id.'" class="btn btn-xs btn-success">'."UNBLOCK".'</a>';
            }else{
                echo '<span class="text-success">'.$movie['name'].'</span>';
            }
            echo "<br>";
            echo $time."<br>";
            echo '<kbd>'.$row_cnt.'</kbd>'. " Seats Available";
            echo "<br>";
            //we carry the schedule id and let the admin book from the admin panel itself
            echo '<a href="manual_book.php?id='.$id.'" class="btn btn-xs btn-primary">'."Book Now".'</a>';
        }
    }

    function insert_seat($seat, $schedule_id, $date){
        global $db;
        
        $status = "available";
        $q = "INSERT INTO seats (seat, schedule_id, date, status) VALUES ('$seat', '$schedule_id', '$date', '$status')";
        $r = $db->insert($q);
    }

    function movies_now(){
        global $db;
        
        $q = "SELECT * FROM movies WHERE status = 'coming soon'";
        $r = $db->select($q);        
        
        if(!$r){
            echo "Online Booking is not available now.<br>";
            echo "Please visit later.<br>";
        }else{            
            while($row = $r->fetch_array()){
             echo '<div class="col-sm-4 col-xs-12 padding-20">'; 
                echo '<div class="movie_box">';
                echo '<img src="uploads/'.$row['image'].'" width="100%" height="160px" alt="'.$row['name'].'">';
                echo '<div class="info_sm_bar">';
                echo "Certificate : ".$row['certificate'].'<span class="pull-right">'."Language : " . $row['language'].'</span>';
                echo '</div>';
                echo '<div class="info_bg_bar">';
                echo '<h3 class="text-primary">'. $row['name'] .'</h3>';
                echo '<p>'."*ing : ". $row['cast'] . '</p>';
                $tags = explode(',', $row['genre']);
               
                foreach ($tags as $tag) {
                echo '<a href="index.php?tag='.trim($tag).'" class="btn btn-xs btn-default">'.trim($tag).'</a>'." ";
                }
                echo '</div>';
                                 
                echo '<a href="" class="btn btn-lg btn-default disabled book_btn" title="Tickets will be available soon!">'."BOOK TICKET NOW".'</a>';
                
                echo '</div>';
            echo '</div>';
            }            
        }
    }

    function movies_now_playing($city_id){
        global $db;
        
        $date = date("m/d/Y");
            
        $q = "SELECT GROUP_CONCAT(DISTINCT CONCAT(movie_id)) FROM movie_schedule WHERE city_id = '$city_id' AND date >= '$date' GROUP BY movie_id";
            
        $r = $db->select($q);
            
        if(!$r){
            $value='';
        }else{
           while($value = $r->fetch_assoc()):
           $m_o = implode(',', $value);
            //$m_o = explode(',', $value);
            //echo $m_o;
            $movie_on = get_value('movies', $m_o);

            echo '<div class="col-sm-4 col-xs-12 padding-20">'; 
            echo '<div class="movie_box">';
            echo '<img src="uploads/'.$movie_on['image'].'" width="100%" height="160px" alt="'.$movie_on['name'].'">';
            echo '<div class="info_sm_bar">';
            echo "Certificate : ".$movie_on['certificate'].'<span class="pull-right">'."Language : " . $movie_on['language'].'</span>';
            echo '</div>';
            echo '<div class="info_bg_bar">';
            echo '<h3 class="text-primary">'. $movie_on['name'] .'</h3>';
            echo '<p class="small text-primary">'."*ing : ". $movie_on['cast'] . '</p>';
            $tags = explode(',', $movie_on['genre']);

            foreach ($tags as $tag) {
            echo '<a href="index.php?tag='.trim($tag).'" class="btn btn-xs btn-default">'.trim($tag).'</a>'." ";
            }
            echo '</div>';

            echo '<a href="book.php?id='.$movie_on['id'].'" class="btn btn-lg btn-success book_btn" title="Book Ticket Now">'."BOOK TICKET NOW".'</a>';
            echo '</div>';
            echo '</div>';
            endwhile;           
        }
        }

    function available_seats($id, $status){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(seat) FROM seats where schedule_id = '$id' AND status = '$status' GROUP BY schedule_id";
        $r = $db->select($q);
        if($r){
            $value = $r->fetch_assoc();        
        }else{
            $value = '';
        }
        
        return $value;
    }

    function free_seats($id){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(seat) FROM seats where schedule_id= '$id' GROUP BY schedule_id";
        $r = $db->select($q);
        if($r){
            $value = $r->fetch_assoc();        
        }else{
            $value = '';
        }
        
        return $value;
    }

    function booked_seats($id){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(DISTINCT seats) FROM order_book where schedule_id = '$id' AND status = 'success' GROUP BY schedule_id ORDER BY seats";
        $r = $db->select($q);
        if($r){
            $value = $r->fetch_assoc();        
        }else{
            $value = '';
        }
        
        return $value;
    }


    function blocked_seats($id){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(DISTINCT seats) FROM order_book where schedule_id = '$id' AND status = '' GROUP BY schedule_id ORDER BY seats";
        $r = $db->select($q);
        if($r){
            $value = $r->fetch_assoc();        
        }else{
            $value = '';
        }
        
        return $value;
    }
    
    function available_dates($id){
        global $db;
        $date = date("m/d/Y");
        $q = "SELECT GROUP_CONCAT(DISTINCT CONCAT(date) ORDER BY date) FROM movie_schedule WHERE movie_id = '$id' AND date >= '$date' GROUP BY movie_id";
        $r = $db->select($q);
        
        $value = $r->fetch_assoc();  
        
        return $value;
    }

    function available_halls($date, $city_id, $movie_id){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(DISTINCT CONCAT(hall_id)) FROM movie_schedule WHERE date = '$date' AND city_id = '$city_id' AND movie_id = '$movie_id' GROUP BY movie_id";
        $r = $db->select($q);
         
        $halls = $r->fetch_assoc();
        return $halls;
    }

    function get_hall_show($hall_id, $movie_id, $date_select){
        global $db;
        
        $q = "SELECT GROUP_CONCAT(DISTINCT CONCAT(show_id)) FROM movie_schedule WHERE date = '$date_select' AND movie_id = '$movie_id' AND hall_id = '$hall_id' GROUP BY hall_id";
        
        $r = $db->select($q);
         
        $shows = $r->fetch_assoc();
        
        $shows = implode(',', $shows);
        $shows = explode(',', $shows);
        
        foreach($shows as $show){
            echo $show;
        }
    }

function exists_coupon($coupon_code){
        global $db;
        $q = "SELECT * FROM coupons WHERE coupon_code = '$coupon_code' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }
    
    function get_coupons(){
        global $db;
        
        $q = "SELECT * FROM coupons ORDER BY id DESC";
        $r = $db->select($q);
        
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }else{
            return false;
        }
        
    }

    function get_coupon_code($coupon_code){
        global $db;
        $q = "SELECT * FROM coupons WHERE coupon_code = '$coupon_code' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
        	$coupon = $r->fetch_array();
        	return $coupon;
        }else{
        	return [];
        }
        
        
    
    }
	
	function get_year($date){
	global $db;
	
	list($month, $day, $year) = explode('/', $date);
    	return $year;
	}
?>