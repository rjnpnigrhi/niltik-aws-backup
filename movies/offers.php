<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
?>
<div class="container container-main">
   <h1>All Offers on our website</h1>
   <p>You can avail any of these offers on purchase of tickets from our website.</p>
   <hr>
    <?PHP
        $offers = get_values('offers');
        
        if(count($offers)<1){
            
        }else{
        foreach($offers as $offer):
    
        echo '<h3>'.$offer['name'].'</h3>';
        echo "<br>";
        echo '<img src="uploads/'.$offer['image'].'" class="img-responsive">';
        echo "<br>";
        echo "Offer Valid for : " . $offer['city'];
        echo "<br><hr>";

        endforeach;
        }
    ?>
</div>    
<?PHP

    include("mods/footer.php");
?>