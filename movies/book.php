<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    //include("mods/carousel.php");

    //include("mods/banner_728.php");
    $id = $db->safe_data($_GET['id']);
    $movie = get_value('movies', $id);
?>
   <div class="container-fluid book_bg hidden-xs" style="background:#333 url(uploads/<?PHP echo $movie['image']; ?>) no-repeat scroll center top/cover;">       
   </div>
   <div class="container-fluid book_page_info_bg">
       <span class="dt_lg"><?PHP echo $movie['name']; ?></span>
       <p>Certificate : <?PHP echo $movie['certificate']; ?>
       &nbsp; &nbsp;
       <?PHP echo $movie['language']; ?>
       &nbsp; &nbsp;       
       <?PHP
       $tags = explode(',', $movie['genre']);
               
        foreach ($tags as $tag) {
            echo '<span class="tag_box">'.trim($tag).'</span>';
        }
        ?>
        &nbsp; &nbsp; 
        <?PHP
        $date = $movie['released'];
            
            //$date = explode('/', $date);
            list($month, $day, $year) = explode('/', $date);
            $m = strftime("%B",mktime(0,0,0,$month));
            echo $m ." ". $day . ", ".  $year;   
        ?>
        &nbsp; &nbsp;
        <i class="fa fa-clock-o" aria-hidden="true"></i>
        <?PHP echo $movie['duration']; ?> Minutes
        
        <span class="small pull-right hidden-xs"> 
        *ing : <?PHP echo $movie['cast']; ?>
        <br>
        Director : <?PHP echo $movie['director']; ?> , 
        Music By : <?PHP echo $movie['music']; ?>
        
        </span>
       </p>
   </div>
    <div class="clearfix"></div>
    <div class="container-fluid date_bar">
        <?PHP
            $dates = available_dates($id);            
            $dates = implode(',', $dates);
            $dates = explode(',', $dates);
            $first = $dates[0];
	    $today = date("m/d/Y");
            if(isset($_GET['date'])){
                $date_select = $_GET['date'];
            }else{
                $date_select = $first;
            }
            foreach ($dates as $date) {
                if($date == $date_select){
                echo '<a href="book.php?id='.$id.'&date='.$date.'" class="btn btn-info btn-sm dt_btn active">';
                list($month, $day, $year) = explode('/', $date);
                //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
                echo '<span class="dt_lg">'.$day.'</span>'."<br>";
                echo '<span>'.$m.'</span>';
                echo '</a>'." ";   
 
                }elseif(get_year($date)=='2018'){
 			//next year we will have to change this to 2019 or delete the schedules with past dates
                }else{
                echo '<a href="book.php?id='.$id.'&date='.$date.'" class="btn btn-default btn-sm dt_btn">';
                list($month, $day, $year) = explode('/', $date);
                //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
                echo '<span class="dt_lg">'.$day.'</span>'."<br>";
                echo '<span>'.$m.'</span>';
                echo '</a>'." ";    
                }
}

   //echo '<a href="book.php?id='.$id.'&date='."01/01/2019".'" class="btn btn-info btn-sm dt_btn">';
                //list($month, $day, $year) = explode('/', $date);
                ////$m = strftime("%B",mktime(0,0,0,$month));
                //$dateObj   = DateTime::createFromFormat('!m', $month);
		//$m = $dateObj->format('F');
                //echo '<span class="dt_lg">'."01".'</span>'."<br>";
                //echo '<span>'."January".'</span>';
                //echo '</a>'." ";     
        ?>        
    </div>
    <div class="clearfix"></div>

     <div class="container">
     <h1 class="text-primary">
        <?PHP
            list($month, $day, $year) = explode('/', $date_select);
            //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
            echo $m . " " . $day . ", " . $year;
        ?>
    </h1>
      <div class="row">
        <?PHP

              if(!isset($_SESSION['city_name'])){
                  //do nothing  
                  header("Location: index.php");

              }else{
                  if($_SESSION['city_name'] == 'Berhampur'){
                  $city_id = '2';
              }elseif($_SESSION['city_name'] == 'Muniguda'){
                  $city_id = '10';
              }elseif($_SESSION['city_name'] == 'Jeypore'){
                  $city_id = '11';
              }

              }              
              ?>
       <?PHP
         //echo $date_select;
         //we willfind which halls are playing the movie on this date and at what time
         //echo "City ID : " . $city_id;
         $halls = available_halls($date_select, $city_id, $id);
         $halls = implode(',', $halls);
         $halls = explode(',', $halls);
         //echo $halls;
         echo "<br>";
         foreach($halls as $hall){
             $movie_id = $id;
             $date = $date_select;
             $hall_id = $hall;
             $hall_full = get_value('halls', $hall);
             $city_id = $hall_full['city_id'];
             $city = get_value('city', $city_id);
             echo '<section class="movie_info_bar" style="display:">';
             echo '<div class="col-sm-3">';
             echo '<span class="dt_lg text-success">'. $hall_full['name'] . ' <a href="#" data-toggle="tooltip" class="test" title="We sell only a limited number of tickets for the Cinema."><i class="fa fa-info-circle" aria-hidden="true"></i></a></span>'."<br>";
             echo '<p>'.$city['name'].'</p>';
             echo '</div>';
             
             echo '<div class="col-sm-9">';
             $q = "SELECT * FROM movie_schedule WHERE movie_id = '$id' AND hall_id = '$hall_id' AND date = '$date_select' ORDER BY show_id";
             $r = $db->select($q);
             
             while($rows = $r->fetch_array()){
                 $schedule_id = $rows['id'];
                 $ticket_price = $rows['ticket'];
                 $show_time = $rows['show_time'];
                 $testDateStr = strtotime($show_time);
                 $finalTime = date("H:i A", strtotime("-150 minutes", $testDateStr));
                 $current_time = date("H:i A");
		 $current_date = date("m/d/Y");
                 if($date_select == $current_date && $current_time >= $finalTime){
        ?>
        <button type="button" class="btn btn-warning btn-sm disabled" title="Show is Over">
            <p>Show <?PHP echo $rows['show_id']; ?></p>
            <?PHP echo $rows['show_time']; ?>
        </button> &nbsp;       
        <?PHP
                 }else{

        ?>
           
            <button 
            type="button"
            data-sheduleid="<?php echo $schedule_id; ?>" 
            class="btn btn-default btn-sm book-popover" 
            data-container="body" 
            data-toggle="popover"             
            data-toggle="modal"
            data-target="#myModal-<?php echo $schedule_id; ?>"
            data-placement="top" 
            data-content="
            <?PHP 
        	$free_seats = free_seats($schedule_id);
		$free_seats = implode(',', $free_seats);
		$fseats = count(explode(',', $free_seats));
		//echo $fseats."<br>";
                $status = "booked";
		$booked_seats = available_seats($schedule_id, $status);
		$booked_seats = implode(',', $booked_seats);
		$bseats = count(explode(',', $booked_seats));
            	
            	//echo $bseats."<br>";
            	$row_cnt = $fseats - $bseats;
            	
		if($row_cnt < 0){
			$row_cntf = '0';
		}else{
			$row_cntf = $row_cnt;
		}
            //$row_cntf = '0';
            echo '<p>'."Ticket @ Rs. ".$ticket_price.".00".'</p>';
            echo '<code>'. $row_cntf . " available".'</code>';
            ?>"> 
            <p>Show <?PHP echo $rows['show_id']; ?></p>
            <?PHP echo $rows['show_time']; ?></button> &nbsp;
            
            <!--- the modal begins here--->
            <div id="myModal-<?php echo $schedule_id; ?>" class="modal" data-backdrop="true">

              <!-- Modal content -->
              <div class="modal-content">
                <span class="close" >&times;</span>
                  <div class="modal-header">Chose Number of Seats</div>
                  <br><br>

               <?PHP
                if($row_cnt > '10'){
                    $aseats = '10';
                }elseif($row_cnt == '0'){
                    $asears = '0'; 
                }else{
                    $aseats = $row_cnt;
                }
                //$row_cnt = '0';
                if($row_cnt < 1){
                    echo '<h3 class="text-danger">'."All Seats Booked for the Show!".'</h3>';
                }else{
                  for($i=1; $i<=$aseats; $i++){
                      echo '<a href="book-2.php?scheduleid='.$schedule_id.'&seatcount='.$i.'" class="book_seat">'.$i.'</a>';
                  }
                }
                ?>

                <br><br>
                <div class="clearfix"></div>
              </div>

            </div>
            <!--- the modal ends here--->
            
        <?PHP
                 }
             }
             
             echo "</div>";
             //echo $halls;
             echo '<div class="clearfix"></div>';
             echo '</section>';
             echo "<br>";
             
         }
         //echo $values;
         ?>
        </div><br><br>  
       
     </div>
     <div class="clearfix"></div>
<br>
<?PHP
    include("mods/footer.php");
?>