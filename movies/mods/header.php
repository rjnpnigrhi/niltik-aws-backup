<!DOCTYPE html>
 <html>
<base href="http://www.niltik.com/movies/">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <head>
     <title>Movies.niltik.com : Book Movie Tickets online</title>
     <!-- let us get the bootstrap css -->
     <link rel="stylesheet" href="css/bootstrap.min.css">
      
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
       
     <!--let us get the font awesome css-->
     <link rel="stylesheet" href="css/font-awesome.min.css">
       
     <!--We will create a custom css for this site-->
     <link rel="stylesheet" href="css/custom.css">
     
     <!--Let us ger the favicon here--->
     <link rel="icon" type="image/png" href="img/favicon.png">
     
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
     
     <!-- Bootstrap Date-Picker Plugin -->
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
     
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->  
     
     
     <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116730534-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116730534-1');
</script>

     
        
</head>
   
<body>
 <!--- SITE top Navigation begins here----> 
  <nav class="navbar navbar-inverse navbar-custom navbar-static-top">
       <div class="container" style="margin-bottom:0;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <a href="home" class="navbar-brand"><img src="img/mnw_logo.png" alt="Niltik Movies" height="32px" style="margin-top:-5px;"></a> 
           <ul class="nav navbar-nav pull-right" id="select-city-wrap">
            <li class="dropdown">
              <a href="javascript:" id="select-city" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                 <i class="fa fa-map-marker" aria-hidden="true"></i>

                  <span class="selected-city-name">
                  <?PHP
                    if(isset($_SESSION['city_name']))  {
                        echo $_SESSION['city_name'];
                    }else{
                        echo "Select City";
                    }
                    ?></span>
                    <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
              	<?PHP
              		$cities = get_values('city');

              		foreach($cities as $row){
              			echo '<li><a class="city-name" href="javascript:">'.$row['name'].'</a></li>';
              		}
              	?>
               
              </ul>
            </li>         
        </ul>        
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">       
            <ul class="nav navbar-nav">
                <li><a href="home">Home</a></li>
                <li><a href="feedback">Feedback</a></li>
                <li><a href="trailers">Trailers</a></li>
                <li><a href="offers">Offers</a></li>
                <li><a href="support">Support</a></li>
                <li><a href="#events" data-toggle="tab">Events</a></li>                
            </ul>  
        </div>
            
        <div class="top_city">            
        
        </div>
        </div>
    </nav>
    <!--- SITE top Navigation ends here---->