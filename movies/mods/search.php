<div class="container">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">               
            <form action="search#" method="post" class="form">
                <div class="input-group">
                  <span class="input-group-addon text-success" id="basic-addon1"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                  <input type="text" class="form-control input-lg" placeholder="City Name" aria-describedby="basic-addon1" />
                  <span class="input-group-btn"><button class="btn btn-warning btn-lg" type="button"><i class="fa fa-search" aria-hidden="true"></i> Go!</button></span>
                </div>
            </form>
        </div>
    </div>