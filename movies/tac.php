<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
?>
<div class="container container-main">
   <?PHP 
   	$q = "SELECT * FROM pages WHERE id = '1'";
   	$r = $db->select($q);
   	
   	$row = $r->fetch_array();
   	
   ?>
   
   <h1><?PHP echo $row['name']; ?></h1>
   <hr>
   <p><?PHP echo nl2br($row['contents']); ?></p>
</div>    
<?PHP

    include("mods/footer.php");
?>