<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");

?>
<div class="text-center">
<h1>Site is temporarily down for Maintenance</h1>
<br><br>
<h3>We will be back soon!</h3>

<br><br>
<p>In case of any problem do contact us at : <strong>700 85 84 789</strong> </p>
</div>
<?PHP
    include("mods/footer.php");
?>