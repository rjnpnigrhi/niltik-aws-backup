$(document).ready(function( ) {
	var $submenu = $('.submenu');
	var $mainmenu = $('.mainmenu');
	$submenu.hide();
	$submenu.first().delay(400).slideDown(700);
	$submenu.on('click', 'li', function( ) {
		$submenu.siblings().find('li').removeClass('chosen');
		$(this).addClass('chosen');
	});
	$mainmenu.on('click', 'li', function( ) {
		$(this).next('.submenu').slideToggle().siblings('.submenu').slideUp();
	});
	$mainmenu.children('li:last-child').on('click', function( ) {
		$mainmenu.fadeOut().delay(500).fadeIn();
	});
    

	//////////////////////////////////////////////////////////
	// Seat Creation Logic
	//////////////////////////////////////////////////////////
    var seats = {};
    var seatsData = '';
    var availableSeats = [];
    var bookedSeats = [];
    var freeSeats = [];
    var selectedSeats = [];
    var enabledSeats = [];
	if (location.href.indexOf('halls.php?edit_seatmap=') > -1) {
        seatsData = $('#seatmap').val();
        try {
            seats = JSON.parse(seatsData);
            drawExistingSeats(seats);
		}catch(e){
        	alert('Invalid Seat Map Data!');
		}
	}
    
	function drawExistingSeats(seats, task) {
		var seatsArray = Object.keys(seats);
        for(var s in seatsArray){
			var seat = seats[seatsArray[s]];
			createSeatDiv(seat.seat, seat.type, seat.position, seat.size, task);
		}
    }

    $('#add-seat-name').click(function () {
		var seatName = $('#seat-name').val();
        var seatType = $('#seat-type').val();
		if(seatName){
            if($('#seat-' + seatName).length > 0){
                alert('Seat already exists!');
            }else{
                createSeatDiv(seatName, seatType);
            }
		}
    });
    
    function bookSeats(btnElem){
        var elem = btnElem;
        try{
            if($(elem).hasClass('enabled')){
                var requiredSeatCount = parseInt($('#seat-count-value').html());
                var enabledSeatCount = $('.seat.enabled').length;
                var bookedSeatCount = $('.seat.booked').length;
                var selectedSeatCount = $('.seat.selected').length;
                //alert('required' + requiredSeatCount);
                //alert('enabled'+ enabledSeatCount);
                //alert('booked'+ bookedSeatCount);
                //alert('selected'+ selectedSeatCount);
                //console.log('booked', bookedSeatCount);
                //console.log(seats, seatsData, availableSeats, bookedSeats);

                if(selectedSeatCount >= requiredSeatCount){
                    alert('You are allowed to book max ' + requiredSeatCount + ' seats.');
                    return;
                }else{
                    $(elem).addClass('selected');
                    $(elem).removeClass('enabled');
                    selectedSeats.push($(elem).html());
                    $('#selected-seats').val(selectedSeats.toString());
                    //console.log(selectedSeats);
                    //availableSeats.push(seatName);
                    //availableSeats.splice(availableSeats.indexOf(seatName), 1);
                }

            }else if($(elem).hasClass('selected')){
                $(elem).addClass('enabled');
                $(elem).removeClass('selected');
                var currentSeatName = $(elem).html();
                selectedSeats.splice(selectedSeats.indexOf(currentSeatName));
                $('#selected-seats').val(selectedSeats.toString());
            }else{
                alert('This seat is not available.');    
            }
            
            if(selectedSeats.length > 0){
                $('#submit-btn').removeClass('disabled');
            }else{
                $('#submit-btn').addClass('disabled');
            }

    //                        $(this).toggleClass('enabled');
    //                        $('#available-seats').val(availableSeats.toString());
    //                        $('#available-seats-type').val(availableSeats.toString());

        }catch(e){
            console.log(e);
        }
    }
    
    function enableSeats(btnElem){
        var elem = btnElem;
        var currentSeatName = $(elem).html();
        try{
            if($(elem).hasClass('enabled')){
                $(elem).removeClass('enabled');
                enabledSeats.splice(enabledSeats.indexOf(currentSeatName));
            }else{
                $(elem).addClass('enabled');
                enabledSeats.push(currentSeatName);
            }
            $('#available-seats').val(enabledSeats.toString());
        }catch(e){
            console.log(e);
        }
    }
	
	function createSeatDiv(seatName, seatType, position, size, task) {
		var seatHtml = '<div class="col seat" id="seat-'+ seatName +'" data-seat-type="'+ seatType +'" title="'+ seatType +'">'+seatName+'</div>';
		$('.enable-seats').append(seatHtml);
		var seatElem = $( "#seat-"+seatName );
		if(task === 'enable'){
            seatElem.click(function () {
                if (location.href.indexOf('book-2.php') > -1) {
                    if($(this).hasClass('booked')){
                       alert('This seat is already booked.');
                    }else{
                        bookSeats(this);
                    }
                }
                if (location.href.indexOf('manual_book.php') > -1) {
                    if($(this).hasClass('booked')){
                       alert('This seat is already booked.');
                    }else{
                        bookSeats(this);
                    }
                }
                if (location.href.indexOf('schedule.php') > -1) {
                    enableSeats(this);
                }
            });
		}else{
            seatElem.draggable({
                //snap: '.col',
                //snapMode: 'outer',
                //snapTolerance: 1,
                revert: 'invalid'
            });

            seatElem.dblclick(function () {
                if(confirm('Are you sure you want to remove the seat?')){
                    $('#seat-'+seatName).remove();
                    delete seats['seat-'+seatName];
                    $('#seatmap').val(JSON.stringify(seats));
                }
            });
		}

        if(position){
            seatElem.css({
				'left': position.left,
				'top': position.top
			});
		}

        if(size){
            seatElem.css({
                'height': size.height,
                'width': size.width
            });
            try{
                $('#seat-height').val(size.height);
                $('#seat-width').val(size.width);
			}catch(e){}
        }
    }

    $( ".enable-seats" ).droppable({
        drop: function( event, ui ) {
			var id = ui.draggable[0].id;
			var seat = ui.draggable[0].id.replace('seat-', '');
            var type = $(ui.draggable[0]).data('seat-type');
            seats[id] = {
				seat: seat,
				position: {
					left: Math.round(ui.position.left),
					top: Math.round(ui.position.top)
				},
				size: {
					height: $('#seat-height').val(),
					width: $('#seat-width').val()
				},
                type: type
			};
			$('#seatmap').val(JSON.stringify(seats));
        }
    });

	$('.seat-elem-attribute').change(function () {
        $( ".seat" ).css({
			height: $('#seat-height').val() + 'px',
			width: $('#seat-width').val() + 'px'
        });
    });



	//////////////////////////////////////////////////////
	// Seat Enable / Disable Logic
	/////////////////////////////////////////////////////
	if(location.href.indexOf('schedule.php?') > -1
		&& location.href.indexOf('add_show=') > -1
		&& location.href.indexOf('hall_id=') > -1
		&& location.href.indexOf('show_id=') > -1){
			showSeats();
    }
    
    if(location.href.indexOf('manual_book.php') > -1
		&& location.href.indexOf('id=') > -1){
        showSeats();
    }
    
    if(location.href.indexOf('book-2.php') > -1){
        showSeats();
    }
    
    
    function showSeats(){
        try{
            	seatsData = $('#seatmap-data').val();
				seats = JSON.parse(seatsData);
				drawExistingSeats(seats, 'enable');
            
                if($('#booked_seats').val()){
                    bookedSeats = $('#booked_seats').val();
                    disableBookedSeats(bookedSeats);
				}
            
                if($('#available_seats').val()){
                    availableSeats = $('#available_seats').val();
                    enableAvailableSeats(availableSeats);
				}
            
//                if($('#free_seats').val()){
//                    freeSeats = $('#free_seats').val();
//                    enableFreeSeats(freeSeats);
//				}
			}catch(e){
                console.log(e);
				//alert('Invalid Seat Map Data!');
			}
    }
    
    function disableBookedSeats(bookedSeats){
        var bookedSeatsArray = bookedSeats.split(',');
        for(var bs in bookedSeatsArray){
			var seatName = bookedSeatsArray[bs];
			$('#seat-'+ seatName).addClass('booked');
		}
    }

    function enableAvailableSeats(availableSeats) {
        var availableSeatsArray = availableSeats.split(',');
        for(var as in availableSeatsArray){
			var seatName = availableSeatsArray[as];
			$('#seat-'+ seatName).addClass('enabled');
		}
        
    //I added the following paragraph
    //+============================================
//    function enableFreeSeats(freeSeats) {
//        var freeSeatsArray = freeSeats.split(',');
//        for(var as in freeSeatsArray){
//			var seatName = freeSeatsArray[as];
//			$('#seat-'+ seatName).addClass('enabled');
//		}
    //+============================================
        
        $('#available-seats').change(function(){
            var availableSeats = JSON.parse($('#available-seats').val());
            var availableSeatsArray = availableSeats.split(',');
            for(var as in availableSeatsArray){
                var seatName = availableSeatsArray[as];
                $('.available-seats').removeClass('enabled');
                $('#seat-'+ seatName).addClass('enabled');
            }
        });
    
    }

    
    /////////////////////////////////////////////////////////
    // Select city
    ///////////////////////////////////////////////////////
    if($('.selected-city-name').html().trim() === 'Select City'){
        $('#city-selector-modal').modal('show');
    }
    
    $('.city-name').click(function(){
        var cityName= $(this).html();
        $('#select-city .selected-city-name').html(cityName);
        $.get( "template.php?city_name="+ cityName, function( data ) {
            location.reload();
        });
    });
    
    $('.city-select').click(function(){
        var cityName = $(this).html();
        $.get( "template.php?city_name="+ cityName, function( data ) {
           location.reload();
           $('.selected-city-name').html(cityName);
           $('#city-selector-modal').modal('hide'); 
        });
    });
    
    
});

////////////////////////////////////////////////////////
// popover initiation
////////////////////////////////////////////////////////

$(document).ready(function(){

    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover',
        html: true,
    });

});

$(document).ready(function(){

    $('[data-toggle="tooltip"]').popover({
        placement : 'top',
        trigger : 'hover',
    });

});

function myFunction() {
    var person = prompt("Number of Seats", "1");
    
    if (person != null) {
        document.getElementById("demo").innerHTML =
        "Hello " + person + "! How are you today?";
    }
}


//Function To Display Popup
function div_show() {
    document.getElementById('abc').style.display = "block";
}

//Function to Hide Popup
function div_hide(){
    document.getElementById('abc').style.display = "none";
}

///////////////////////////////////////////
//Modal for asking the number of seats
/////////////////////////////////////////////
// Get the modal
// Get the modal
var modal = $('.modal');

// Get the button that opens the modal
var btn = $(".book-popover");

// Get the <span> element that closes the modal
//var span = document.getElementsByClassName("close")[0];
var sheduleId;
btn.click(function(){
    sheduleId = $(this).data('sheduleid');
    //alert(sheduleId);
    $('#myModal-'+ sheduleId).show();
});


// When the user clicks the button, open the modal 
//btn.onclick = function() {
//    modal.style.display = "block";
//}

// When the user clicks on <span> (x), close the modal
$('.close').click(function(){
    $(this).closest('.modal').hide();
});

// When the user clicks anywhere outside of the modal, close it
//window.onclick = function(event) {
//    if (event.target == modal) {
//        $('#myModal-'+sheduleId).hide();
//    }
//}