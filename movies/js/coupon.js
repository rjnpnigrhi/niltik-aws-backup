try{


$(document).ready(function(){

	$('#gift_coupon_submit').click(function(){
		var finalAmount = $('#final-amount-value').text();
		var couponValue = $('#gift_coupon').val();
		var url = "/movies/api.php?coupon-code=" + couponValue;
		
		$.get(url, function( data ) {
		  	var couponObj = JSON.parse(data);
		  	checkCoupon(couponObj, couponValue, finalAmount);
		});
	});
	
	//amount coupon_code discount id info min_spend valid_for valid_from valid_upto
	
	var couponApplied = false;
	function checkCoupon(couponObj, couponValue, finalAmount){
	
		//console.log(couponObj);
		
		finalAmount = parseFloat(finalAmount);
		var min_spend = parseFloat(couponObj.min_spend);
		
		//console.log(finalAmount, min_spend);
			
		if(finalAmount < min_spend){
			alert("You have to spend minimum " + couponObj.min_spend + " to avail this coupon.");
			return;
		}
		
		var current_date = moment(Date.now());
		var valid_from = moment(couponObj.valid_from, "DD/MM/YYYY");
		var valid_upto = moment(couponObj.valid_upto, "DD/MM/YYYY");
		
		//console.log(current_date, valid_from, valid_upto);
		
		if(!moment(current_date).isBetween(valid_from, valid_upto)){
			alert('This coupon is not valid for today.');
			return;
		}
		
		var couponAmount = parseFloat(couponObj.amount);
		finalAmount = finalAmount - couponAmount;
		
		//console.log(couponAmount, finalAmount);
		
		$('.final-amount').text(finalAmount);
		$('#final-amount').val(finalAmount);
		
		$('#gift_coupon').prop("disabled", "disabled");
		$('#gift_coupon_submit').prop("disabled", "disabled");
			
	}

});


}catch(e){
	console.log("Error in calculating coupon value: ", e);
}