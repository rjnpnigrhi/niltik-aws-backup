<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    //echo $txnid;
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    //include("mods/carousel.php");

    //include("mods/banner_728.php");         
    
//    if(isset($_GET['schdeuleid'])){
//        $scheduleid = $db->safe_data($_GET['scheduleid']);         
//    }else{
//        $scheduleid = $db->safe_data($_POST['scheduleid']);                 
//    }
//
//    if(isset($_GET['seatcount'])){
//        $seatcount = $db->safe_data($_GET['seatcount']);         
//    }else{
//        $seatcount = $db->safe_data($_POST['seatcount']); 
//    }
    
    $scheduleid = $_GET['scheduleid'];
    $seatcount = $_GET['seatcount'];


    $show = get_value('movie_schedule', $scheduleid);

    $availableseats = $show['available_seats'];

    $hallid = $show['hall_id'];
    $hall = get_value('halls', $hallid);

    //free seats : all seats created for this event
    $free_seats = free_seats($scheduleid);
    //$free_seats = available_seats($scheduleid, 'available');
    $free_seats = implode(',', $free_seats);

    //booked seats are collected from order book
    $booked_seats = available_seats($scheduleid, 'booked');
    //$booked_seats = booked_seats($scheduleid);
    $booked_seats = implode(',', $booked_seats);
         
        
         
//echo $hall['seatmap'];
         
    ?>
      <div class="container">
         <section class="book_header">
             
          <h1><?PHP echo $hall['name']; ?></h1>
          <p>Show : <?PHP echo $show['show_id']; ?> &nbsp; &nbsp; 
          <?PHP
            list($month, $day, $year) = explode('/', $show['date']);
                        //$m = strftime("%B",mktime(0,0,0,$month));
                $dateObj   = DateTime::createFromFormat('!m', $month);
		$m = $dateObj->format('F');
                        echo $m ." ". $day . ", ".  $year;      
            ?>
            &nbsp; &nbsp; 
            Tickets : <?PHP echo $seatcount; ?>
          </p>
         </section>
<?PHP
      if(isset($_GET['ticket'])){
        //let us get the final list
        $scheduleid = $db->safe_data($_POST['scheduleid']);
        $show = get_value('movie_schedule', $scheduleid);
          
        $seats = $db->safe_data($_POST['seats']);          
             
?>
    <div class="col-sm-6 col-sm-offset-3">
        <h3>Final Steps to Book Your Ticket</h3>
        <p class="text-danger">You must complete the booking within next 10 Minutes</p>
        <br>
     <form action="form_process.php" method="post" class="form">
        <div class="form-group">
            <label for="seats">Selected Seats</label>
            <input type="text" name="seats" class="form-control" value="<?PHP echo $seats; ?>" readonly>
        </div>
         <div class="form-group">
             <label for="email">Your e-Mail ID :</label>
             <input type="email" name="email" class="form-control" placeholder="Type your valid e-Mail ID" required>
             <p class="small">Your Ticket will be mailed to your email ID</p>
         </div>
         
         <div class="form-group">
             <label for="mobile">Your Mobile No.</label>
             <input type="text" name="phone" class="form-control" placeholder="Your Mobile No" required>
             <p class="small">To send OTP and confirmation message for Ticket</p>
         </div>
         <div class="form-group">
             <h2>Offers</h2>
             <?PHP
                $q = "SELECT * FROM offers WHERE city IN ('all cities', 'Berhampur')";
                $r = $db->select($q);
                
                if(!$r){
                    echo '<p>'."No Offers are available for your city".'</p>';
                }else{
                    echo "Chose any one Offer.<br><br>";
                    while($rows = $r->fetch_array()){
            ?>
                 <div class="radio">
                  <label><input type="radio" name="optradio" value="<?PHP echo $rows['id']; ?>" checked="checked" >
                    <img src="uploads/<?PHP echo $rows['image']; ?>" alt="<?PHP echo $rows['name']; ?>" class="img-responsive">         
                 </label>
                </div>  
            <?PHP 
                    }
                }
              ?>
         </div>
         
         <div class="form-group">
             <table class="table">
                 <tr>
                     <th>ID</th>
                     <th>Tickets</th>
                     <th>Price</th>
                     <th>Total</th>
                 </tr>
                 <tr>
                     <td><?PHP echo $scheduleid; ?></td>
                     <td>
             <?PHP
               $seats_count = count(explode(',', $seats));
              echo $seats . " ( " .$seats_count . " Seats )";
              ?>
                     </td>
                     <td>
               <?PHP echo "Rs. ".$show['ticket'] . ".00"; ?>
                     </td>
                     <td>
             <?PHP
              $toto = $seats_count*$show['ticket'];
              echo "Rs. " . $toto . ".00";
              ?>
                     </td>
                 </tr>
                 <tr>
                     <td></td>
                     <td>Internet Maintenance Charges</td>
                     <td><?PHP $charge = $show['charges']; echo "Rs. " . $charge.".00/Ticket"; ?></td>
                     <td>
                 <?PHP
                  $extras = $seats_count*$charge;
                echo "Rs. " . $extras.".00";    
                  ?>
                     </td>
                 </tr>
                 
		<tr>		
			<td colspan="2" align="right"><span style="color:red;">Do You Have a Gift Coupon : </span></td>
			<td><input type="text" placeholder="Coupon Code" name="coupon_code" id="gift_coupon" size="20"></td>
			<td><a href="javascript:" class="btn btn-success btn-sm" id="gift_coupon_submit">Apply</a></td>           
		</tr>
				
							
		</tr>

                 <tr>
                     <td colspan="2"></td>
                     <td align="right">Total :</td>
                     <td><strong>
                                            
                     <?PHP
                     
                        $final = $toto + $extras;
                        
                        //if(isset($_SESSION['coupon_code'])){
                     	//	$final = $final-$_SESSION['coupon_code'];
                     	//}
                        
                        echo "Rs. <span class='final-amount'>" . $final. "</span>.00";
                    ?>
                     </strong>
                     </td>
                 </tr>
                 
                 <tr class="hide">
                     <td colspan="2"></td>
                     <td align="right">Total :</td>
                     <td><strong>
                                            
                     <?PHP
                     
                        echo "Rs. <span id='final-amount-value'>" . $final. "</span>.00";
                    ?>
                     </strong>
                     </td>
                 </tr>
				 
             </table>
         </div>
         <div class="form-group">
		 
		 </div>
         <div class="form-group">
             <label for="Proceed">Proceed to Pay</label>
             <input type="hidden" name="scheduleid" value="<?PHP echo $scheduleid; ?>">
             <input type="hidden" name="seats_count" value="<?PHP echo $seats_count; ?>">
             <input type="hidden" name="txnid" value="<?PHP echo $txnid; ?>">
             <input type="hidden" name="amount" id="final-amount" value="<?PHP echo $final; ?>">
             <input type="hidden" name="productinfo" value="Movie Ticket">
             <input type="hidden" name="service_provider" value="payu_paisa">
             <input type="hidden" name="firstname" value="Customer">
             <input type="hidden" name="surl" value="http://www.niltik.com/movies/success.php">
             <input type="hidden" name="furl" value="http://www.niltik.com/movies/failure.php">
             <button class="btn btn-success btn-lg pull-right" type="submit">PROCEED</button>
         </div>
     </form>   
    </div>
<?PHP
         }else{
            
        ?>
        <br><br>
<div class="col-sm-2">
        <div class="seat2 enabled" style="margin:0 20px 0 0; float:left;"></div> Available Seat 
    </div>
    <div class="col-sm-2">
        <div class="seat2 booked" style="margin:0 20px 0 0; float:left;"></div> Booked Seat 
    </div>
    <div class="col-sm-2">
        <div class="seat2 selected" style="margin:0 20px 0 0; float:left;"></div> Selected Seat
    </div>
    
    <div class="clearfix"></div>
    <br>
         <form action="book-2.php?scheduleid=<?PHP echo $scheduleid; ?>&seatcount=<?PHP echo $seatcount; ?>&ticket=true" method="post" class="form">
             
             <!-- TODO: To be saved into database-->
            <div class="form-group hide">
            <label for="Seat_select">Select Seats :</label>
            <input type="text" id="selected-seats" name="seats" class="form-control" required>
        </div>
        
        <div class="form-group hide">
            <label for="select_seats">Booked Seats</label>
            <input type="text" id="booked_seats" name="booked_seats" value="<?PHP echo $booked_seats; ?>" >
            <input type="text" id="available_seats" name="free_seats" value="<?PHP echo $free_seats; ?>" >
        </div>


        <!-- Hidden, JavaScript will get data from PHP -->
        <div class="form-group hide">
            <input type="text" id="seatmap-data" name="seatmap_data" class="form-control" value='<?PHP echo $hall["seatmap"]; ?>'>
        </div>

        <div class="form-group hide">
            <input type="text" id="booked_seats" name="booked_seats" class="form-control" value='<?PHP echo $booked_seats; ?>'>
        </div>

         <div id="seat-count-value" class="hide"><?php echo $seatcount; ?></div>

        <div class="form-group" id="seat-arrangement">
            <label for="showtime">Select Seats : (Click on the seat to Select / Deselect)</label>
            <div class="enable-seats">
                <img src="uploads/<?PHP echo $hall['image']; ?>">
            </div>
        </div>

        <div class="form-group">
           <input type="hidden" name="scheduleid" value="<?PHP echo $_GET['scheduleid']; ?>">
            <button class="btn btn-success pull-right disabled" type="submit" id="submit-btn">PROCEED</button>
        </div>
     </form>      
<?PHP } ?>
      </div>
     <div class="clearfix"></div>
<br>
<?PHP
    include("mods/footer.php"); 
?>