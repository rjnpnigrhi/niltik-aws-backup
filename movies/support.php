<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
    
?>
    <div class="container container-main">
        <h1>Support and Help Page</h1>
        <h4>Aurobindo Nagar, 1st Lane<br>
        Berhampur, 760001 <br>
        Odisha
        </h4>
        <h3>Call : 8908264076, 7008584789 </h3>
    </div>
<?PHP

    include("mods/footer.php");
?>