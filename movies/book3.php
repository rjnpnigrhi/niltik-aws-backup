<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    //include("mods/carousel.php");

    //include("mods/banner_728.php");
    $scheduleid = $db->safe_data($_POST['scheduleid']);
    $seats = $db->safe_data($_POST['seats']);    

    $seatcount = count(explode(',', $seats));
    
    $show = get_value('movie_schedule', $scheduleid);
    $availableseats = $show['available_seats'];

    $movie_id = $show['movie_id'];
    $show_id = $show['show_id'];

    $hallid = $show['hall_id'];
    $hall = get_value('halls', $hallid);
    $movie = get_value('movies', $movie_id);   

?>
<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  
<div class="container top-20">
   <section class="book_header">
     <div class="col-sm-6">         
      <h1><?PHP echo $hall['name']; ?></h1>
      <p>Show : <?PHP echo $show['show_id']; ?> &nbsp; &nbsp; 
      <?PHP
        list($month, $day, $year) = explode('/', $show['date']);
            $m = strftime("%B",mktime(0,0,0,$month));
            echo $m ." ". $day . ", ".  $year;      
        ?>
        &nbsp; &nbsp; 
        Tickets : <?PHP echo $seatcount; ?>
        &nbsp; &nbsp; &nbsp; &nbsp; Show Begins : <?PHP echo $show['show_time']; ?>
      </p>
     </div>
     <div class="col-sm-6 text-right">
         <h1><?PHP echo $movie['name']; ?></h1>
         <p>Certificate : <?PHP echo $movie['certificate'] ?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Language : <?PHP echo $movie['language']; ?> </p>
     </div>
     <div class="clearfix"></div>
   </section>
</div>  
<br><br>
<div class="container top-20">
   <div class="col-sm-6">
        <form action="book3.php?confirm=true" method="post" class="form">
            <div class="form-group">
                <label for="selected_seats">Selected Seats</label>
                <input type="text" name="seats" class="form-control" value="<?PHP echo $seats; ?>" required readonly>
            </div>
            <div class="form-group">
                <label for="email">e-Mail ID</label>
                <input type="email" name="email" class="form-control" placeholder="Type your working e_mail ID" required>
            </div>
            <div class="form-group">
                <label for="mobile">Mobile No.</label>
                <input type="text" name="phone" class="form-control" placeholder="Type your active Mobile No" required>
            </div>
            
            <h2>Offers</h2>
            <div class="form-group">
                <?PHP
                $q = "SELECT * FROM offers WHERE city IN ('all cities', 'Berhampur')";
                $r = $db->select($q);
                
                if(!$r){
                    echo '<p>'."No Offers are available for your city".'</p>';
                }else{
                    echo "Chose any one Offer. (Optional)<br><br>";
                    while($rows = $r->fetch_array()){
            ?>
                 <div class="radio">
                  <label><input type="radio" name="optradio">
                    <img src="uploads/<?PHP echo $rows['image']; ?>" alt="<?PHP echo $rows['name']; ?>" class="img-responsive">         
                 </label>
                </div>  
            <?PHP 
                    }
                }
              ?>
            </div>
            <table class="table">
                 <tr>
                     <th>ID</th>
                     <th>Tickets</th>
                     <th>Price</th>
                     <th>Total</th>
                 </tr>
                 <tr>
                     <td><?PHP echo $scheduleid; ?></td>
                     <td>
             <?PHP
               $seats_count = count(explode(',', $seats));
              echo $seats . " ( " .$seats_count . " Seats )";
              ?>
                     </td>
                     <td>
               <?PHP $price = '100'; echo "Rs. ".$price . ".00"; ?>
                     </td>
                     <td>
             <?PHP
              $toto = $seats_count*$price;
              echo "Rs. " . $toto . ".00";
              ?>
                     </td>
                 </tr>
                 <tr>
                     <td></td>
                     <td>Internet Maintenance Charges</td>
                     <td><?PHP $charge = '30'; echo "Rs. " . $charge.".00/Ticket"; ?></td>
                     <td>
                 <?PHP
                  $extras = $seats_count*$charge;
                echo "Rs. " . $extras.".00";    
                  ?>
                     </td>
                 </tr>
                 <tr>
                     <td colspan="2"></td>
                     <td align="right">Total :</td>
                     <td>
                     <?PHP
                        $final = $toto + $extras;
                        echo "Rs. " . $final. ".00";
                    ?>
                     </td>
                 </tr>
             </table>
             <div class="form-group">
                 <input type="hidden" name="scheduleid" value="<?PHP echo $scheduleid; ?>">
                 <input type="hidden" name="seatcount" value="<?PHP echo $seatcount; ?>">
                 <input type="hidden" name="txnid" value="<?PHP echo $txnid; ?>">
                 <input type="hidden" name="amount" value="<?PHP echo $final; ?>">
                 <input type="hidden" name="productinfo" value="Movie Ticket">
                 <input type="hidden" name="firstname" value="Customer">
                 <input type="hidden" name="service_provider" value="payu_paisa">
                 
             </div>
        </form>
   </div>
</div>
<?PHP

    include("mods/footer.php");
?>