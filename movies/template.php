<?PHP
    session_start();

if(isset($_GET['city_name'])){
    $_SESSION['city_name'] = $_GET['city_name'];
}
?>
<!DOCTYPE html>
<base href="http://www.niltik.com/movies/">
 <html>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <head>
     <title>Movies.niltik.com : Book Movie Tickets online</title>
     <!-- let us get the bootstrap css -->
     <link rel="stylesheet" href="css/bootstrap.min.css">
      
      
       
     <!--let us get the font awesome css-->
     <link rel="stylesheet" href="css/font-awesome.min.css">
       
     <!--We will create a custom css for this site-->
     <link rel="stylesheet" href="css/custom.css">
     
     <!--Let us ger the favicon here--->
     <link rel="icon" type="image/png" href="img/favicon.png">
     
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
     
     <!-- Bootstrap Date-Picker Plugin -->
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
     
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->     
</head>
   
<body>
 <!--- SITE top Navigation begins here----> 
  <nav class="navbar navbar-inverse navbar-static-top">
       <div class="container" style="margin-bottom:0;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><img src="img/favicon.png" alt="Niltik Movies" width="32px" style="margin-top:-5px;"></a>
        </div>

        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Feedback</a></li>
                <li><a href="#">Trailers</a></li>
                <li><a href="#">Offers</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Events</a></li>
            </ul>
            <a href="#" class="btn btn-success btn-sm pull-right book_ticket">Book a Ticket</a>
            <ul class="nav navbar-nav navbar-right" id="select-city-wrap">
                <li class="dropdown">
                  <a href="javascript:" id="select-city" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                     <i class="fa fa-map-marker" aria-hidden="true"></i>
                     
                      <span class="selected-city-name">
                      <?PHP
                        if(isset($_SESSION['city_name']))  {
                            echo $_SESSION['city_name'];
                        }else{
                            echo "Select City";
                        }
                        ?></span> <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a class="city-name" href="javascript:">Berhampur</a></li>
                    <li><a class="city-name" href="javascript:">Muniguda</a></li>
                  </ul>
                </li>         
            </ul>
        </div>
        </div>
    </nav>
    <!--- SITE top Navigation ends here---->
    
    <div class="container">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">               
            <form action="search#" method="post" class="form">
                <div class="input-group">
                  <span class="input-group-addon text-success" id="basic-addon1"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                  <input type="text" class="form-control input-lg" placeholder="City Name" aria-describedby="basic-addon1" />
                  <span class="input-group-btn"><button class="btn btn-warning btn-lg" type="button"><i class="fa fa-search" aria-hidden="true"></i> Go!</button></span>
                </div>
            </form>
        </div>
    </div>
    <br>
     <!---Carousel of Images begins here--->
	<div class="container-fluid">
	    <div class="row">	       
	<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide img-responsive" width="100%" src="img/slide_01.jpg" alt="First slide">
          <div class="container">
            
          </div>
        </div>
        <div class="item">
          <img class="second-slide img-responsive" width="100%" src="img/slide_02.jpg" alt="Second slide">
          <div class="container">
            
          </div>
        </div>
        <div class="item">
          <img class="third-slide img-responsive" width="100%" src="img/slide_03.jpg" alt="Third slide">
          <div class="container">
            
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
    
	    </div>
	</div>
	<!---Carousel of Images ends here--->
     <br>
     <!---- AD SPACE FOR banner ad ---->
     <div class="container text-center hidden-xs">
         SPACE FOR 728X90 Google Adsense Ad
     </div>
     <!---- AD SPACE FOR banner ad ---->
     <br>
     
     <div class="container">
       <div class="col-sm-8 col-sm-offset-2 col-xs-12">
        <ul class="nav nav-pills nav-justified">
            <li role="presentation" class="pill1"><a href="#" class="text-success">Now Showing</a></li>
            <li role="presentation" class="pill1"><a href="#" class="text-warning">Coming Soon</a></li>
        </ul>
       </div>
       <div class="clearfix"></div>
       <br><br>
       
       <div class="col-sm-4 col-xs-12">
           <img src="uploads/14903659651.jpg" width="100%" class="img-responsive" alt="Title of the Movie">
           <div class="numberCircle">UA</div>, <strong>Hindi</strong>
           <h2><strong>Name of the Movie</strong></h2>
           *ing : Name of the Actors in the Movie Here
           <h2 class="book_btn">BOOK TICKET NOW</h2>
       </div>
       
       <div class="col-sm-4 col-xs-12">
           <img src="uploads/14903659651.jpg" width="100%" class="img-responsive" alt="Title of the Movie">
           <div class="numberCircle">UA</div>, <strong>Hindi</strong>
           <h2><strong>Name of the Movie</strong></h2>
           *ing : Name of the Actors in the Movie Here
           <h2 class="book_btn">BOOK TICKET NOW</h2>
       </div>
       
       <div class="col-sm-4 col-xs-12">
           <img src="uploads/14903659651.jpg" width="100%" class="img-responsive" alt="Title of the Movie">
           <div class="numberCircle">UA</div>, <strong>Hindi</strong>
           <h2><strong>Name of the Movie</strong></h2>
           *ing : Name of the Actors in the Movie Here
           <h2 class="book_btn">BOOK TICKET NOW</h2>
       </div>
     </div>
     
     <div class="clearfix"></div>
<br>
     
     <footer class="footer">
         <div class="container">
             <h1 class="text-center footer_title"><strong>Movies.niltik.com</strong></h1>
             <div class="col-sm-6">
             Copy Rights &copy; Reserved, <a href="http://www.niltik.com" target="_blank">Niltik.com</a>, 2017                 
             </div>
             
             <div class="col-sm-6 text-right">
                 <a href="#"><img src="img/fb_icon.jpg" width="32px" alt="Facebook Like"></a>
                 <a href="#"><img src="img/twitter_icon.jpg" width="32px" alt="Twitter Like"></a>
                 <a href="#"><img src="img/youtube_icon.jpg" width="32px" alt="YouTube Like"></a>
                 <a href="#"><img src="img/mail_icon.jpg" width="32px" alt="Mail to a Friend"></a>
             </div>
             <div class="clearfix"></div>
             <br><br>
         </div>
     </footer>

     
     <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="js/bootstrap.min.js"></script>  
     <!--Le us include a new script here-->     
     <script src="js/script.js"></script>
     <script type="text/javascript">
    
    </script>
</body>
    
</html>


<?php



?>