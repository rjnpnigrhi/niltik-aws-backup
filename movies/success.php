<?PHP
    session_start();

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    error_reporting(0);

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    include("mods/carousel.php");

    //include("mods/banner_728.php");
?>    
    <div class="container top-40 container-main">
    <?php
    $status=$_POST["status"];
    $firstname=$_POST["firstname"];
    $amount=$_POST["amount"];
    $txnid=$_POST["txnid"];
    $posted_hash=$_POST["hash"];
    $key=$_POST["key"];
    $productinfo=$_POST["productinfo"];
    $email=$_POST["email"];
    $salt="yLokMjwAW7";
	
	if($status ==''||$firstname==''||$amount==''||$txnid==''){
		echo "There is some error in this page.<br>";
		echo "You can not refresh or press Back button on this page.<br>";
		echo '<a href="index.php" class="btn btn-sm btn-success">'."Go Back to Home Page".'</a>';
	}else{

    If (isset($_POST["additionalCharges"])) {
        $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

    }else{	  

        $retHashSeq = $salt.'|'.$status.'|||||||||||'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

     }
         $hash = hash("sha512", $retHashSeq);

    if ($hash != $posted_hash) {
        echo "Invalid Transaction. Please try again";
    }else{
        
        //we will update the order_book for success
        $q = "UPDATE order_book SET status = 'success' WHERE txnid = '$txnid'";
        $r = $db->update($q);
        
        //let us get information from order_book
        $q1 = "SELECT * FROM order_book WHERE txnid = '$txnid' LIMIT 1";
        $r1 = $db->select($q1);
        
        $info = $r1->fetch_array();
        $schedule_id = $info['schedule_id'];
        $seat_names = $info['seats'];
        $seatcount = $info['seatcount'];

        //we wll change the status of the seats to booked
            $seats = explode(',', $seat_names);

	    foreach($seats as $seat){
	    	$status = "booked";
		    $q2 = "UPDATE seats SET status = '$status' WHERE schedule_id = '$schedule_id' AND seat = '$seat'";
	            $r2 = $db->update($q2);                
	    }
        
        $optradio = $info['optradio'];
        $date_booking = $info['date'];
        $phone = $info['phone'];
        //let us get these things on screen
        //echo $optradio."<br>";
        //echo $date_booking."<br>";
        //echo $phone."<br>";

        //let us get schedule details
        $show = get_value('movie_schedule', $schedule_id);
        $movie_id = $show['movie_id'];
        $hall_id = $show['hall_id'];
        $show_id = $show['show_id'];
        $show_time = $show['show_time'];
        $date = $show['date'];
        $ticket_price = $show['ticket'];
        
        //let us get the movie details
        $movie = get_value('movies', $movie_id);
        $movie_name = $movie['name'];
        
        //let us get hall details
        $hall = get_value('halls', $hall_id);
        $theater = $hall['name'];
        $address = $hall['address'];

        //we will send an sms to the user
        //API Details
        $msg = "Dear Customer, Booking ID $txnid for $seatcount Balcony seats ($seat_names) booked for $movie_name on $date at $show_time at $theater, Thank you. NILTIK";
        
        $encoded_msg= urlencode($msg);
        //Create API URL
        $fullapiurl="http://api.msg91.com/api/sendhttp.php?sender=NILTIK&route=4&mobiles=$phone&authkey=215426ARSXDZ9i5af98d91&country=91&message=$msg&campaign=MovieTicket";

        //Call API
        $ch = curl_init($fullapiurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        //echo $result ; // For Report or Code Check
        curl_close($ch);

        //we will send an e-Mail to the user
        $to = $email;
        $subject = "Online Movie Booking Confirmation Mail : Niltik.com";
        $headers = "From: helpdesk@niltik.com\r\n";
        $headers .= "Reply-To: helpdesk@niltik.com\r\n";
        $headers .= "CC: sonubisoyi56@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message = '<html><body>';
        $message .= '<img src="http://www.niltik.com/movies/img/mnw_print.jpg" alt="Movies.niltik.com" />';
        $message .= '<h1>Online Ticket Booking on Niltik Movies</h1>';
        $message .= '<table rules="all" style="border-color: #ff6600;" cellpadding="10">';
        $message .= "<tr style='background: #eee;'><td><strong>eMail ID : </strong> </td><td>" . strip_tags($email) . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>Phone : </strong> </td><td>" . strip_tags($phone) . "</td></tr>";
        $message .= "<tr><td><strong>Movie:</strong> </td><td>" . strip_tags($movie_name) . "</td></tr>";
        $message .= "<tr><td><strong>Show Time:</strong> </td><td>" . strip_tags($show_time) . "</td></tr>";
        $message .= "<tr style='background: #eee;'><td><strong>Tickets : </strong> </td><td>" . strip_tags($seat_names) . "</td></tr>";
        $message .= "<tr><td><strong>Date:</strong> </td><td>" . strip_tags($date) . "</td></tr>";
        $message .= "<tr><td><strong>Theater:</strong> </td><td>" . strip_tags($theater) . "</td></tr>";
        $message .= "<tr><td><strong>Address:</strong> </td><td>" . strip_tags($address) . "</td></tr>";
        $message .= "<tr><td><strong>Total Amount Paid:</strong> </td><td> Rs. " . strip_tags($amount) . ".00</td></tr>";
        $message .= "</table>";
        $message .= "</body></html>";
        
        

        $message  = '<div id="to-print">';
      	$message .= '<table class="table">';
        $message .= '<tr style="background-color:#333; color:white;">';
        $message .= '<td><img src="http://niltik.com/movies/img/mnw_print.jpg" width="240px" alt=""></td>';
        $message .= '<td align="right"><br>Carry this Ticket with you! <br></td></tr><tr>';
        $message .= '<td colspan="2">';
        $message .= 'Dear Customer,<br>';
        $message .= 'Your ticket(s) are Confirmed!';
        $message .= '<br></td></tr>';
        $message .= '<tr style="background-color:#f7f7f7;">';
        $message .= '<td>';
        
        
        $message .= '<h2>'. strip_tags($movie_name) .'</h2>';
        $message .= '<h4>'. strip_tags($theater) .'</h4>';
        $message .= '<p>'. strip_tags($address) .'</p>';
        $message .= '</td>';
        $message .= '<td align="right">';
        $message .= '<h2>Balcony</h2>';
        $message .= '<h4>Seats: '. strip_tags($seat_names) .'</h4>';
        $message .= '<p>('. strip_tags($date) . ') ' . strip_tags($show_time) .'</p>';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td colspan="2">';
        $message .= '<br><br>';
        $message .= 'ORDER SUMMARY';
        $message .= '</td></tr><tr>';
        $message .= '<td><strong class="text-danger">Total Amount Paid :</strong></td>';
        $message .= '<td align="right">Rs . '. strip_tags($amount) .'.00</td>';
        
        
        
        $message .= '</tr>';
        $message .= '<tr></tr>';
        $message .= '<tr style="border-bottom:2px dotted #333;">';
        $message .= '<td colspan="2">';
        $message .= '<br><br>';
        $message .= '<strong>Important Instructions</strong>';
        $message .= '<br><br>';
        $message .= '<p>';                
        $message .= 'Tickets once booked cannot be exchanged, cancelled or refunded. <br>';
        $message .= 'In case of 3D Movie, a refundable amount of Rs. 30.00 must be paid for each 3D glass.<br>';
        $message .= '</p>';    
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '<tr>';
        $message .= '<td colspan="2" style="background-color:#333333; padding:20px; color:white; text-align:center;">';
        $message .= 'For Assistance or Phone Booking please contact <i class="fa fa-phone" aria-hidden="true"></i> : 7008584789 | mail us at <i class="fa fa-envelope-o" aria-hidden="true"></i> : niltikbam@gmail.com';
        $message .= '</td>';
        $message .= '</tr>';
        $message .= '</table>';
	$message .= '</div>';
        
        
        
        
        
        // Mailgun e-send API
        define('MAILGUN_URL', 'https://api.mailgun.net/v3/mg.niltik.com');
	define('MAILGUN_KEY', 'key-159b6adbe0a83226f1b2fce6b7aa6606');
	
	function sendmailbymailgun($to,$toname,$mailfromname,$mailfrom,$subject,$html,$text,$tag,$replyto){
	    $array_data = array(
			'from'=> $mailfromname .'<'.$mailfrom.'>',
			'to'=> $toname.'<'.$to.'>,Niltik Admin<sonubisoyi56@gmail.com>',
			'subject'=>$subject,
			'html'=>$html,
			'text'=>$text,
			'o:tracking'=>'yes',
			'o:tracking-clicks'=>'yes',
			'o:tracking-opens'=>'yes',
			'o:tag'=>$tag,
			'h:Reply-To'=>$replyto
	    );
	    
	    $session = curl_init(MAILGUN_URL.'/messages');
	    curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($session, CURLOPT_USERPWD, 'api:'.MAILGUN_KEY);
	    curl_setopt($session, CURLOPT_POST, true);
	    curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
	    curl_setopt($session, CURLOPT_HEADER, false);
	    curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
	    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
	    
	    $response = curl_exec($session);
	    curl_close($session);
	    $results = json_decode($response, true);
	    //print_r($results);
	    return $results;
	}
        
        
        //if (mail($to, $subject, $message, $headers)) {
        if (sendmailbymailgun($to,'','Niltik.com','sales@niltik.com',$subject,$message,'','niltik','sales@niltik.com')) {
            echo "Congratulations. Booking Successful.<br>";
            echo "We have sent confirmation by SMS and e-Mail.<br>";
        } else {
            echo "Congratulations. Booking Successful.<br>";
            echo "There was a problem sending the email.<br>";
            echo "Save your Booking id : " . $txnid."<br>";
            echo "Give a call to : 7008584789. Thank You.";
        }
        
        
?>
<br><br>
   <div id="to-print">
      <button type="submit" class="btn btn-info pull-right" onclick="printDiv('to-print')">
         <i class="fa fa-print" aria-hidden="true"></i> Print
     </button>
      <div class="clearfix"></div>
      <br>
      <table class="table">
          <tr style="background-color:#333; color:white;">
              <td><img src="img/mnw_print.jpg" width="240px" alt=""></td>
              <td align="right"><br>Carry this Ticket with you! <br></td>
          </tr>
          <tr>
              <td colspan="2">
                Dear Customer,<br>
                Your ticket(s) are Confirmed!
                <br>
              </td>
          </tr>
          <tr style="background-color:#f7f7f7;">
              <td>
                <h2><?PHP echo $movie_name; ?></h2>
                <h4><?PHP echo $theater; ?></h4>
                <p><?PHP echo $address; ?></p>
              </td>
              <td align="right">
                <h2>Balcony</h2>
                <h4><?PHP echo $seat_names; ?></h4>
                <p><?PHP echo $show_time; ?> | 
                <?PHP
                    list($month, $day, $year) = explode('/', $date);
                    $m = strftime("%B",mktime(0,0,0,$month));
                    echo $m ." ". $day . ", ".  $year;
                ?>
                </p>    
              </td>
          </tr>
          <tr>
              <td colspan="2">
                 <br><br>
                  ORDER SUMMARY
              </td>
          </tr>
          <tr>
               <td>
                   <strong>Ticket Amount : (<?PHP echo $seatcount; ?> Tickets)</strong>                       
               </td>
               <td align="right">
                <?PHP
                    $total = $ticket_price*$seatcount;
                    echo "Rs. ". $total .".00";
                ?>
               </td>
           </tr>
           <!-- <tr>
               <td>Internet Charges : (Rs. <?PHP echo $show['charges'] ?> / Ticket)</td>
               <td align="right">
               <?PHP
                    $charges = $show['charges'];
                    $extras = $charges*$seatcount;
                    echo "Rs. ". $extras .".00";
		    $final = $total+$extras;
                ?>    
               </td>
           </tr> -->
           <tr>
               <td><strong class="text-danger">Amount Paid :</strong></td>
               <td align="right">Rs. <?PHP echo $final; ?>.00</td>
           </tr>
           <tr></tr>
           <tr style="border-bottom:2px dotted #333;">
               <td colspan="2">
               <br><br>
               <strong>Important Instructions</strong>
                <br><br>
                <p>                
                    Tickets once booked cannot be exchanged, cancelled or refunded. <br>
                    In case of 3D Movie, a refundable amount of Rs.40.00 must be paid for each 3D glass.<br>
                    Service Tax, Swachh Bharat Cess &amp; Krishi Kalyan Cess collected and paid to the department. <br>
                    For your proof of Identity at the hall counter, carry your Credit / Debit Card with you.
                </p>    
               </td>
           </tr>
           <tr>
               <td colspan="2">
               <p>Cut Below this Line and Preserve for the Offer</p>
               <img src="uploads/1505406278.jpg" width="100%" alt="Offer for You" />                 
               </td>
           </tr>
           <tr>
               <td colspan="2" style="background-color:#333333; padding:20px; color:white; text-align:center;">
                   For Assistance or Phone Booking please contact <i class="fa fa-phone" aria-hidden="true"></i> : 7008584789 | mail us at <i class="fa fa-envelope-o" aria-hidden="true"></i> : helpdesk@niltik.com
               </td>
           </tr>
      </table>
          
       </div>
       <script type="text/javascript">
        function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();
        document.body.innerHTML = originalContents;
        }
    </script>
<?PHP
    }
	
	
    }
	         
?>
    </div>
<?PHP

    include("mods/footer.php");
?>