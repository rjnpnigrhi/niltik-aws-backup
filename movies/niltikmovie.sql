-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2017 at 03:54 PM
-- Server version: 5.1.36
-- PHP Version: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `niltikmovie`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_login` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `mobile`, `password`, `last_login`) VALUES
(1, 'Krushna Prasad Dash', 'kpdash345@gmail.com', '9040985463', '15ada512f7481146cba09b2c3e8abf6c', '03/29/2017/20/09/PM'),
(2, 'Dr Jyoti Prasad Pattnaik', 'aahwaan@gmail.com', '9861962160', '6146ae1537d5f350cc1a4f1580c330a6', '');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_schedule_id` int(11) NOT NULL,
  `show_id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `seats` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `movie_schedule_id`, `show_id`, `hall_id`, `movie_id`, `date`, `seats`, `name`, `email`, `mobile`, `price`, `payment_mode`) VALUES
(1, 4, 1, 3, 1, '03/26/2017', 'F1, F2, F3', 'Dr Jyoti Prasad Pattnaik', 'aahwaan@gmail.com', '9040985463', '360', ''),
(2, 4, 1, 3, 1, '03/26/2017', 'F4, F5, F6', 'Nidan Pathology', 'nidan@gmail.com', '8080808080', '360', '');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE IF NOT EXISTS `certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `name`, `details`, `date`) VALUES
(1, 'U', 'Unrestricted Public Exhibition', '03/24/2017'),
(2, 'U/A', 'Parental Guidance for children below the age of 12 years', '03/24/2017'),
(3, 'A', 'Restricted to adults\r\n\r\nFilms with the A certification are available for public exhibition, but with restriction to adults. These films can contain heavily strong violence, strong sex (but full frontal and rear nudity is not allowed usually), strong abusive language (but words which insults or degrades women are not allowed), and even some controversial and adult themes considered unsuitable for young viewers. Such films are often recertified for TV and video viewing, which doesn''t happen in case of U and U/A certified movies.', '03/23/2017'),
(4, 'S', 'Restricted to any special class of persons\r\n\r\nFilms with S certification should not be viewed by the public. Only people associated with it (Engineers, Doctors, Scientists, etc.), have permission to watch those films.\r\n\r\nAdditionally, V/U, V/UA, V/A are used for video releases with U, U/A and A carrying the same meaning as above.', '03/23/2017');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `date`) VALUES
(1, 'Ichhapuram', '03/23/2017'),
(2, 'Brahmapur', '03/24/2017');

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE IF NOT EXISTS `halls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `location` text NOT NULL,
  `seatmap` text NOT NULL,
  `seats` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `halls`
--

INSERT INTO `halls` (`id`, `city_id`, `name`, `image`, `address`, `location`, `seatmap`, `seats`, `status`, `date`) VALUES
(1, 2, 'Rukmini Theaters', '1490298108.jpg', 'Dharma Nagar Square\r\nNear ParamJyoti Hall\r\nBerhampur, 760001\r\nOdisha', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.32751309936!2d84.78300761410463!3d19.311589449545032!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a3d500f6aa88c77%3A0xf04df7b22e6c7fc8!2sRukmini+Talkies!5e0!3m2!1sen!2sin!4v1490294040806" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 'Let me add something here.', 'G1, G2, G3, G4, G5, G6, G7, G8, G9, G10', 'active', '03/24/2017'),
(3, 2, 'Goutam Talkies', '1490351185.jpg', 'Gandhi Nagar\r\nBerhampur\r\n760001\r\nOdisha', 'location', 'add later', 'F1, F2, F3, F4, F5, F6, F7, F8, F9, F10', '', '03/24/2017');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `date`) VALUES
(1, 'English', 'En'),
(2, 'Hindi', 'Hn'),
(3, 'Odia / Oriya', 'Od'),
(4, 'Telugu', 'Tl'),
(5, 'Dubbed', 'Db'),
(6, 'Tamil', '03/24/2017'),
(7, 'Gujrati', '03/24/2017');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE IF NOT EXISTS `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `certificate` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `dd` varchar(255) NOT NULL,
  `cast` text NOT NULL,
  `director` varchar(255) NOT NULL,
  `music` varchar(255) NOT NULL,
  `released` varchar(255) NOT NULL,
  `trailer` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `genre` text NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `name`, `language`, `certificate`, `duration`, `dd`, `cast`, `director`, `music`, `released`, `trailer`, `image`, `genre`, `date`, `status`) VALUES
(1, 'Phillouri', 'Hindi', 'UA', '', '', 'Anoushka Sharma\r\nDiljit Dosanjh', 'Unknown', 'Unknown', '03/24/2017', '<iframe width="560" height="315" src="https://www.youtube.com/embed/uCTr7MGFK0U" frameborder="0" allowfullscreen></iframe>', '1490337504.jpg', 'horror, comedy, romance', '', 'now playing'),
(3, 'Beauty and the Beast', 'English', 'U', '125 Minutes', '2D', 'Emma Watson & Dan Stevens', 'Unknown', 'Unknown', '03/24/2017', '<iframe width="560" height="315" src="https://www.youtube.com/embed/OvW_L8sTu5E" frameborder="0" allowfullscreen></iframe>', '14903659651.jpg', 'Comedy, Action', '03/24/2017', 'coming soon');

-- --------------------------------------------------------

--
-- Table structure for table `movie_schedule`
--

CREATE TABLE IF NOT EXISTS `movie_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hall_id` int(11) NOT NULL,
  `show_id` int(11) NOT NULL,
  `show_time` varchar(255) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `movie_schedule`
--

INSERT INTO `movie_schedule` (`id`, `hall_id`, `show_id`, `show_time`, `movie_id`, `date`, `status`) VALUES
(4, 3, 1, '11:00 AM', 1, '03/26/2017', 'on'),
(5, 3, 1, '11:00 AM', 1, '04/02/2017', 'on'),
(6, 3, 1, '11:00 AM', 1, '04/14/2017', ''),
(7, 3, 1, '11:00 AM', 1, '03/30/2017', ''),
(8, 3, 2, '12:00 AM', 3, '03/30/2017', ''),
(9, 3, 1, '11', 1, '03/29/2017', ''),
(10, 3, 1, '1', 1, '11/12/2016', ''),
(11, 3, 3, '11:00 AM', 3, '03/30/2017', ''),
(12, 3, 4, '11:00 AM', 3, '03/30/2017', ''),
(13, 3, 5, '11:00 AM', 3, '03/30/2017', ''),
(14, 1, 1, '11:00 AM', 3, '03/30/2017', ''),
(15, 1, 2, '05:00 PM', 3, '03/30/2017', ''),
(16, 1, 3, '12:00 AM', 3, '03/30/2017', ''),
(17, 1, 4, '05:00 PM', 3, '03/30/2017', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `contents` text NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `contents`, `date`) VALUES
(1, 'Terms and Conditions', '<p>Disney&rsquo;s &ldquo;Beauty and the Beast&rdquo; is a live-action re-telling of the studio&rsquo;s animated classic which refashions the classic characters from the tale as old as time for a contemporary audience, staying true to the original music while updating the score with several new songs. &ldquo;Beauty and the Beast&rdquo; is the fantastic journey of Belle, a bright, beautiful and independent young woman who is taken prisoner by a beast in his castle. Despite her fears, she befriends the castle&rsquo;s enchanted staff and learns to look beyond the Beast&rsquo;s hideous exterior and realize the kind heart and soul of the true Prince within. The film stars: Emma Watson as Belle; Dan Stevens as the Beast; Luke Evans as Gaston, the handsome, but shallow villager who woos Belle; Oscar&reg; winner Kevin Kline as Maurice, Belle&rsquo;s eccentric, but lovable father; Josh Gad as Lefou, Gaston&rsquo;s long-suffering aide-de-camp; Golden Globe&reg; nominee Ewan McGregor as Lumiere, the candelabra; Oscar nominee Stanley Tucci as Maestro Cadenza, the harpsichord; Oscar nominee Ian McKellen as Cogsworth, the mantel clock; and two-time Academy Award&reg; winner Emma Thompson as the teapot, Mrs. Potts.<br /><br />Directed by Oscar&reg; winner Bill Condon based on the 1991 animated film, &ldquo;Beauty and the Beast&rdquo; is produced by Mandeville Films&rsquo; David Hoberman and Todd Lieberman with eight-time Oscar-winning composer Alan Menken, who won two Academy Awards&reg; (Best Original Score and Best Song) for the 1991 animated film, providing the score, which will include new recordings of the original songs written by Menken and Howard Ashman, as well as several new songs written by Menken and three-time Oscar winner Tim Rice. &ldquo;Beauty and the Beast&rdquo; will be released in U.S. theaters on March 17, 2017.</p>\r\n<p>This is an extra paragraph created for editing only.</p>', '03/24/2017');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE IF NOT EXISTS `seats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seat` varchar(255) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `show_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=151 ;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`id`, `seat`, `hall_id`, `show_id`, `movie_id`, `date`, `status`) VALUES
(40, ' F10', 3, 1, 1, '03/26/2017', 'available'),
(39, ' F9', 3, 1, 1, '03/26/2017', 'available'),
(38, ' F8', 3, 1, 1, '03/26/2017', 'available'),
(37, ' F7', 3, 1, 1, '03/26/2017', 'available'),
(36, ' F6', 3, 1, 1, '03/26/2017', 'available'),
(35, ' F5', 3, 1, 1, '03/26/2017', 'available'),
(34, ' F4', 3, 1, 1, '03/26/2017', 'available'),
(33, ' F3', 3, 1, 1, '03/26/2017', 'available'),
(32, ' F2', 3, 1, 1, '03/26/2017', 'available'),
(31, 'F1', 3, 1, 1, '03/26/2017', 'available'),
(41, 'F1', 3, 1, 1, '04/02/2017', 'available'),
(42, ' F2', 3, 1, 1, '04/02/2017', 'available'),
(43, ' F3', 3, 1, 1, '04/02/2017', 'available'),
(44, ' F4', 3, 1, 1, '04/02/2017', 'available'),
(45, ' F5', 3, 1, 1, '04/02/2017', 'available'),
(46, ' F6', 3, 1, 1, '04/02/2017', 'available'),
(47, ' F7', 3, 1, 1, '04/02/2017', 'available'),
(48, ' F8', 3, 1, 1, '04/02/2017', 'available'),
(49, ' F9', 3, 1, 1, '04/02/2017', 'available'),
(50, ' F10', 3, 1, 1, '04/02/2017', 'available'),
(51, 'F1', 3, 1, 1, '04/14/2017', 'available'),
(52, ' F2', 3, 1, 1, '04/14/2017', 'available'),
(53, ' F3', 3, 1, 1, '04/14/2017', 'available'),
(54, ' F4', 3, 1, 1, '04/14/2017', 'available'),
(55, ' F5', 3, 1, 1, '04/14/2017', 'available'),
(56, ' F6', 3, 1, 1, '04/14/2017', 'available'),
(57, ' F7', 3, 1, 1, '04/14/2017', 'available'),
(58, ' F8', 3, 1, 1, '04/14/2017', 'available'),
(59, ' F9', 3, 1, 1, '04/14/2017', 'available'),
(60, ' F10', 3, 1, 1, '04/14/2017', 'available'),
(61, 'F1', 3, 1, 1, '03/30/2017', 'available'),
(62, ' F2', 3, 1, 1, '03/30/2017', 'available'),
(63, ' F3', 3, 1, 1, '03/30/2017', 'available'),
(64, ' F4', 3, 1, 1, '03/30/2017', 'available'),
(65, ' F5', 3, 1, 1, '03/30/2017', 'available'),
(66, ' F6', 3, 1, 1, '03/30/2017', 'available'),
(67, ' F7', 3, 1, 1, '03/30/2017', 'available'),
(68, ' F8', 3, 1, 1, '03/30/2017', 'available'),
(69, ' F9', 3, 1, 1, '03/30/2017', 'available'),
(70, ' F10', 3, 1, 1, '03/30/2017', 'available'),
(71, 'F1', 3, 2, 3, '03/30/2017', 'available'),
(72, ' F2', 3, 2, 3, '03/30/2017', 'available'),
(73, ' F3', 3, 2, 3, '03/30/2017', 'available'),
(74, ' F4', 3, 2, 3, '03/30/2017', 'available'),
(75, ' F5', 3, 2, 3, '03/30/2017', 'available'),
(76, ' F6', 3, 2, 3, '03/30/2017', 'available'),
(77, ' F7', 3, 2, 3, '03/30/2017', 'available'),
(78, ' F8', 3, 2, 3, '03/30/2017', 'available'),
(79, ' F9', 3, 2, 3, '03/30/2017', 'available'),
(80, ' F10', 3, 2, 3, '03/30/2017', 'available'),
(81, 'F1', 3, 3, 3, '03/30/2017', 'available'),
(82, ' F2', 3, 3, 3, '03/30/2017', 'available'),
(83, ' F3', 3, 3, 3, '03/30/2017', 'available'),
(84, ' F4', 3, 3, 3, '03/30/2017', 'available'),
(85, ' F5', 3, 3, 3, '03/30/2017', 'available'),
(86, ' F6', 3, 3, 3, '03/30/2017', 'available'),
(87, ' F7', 3, 3, 3, '03/30/2017', 'available'),
(88, ' F8', 3, 3, 3, '03/30/2017', 'available'),
(89, ' F9', 3, 3, 3, '03/30/2017', 'available'),
(90, ' F10', 3, 3, 3, '03/30/2017', 'available'),
(91, 'F1', 3, 4, 3, '03/30/2017', 'available'),
(92, ' F2', 3, 4, 3, '03/30/2017', 'available'),
(93, ' F3', 3, 4, 3, '03/30/2017', 'available'),
(94, ' F4', 3, 4, 3, '03/30/2017', 'available'),
(95, ' F5', 3, 4, 3, '03/30/2017', 'available'),
(96, ' F6', 3, 4, 3, '03/30/2017', 'available'),
(97, ' F7', 3, 4, 3, '03/30/2017', 'available'),
(98, ' F8', 3, 4, 3, '03/30/2017', 'available'),
(99, ' F9', 3, 4, 3, '03/30/2017', 'available'),
(100, ' F10', 3, 4, 3, '03/30/2017', 'available'),
(101, 'F1', 3, 5, 3, '03/30/2017', 'available'),
(102, ' F2', 3, 5, 3, '03/30/2017', 'available'),
(103, ' F3', 3, 5, 3, '03/30/2017', 'available'),
(104, ' F4', 3, 5, 3, '03/30/2017', 'available'),
(105, ' F5', 3, 5, 3, '03/30/2017', 'available'),
(106, ' F6', 3, 5, 3, '03/30/2017', 'available'),
(107, ' F7', 3, 5, 3, '03/30/2017', 'available'),
(108, ' F8', 3, 5, 3, '03/30/2017', 'available'),
(109, ' F9', 3, 5, 3, '03/30/2017', 'available'),
(110, ' F10', 3, 5, 3, '03/30/2017', 'available'),
(111, 'G1', 1, 1, 3, '03/30/2017', 'available'),
(112, ' G2', 1, 1, 3, '03/30/2017', 'available'),
(113, ' G3', 1, 1, 3, '03/30/2017', 'available'),
(114, ' G4', 1, 1, 3, '03/30/2017', 'available'),
(115, ' G5', 1, 1, 3, '03/30/2017', 'available'),
(116, ' G6', 1, 1, 3, '03/30/2017', 'available'),
(117, ' G7', 1, 1, 3, '03/30/2017', 'available'),
(118, ' G8', 1, 1, 3, '03/30/2017', 'available'),
(119, ' G9', 1, 1, 3, '03/30/2017', 'available'),
(120, ' G10', 1, 1, 3, '03/30/2017', 'available'),
(121, 'G1', 1, 2, 3, '03/30/2017', 'available'),
(122, ' G2', 1, 2, 3, '03/30/2017', 'available'),
(123, ' G3', 1, 2, 3, '03/30/2017', 'available'),
(124, ' G4', 1, 2, 3, '03/30/2017', 'available'),
(125, ' G5', 1, 2, 3, '03/30/2017', 'available'),
(126, ' G6', 1, 2, 3, '03/30/2017', 'available'),
(127, ' G7', 1, 2, 3, '03/30/2017', 'available'),
(128, ' G8', 1, 2, 3, '03/30/2017', 'available'),
(129, ' G9', 1, 2, 3, '03/30/2017', 'available'),
(130, ' G10', 1, 2, 3, '03/30/2017', 'available'),
(131, 'G1', 1, 3, 3, '03/30/2017', 'available'),
(132, ' G2', 1, 3, 3, '03/30/2017', 'available'),
(133, ' G3', 1, 3, 3, '03/30/2017', 'available'),
(134, ' G4', 1, 3, 3, '03/30/2017', 'available'),
(135, ' G5', 1, 3, 3, '03/30/2017', 'available'),
(136, ' G6', 1, 3, 3, '03/30/2017', 'available'),
(137, ' G7', 1, 3, 3, '03/30/2017', 'available'),
(138, ' G8', 1, 3, 3, '03/30/2017', 'available'),
(139, ' G9', 1, 3, 3, '03/30/2017', 'available'),
(140, ' G10', 1, 3, 3, '03/30/2017', 'available'),
(141, 'G1', 1, 4, 3, '03/30/2017', 'available'),
(142, ' G2', 1, 4, 3, '03/30/2017', 'available'),
(143, ' G3', 1, 4, 3, '03/30/2017', 'available'),
(144, ' G4', 1, 4, 3, '03/30/2017', 'available'),
(145, ' G5', 1, 4, 3, '03/30/2017', 'available'),
(146, ' G6', 1, 4, 3, '03/30/2017', 'available'),
(147, ' G7', 1, 4, 3, '03/30/2017', 'available'),
(148, ' G8', 1, 4, 3, '03/30/2017', 'available'),
(149, ' G9', 1, 4, 3, '03/30/2017', 'available'),
(150, ' G10', 1, 4, 3, '03/30/2017', 'available');
