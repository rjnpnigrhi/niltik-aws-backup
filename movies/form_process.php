<?PHP
    session_start();

    if(isset($_GET['city_name'])){
        $_SESSION['city_name'] = $_GET['city_name'];
    }

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
    include('admin/mods/city-selector.php');
	
    //error_reporting(0);
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();

    include("mods/header.php");

    //include("mods/search.php");

    //include("mods/carousel.php");

    //include("mods/banner_728.php");

    
    // Change the Merchant key here as provided by Payumoney
    $MERCHANT_KEY = "I6wccB5q";

    // Change the Merchant Salt as provided by Payumoney
    $SALT = "yLokMjwAW7";

	$scheduleid =$db->safe_data($_POST['scheduleid']);
	$firstname =$db->safe_data($_POST['firstname']);
	$seats =$db->safe_data($_POST['seats']);
	$seatcount =$db->safe_data($_POST['seats_count']);
    	$date = date("m/d/Y");
	$email =$db->safe_data($_POST['email']);
	$phone =$db->safe_data($_POST['phone']);
	$productinfo =$db->safe_data($_POST['productinfo']);
	$service_provider =$db->safe_data($_POST['service_provider']);
	$amount =$db->safe_data($_POST['amount']);
	$txnid =$db->safe_data($_POST['txnid']);
	$surl =$db->safe_data($_POST['surl']);
	$furl =$db->safe_data($_POST['furl']);
	$optradio =$db->safe_data($_POST['optradio']);
	$hashseq=$MERCHANT_KEY.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|||||||||||'.$SALT;
	$hash =strtolower(hash("sha512", $hashseq)); 
	
	
    //let us add these values to a orderbook table
    $q = "INSERT INTO order_book (email, phone, txnid, seats, seatcount, amount, optradio, date, schedule_id, payuhash) VALUES ('$email', '$phone', '$txnid', '$seats', '$seatcount', '$amount', '$optradio', '$date', '$scheduleid', '$hash')";
    $r = $db->insert($q);   
	
	$seat_names = explode(',', $seats);
	
	$proceed_to_booking = 'true';

        foreach($seat_names as $seat){
	$status = "booked";
	$status2 = "ontransit";
            $q3 = "SELECT * FROM seats WHERE seat = '$seat' AND schedule_id = '$scheduleid' AND status = '$status'";
            $r3 = $db->select($q3);

            //echo $r3;

            if(!$r3){
                $q2 = "UPDATE seats SET status = 'booked' WHERE schedule_id = '$scheduleid' AND seat = '$seat'";
        	$r2 = $db->update($q2);
            }else{
                $proceed_to_booking = 'false';
            }
        }

?>
   <script>
       window.onload = function(){
          if($("#proceed").html() == 'true'){
              document.forms['myForm'].submit();
          }else{
          	alert("Some or all seats selected are not avaiable at this moment.");
          	$("#message").hide();
          	$("#button").show();
          }
          
          if( window.history && window.history.pushState ){
		  history.pushState( "nohb", null, "" );
		  $(window).on( "popstate", function(event){
		  
		    if(confirm("Are you sure you want to cancel the transaction?")){
		    	window.location.href = "http://movies.niltik.com";
		    }else{
		    	document.forms['myForm'].submit();
		    }
		  
		    if( !event.originalEvent.state ){
		      history.pushState( "nohb", null, "" );
		      return;
		    }
		  });
		}
        }
    </script>
    
    <div class="loading">
    	<p>Loading.. Please wait..</p>
    </div>
    
    <style>
    
    	/* Absolute Center Spinner */
	.loading {
	  position: fixed;
	  z-index: 999;
	  height: 2em;
	  width: 2em;
	  overflow: show;
	  margin: auto;
	  top: 0;
	  left: 0;
	  bottom: 0;
	  right: 0;
	}
	
	/* Transparent Overlay */
	.loading:before {
	  content: '';
	  display: block;
	  position: fixed;
	  top: 0;
	  left: 0;
	  width: 100%;
	  height: 100%;
	  background-color: rgba(0,0,0,0.3);
	}
	
	/* :not(:required) hides these rules from IE9 and below */
	.loading:not(:required) {
	  /* hide "loading..." text */
	  font: 0/0 a;
	  color: transparent;
	  text-shadow: none;
	  background-color: transparent;
	  border: 0;
	}
	
	.loading:not(:required):after {
	  content: '';
	  display: block;
	  font-size: 10px;
	  width: 1em;
	  height: 1em;
	  margin-top: -0.5em;
	  -webkit-animation: spinner 1500ms infinite linear;
	  -moz-animation: spinner 1500ms infinite linear;
	  -ms-animation: spinner 1500ms infinite linear;
	  -o-animation: spinner 1500ms infinite linear;
	  animation: spinner 1500ms infinite linear;
	  border-radius: 0.5em;
	  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
	  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
	}
	
	/* Animation */
	
	@-webkit-keyframes spinner {
	  0% {
	    -webkit-transform: rotate(0deg);
	    -moz-transform: rotate(0deg);
	    -ms-transform: rotate(0deg);
	    -o-transform: rotate(0deg);
	    transform: rotate(0deg);
	  }
	  100% {
	    -webkit-transform: rotate(360deg);
	    -moz-transform: rotate(360deg);
	    -ms-transform: rotate(360deg);
	    -o-transform: rotate(360deg);
	    transform: rotate(360deg);
	  }
	}
	@-moz-keyframes spinner {
	  0% {
	    -webkit-transform: rotate(0deg);
	    -moz-transform: rotate(0deg);
	    -ms-transform: rotate(0deg);
	    -o-transform: rotate(0deg);
	    transform: rotate(0deg);
	  }
	  100% {
	    -webkit-transform: rotate(360deg);
	    -moz-transform: rotate(360deg);
	    -ms-transform: rotate(360deg);
	    -o-transform: rotate(360deg);
	    transform: rotate(360deg);
	  }
	}
	@-o-keyframes spinner {
	  0% {
	    -webkit-transform: rotate(0deg);
	    -moz-transform: rotate(0deg);
	    -ms-transform: rotate(0deg);
	    -o-transform: rotate(0deg);
	    transform: rotate(0deg);
	  }
	  100% {
	    -webkit-transform: rotate(360deg);
	    -moz-transform: rotate(360deg);
	    -ms-transform: rotate(360deg);
	    -o-transform: rotate(360deg);
	    transform: rotate(360deg);
	  }
	}
	@keyframes spinner {
	  0% {
	    -webkit-transform: rotate(0deg);
	    -moz-transform: rotate(0deg);
	    -ms-transform: rotate(0deg);
	    -o-transform: rotate(0deg);
	    transform: rotate(0deg);
	  }
	  100% {
	    -webkit-transform: rotate(360deg);
	    -moz-transform: rotate(360deg);
	    -ms-transform: rotate(360deg);
	    -o-transform: rotate(360deg);
	    transform: rotate(360deg);
	  }
	}
	    
    </style>
    
    <div id="proceed" class="hide"><?php echo $proceed_to_booking; ?></div>
    
    <div class="container top-40 container-main">
      <div class="col-sm-6">
           <h1>Your Payment Processing</h1>
           <table class="table">
               <tr>
                   <td>Transaction ID : </td>
                   <td><?PHP echo $txnid; ?></td>
               </tr>
               <tr>
                   <td>e-Mail :</td>
                   <td><?PHP echo $email; ?></td>
               </tr>
               <tr>
                   <td>Phone :</td>
                   <td><?PHP echo $phone; ?></td>
               </tr>
               <tr>
                   <td>Tickets :</td>
                   <td><?PHP echo $seatcount; ?> - (<?PHP echo $seats; ?>)</td>
               </tr>
               <tr>
                   <td>Amount :</td>
                   <td>Rs. <?PHP echo $amount; ?>.00</td>
               </tr>
           </table>
           <p id="message">
               Please be Patient. This Process might take sometime. <br><br>
               Please do not hit Refresh or Browser Back Button or Close this Window.
           </p>
           
           <p id="button" class="text-danger hide">
           Someone may be booking the same seat(s), Please wait for 15 minutes <br>
           and try again or Select other available seat(s).<br><br>
               <a href="index.php" class="btn btn-default" id="goback">Go Back</a>
           </p>
           
        </div>
        <div class="col-sm-6">
            <form action="https://secure.payu.in/_payment" id="myForm" method="post" class="form">
                <input type="hidden" name="key" value="<?PHP echo $MERCHANT_KEY; ?>">
                <input type="hidden" name="hash" value="<?PHP echo $hash; ?>">
                <input type="hidden" name="txnid" value="<?PHP echo $txnid; ?>">
                <input type="hidden" name="amount" value="<?PHP echo $amount; ?>">
                <input type="hidden" name="firstname" value="<?PHP echo $firstname; ?>">
                <input type="hidden" name="email" value="<?PHP echo $email; ?>">
                <input type="hidden" name="phone" value="<?PHP echo $phone; ?>">
                <input type="hidden" name="productinfo" value="<?PHP echo $productinfo; ?>">
                <input type="hidden" name="service_provider" value="<?PHP echo $service_provider; ?>">
                <input type="hidden" name="surl" value="<?PHP echo $surl; ?>">
                <input type="hidden" name="furl" value="<?PHP echo $furl; ?>">
            </form>
        </div>
      </div>
<?PHP

    include("mods/footer.php");
?>