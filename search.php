<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row">
    <div class="col-md-6">
<?PHP
    if(isset($_GET['submit'])){
        $search_term = $_POST['term'];
        $product_id = $_POST['product_id'];
        $location_id = $_POST['location_id'];
         
        $q = "SELECT * FROM classifieds WHERE status = 'approved' ";
        
        if($location_id == ''){
            
        }else{
            $q .= "AND FIND_IN_SET('$location_id', location_id)";
        }
        
        if($product_id == ''){
            
        }else{
            $q .= "AND FIND_IN_SET('$product_id', product_id)";
        }
        
        $q .= "AND ad_title LIKE '%$search_term%' 
                OR ad_details LIKE '%$search_term%' 
                OR name LIKE '%$search_term%' 
                OR address LIKE '%$search_term%' 
                OR email LIKE '%$search_term%'";
        
        $q .= "ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, We could not find ads as per your search.<br>";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{            
            while($ads = $r->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 4);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers); 
            
            foreach($data as $ad){
?>
      <div class="row">
            <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><h3 style="padding-left:20px;"><?PHP echo $ad['ad_title']; ?></h3></a>
            <div class="col-sm-4">
                <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                ?>
            <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><img src="img/no-img.jpg" alt="<?PHP echo $ad['ad_title']; ?>" class="img-responsive"></a>
                <?PHP
                    }else{
                       $img = $r3->fetch_array();
                ?>
            <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" alt="<?PHP echo $ad['ad_title']; ?>" class="img-responsive"></a>
                <?PHP } ?>
            </div>
            <div class="col-sm-8">                               
            <h2>Rs. <?PHP echo $ad['price']; ?>.00</h2>
            <a href="view_product.php?id=<?PHP echo $ad['id']; ?>" class="btn btn-default btn-right">View Details</a>
            </div>
            </div>
            
            <div class="clearfix"></div>
        <hr>
                <?PHP 
                    }
                ?>   
                        
           <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="search.php?term='.$search_term.'&product_id='.$product_id.'&location_id='.$location_id.'&page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="search.php?term='.$search_term.'&product_id='.$product_id.'&location_id='.$location_id.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="search.php?term='.$search_term.'&product_id='.$product_id.'&location_id='.$location_id.'&page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
            }
    }
               ?>
              </ul>
            </nav>
</div>
<div class="col-md-6"><a href="post_free_ad.php"><img src="img/sqad_01.gif" class="img-responsive" /></a></div>
<div class="clearfix"></div>
<?PHP
        
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>