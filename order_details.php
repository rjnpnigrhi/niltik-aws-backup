<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
    $i = '';
    $sum = '';
    $total_items = '';
    $total_price = '';
    $order_id = $_GET['id'];

    if($order_id == ''){
        header("Location: index.php");
    }else{
        //we will get information from the tables here
        $q = "SELECT * FROM order_book WHERE id = '$order_id' LIMIT 1";
        $r = $db->select($q);
        
        if(!$r){
            echo "The Invoice You are searching is not in our Log Book.<br><br>";
            echo '<a href="index.php" class="btn btn-warning">'."Back to Niltik.com".'</a>';
        }else{
            
            $order = $r->fetch_array();
    
?>
    <div class="row">
     <div id="print-content">
      <div class="col-md-8">
          <h2>Online Laundry Service Invoice</h2>
      </div>
      <div class="col-md-4 text-right">
     
     <button type="submit" class="btn btn-info" onclick="printDiv('print-content')">
         <i class="fa fa-print" aria-hidden="true"></i> Print
     </button>
     </div>
      <div class="clearfix"></div>
      <hr>
      
       <div class="col-md-6">
           <h3><?PHP echo $order['name']; ?></h3>
           <p><i class="fa fa-envelope" aria-hidden="true"></i> : <?PHP echo $order['email']; ?></p>
           <p><i class="fa fa-phone-square" aria-hidden="true"></i> : <?PHP echo $order['phone']; ?></p>
           <p>Address : <br>
           <?PHP echo $order['address']; ?>
           </p>
       </div>
       <div class="col-md-6 text-right">
           <h3>Order No : #<?PHP echo $order['id']; ?></h3>
           <p>Posted on : 
           <?PHP 
            
            $timestamp = strtotime($order['posted_on']);
                   
            $p_day = date('d', $timestamp);
            $p_month = date('M', $timestamp);
            $p_year = date('Y', $timestamp); 
            
            echo "$p_day $p_month, $p_year";
               
            ?>           
           </p>
           <p>Amount due : <strong style="color:RED;"> Rs. <?PHP echo number_format((float)$order['final_bill'], 2, '.', ''); ?></strong></p>
       </div>
       
       <div class="clearfix"></div>
       <table class="table table-striped" style="border:1px solid #cccccc;">
           <tr>
               <th width="5%">No.</th>
               <th>Cloth</th>
               <th width="20%">Service</th>
               <th width="10%">Quantity</th>
               <th width="15%">Price</th>
               <th width="15%">Sub-total</th>
           </tr>
           <?PHP
                //let us get info from invoice book
                $q1 = "SELECT * FROM invoice_book WHERE order_id = '$order_id'";
                $r1 = $db->select($q1);
            
                if(!$r1){
                    echo "Our Website Server is busy. Please come back again. <br>";
                    echo '<a href="index.php" class="btn btn-warning">'."Back to Niltik.com".'</a>';
                }else{
                    $count = mysqli_num_rows($r1);                     
                    while($cloth = $r1->fetch_array()):
                    $i++;
                    
                    $service = $cloth['service'];
                    $qty = $cloth['qty'];
                    $price = $cloth['price'];
                    $sub_total = $qty*$price;
                    
           ?>
           <tr>
               <td><?PHP echo $i; ?></td>
               <td>
               <?PHP 
                    $cloth_id = $cloth['cloth_id']; 
                    
                    $qx = "SELECT * FROM cloth_iron WHERE id = '$cloth_id' LIMIT 1";
                    $rx = $db->select($qx);

                    $item = $rx->fetch_array();
                    
                    echo ucfirst($item['cloth']) . " ( " . ucfirst($item['category']) . " ) ";
               ?>
               
               </td>               
               <td><?PHP echo ucfirst($service); ?></td>
               <td><?PHP echo $qty; ?></td>
               <td>Rs. <?PHP echo number_format((float)$price, 2, '.', ''); ?></td>
               <td>Rs. <?PHP echo number_format((float)$sub_total, 2, '.', ''); ?></td>
           </tr>
           <?PHP 
                    $total_price += $sub_total;
                    $total_items += $qty;
                    endwhile; ?>
           <tr>
               <td colspan="3" align="right">Total Items :</td>
               <td><kbd><?PHP echo $total_items; ?></kbd></td>               
               <td>Sub-Total :</td>
               <td style="color:RED;">Rs. <?PHP echo number_format((float)$total_price, 2, '.', ''); ?></td>
           </tr>
            <?PHP
                        if($order['pickup']>0){
            ?>
            <tr>
               <td colspan="3"></td>
               <td colspan="2" align="right">Pick Up Charges :</td>
               <td>Rs. <?PHP echo $order['pickup']; ?>.00</td>
           </tr>
            <?PHP
                        }
                        }
                }
                
            
                if($order['coupon_code'] !==''){
            ?>
           
           
           <tr>
               <td colspan="3" align="center">Gift Coupon Applied : <span style="color:RED;"> <?PHP echo $order['coupon_code']; ?> </span></td>
               <td colspan="2" align="right">Discount : </td>
               <td style="color:BLUE;">Rs. <?PHP echo number_format((float)$order['discount'], 2, '.', ''); ?></td>
           </tr>
           <?PHP } ?>
           <tr>
               <td colspan="5" align="right">Final Bill Amount :</td>
               <td style="color:GREEN;">Rs. <?PHP echo number_format((float)$order['final_bill'], 2, '.', ''); ?></td>
           </tr>
           <tr>
               <td colspan="2">
                   <p><strong> Operations Manager</strong></p>
                   <h3><i class="fa fa-cart-plus" aria-hidden="true"></i> Niltik.com</h3>
                   <p><i class="fa fa-street-view" aria-hidden="true"></i> : Berhampur, Odisha <br></p>
                   <p><i class="fa fa-envelope-o" aria-hidden="true"></i> : support@niltik.com</p>
                   <p><i class="fa fa-phone-square" aria-hidden="true"></i> : 8908264076</p>
               </td>
               <td colspan="4">
               <h3>Order Status :</h3>
               <?PHP echo ucfirst($order['status']); ?>
               </td>
           </tr>
           
       </table>
       <p>If you find any errors in this Invoice Order, please give us a call for rectification.</p>
        </div>   
        <script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
        </script>
<?PHP
        }
    

    include("mods/trending_ads.php");

    include("mods/footer.php");
?>