<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
    if(!isset($_SESSION['cart'])){
                
        $_SESSION['cart'] = array();
        $_SESSION['total_items'] = 0;
        $_SESSION['total_price'] = '0.00';
                
    } 

	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
    $total_price = '';     

    //let us get all clothes list here according to their service i.e. Iron, Wash, Drywash
?>
   <div class="row">
        <?PHP 
            if(isset($_GET['add_to_cart'])){
                
                $id = $_POST['id'];
                $qty = $_POST['qty'];
                
                add_to_cart($id, $qty);
             
                $_SESSION['total_items'] = total_items($_SESSION['cart']);
                
                header("Location:laundry.php");
            }
       
            if($_SESSION['total_items']==0){
                
echo '<img src="img/open.gif" class="img-responsive" />';
echo "<br>";
echo '<div class="text-center">'."Introductory Offer : 20% Off on order of above Rs.160. Use Coupon Code : ".'<strong>'."HOLI20".'</strong></div>';
                     
            }elseif(isset($_GET['delete'])){
                $id = $_GET['delete'];
                
                unset($_SESSION['cart'][$id]);
                unset($_SESSION['coupon_code']);
                unset($_SESSION['amount']);
                unset($_SESSION['discount']);
                
                $_SESSION['total_items'] = total_items($_SESSION['cart']);
                
                if($_SESSION['total_items'] < 1){
                    unset($_SESSION['cart']);
                    unset($_SESSION['coupon_code']);
                    unset($_SESSION['amount']);
                    unset($_SESSION['discount']);
                    header("Location:laundry.php");
                }
                
                echo "Product Removed from Cart.<br>";
                echo '<a href="laundry.php">'."Check Your Basket".'</a>';
                
            }elseif(isset($_GET['unset'])){
                unset($_SESSION['cart']);
                unset($_SESSION['coupon_code']);
                unset($_SESSION['amount']);
                unset($_SESSION['discount']);
                
                header('Location:laundry.php');
            }elseif(isset($_GET['update'])){
                $id = $_POST['id'];
                $qty = $_POST['qty'];
                
                $_SESSION['cart'][$id] = $qty;
                
                $_SESSION['total_items'] = total_items($_SESSION['cart']);
                
                if($_SESSION['total_items'] < 1){
                    unset($_SESSION['cart']);
                    unset($_SESSION['coupon_code']);
                    unset($_SESSION['amount']);
                    unset($_SESSION['discount']);
                    header("Location:laundry.php");
                }
                
                echo "Your Basket Articles have been updated.<br> ";
                echo '<a href="laundry.php">'."Check Your Basket".'</a>';
            }else{
                $_SESSION['total_items'] = total_items($_SESSION['cart']);
                
                //$_SESSION['total_price'] = total_price($_SESSION['cart']);
                
        ?>
        <h2>My Cart :</h2>
        <p style="text-align:right;"><a href="laundry.php?unset=true" class="btn btn-danger btn-sm btn-right">Clear Contents of Cart</a></p>
        <table class="table table-striped" width="100%">
            <tr>
                <th width="10%">Service</th>
                <th width="72px">Icon</th>
                <th>Article</th>
                <th>Category</th>
                <th width="10%">Quantity</th>
                <th width="15%">Price/Item</th>
                <th width="12%">Sub-total</th>
                <th width="15%" align="center">Actions</th>
            </tr>
        <?PHP 
            foreach($_SESSION['cart'] as $id => $qty):
            
            $q = "SELECT * FROM cloth_iron WHERE id = '$id' LIMIT 1";
            $r = $db->select($q);
            
            $prod = $r->fetch_array();
            
            $price = $prod['price']*$qty;
            ?>
            <tr>
               <form action="laundry.php?update=true" method="post">
               <input type="hidden" name="id" value="<?PHP echo $prod['id']; ?>">
                <td><?PHP echo ucfirst($prod['service']); ?></td>
                <td><img src="iron/<?PHP echo $prod['cloth_icon']; ?>" alt="<?PHP echo ucfirst($prod['cloth']); ?>" class="img-responsive"></td>
                <td><?PHP echo ucfirst($prod['cloth']); ?></td>
                <td><?PHP echo ucfirst($prod['category']); ?></td>
                <td><input type="text" name="qty" value="<?PHP echo $qty; ?>" size="4"></td>
                <td>Rs. <?PHP echo $prod['price']; ?>.00</td>
                <td>Rs. <?PHP echo number_format($price); ?>.00</td>
                <td align="middle">
                
                <button type="submit" class="btn btn-xs btn-success" title="Update Items"><i class="fa fa-refresh fa-fw" aria-hidden="true"></i></button> 
                
                <a href="laundry.php?delete=<?PHP echo $id; ?>" class="btn btn-danger btn-xs" title="Remove Item"><i class="fa fa-trash fa-fw" aria-hidden="true"></i></a>               
                </td>
                </form>
            </tr>
        <?PHP 
            
            $total_price += $price;
            endforeach; 
        ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">Total Articles :</td>
            <td><?PHP echo $_SESSION['total_items']; ?></td>
            <td align="right">Total Price :</td>
            <td colspan="2">Rs. <?PHP echo $total_price; ?>.00</td>
        </tr>
        <?PHP
                $req = '';
                $pickup = '';
            if($total_price < 100){
                $req = 100 - $total_price;
                $pickup = 20;
               echo '<tr><td colspan="8" align="center"><p class="help_block">'."For <strong>FREE Pick up and Delivery</strong> and Use of <strong>Gift Coupons</strong> add more articles of ".'<span style="color:red; font-weight:bold;">'." Rs. ".$req.".00 ".'</span></td></tr>';
                echo '<tr><td colspan="2"></td><td colspan="4" align="right">'."Pick Up Charges :".'</td><td colspan="2">'."Rs. 20.00".'</td></tr>';
               
            }else{
                $pickup = 0;
            }
        ?>
        
        <tr>
            <td colspan="4">
                <?PHP
                    if($total_price > 100){
                        echo '<p>'.'<i class="fa fa-smile-o" aria-hidden="true"></i>' . " FREE Pickup and Drop For You".'</p>';
                    }
                ?>
            </td>
            <td colspan="2" align="right">Final Billing Amount :</td>
            <td colspan="2">Rs. <?PHP echo number_format((float)$total_price+$pickup, 2, '.', ''); ?></td>
        </tr>
        <tr>
            <td colspan="4"><p>Gift coupons can be used during confirmation</p></td>   
            <td></td>
            <td colspan="3" align="right"><a href="#articles" class="btn btn-info btn-sm btn-right">Add More Articles</a></td>
        </tr>
        </table>
<a href="iron_order.php" class="btn btn-success">Check Out &amp; Place Order</a>
        <?PHP             
            } 
        ?>
<div class="text-center" id="top"><a href="#how_do_we_work" class="btn btn-info btn-right"><h2>How do we Work ? </h2></a></div>

    <div class="clearfix"></div>

    <h4>Our Service Chart with Price : <span style="float:right; background-color:RED; color:WHITE; padding:5px 10px;">FOR BERHAMPUR ONLY</span></h4>
      <br>
      <hr>
       <ul  class="nav nav-pills nav-justified" id="articles">
			<li class="active"><a  href="#wash" data-toggle="tab">Wash and Iron Service</a></li>
			<li><a href="#iron" data-toggle="tab">Iron Service Only</a></li>
			<li><a href="#drywash" data-toggle="tab">Drywash Services</a></li>
		</ul>

	<div class="tab-content clearfix">
	
		<div class="tab-pane active" id="wash">
        <h2 class="text-center">Wash and Iron Services</h2>
        <h3 class="text-center" style="color:RED;">Mens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                
                $irons4 = get_clothes('washing', 'men'); 

                $count4 = count($irons4);

                if($count4 < 1){
                    echo "We are soon going to begin Wash and Press Services Services of Clothes.<br>";
                }else{

                    foreach($irons4 as $cloth4):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth4['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth4['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth4['price']; ?>.00 </strong>                
                     <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth4['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>                
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:PINK;">Womens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons5 = get_clothes('washing', 'women'); 

                $count5 = count($irons5);

                if($count5 < 1){
                    echo "We are soon going to begin Wash and Press Services Services of Clothes.<br>";
                }else{

                    foreach($irons5 as $cloth5):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth5['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth5['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth5['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth5['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>              
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:BLUE;">Household Items</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons6 = get_clothes('washing', 'housecare'); 

                $count6 = count($irons6);

                if($count6 < 1){
                    echo "We are soon going to begin Wash and Press Services of Clothes.<br>";
                }else{

                    foreach($irons6 as $cloth6):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth6['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth6['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth6['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth6['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>                
                <?PHP             
                    endforeach;
                    }
            ?>
		</div>
		
		<div class="tab-pane" id="iron">
        <h2 class="text-center">Ironing (Cloth Press) Services</h2>
        <h3 class="text-center" style="color:GREEN;">Mens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons1 = get_clothes('ironing', 'men'); 

                $count1 = count($irons1);

                if($count1 < 1){
                    echo "We are soon going to begin Ironing (Cloth Press) Services of Clothes.<br>";
                }else{

                    foreach($irons1 as $cloth1):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth1['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth1['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth1['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth1['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>               
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:GREY;">Womens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons2 = get_clothes('ironing', 'women'); 

                $count2 = count($irons2);

                if($count2 < 1){
                    echo "We are soon going to begin Ironing (Cloth Press) Services of Clothes.<br>";
                }else{

                    foreach($irons2 as $cloth2):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth2['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth2['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth2['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth2['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>         
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:ORANGE;">Household Items</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons3 = get_clothes('ironing', 'housecare'); 

                $count3 = count($irons3);

                if($count3 < 1){
                    echo "We are soon going to begin Ironing (Cloth Press) Services of Clothes.<br>";
                }else{

                    foreach($irons3 as $cloth3):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth3['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth3['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth3['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth3['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>          
                <?PHP             
                    endforeach;
                    }
            ?>
		</div>
        
		<div class="tab-pane" id="drywash">
        <h2 class="text-center">Dry - Washing Services</h2>
        <h3 class="text-center" style="color:BLUE;">Mens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons7 = get_clothes('drywashing', 'men'); 

                $count7 = count($irons7);

                if($count7 < 1){
                    echo "We are soon going to begin Dry Washing of Clothes.<br>";
                }else{

                    foreach($irons7 as $cloth7):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth7['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth7['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth7['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth7['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>                     
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:GREEN;">Womens Wear</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons8 = get_clothes('drywashing', 'women'); 

                $count8 = count($irons8);

                if($count8 < 1){
                    echo "We are soon going to begin Dry Washing of Clothes.<br>";
                }else{

                    foreach($irons8 as $cloth8):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth8['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth8['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth8['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth8['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>                     
                <?PHP             
                    endforeach;
                    }
            ?>
            <div class="clearfix"></div>
            <hr>
            <h3 class="text-center" style="color:VIOLET;">Household Items</h3>
            <?PHP
                //let us get the price and product from iron solutions here
                $irons9 = get_clothes('drywashing', 'housecare'); 

                $count9 = count($irons9);

                if($count9 < 1){
                    echo "We are soon going to begin Dry Washing of Clothes.<br>";
                }else{

                    foreach($irons9 as $cloth9):
                ?>
                    <div class="col-sm-2 col-xs-3 text-center" style="margin:0 0 20px 0; border-bottom:1px solid #e5e5e5; padding:10px 0;">
                    <img src="iron/<?PHP echo $cloth9['cloth_icon']; ?>" alt="<?PHP echo ucfirst($cloth9['cloth']); ?>" class="img-responsive">
                    <strong>@ Rs. <?PHP echo $cloth9['price']; ?>.00 </strong>                
                    <form action="laundry.php?add_to_cart=true" class="form-inline" method="post">
                  <input type="hidden" name="id" value="<?PHP echo $cloth9['id']; ?>">
                  <input type="number" name="qty" min="0" style="padding:4px 5px 5px 5px; width:72px;" placeholder="0" required>
                  <button type="submit" class="btn btn-info btn-sm">Add to cart</button>
                </form>
			</div>                    
                <?PHP             
                    endforeach;
                    }
            ?>
		</div>        
		
	</div>
       
        
        <div class="clearfix"></div>
        <hr>

       <div class="row" id="how_do_we_work">
<br><br>
<h1 class="text-center">How do We Work ?</h1>
        <div class="col-md-3 col-sm-6 col-xs-12 text-center">
            <img src="img/one.png" alt="Step 1" width="100%" class="img-responsive">
            <h3>Step One</h3>
            Place Order on Our site or over Phone <br>
            Confirm Order by SMS OTP and by Phone
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12 text-center">
            <img src="img/two.png" alt="Step 2" width="100%" class="img-responsive">
            <h3>Step Two</h3>
            We collect Your stuff<br>
            Collection in 8 Hours in the Day
            
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12 text-center">
            <img src="img/three.png" alt="Step 3" width="100%" class="img-responsive">
            <h3>Step Three</h3>
            We Clean and Press Your Cloth <br>
            With utmose care we do our job well
            
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12 text-center">
            <img src="img/four.png" alt="Step 4" width="100%" class="img-responsive">
            <h3>Step Four</h3>
            We deliver your stuff within 48 Hours <br>
            Pay the dues at the time of Delivery
        </div>
        
<div class="clearfix"></div>
<br>
<a href="#top" class="btn btn-default btn-right" style="float:right;">Back to Top</a>
    </div>
<?PHP
    
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>