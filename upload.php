<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

ini_set('memory_limit', -1);

class ImgResizer {
  private $originalFile = '';
  public function __construct($originalFile = '') {
      $this -> originalFile = $originalFile;
  }
  public function resize($newWidth, $targetFile) {
      if (empty($newWidth) || empty($targetFile)) {
          return false;
      }
      $src = imagecreatefromjpeg($this -> originalFile);
      list($width, $height) = getimagesize($this -> originalFile);
      $newHeight = ($height / $width) * $newWidth;
      $tmp = imagecreatetruecolor($newWidth, $newHeight);
      imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
      if (file_exists($targetFile)) {
          unlink($targetFile);
      }
      imagejpeg($tmp, $targetFile, 85); // 85 is my choice, make it between 0 – 100 for output image quality with 100 being the most luxurious
  }
}

    if(isset($_GET['submit'])){
        $otp = mt_rand(100000, 999999);
        $otp2 = mt_rand(10000000, 99999999);
        
        if(isset($_FILES['images']['name'])){

	define("MAX_SIZE", "500000");
	
	for($i=0; $i<count($_FILES['images']['tmp_name']); $i++){
		$size = filesize($_FILES['images']['tmp_name'][$i]);
		
		if($size == 0){
			//if there is no image the size will be zero 
			//we will do nothing in such case
		}elseif($size < (MAX_SIZE*1024)){
			$path = "uploads/";
			$imagename = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			
			list($txt, $ext) = explode(".", $imagename);
			
			date_default_timezone_set ("Asia/Kolkata");
			$currentdate = date("d M Y");
			
			$file = time().substr(str_replace("", "_", $txt), 0);
			$info = pathinfo($file);
			$filename = $file.".".$ext;
			
			if(move_uploaded_file($_FILES['images']['tmp_name'][$i], $path.$filename)){
$work = new ImgResizer($path.$filename); 
$work -> resize(960, $path.$filename); 

				$qm = "INSERT INTO uploads (image, otp, otp2, posted_on) VALUES ('$filename', '$otp', '$otp2', '$currentdate')";
				$rm = $db->insert($qm);
				
				if($rm){
					echo "Files Uploaded Successfully.<br>";
				}
			}else{
				echo "Files could not be inserted into the folder.<br>";
			}
		}else{
			echo "The size of the file is bigger than allowed.<br>";
		}
	}

}

        
    }else{
?>
    <div class="row">
        <h1>About Niltik.com </h1>
        <form action="upload.php?submit=true" method="post" enctype="multipart/form-data">
            <!--let us try to put some image uploaders here--> 
        <div class="form-group">
           <label for="image upload">Image 1</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_1').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_1" alt="your image" width="100" height="100" />
       </div>
       
         <div class="form-group">
           <label for="image upload">Image 2</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_2').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_2" alt="your image" width="100" height="100" />
       </div>
         
       <div class="form-group">
           <label for="image upload">Image 3</label> 
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_3').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_3" alt="your image" width="100" height="100" />
       </div>
          
          <div class="form-group">
           <label for="image upload">Image 4</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_4').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_4" alt="your image" width="100" height="100" />
       </div>
         
          <div class="form-group">
           <label for="image upload">Image 5</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_5').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_5" alt="your image" width="100" height="100" />
       </div>
       
       <button class="btn btn-default" type="submit">Submit Ad</button>
        </form>
        
    </div>
    
<?PHP
    }
ini_restore('memory_limit');
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>