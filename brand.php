<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row">
        <?PHP
            $product_company_id = $_POST['brand_id'];
        
            if($product_company_id==''){
                echo "Sorry, we do not have the brand in our database.<br>";
                
            }else{
            //let us get products for this category
            $q1 = "SELECT * FROM classifieds WHERE product_company_id = '$product_company_id' AND status = 'approved' ORDER BY id DESC";
            $r1 = $db->select($q1);
            if(!$r1){
                echo '<h1>'."There are no Ads in this category".'</h1>';
                echo '<p>'."Be the first to post an ad here.".'</p>';
            }else{
                
                echo "<hr>";
            while($ads = $r1->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 4);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers); 
                
            foreach($data as $ad){
?>
           <div class="row">
                <table class="table table-responsive" style="width:95%; background-color:#ffffcc;">
                    <tr>
                        <td width="25%">
                            <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                ?>
                <a href="view_product.php?id=<?PHP echo $ad['id']; ?>" title="<?PHP echo $ad['ad_title']; ?>"><img src="img/no-img.jpg" style="width:100%; height:150px; float:left;" alt="<?PHP echo $ad['ad_title']; ?>" ></a>
                <?PHP
                    }else{
                       $img = $r3->fetch_array();
                ?>
                    <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" style="width:100%; height:150px; float:left;" alt="<?PHP echo $ad['ad_title']; ?>" class="img-responsive"></a>
                <?PHP } ?>
                        </td>
                        <td>
                           <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><span style="font-size:1.4em; color:black; text-shadow:1px 1px #ffffff;"><?PHP echo $ad['ad_title']; ?></span></a>
                                     <span style="float:right; font-size:1.4em; color:RED;">Rs. <?PHP echo $ad['price']; ?>.00</span>
                                     <br>
                                     <?PHP
                                    $product = $ad['product_id'];
                                    $product = explode(",", $product);

                                    $product_id = $product[0];
                                    $sub_product_id = $product[1];
                                    
                                    $qpro = "SELECT * FROM products WHERE id = '$product_id'";
                                    $rpro = $db->select($qpro);

                                    $prod = $rpro->fetch_array();
                
     if($sub_product_id == ''){
       echo '<a href="buynsell.php?view_product='.$prod['id'].'">'.$prod['product_name'].'</a>';
                                    }else{
                                    $qspro = "SELECT * FROM products WHERE id = '$sub_product_id'";
                                    $rspro = $db->select($qspro);

                                    $sprod = $rspro->fetch_array();
      echo '<a href="buynsell.php?view_product='.$prod['id'].'">'.$prod['product_name'].'</a>'.">>".'<a href="buynsell.php?view_product='.$sprod['id'].'">'.$sprod['product_name'].'</a>';
                                    }
                
                                    ?>
                                  <br><br><br><br>
                                      <?PHP
                                   $timestamp = strtotime($ad['posted_on']);
                   
                                   $p_day = date('d', $timestamp);
                                   $p_month = date('M', $timestamp);
                                   $p_year = date('Y', $timestamp);
                                    echo "Posted on : ". "$p_day $p_month, $p_year";
                                  ?>
                                      &nbsp; &nbsp; Place : 
                                      <?PHP
                                    $location = $ad['location_id'];
                                    $location = explode(",", $location);

                                    $location_id = $location[0];
                                    
                                    $q1 = "SELECT * FROM location WHERE id = '$location_id'";
                                    $r1 = $db->select($q1);

                                    $loc = $r1->fetch_array();
                                    ?>
                                      <strong><?PHP echo $loc['location_name']; ?></strong>
                                  <a href="view_product.php?id=<?PHP echo $ad['id']; ?>" class="btn btn-info btn-right" style="float:right">View Details</a>  
                                  
                        </td>
                    </tr>
                </table> 
                       </div>
                     <?PHP 
                        }
                     ?>   
                        
           <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="brand.php?brand_id='.$product_company_id.'&page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="brand.php?brand_id='.$product_company_id.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="brand.php?brand_id='.$product_company_id.'&page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
            }
               ?>
              </ul>
            </nav>

<div class="clearfix"></div>
<a href="javascript:history.back()" class="btn btn-default">Go Back to Previous Page</a>
<?PHP 
            }
            
 ?>
                    
            <div class="clearfix"></div> 
              <br><br>
            <a class="btn btn-success btn-right" role="button" type="submit" href="post_free_ad.php">Post a Free Ad</a>                 
    
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>