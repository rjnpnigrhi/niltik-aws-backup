</div>
    </div>
    
    <footer class="footer">
        <div class="container">
            Footer area
            <div class="clearfix"></div>
            <hr>
            <div class="row text-center">
               All &copy; Rights Reserved <a href="http://www.niltik.com" title="Niltik.com">Niltik.com</a>, 2016 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                Site Architect <a href="http://www.odiaguru.com" title="Odiaguru.com">Odiaguru.com</a>
            </div>
        </div>
    </footer>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
     <script src="../js/bootstrap.min.js"></script>  
     <!--Le us include a new script here-->     
     <script src="../js/script.js"></script>
     <!-- Compiled and minified JavaScript -->
     <script src="../js/mfb.min.js"></script>
     <!-- build:js modernizr.touch.js -->
    <script src="../js/modernizr.touch.js"></script>
    <!-- endbuild -->
     <!--LIGHTBOX scripts here-->
     <script src="//cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/4.0.1/ekko-lightbox.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function ($) {

				// delegate calls to data-toggle="lightbox"
				$(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						onShown: function() {
							if (window.console) {
								return console.log('onShown event fired');
							}
						},
						onContentLoaded: function() {
							if (window.console) {
								return console.log('onContentLoaded event fired');
							}
						},
						onNavigate: function(direction, itemIndex) {
							if (window.console) {
								return console.log('Navigating '+direction+'. Current item: '+itemIndex);
							}
						}
					});
				});

				//Programatically call
				$('#open-image').click(function (e) {
					e.preventDefault();
					$(this).ekkoLightbox();
				});
				$('#open-youtube').click(function (e) {
					e.preventDefault();
					$(this).ekkoLightbox();
				});

				$(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox({
						onShown: function() {
							var lb = this;
							$(lb.modal_content).on('click', '.modal-footer a#jumpit', function(e) {
								e.preventDefault();
								lb.navigateTo(2);
							});
							$(lb.modal_content).on('click', '.modal-footer a#closeit', function(e) {
								e.preventDefault();
								lb.close();
							});
						}
					});
				});

			});
		</script>
</body>
</html>