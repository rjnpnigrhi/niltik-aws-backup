<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<head>
<title>Niltik.com : Administrators Panel</title>
    
     <!--Bootstrap css here-->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!--Awesome fonts css here-->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!--Custom css here-->
    <link rel="stylesheet" href="../css/styles.css">
    <!--Floating button css here-->
    <link rel="stylesheet" href="../css/mfb.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400' rel='stylesheet' type='text/css'>
    <!--We can add a favicon here-->
    <link rel="icon" type="image/png" href="../img/favicon.png" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <!-- for dynamic drop down list -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="../js/dropdown.js"></script>
    <!-- Bootstrap Date-Picker Plugin -->
    <link rel="stylesheet" type="text/css" href="../css/datepicker.css"/>
    <script type="text/javascript" src="../js/bootstrap-datepicker.js"></script>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
</head>
<body>
   <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
          <i class="mfb-component__main-icon--resting ion-plus-round"></i>
          <i class="mfb-component__main-icon--active ion-close-round"></i>
        </a>
        <ul class="mfb-component__list">
          <li>
            <a href="user.php" data-mfb-label="Manage Users on Website" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-pencil fa-fw"></i>
            </a>
          </li>
          
          <li>
            <a href="products.php"
               data-mfb-label="Products Management" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-home fa-fw" aria-hidden="true"></i>
            </a>
          </li>
          
          <li>
            <a href="iron.php" data-mfb-label="Book Iron Solution" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-refresh fa-fw"></i>
            </a>
          </li>
        </ul>
      </li>
    </ul>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">
          <span class="col-xs-3"><img src="../img/favicon.png" alt="Niltik.com" width="35px" style="margin-top:-10px;"></span><span class="col-xs-4 francois">Niltik.com</span>
          </a>
        </div>
        <?PHP
                if(isset($_SESSION['admin'])){
                    
            ?>  
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="orders.php"><i class="fa fa-book" aria-hidden="true"></i>Order Book</a></li>
            <li><a href="accounts.php"><i class="fa fa-book" aria-hidden="true"></i>Cash Book</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list" aria-hidden="true"></i> Settings <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="user.php">User Setting</a></li>
<li><a href="feedback.php">User Feedback</a></li>
                <li><a href="laundry.php">Iron Items</a></li>
                <li><a href="location.php">City / Towns</a></li>
                <li><a href="products.php">products</a></li>
                <li><a href="companies.php">Product Manufacturers</a></li>
              </ul>
            </li>
            <li><a href="house.php"><i class="fa fa-wpforms" aria-hidden="true"></i>Home Ads</a></li>
            <li><a href="ads.php"><i class="fa fa-wpforms" aria-hidden="true"></i>Free Ads</a></li>
            <li><a href="coupons.php"><i class="fa fa-wpforms" aria-hidden="true"></i>Coupons</a></li>
            <li><a href="contacts.php"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact Us</a></li>        
            <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
        <?PHP } ?>
      </div><!-- /.container-fluid -->
    </nav>

    <div class="container header">
        <div class="row">
            <div class="col-md-2 text-center">
                <a href="index.php"><img src="../img/site_logo.png" alt="Niltik.com" width="72px"></a>
            </div>
            <div class="col-md-10 text-center">
                Google Ad here
            </div>
        </div>
    </div>
    
    <div class="container" style="margin-bottom:40px;">
        <div class="row">