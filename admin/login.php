<?PHP
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
    
    if(isset($_GET['login'])){
        //login the user
            $email = $db->real_escape_string($_POST['email']);
            $password = $db->real_escape_string($_POST['password']);
            
            $q = "SELECT * FROM admins WHERE email = '$email' AND password = '$password' LIMIT 1";
            $r = $db->select($q);
            
            if(!$r){
                echo "Your Log in credentials are incorrect.<br>";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
            }else{
            
                $user = $r->fetch_array();
                session_start();
                $_SESSION['id'] = $user['id'];
                $_SESSION['name'] = $user['name'];
                $_SESSION['email'] = $user['email'];                
                $_SESSION['phone'] = $user['phone'];
		$_SESSION['admin'] = $user['id'];
                $_SESSION['logged_in'] = TRUE;
                
                echo '<h2>'."Welcome Admin !".'</h2>';
                echo "You have been successfully logged in.<br>";
                echo '<a href="index.php" class="btn btn-default btn-sm">'."Admin Dashboard".'</a>';
            }
    }else{
?>
  <div class="col-sm-6">
      <h3>Administrator Login</h3>
   <form action="login.php?login=true" method="post">
       <div class="form-group">
           <label for="username">Username</label>
           <input type="email" name="email" class="form-control" placeholder="Your email Id" required>
       </div>
       
       <div class="form-group">
           <label for="password">Password</label>
           <input type="password" name="password" class="form-control" placeholder="Password" required>
       </div>
       
       <div class="form-group">
           <button type="submit" class="btn btn-default">Sign in</button>
       </div>
       <div class="form-group">                        
           <p class="help-block">If you forget your login credentials contact Web Administrator @ 9040985463</p>
       </div>
   </form>
  </div>
  <div class="col-sm-6">
      Google Ads here
  </div>
   
<?PHP
    }
    include('mods/footer.php');
?>