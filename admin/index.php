<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
   Welcome <?PHP echo $_SESSION['name']; ?> | <a href="logout.php" class="btn btn-default">Log Out</a>
<?PHP
    }
    include('mods/footer.php');
?>