<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
    if(isset($_GET['edit'])){
        $id = $_GET['edit'];
        
        $q = "SELECT * FROM house_ad WHERE id = '$id'";
        $r = $db->select($q);
        
        $ad = $r->fetch_array();
        
        //let us show a form for editing the data
?>
   <form action="house.php?update=true" method="post">
           <div class="form-group">
               <label for="ad_title">Ad Title <span style="color:red;">*</span></label>
               <input type="text" class="form-control" name="ad_title" placeholder="Type a Attractive Ad Title" value="<?PHP echo $ad['ad_title']; ?>">
           </div>
           
           <div class="form-group">
           <label for="product_id">Category :</label>
           <select id="category_id" name="category_id" class="form-control">
                <option value="">Select a Category</option>
                <?PHP
                    $q = "SELECT * FROM house_category";
                    $r = $db->select($q);
                
                    if(!$r){
                        
                    }else{
                        while($d = $r->fetch_array()):
                ?>
                <option value="<?PHP echo $d['id']; ?>"><?PHP echo ucfirst($d['category']); ?></option>
                <?PHP
                  endwhile;
                }
                ?>
            </select>
        </div>  
                        
       <div class="form-group">
       <label for="description">Description / Details of the Ad <span style="color:red;">*</span></label>
       <textarea name="details" id="details" class="form-control" cols="30" rows="10" required><?PHP echo $ad['details']; ?></textarea>
       </div>
                        
       <div class="form-group">
       <label for="price">Price <span style="color:red;">*</span></label>
       <input type="text" name="price" class="form-control" placeholder="Type your Quoted Price" required value="<?PHP echo $ad['price']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="name">Name <span style="color:red;">*</span></label>
       <input type="text" name="name" class="form-control" placeholder="Your Complete Name" required value="<?PHP echo $ad['name']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="email">email <span style="color:red;">*</span></label>
       <input type="email" name="email" class="form-control" placeholder="Your Complete email" required value="<?PHP echo $ad['email']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="mobile">Mobile No <span style="color:red;">*</span></label>
       <input type="text" name="phone" class="form-control" placeholder="Your Mobile Number" required value="<?PHP echo $ad['phone']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id1" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                                            
       <div class="form-group">
       <label for="Address">Address <span style="color:red;">*</span></label>
       <textarea name="address" id="address" class="form-control" cols="30" rows="10"><?PHP echo $ad['address']; ?></textarea>
       <input type="hidden" name="id" value="<?PHP echo $ad['id']; ?>">
       </div>
                        
       <button class="btn btn-default" type="submit">Update Ad</button>
   </form>
<?PHP
    }elseif(isset($_GET['update'])){
        $ad_title = $_POST['ad_title'];
        $category_id = $_POST['category_id'];
        $details = $_POST['details'];
        $price = $_POST['price'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $location_id = $_POST['location_id'];
        
        if(!isset($_POST['locality_id'])){
            $locality_id = '';
        }else{
            $locality_id = $_POST['locality_id'];
        }
        $address = $_POST['address'];
        
        $id = $_POST['id'];
        
        if($locality_id !==''){
            $loc_id = $locality_id;
        }else{
            $loc_id = $location_id;
        }
        
        $q = "UPDATE house_ad SET ad_title = '$ad_title', category_id = '$category_id', details = '$details', price = '$price', name = '$name', email = '$email', phone = '$phone', location_id = '$loc_id', address = '$address' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "The ad has been updated Successfully. <br>";
        echo '<a href="house.php" class="btn btn-info">'."Back to House Ads Page".'</a>';
        
    }elseif(isset($_GET['approve'])){
        $id = $_GET['approve'];
        
        $q = "SELECT * FROM house_ad WHERE id = '$id'";
        $r = $db->select($q);
        
        $ad = $r->fetch_array();
        
        //let us find location
        $location = $ad['location_id'];
        $location = explode(",", $location);

        $location_id = $location[0];
        $locality_id = $location[1];
        
        $q1 = "SELECT * FROM location WHERE id = '$location_id'";
        $r1 = $db->select($q1);
        
        $loc = $r1->fetch_array();
        
        //let us find the locality
        $q2 = "SELECT * FROM location WHERE id = '$locality_id'";
        $r2 = $db->select($q2);
        if(!$r2){
            
        }else{
            $st = $r2->fetch_array();
        }
        echo '<h3>'.$ad['ad_title'].'</h3>';
        echo '<p>'.$ad['name']." | ".$ad['email']." | ".$ad['phone']." | ".$ad['address'].'</p>';
        echo "<hr>";
        echo '<p>'.$ad['details'].'</p>';
        echo '<p><strong>'."Price : Rs. ".$ad['price'].".00".'</strong></p>';
        echo '<p>'."Location : ".$loc['location_name']." &nbsp; &nbsp; &nbsp; &nbsp; Locality / Street : ".$st['location_name'].'</p>';
        echo "<hr>";
        $otp = $ad['otp'];
        $otp2 = $ad['otp2'];
        $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
        $r3 = $db->select($q3);
                                  
        if(!$r3){
        echo "NO Images Uploaded.<br>";
        }else{
        while($img = $r3->fetch_array()):
        
        echo '<img src="../uploads/'.$img['image'].'" style="width:120px; float:left; margin:0 10px 0 0;">';
            
        endwhile;
        }
            
        echo '<div class="clearfix"></div>';
        echo "<hr>";
        if($ad['status']== "approved"){
            echo '<a href="house.php?block='.$ad['id'].'" class="btn btn-info">'."Block this Ad".'</a>';
        }else{
            echo '<a href="house.php?approve_final='.$ad['id'].'" class="btn btn-info">'."Approve this Ad".'</a>';
        }
        echo " | ".'<a href="house.php?edit='.$ad['id'].'" class="btn btn-danger">'."Edit AD".'</a>';
        echo " | ".'<a href="house.php?delete='.$ad['id'].'" class="btn btn-danger">'."DELETE THIS AD".'</a>';
        echo " | ".'<a href="javascript:history.back()" class="btn btn-default">'."Back to House Ads".'</a>';
    }elseif(isset($_GET['approve_final'])){
        $id = $_GET['approve_final'];
        
        $q = "UPDATE house_ad SET status = 'approved' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "House Ad has been approved.<br>";
        echo '<a href="house.php" class="btn btn-info">'."House Ads Management".'</a>';
        //stop the ad 
    
    }elseif(isset($_GET['block'])){
        $id = $_GET['block'];
        
        $q = "UPDATE house_ad SET status = 'blocked' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "House Ad has been blocked.<br>";
        echo '<a href="house.php" class="btn btn-info">'."House Ads Management".'</a>';
        //stop the ad 
    }elseif(isset($_GET['delete'])){
        $id = $_GET['delete'];
        
        $q = "DELETE FROM house_ad WHERE id = '$id'";
        $r = $db->delete($q);
        
        echo "House Ad has been DELETED.<br>";
        echo '<a href="house.php" class="btn btn-info">'."House Ads Management".'</a>';
        //stop the ad 
        
    }else{
        $q = "SELECT * FROM house_ad ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo '<h3>'."No House Ads in Database".'</h3>';
        }else{
            while($ads = $r->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 24);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
?>
          <table class="table table-hover">
            <tr>
                <th width="5%">ID</th>
                <th>Title of Ad</th>
                <th width="20%">Posted by :</th>
                <th width="20%">Location :</th>
                <th width="10%">Price :</th>
                <th width="15%">Action</th>
            </tr>
            <?PHP
                //let us get the data for this page
                foreach($data as $d){
            ?>
                <tr>
                    <td><?PHP echo $d['id']; ?></td>
                    <td><a href="house.php?approve=<?PHP echo $d['id']; ?>" class="btn btn-default"><?PHP echo ucfirst($d['ad_title']); ?></a>
                    <?PHP
                        if($d['na']=="want"){
                                echo '<span style="font-weight:bold; color:red;">'."NILTIK ASSURED WANTED".'</span>';
                            }else{
                                echo "";
                            }
                    ?>
                    </td>
                    <td>
                    <?PHP echo $d['name']."<br>".$d['email']."<br>".$d['phone']."<br>".$d['address']; ?></td>
                    <td>
                    
                       <?PHP
                        $location = $d['location_id'];
                        $location = explode(",", $location);

                        $location_id = $location[0];
                        $locality_id = $location[1];
                    
                        $q1 = "SELECT * FROM location WHERE id = '$location_id'";
                        $r1 = $db->select($q1);

                        $loc = $r1->fetch_array();
                        
                        echo "City : " . $loc['location_name']. "<br>";

                        //let us find the locality
                        $q2 = "SELECT * FROM location WHERE id = '$locality_id'";
                        $r2 = $db->select($q2);
                        if(!$r2){

                        }else{
                            $st = $r2->fetch_array();
                            echo "Street : " . $st['location_name'];
                        }                        
                        
                        ?>
                    
                    </td>
                    <td>Rs. <?PHP echo $d['price']; ?>.00</td>
                    <td>                    
                    <?PHP
                        if($d['status']=="approved"){
                            echo '<a href="#" class="btn btn-primary btn-sm">'."Approved".'</a>';
                        }elseif($d['status']=="blocked"){
                            echo '<a href="#" class="btn btn-danger btn-sm">'."Blocked".'</a>';
                        }else{
                            echo '<a href="house.php?approve='.$d['id'].'" class="btn btn-success btn-sm">'."Approve".'</a>';
                        }
                    ?>
                    &nbsp; <a href="house.php?delete=<?PHP echo $d['id']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>                    
                </tr>
            <?PHP
                }
                }
            ?>
        </table>
        <hr>
          
           <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="house.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="house.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="house.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
<?PHP 
        }
    }
    include('mods/footer.php');
?>