<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');    
?>
  <div class="col-md-4">
    <?PHP
        if(isset($_GET['add'])){
            //we will add the coupon
            $coupon_code = strtoupper($_POST['coupon_code']);
            $amount = $_POST['amount'];
            $discount = $_POST['discount'];
            $info = $_POST['info'];
            $valid_for = $_POST['valid_for'];
            $valid_from = $_POST['valid_from'];
            $valid_upto = $_POST['valid_upto'];
            $min_spend = $_POST['min_spend'];
            
            //let us check if the coupon code exists
            if(exists_coupon($coupon_code) > 0){
                echo "The coupon code you typed already exists in our database.<br>";
                echo "Try something new.<br>";
                echo '<a href="coupons.php" class="btn btn-default">'."Back to Coupons Manager".'</a>';
            }elseif($amount!=='' AND $discount!==''){
                echo "You can only chose either from offer or discount, not both.<br>";
                echo '<a href="coupons.php" class="btn btn-default">'."Back to Coupons Manager".'</a>';
            }elseif($amount==''AND $discount==''){
                echo "You Must chose one from offer or discount.<br>";
                echo '<a href="coupons.php" class="btn btn-default">'."Back to Coupons Manager".'</a>';
            }else{
                //we update the coupon on website
                $q = "INSERT INTO coupons (coupon_code, amount, discount, info, valid_for, valid_from, valid_upto, min_spend) VALUES ('$coupon_code', '$amount', '$discount', '$info', '$valid_for', '$valid_from', '$valid_upto', '$min_spend')";
                $r = $db->insert($q);
                
                echo "New Coupon is Made Available for Users.<br>";
                echo '<a href="coupons.php" class="btn btn-default">'."Back to Coupons Manager".'</a>';
            }
            
        }else{
    ?>
     <h3>Add a New Coupon</h3>
     <p>You can give discount as 10 / 20 % or Rs. 10, Rs. 20 <br>
     You can give only one offer though. </p>
      <form action="coupons.php?add=true" method="post" class="form-horizontal">
          <div class="form-group">
              <label for="coupon_name">Coupon ID : </label>
              <input type="text" id="coupon_name" name="coupon_code" class="form-control" placeholder="Type a Coupon Code" required>
          </div>
          <div class="form-group">
              <label for="coupon_amount">Amount : </label>
              <input type="text" class="form-control" id="coupon_amount" name="amount" placeholder="Type an Amount, numbers Only">
              <p class="help-block">Type an Amount i.e. 20 / 30 / 100 etc.</p>
          </div>
          <div class="form-group">
          <label for="coupon_discount">Discount</label>
          <input type="text" class="form-control" id="coupon_discount" placeholder="Enter a Number" name="discount">
          <p class="help-block">You can chose to add a discount ( %ge ). </p>
          </div>
          <div class="form-group">
          <label for="coupon_info">Information about the Offer :</label>
          <input type="text" class="form-control" id="coupon_info" placeholder="Type in a Sentence why the offer is given" name="info" required>
          </div>
          
          <div class="form-group">
              <label for="valid_for">Valid For</label>
              <select name="valid_for" id="valid_for" class="form-control">
                  <option value="users">Only Our Registered Members</option>
                  <option value="all">Anyone can avail this Offer</option>
              </select>
          </div>
          
          <div class="form-group">
          <label for="valid_upto">Offer Begins on</label>
          <input name="valid_from" class="datepicker1 form-control" type="text" id="datepicker1">
          </div>
          <script>
            $(function(){
                $('.datepicker1').datepicker();
            });
          </script>
          
          <div class="form-group">
          <label for="valid_upto">Offer Ends on</label>
          <input name="valid_upto" class="datepicker2 form-control" type="text" id="datepicker2">
          </div>
          <script>
            $(function(){
                $('.datepicker2').datepicker();
            });
          </script>
          
          <div class="form-group">
          <label for="min_spend">Minimum Expenditure</label>
          <input type="text" class="form-control" id="min_spend" placeholder="Minimum Bill Amount Required" name="min_spend" value="0">
          <p class="help-block">If there is a minimum amount fill it or else leave it at 0.</p>
          </div>
          <div class="form-group">
              <button class="btn btn-default" type="submit">Submit</button>
          </div>
      </form>
      <?PHP } ?>
  </div>
  <div class="col-md-8">
     
      <table class="table table-hover">
          <tr>
              <th>ID</th>
              <th>Coupon Code</th>
              <th>Rs.</th>
              <th>%ge</th>
              <th>Valid From</th>
              <th>Valid Upto</th>
              <th>On Bill</th>
              <th>Valid For</th>
          </tr>
          <?PHP
            //let us get all coupons here
            $coupons = get_coupons();
            
            foreach($coupons as $c):
            ?>
          <tr>
              <td><?PHP echo $c['id']; ?></td>
              <td><strong style="color:red;"><a href="#" title="<?PHP echo $c['info']; ?>" class="btn btn-default btn-sm"><?PHP echo $c['coupon_code']; ?></a></strong></td>
              <td><kbd><?PHP echo $c['amount']; ?></kbd></td>
              <td><kbd><?PHP echo $c['discount']; ?></kbd></td>
              <td><?PHP echo $c['valid_from']; ?></td>
              <td><?PHP echo $c['valid_upto']; ?></td>
              <td><?PHP echo $c['min_spend']; ?></td>
              <td><?PHP echo ucfirst($c['valid_for']); ?></td>
          </tr>
          <?PHP endforeach; ?>
      </table>
      
  </div>
<?PHP
    }
    
    include('mods/footer.php');
?>