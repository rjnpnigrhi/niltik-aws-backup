<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
  <a href="iron.php" class="btn btn-default">Product Chart</a>
  <br><hr>
  
  <?PHP
    if(isset($_GET['add_final'])){
        //insert product into the database
        $cloth = $_POST['cloth'];
        $price = $_POST['price'];
        $service = $_POST['service'];
        $category = $_POST['category'];
        
        if($cloth==''||$price==''){
            echo "Please fill in all the fields before submitting.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }
        
        $q = "SELECT * FROM cloth_iron WHERE cloth = '$cloth' AND category = '$category' AND service = '$service'";
        $r = $db->select($q);
        
        if($r){
            echo "This item already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //we will insert this into website database
            $qi = "INSERT INTO cloth_iron (cloth, category, service, price) VALUES ('$cloth', '$category', '$service', '$price')";
            $ri = $db->insert($qi);
            
                echo "New Item has been added to the list on database.<br />";
                echo '<a href="iron.php" class="btn btn-default btn-sm">'."Back to List View".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete product from the database
        $id = $_GET['delete'];
        
        $qd = "DELETE FROM cloth_iron WHERE id = '$id'";
        $rd = $db->delete($qd);
        
        echo "The cloth Item has been deleted from our database.<br>";
        echo '<a href="iron.php" class="btn btn-default btn-sm">'."Back to List View".'</a>';
    }else{
  ?>
  <div class="col-md-4">
     <h3>Add a New Cloth Items</h3>
      <form action="iron.php?add_final=true" method="post">
          <div class="form-group">
              <label for="cloth">Cloth Item</label>
              <input type="text" name="cloth" class="form-control" placeholder="name of the item" required>
          </div>
          
          <div class="form-group">
              <label for="price">Price for Service</label>
              <input type="text" name="price" class="form-control" placeholder="price for service" required>
          </div>
          
          <div class="form-group">
              <label for="service">Service Category</label>
              <select name="service" id="service" class="form-control">                  
                  <option value="ironing">Ironing</option>
                  <option value="dry wash">Dry Washing</option>
              </select>
          </div>
          
          <div class="form-group">
              <label for="category">Cloth Categories</label>
              <select name="category" id="category" class="form-control">
                  <option value="men">Men's Clothes</option>
                  <option value="women">Women's Clothes</option>
              </select>
          </div>
          <button class="btn btn-primary" type="submit">Add Item</button>
      </form>
  </div>
  
  <div class="col-md-8">
     <h3>List of Items for Service</h3>
      <table width="100%" class="table table-hover table-responsive">
      <tr>
          <th width="5%">ID</th>
          <th width="15%">Category</th>
          <th>Name</th>
          <th>Service</th>
          <th width="10%">Price</th>
          <th width="20%">Action</th>
      </tr>
      <?PHP
        //show all present products
        $qp = "SELECT * FROM cloth_iron ORDER BY category ASC";
        $rp = $db->select($qp);
        
        while($pd = $rp->fetch_array()):
        ?>
      <tr>
          <td><?PHP echo $pd['id']; ?></td>
          <td><?PHP echo ucwords($pd['category']); ?></td>
          <td><?PHP echo ucwords($pd['cloth']); ?></td>
          <td><?PHP echo ucwords($pd['service']); ?></td>
          <td>Rs. <?PHP echo $pd['price']; ?>.00</td>
          <td><a href="iron.php?delete=<?PHP echo $pd['id']; ?>" class="btn btn-primary btn-xs">Remove from List</a></td>
      </tr>
      <?PHP endwhile; } ?>
  </table>
  </div>
  
  
<?PHP
    }
    include('mods/footer.php');
?>