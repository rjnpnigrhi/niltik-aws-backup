<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
        
    if(isset($_GET['add'])){
        //we will show a form here
        //we will insert service automatically
        $service = $_GET['add'];
        
        if($service == 'ironing'){
            $service_name = 'Iron only Services';
        }elseif($service == 'washing'){
            $service_name = 'Wash and Iron Services';
        }elseif($service == 'drywashing'){
            $service_name = 'Drywash Services';            
        }else{
            echo "You are on the wrong place.<br>";
            echo '<a href="laundry.php" class="btn btn-default">'."Go Back Buddy !".'</a>';
        }
        
?>
<h3>Add a New Cloth Item</h3>
    <form action="laundry.php?add_final=true" method="post">
        <div class="form-group">
          <label for="cloth">Cloth Item</label>
          <input type="text" name="cloth" class="form-control" placeholder="name of the item" required>
        </div>
         
        <div class="form-group">
          <label for="cloth">Cloth Icon</label>
          <input type="text" name="cloth_icon" class="form-control" placeholder="Image for the Item" required>
        </div> 
          
        <div class="form-group">
          <label for="price">Price for Service</label>
          <input type="text" name="price" class="form-control" placeholder="price for service" required>
        </div>
          
        <div class="form-group">
          <label for="service">Service Category</label>
          <input type="text" name="service_name" class="form-control" placeholder="price for service" value="<?PHP echo $service_name; ?>" readonly="readonly" required>
          <input type="hidden" name="service" value="<?PHP echo $service; ?>">
        </div>
          
        <div class="form-group">
          <label for="category">Cloth Categories</label>
          <select name="category" id="category" class="form-control">
              <option value="men">Men's Clothes</option>
              <option value="women">Women's Clothes</option>
              <option value="housecare">Household Items</option>
          </select>
        </div>
        <button class="btn btn-primary" type="submit">Add Item</button>
        <a href="laundry.php" class="btn btn-default">Go Back !</a>
</form>
<?PHP
    }elseif(isset($_GET['add_final'])){
        //we will include the data on our database
        //insert product into the database
        $cloth = $_POST['cloth'];
        $cloth_icon = $_POST['cloth_icon'];
        $price = $_POST['price'];
        $service = $_POST['service'];
        $category = $_POST['category'];
        
        if($cloth==''||$price==''||$cloth_icon==''){
            echo "Please fill in all the fields before submitting.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }
        
        $q = "SELECT * FROM cloth_iron WHERE cloth = '$cloth' AND category = '$category' AND service = '$service'";
        $r = $db->select($q);
        
        if($r){
            echo "This item already exists in the database.<br />";
            echo '<a href="laundry.php" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //we will insert this into website database
            $qi = "INSERT INTO cloth_iron (cloth, cloth_icon, category, service, price) VALUES ('$cloth', '$cloth_icon', '$category', '$service', '$price')";
            $ri = $db->insert($qi);
            
                echo "New Item has been added to the list on database.<br />";
                echo '<a href="laundry.php" class="btn btn-default btn-sm">'."Back to List View".'</a>';
        }
    }elseif(isset($_GET['edit'])){
        //get the data about the cloth
        //show a form for editing
        $cloth_id = $_GET['edit'];
        $cloth = get_cloth_for_id($cloth_id);
        
?>
    <form action="laundry.php?update=true" method="post">
        <div class="form-group">
          <label for="cloth">Cloth Item</label>
          <input type="text" name="cloth" class="form-control" value="<?PHP echo $cloth['cloth']; ?>" required>
          <input type="hidden" name="id" value="<?PHP echo $cloth_id; ?>">
        </div>
         
        <div class="form-group">
          <label for="cloth">Cloth Icon</label>
          <input type="text" name="cloth_icon" class="form-control" value="<?PHP echo $cloth['cloth_icon']; ?>" required>
        </div> 
          
        <div class="form-group">
          <label for="price">Price for Service</label>
          <input type="text" name="price" class="form-control" value="<?PHP echo $cloth['price']; ?>" required>
        </div>
          
        <div class="form-group">
          <label for="service">Service Category</label>
          <select name="service" id="service" class="form-control">                                   
                  <option value="washing">Wash and Iron</option>
                  <option value="ironing">Ironing</option>
                  <option value="drywashing">Dry Washing</option>
              </select>
        </div>
          
        <div class="form-group">
          <label for="category">Cloth Categories</label>
          <select name="category" id="category" class="form-control">
              <option value="men">Men's Clothes</option>
              <option value="women">Women's Clothes</option>
              <option value="housecare">Household Items</option>
          </select>
        </div>
        <button class="btn btn-primary" type="submit">Update Item</button>
        <a href="laundry.php" class="btn btn-default">Go Back !</a>
    </form>
<?PHP 
        
    }elseif(isset($_GET['update'])){
        //update the table with new information
        $cloth = $_POST['cloth'];
        $cloth_icon = $_POST['cloth_icon'];
        $price = $_POST['price'];
        $service = $_POST['service'];
        $category = $_POST['category'];
        $id = $_POST['id'];
        
        if($cloth==''||$price==''||$cloth_icon==''){
            echo "Please fill in all the fields before submitting.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }
        
            //we will insert this into website database
            $q = "UPDATE cloth_iron SET cloth = '$cloth', cloth_icon = '$cloth_icon', category = '$category', service = '$service', price = '$price' WHERE id = '$id'";
           
            $r = $db->insert($q);
            
                echo "New Item information has been updated on database.<br />";
                echo '<a href="laundry.php" class="btn btn-default btn-sm">'."Back to List View".'</a>';
    }elseif(isset($_GET['delete'])){
        //let us delete the item
        $id = $_GET['delete'];
        
        $qd = "DELETE FROM cloth_iron WHERE id = '$id'";
        $rd = $db->delete($qd);
        
        echo "The cloth Item has been deleted from our database.<br>";
        echo '<a href="laundry.php" class="btn btn-default btn-sm">'."Back to List View".'</a>';
    
    }else{
?>
   <h2>Laundry Services We Offer : <span style="float:right;"><a href="media.php" target="_blank" class="btn btn-default">Media Files</a></span> </h2>
   <hr>
    <div class="col-md-4">
        <h3>Wash and Iron Services <span style="float:right;"><a href="laundry.php?add=washing" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i></a></span></h3>
        <table class="table table-hover">
            <tr>
                <th>Cloth</th>
                <th width="84px">Price</th>
                <th width="92px" style="text-align:center;">Options</th>
            </tr>
            <?PHP
                //let us get all clothes for wash and iron
                $clothes = clothes_wash();
                foreach($clothes as $wash):
                $cat = $wash['category'];
                $c = $cat[0]
            ?>
            <tr>
                <td><?PHP echo $wash['cloth'] . " ( " . ucfirst($c) . " ) "; ?></td>
                <td>Rs. <?PHP echo $wash['price']; ?>.00</td>
                <td align="right">
                <a href="laundry.php?edit=<?PHP echo $wash['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="laundry.php?delete=<?PHP echo $wash['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?PHP
                endforeach;
            ?>
        </table>
    </div>
    <div class="col-md-4">
        <h3>Iron Services <span style="float:right;"><a href="laundry.php?add=ironing" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i></a></span></h3>
        <table class="table table-hover">
            <tr>
                <th>Cloth</th>
                <th width="84px">Price</th>
                <th width="92px" style="text-align:center;">Options</th>
            </tr>
            <?PHP
                //let us get all clothes for wash and iron
                $clothes = clothes_iron();
                foreach($clothes as $iron):
                $cat1 = $iron['category'];
                $c1 = $cat1[0]
            ?>
            <tr>
                <td><?PHP echo $iron['cloth'] . " ( " . ucfirst($c1) . " ) "; ?></td>
                <td>Rs. <?PHP echo $iron['price']; ?>.00</td>
                <td align="right">
                <a href="laundry.php?edit=<?PHP echo $iron['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="laundry.php?delete=<?PHP echo $ironh['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?PHP
                endforeach;
            ?>
        </table>
    </div>
    <div class="col-md-4">
        <h3>Drywashing Services <span style="float:right;"><a href="laundry.php?add=drywashing" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i></a></span></h3>
        <table class="table table-hover">
            <tr>
                <th>Cloth</th>
                <th width="84px">Price</th>
                <th width="92px" style="text-align:center;">Options</th>
            </tr>
            <?PHP
                //let us get all clothes for wash and iron
                $clothes = clothes_drywash();
                foreach($clothes as $drywash):
                $cat2 = $drywash['category'];
                $c2 = $cat2[0]
            ?>
            <tr>
                <td><?PHP echo $drywash['cloth'] . " ( " .ucfirst($c2). " ) "; ?></td>
                <td>Rs. <?PHP echo $drywash['price']; ?>.00</td>
                <td align="right">
                <a href="laundry.php?edit=<?PHP echo $drywash['id']; ?>" class="btn btn-sm btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                <a href="laundry.php?delete=<?PHP echo $drywash['id']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?PHP
                endforeach;
            ?>
        </table>
    </div>   
    
    
    <div class="clearfix"></div> <hr>
<p>N.B. : W, M and H indicated Women, Men and Household Articles in the above tables.</p>
<?PHP
    }
    }
    include('mods/footer.php');
?>