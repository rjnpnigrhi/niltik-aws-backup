<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');

    echo '<div class="container">
            <div class="row">';

    if(isset($_GET['delete'])){
        //delete comment
        $id = $_GET['delete'];
        
        $q = "DELETE FROM feedback WHERE id = '$id'";
        $r = $db->delete($q);
        
        echo '<a href="feedback.php" class="btn btn-info">'."Feedback Directory".'</a>';
    }elseif(isset($_GET['publish'])){
        //publish Comment
        $id = $_GET['publish'];
        
        $q = "UPDATE feedback SET status = 'publish' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo '<a href="feedback.php" class="btn btn-info">'."Feedback Directory".'</a>';
    }else{
        
    $q = "SELECT * FROM feedback ORDER BY id DESC";
    $r = $db->select($q);
        
    if(!$r){
                    
    }else{
                   
    while($rows = $r->fetch_array()){
        $result[] = $rows;
    }

    //this is where we ask for the pagination
    //how many pages are there in the result set 
    $numbers = $pagination->paginate($result, 12);

    //what are the data to be presented in these pages
    $data = $pagination->fetchresults();

    //let us get the current page number
    $pn = $pagination->page_num();

    //let us get all page numbers
    $tp = count($numbers);
?>
    <!-- feedbacks listed here begins-->
           <table class="table table-striped">
              <tr>
                  <td colspan="4" align="center"><h1>Moderate Feedback received from visitors</h1></td>
              </tr>
               <tr>
                   <th width="10px">ID</th>
                   <th>Subject</th>
                   <th>Feedback</th>
                   <th>Name &amp; eMail</th>
                   <th width="50px">Status</th>
               </tr>
               <?PHP //let us get the data for the page
                    foreach ($data as $f){ 
               ?>
               <tr>
                   <td><p><?PHP echo $f['id']; ?></p></td>
                   <td><p><?PHP echo nl2br($f['subject']); ?></p></td>
                   <td><p><?PHP echo nl2br($f['feedback']); ?></p></td>
                   <td><p>
                   <?PHP 
                        echo $f['name']."<br />";
                        echo $f['email'];                       
                    ?>                   
                   </p></td>
                   <td>
                       <?PHP
                            if($f['status']==''){
                                echo '<a href="feedback.php?publish='.$f['id'].'" class="btn btn-xs btn-info">'."Publish".'</a>';
                            }else{
                                echo '<a href="#" class="btn btn-xs btn-success">'."PUBLISHED".'</a>';
                            }
                        
                            echo '<a href="feedback.php?delete='.$f['id'].'" class="btn btn-xs btn-danger">'."Delete !".'</a>';
                        ?>
                   </td>
               </tr>
               <?PHP } ?>
           </table>
           
           <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="feedback.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="feedback.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="feedback.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                }
               ?>
              </ul>
            </nav>
   <!-- feedbacks listed here ends-->
   
<?PHP
    }
    echo '</div>
            </div>';
    }
    include('mods/footer.php');
?>