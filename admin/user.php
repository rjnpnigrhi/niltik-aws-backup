<?PHP
    session_start();

    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');

    if(isset($_GET['add'])){
        //add user
        $user_name = $_POST['user_name'];
        $user_email = $_POST['user_email'];
        $user_mobile = $_POST['user_mobile'];
        $user_address = $_POST['user_address'];
        $user_location_id = $_POST['user_location_id'];
        $user_locality_id = $_POST['user_locality_id'];
        $user_pass = $_POST['user_pass'];
        
        $ql = "SELECT * FROM users WHERE user_mobile = '$user_mobile' LIMIT 1";
        $rl = $db->select($ql);
        
        if($rl){
            echo "This Mobile Number already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            $qi = "INSERT INTO users (user_name, user_email, user_mobile, user_address, user_pass, user_location_id, user_locality_id) VALUES ('$user_name', '$user_email', '$user_mobile', '$user_address', '$user_pass', '$user_location_id', '$user_locality_id')";
            $ri = $db->insert($qi);
            
            echo "New User has been added to our database.<br>";
            echo "Since the user is added by the admin to OTP is not required.<br>";
            echo '<a href="user.php" class="btn btn-info">'."User Management".'</a>';
        }
        
    }elseif(isset($_GET['approve'])){
        $id = $_GET['approve'];
        
        $q = "UPDATE users SET status = 'approved' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "User has been approved.<br>";
        echo '<a href="user.php" class="btn btn-info">'."User Management".'</a>';
}elseif(isset($_GET['delete'])){
        $id = $_GET['delete'];
        
        $q = "DELETE FROM users WHERE id = '$id'";
        $r = $db->delete($q);
        
        echo "User has been deleted.<br>";
        echo '<a href="user.php" class="btn btn-info">'."User Management".'</a>';
    }elseif(isset($_GET['block'])){
        $id = $_GET['block'];
        
        $q = "UPDATE users SET status = 'blocked' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "User has been blocked.<br>";
        echo '<a href="user.php" class="btn btn-info">'."User Management".'</a>';
    }else{
?>
   <div class="col-md-4">
      <h3>Add a New User</h3>
       <form action="user.php?add=true" method="post">
           <div class="form-group">
               <label for="name">Name of User <span style="color:red;">*</span></label>
               <input type="text" name="user_name" placeholder="Type the name of user" class="form-control" required>
           </div>
           <div class="form-group">
               <label for="email">e-Mail ID of User <span style="color:red;">*</span></label>
               <input type="email" name="user_email" placeholder="Type the email of user" class="form-control" required>
           </div>
           <div class="form-group">
               <label for="phone">Mobile No. <span style="color:red;">*</span></label>
               <input type="text" name="user_mobile" placeholder="Type the mobile number of user" class="form-control" required>
           </div>
           <div class="form-group">
               <label for="password">Password <span style="color:red;">*</span></label>
               <input type="text" name="user_pass" placeholder="Type the password of user" class="form-control" required>
           </div>
           <div class="form-group">
               <label for="address">Address <span style="color:red;">*</span></label>
               <textarea name="user_address" id="user_address" cols="30" rows="10" class="form-control" placeholder="User Address Here" required></textarea>
           </div>
           <div class="form-group">
               <label for="location">Location of User</label>
               <select name="user_location_id" id="location_id" class="form-control"  onChange="getStreet(this.value)">                   
                   <?PHP
                        $ql = "SELECT * FROM location WHERE city_id = 0";
                        $rl = $db->select($ql);

                        while($location = $rl->fetch_array()):
                    ?>
                    <option value="<?PHP echo $location['id']; ?>"><?PHP echo $location['location_name'] ?></option>
                    <?PHP endwhile; ?>
               </select>
           </div>
           <div class="form-group">
               <label for="location">Nearest Landmark / Street / Square</label>
               <select name="user_locality_id" id="locality_id" class="form-control">                   
                   <?PHP
                        $ql1 = "SELECT * FROM location WHERE city_id != 0";
                        $rl1 = $db->select($ql1);

                        while($street = $rl1->fetch_array()):
                    ?>
                    <option value="<?PHP echo $street['id']; ?>"><?PHP echo $street['location_name'] ?></option>
                    <?PHP endwhile; ?>
               </select>
           </div>
           
           <button class="btn btn-default" type="submit">Add New User</button>
       </form>
   </div>
   <div class="col-md-8">
       <h3>Users Database</h3>
       <?PHP
        //let us query the database for users
        $qu = "SELECT * FROM users ORDER BY id DESC";
        $ru = $db->select($qu);
        
        if(!$ru){
            
        }else{
            while($users = $ru->fetch_array()){
                $result[] = $users;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 24);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
        ?>
        <table class="table table-hover">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>e-Mail</th>
                <th>Action</th>
            </tr>
            <?PHP
                //let us get the data for this page
                foreach($data as $d){
            ?>
                <tr>
                    <td><?PHP echo $d['id']; ?></td>
                    <td><?PHP echo $d['user_name']; ?></td>
                    <td><strong><?PHP echo $d['user_mobile']; ?></strong><br>
OTP : <?PHP echo $d['otp']; ?>
</td>
                    <td><?PHP echo $d['user_email']; ?><br>
PASS : <?PHP echo $d['user_pass']; ?>
</td>
                    <td>
                    <?PHP
                        if($d['status']=="approved"){
                            echo '<a href="user.php?block='.$d['id'].'" class="btn btn-warning btn-sm">'."BLOCK".'</a>';
                        }else{
                            echo '<a href="user.php?approve='.$d['id'].'" class="btn btn-success btn-sm">'."Approve".'</a>';
                        }
                    ?>
&nbsp;<a href="user.php?delete=<?PHP echo $d['id']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </td>                    
                </tr>
            <?PHP
                }
            ?>
        </table>
       <hr>
           <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="user.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="user.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="user.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
        <?PHP
        }
        ?>
   </div>
<?PHP
    }
    }
    include('mods/footer.php');
?>