<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');

    if(isset($_GET['reply'])){
        //mark the message as replied 
        $id = $_GET['reply'];
        $q = "SELECT * FROM contacts WHERE id = '$id'";
        $r = $db->select($q);
        
        $m = $r->fetch_array();
        
?>
    <form action="contacts.php?sendmail=true" method="post">
        <div class="form-group">
            <label for="to_id">To :</label>
            <input type="text" name="to_email" value="<?PHP echo $m['email']; ?>" class="form-control">
        </div>
        <div class="form-group">
            <label for="subject">Subject :</label>
            <input type="text" name="subject" class="form-control">
        </div>
        <div class="form-group">
            <label for="message">Message :</label>
            <textarea name="message" id="message" class="form-control" cols="30" rows="10" >

User Mail Details : -<br>
                <?PHP echo $m['message']; ?>
            </textarea>
        </div>
        
        <div class="form-group">
            <button class="btn btn-default" type="submit">Submit</button>
            <input type="hidden" name="id" value="<?PHP echo $id; ?>">
        </div>
    </form>
<?PHP
    }elseif(isset($_GET['sendmail'])){
        $id = $_POST['id'];
        $to = $_POST['to_email'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];
        $headers = "From: webmaster@niltik.com" . "\r\n" .
            "BCC: support@niltik.com";

        mail($to,$subject,$message,$headers);
        
        $q = "UPDATE contacts SET status = 'replied' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "You have successfully replied to a contact message.<br>";
        echo '<a href="contacts.php">'."Message Center".'</a>';
        
        echo '<p>We have sent a OTP to your registered Mobile Phone to authenticate your ad and also to your email inbox.</p>';
        
    }else{
        $q = "SELECT * FROM contacts ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            
        }else{
            while($msgs = $r->fetch_array()){
                $result[] = $msgs;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 12);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
?>
          <table class="table table-hover table-responsive">
            <tr>
                <th width="5%">ID</th>
                <th width="20%">Name</th>
                <th width="20%">e-Mail</th>
                <th>Message</th>
                <th width="10%">Action</th>
            </tr>
            <?PHP
                //let us get the data for this page
                foreach($data as $d){
            ?>
                <tr>
                    <td><?PHP echo $d['id']; ?></td>
                    <td><?PHP echo $d['name']; ?></td>
                    <td><?PHP echo $d['email']; ?></td>
                    <td><p><?PHP echo $d['message']; ?></p></td>
                    <td>
                    <?PHP
                        if($d['status']=="replied"){
                            echo '<a href="#" class="btn btn-primary btn-sm">'."REPLIED".'</a>';
                        }else{
                            echo '<a href="contacts.php?reply='.$d['id'].'" class="btn btn-success btn-sm">'."Reply Now".'</a>';
                        }
                    ?>
                    </td>                    
                </tr>
            <?PHP
                }
            ?>
        </table>
        <hr>
           <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="contacts.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="contacts.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="contacts.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
<?PHP 
        }

    }
    }
    include('mods/footer.php');
?>