<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
        $page_total = '';
        $page_final = '';
        $page_disc = '';
        
    include('mods/header.php');
        
?>
      <div align="right">
           <button type="submit" class="btn btn-success" onclick="printDiv('print-content')">
             <i class="fa fa-print" aria-hidden="true"></i> Print Ledger Page
         </button>
      </div>
      
     <div id="print-content">    
   
       <?PHP
            //let us get all orders which are completed
            $q = "SELECT * FROM order_book WHERE status = 'complete' ORDER BY id DESC";
            $r = $db->select($q);
        
            if(!$r){
                echo "There are no Valid Transactions. <br>";                
            }else{
                while($order = $r->fetch_array()){
                    $result[] = $order;
                }

                //here we start paginating the data
                $numbers = $pagination->paginate($result, 12);

                //what are the data to be presented in these pages
                $data = $pagination->fetchresults();

                //let us get the current page number
                $pn = $pagination->page_num();

                //let us get all page numbers 
                $tp = count($numbers);
        ?>
        <h3>Cash Ledger for Niltik.com (Laundry Services) &nbsp; &nbsp; <span style="float:right;"> Page No : <?PHP echo $pn; ?> </span> </h3>
        <table class="table table-hover">
       <tr>
           <th>Order ID</th>
           <th>User Info</th>
           <th>Total Price</th>
           <th>Discounts</th>
           <th>Final Price</th>
           <th>Actions</th>
       </tr>
       <?PHP foreach($data as $d){ ?>
       <tr>
           <td>
               <?PHP 
                    echo "Order ID : " . $d['id'] . "<br>"; 
                    $timestamp = strtotime($d['posted_on']);
                   
                    $p_day = date('d', $timestamp);
                    $p_month = date('M', $timestamp);
                    $p_year = date('Y', $timestamp);
                    $p_hour = date('H', $timestamp);
                    $p_minute = date('i', $timestamp);
                    $p_second = date('s', $timestamp);

                    echo '<i class="fa fa-calendar-check-o" aria-hidden="true"></i>'. " : " . $p_day ."-". $p_month ."-". $p_year ;
               ?>
           
           </td>
           <td><?PHP 
                echo '<strong>'.$d['name'].'</strong>'."<br>";  
                echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>'. " : " . $d['email'] . "<br>";
                echo '<i class="fa fa-phone-square" aria-hidden="true"></i>'. " : " . $d['phone'] . "<br>";               
               ?>
           </td>
           <td>Rs. <?PHP echo number_format((float)$d['total_price'], 2, '.', ''); ?></td>
           <td>Coupon Code : <?PHP echo $d['coupon_code']; ?> <br>
                Discount : Rs. <?PHP echo number_format((float)$d['discount'], 2, '.', ''); ?> <br></td>
           <td>Rs. <?PHP echo number_format((float)$d['final_bill'], 2, '.', ''); ?></td>
           <td><a href="../order_details.php?id=<?PHP echo $d['id']; ?>" class="btn btn-default" title="View and Print Order" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a></td>
       </tr>
       <?PHP
                $page_total += $d['total_price'];
                $page_final += $d['final_bill'];
                $page_disc += $d['discount'];

            }
        ?>
        <tr>
            <td colspan="2" align="right">Page Total :</td>
            <td>Rs. <?PHP echo number_format((float)$page_total, 2, '.', ''); ?></td>
            <td>Discount : Rs. <?PHP echo number_format((float)$page_disc, 2, '.', ''); ?></td>
            <td style="color:RED;">Rs. <?PHP echo number_format((float)$page_final, 2, '.', ''); ?></td>
            <td></td>
        </tr>
   </table>
   <hr>
     <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="accounts.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="accounts.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="accounts.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
            <hr>
            <p>For Safety and Security of Sensitive transaction information and payments, please maintain paperback registers. Thank You. <br>
            Due to technical snags, important information may be lost and so it is wise to have hard copy of registers.
            </p>
   <?PHP } ?>
</div>
<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
        </script>
<?PHP
    }
    include('mods/footer.php');
?>