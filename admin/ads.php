<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
	
	if(isset($_GET['edit'])){
		$id = $_GET['edit'];
		
		$q = "SELECT * FROM classifieds WHERE id = '$id'";
        $r = $db->select($q);
        
        $ad = $r->fetch_array();
		
		//show form for the ad
		//AD FORM STARTS HERE
?>
	<form action="ads.php?update=true" method="post" enctype="multipart/form-data">
           <div class="form-group">
               <label for="ad_title">Ad Title <span style="color:red;">*</span></label>
               <input type="text" class="form-control" name="ad_title" placeholder="Type a Attractive Ad Title" required value="<?PHP echo $ad['ad_title']; ?>">
           </div>
           
       <div class="form-group">
           <label for="product_id">Product :</label>
           <select id="product_id2" name="product_id" class="form-control">
                <option value="">Select a Product</option>
                <?PHP
                    $q = "SELECT * FROM products WHERE parent_id = 0";
                    $r = $db->select($q);
                
                    if(!$r){
                        
                    }else{
                        while($d = $r->fetch_array()):
                ?>
                <option value="<?PHP echo $d['id']; ?>"><?PHP echo $d['product_name']; ?></option>
                <?PHP endwhile; } ?>
            </select>
            </div>

		<div class="form-group">
        <label for="sub_product">Category under Product :</label>
            <select name="sub_product_id" id="sub_product" class="form-control">
                <option value="">Chose Category</option>
            </select>
        </div>  
                        
       <div class="form-group">
       <label for="product_company">Brand or Manufacturer</label>
       <select name="product_company_id" id="brand" class="form-control">
       <option value="">Chose a Brand Name</option>
       </select>
       </div>    
                        
       <div class="form-group">
       <label for="description">Description / Details of the Ad <span style="color:red;">*</span></label>
       <textarea name="ad_details" id="ad_details" class="form-control" cols="30" rows="10" required><?PHP echo $ad['ad_details']; ?></textarea>
       </div>
                        
       <div class="form-group">
       <label for="price">Price <span style="color:red;">*</span></label>
       <input type="text" name="price" class="form-control" value="<?PHP echo $ad['price']; ?>" required>
<p class="help-block">Please type only Numbers, Do not type Negotiable or other words.</p>
       </div>
                        
       <div class="form-group">
       <label for="name">Name <span style="color:red;">*</span></label>
       <input type="text" name="name" class="form-control" placeholder="Your Complete Name" required value="<?PHP echo $ad['name']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="email">email <span style="color:red;">*</span></label>
       <input type="text" name="email" class="form-control" placeholder="Your Complete email" required value="<?PHP echo $ad['email']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="mobile">Mobile No <span style="color:red;">*</span></label>
       <input type="text" name="mobile" class="form-control" placeholder="Your Mobile Number" required value="<?PHP echo $ad['phone']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id1" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                                    
       <div class="form-group">
       <label for="Address">Address <span style="color:red;">*</span></label>
       <textarea name="address" id="address" class="form-control" cols="30" rows="10"><?PHP echo $ad['address']; ?></textarea>
       </div>
       <input type="hidden" name="id" value="<?PHP echo $id; ?>" />
       <button class="btn btn-default" type="submit">Update Ad</button>
<a href="javascript:history.back()" class="btn btn-info">Cancel and Go Back</a>
                        
       </form>
<?PHP
		//AD FORM ENDS HERE
	}elseif(isset($_GET['update'])){
		//update the ad
		$id = $_POST['id'];
		$ad_title = $_POST['ad_title'];
        $product_id = $_POST['product_id'];
        
        if(!isset($_POST['sub_product_id'])){
            $sub_product_id = '';
        }else{
            $sub_product_id = $_POST['sub_product_id'];
        }
        
        if(!isset($_POST['product_company_id'])){
            $product_company_id = '';
        }else{
            $product_company_id = $_POST['product_company_id'];
        }
        $ad_details = $_POST['ad_details'];
        $price = $_POST['price'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $location_id = $_POST['location_id'];
        date_default_timezone_set('Asia/Kolkata');

        // Then call the date functions
        $posted_on = date('Y-m-d H:i:s');

        if(!isset($_POST['locality_id'])){
            $locality_id = '';
        }else{
            $locality_id = $_POST['locality_id'];
        }
        
        $address = $_POST['address'];
		
		if($sub_product_id ==''){
            $prod_id = $product_id;
        }else{
            $prod_id = $sub_product_id;
        }
        
        if($locality_id ==''){
            $loc_id = $location_id;
        }else{
            $loc_id = $locality_id;
        }
		
		$q = "UPDATE classifieds SET ad_title = '$ad_title', product_id = '$product_id', ad_details = '$ad_details', price = '$price', name = '$name', email = '$email', phone = '$mobile', location_id = '$loc_id', address = '$address' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "The ad has been updated Successfully. <br>";
        echo '<a href="ads.php" class="btn btn-info">'."Back to Classified Ads Page".'</a>';
		
	}elseif(isset($_GET['approve'])){
        $id = $_GET['approve'];
        
        $q = "SELECT * FROM classifieds WHERE id = '$id'";
        $r = $db->select($q);
        
        $ad = $r->fetch_array();
        
        //let us find the product
        $prod_id = $ad['product_id'];
        $q3 = "SELECT * FROM products WHERE id = '$prod_id'";
        $r3 = $db->select($q3);
        
        $prod = $r3->fetch_array();
        
        //let us find location
        $location = $ad['location_id'];
        $location = explode(",", $location);

        $location_id = $location[0];
        $locality_id = $location[1];
        
        $q1 = "SELECT * FROM location WHERE id = '$location_id'";
        $r1 = $db->select($q1);
        if(!$r1){
		}else{
        $loc = $r1->fetch_array();
		}
        
        //let us find the locality
        $q2 = "SELECT * FROM location WHERE id = '$locality_id'";
        $r2 = $db->select($q2);
        if(!$r2){
            
        }else{
            $st = $r2->fetch_array();
        }
        echo '<h3>'.$ad['ad_title']. "&nbsp; &nbsp; &nbsp; &nbsp; Posted in (".ucwords($prod['product_name']).")".'</h3>';
        echo '<p>'.$ad['name']." | ".$ad['email']." | ".$ad['phone']." | ".$ad['address'].'</p>';
        echo "<hr>";
        echo '<p>'.$ad['ad_details'].'</p>';
        echo '<p><strong>'."Price : Rs. ".$ad['price'].".00".'</strong></p>';
        echo '<p>'."Location : ".$loc['location_name']." &nbsp; &nbsp; &nbsp; &nbsp; Locality / Street : ".$st['location_name'].'</p>';
        echo "<hr>";
        $otp = $ad['otp'];
        $otp2 = $ad['otp2'];
        $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
        $r3 = $db->select($q3);
                                  
        if(!$r3){
        echo "NO Images Uploaded.<br>";
        }else{
        while($img = $r3->fetch_array()):
        
        echo '<img src="../uploads/'.$img['image'].'" style="width:120px; float:left; margin:0 10px 0 0;">';
            
        endwhile;
        }
            
        echo '<div class="clearfix"></div>';
        echo "<hr>";
        if($ad['status']== "approved"){
            echo '<a href="ads.php?block='.$ad['id'].'" class="btn btn-info">'."Block this Ad".'</a>';
        }else{
            echo '<a href="ads.php?approve_final='.$ad['id'].'" class="btn btn-info">'."Approve this Ad".'</a>';
        }
         echo " | ".'<a href="ads.php?edit='.$ad['id'].'" class="btn btn-default">'."EDIT THIS AD".'</a>';
         echo " | ".'<a href="ads.php?delete='.$ad['id'].'" class="btn btn-danger">'."DELETE THIS AD".'</a>';
        echo " | ".'<a href="javascript:history.back()" class="btn btn-default">'."Back to Ads Management Page".'</a>';
    }elseif(isset($_GET['approve_final'])){
        $id = $_GET['approve_final'];
        
        $q = "UPDATE classifieds SET status = 'approved' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "Classified Ad has been approved.<br>";
        echo '<a href="ads.php" class="btn btn-info">'."Ads Management".'</a>';
        //stop the ad 
    
    }elseif(isset($_GET['block'])){
        $id = $_GET['block'];
        
        $q = "UPDATE classifieds SET status = 'blocked' WHERE id = '$id'";
        $r = $db->update($q);
        
        echo "Classified Ad has been blocked.<br>";
        echo '<a href="ads.php" class="btn btn-info">'."Ads Management".'</a>';
        //stop the ad 
    }elseif(isset($_GET['delete'])){
        $id = $_GET['delete'];
        
        $q = "DELETE FROM classifieds WHERE id = '$id'";
        $r = $db->delete($q);
        
        echo "Classified Ad has been DELETED.<br>";
        echo '<a href="ads.php" class="btn btn-info">'."Ads Management".'</a>';
        //stop the ad 
        
    }else{
        $q = "SELECT * FROM classifieds ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo '<h3>'."No Classified Ads in Database".'</h3>';
        }else{
            while($ads = $r->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 24);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers);
?>
          <table class="table table-hover">
            <tr>
                <th width="5%">ID</th>
                <th width="5%">Product</th>
                <th width="20%">Title of Ad</th>
                <th width="20%">Posted by :</th>
                <th width="5%">Action</th>
            </tr>
            <?PHP
                //let us get the data for this page
                foreach($data as $d){
            ?>
                <tr>
                    <td><?PHP echo $d['id']; ?></td>
                    <td>
                    <?PHP 
                    
                    $product_id =  $d['product_id']; 
                    $qp = "SELECT * FROM products WHERE id = '$product_id'";
                    $rp = $db->select($qp);
                        if(!$rp){
                            
                        }else{
                    $product = $rp->fetch_array();
                    
                    echo ucfirst($product['product_name']);
                    ?>                    
                    </td>
                    <td><a href="ads.php?approve=<?PHP echo $d['id']; ?>" class="btn btn-default"><?PHP echo ucfirst($d['ad_title']); ?></a>
                    <br>
                    <?PHP 
                            echo $d['posted_on']; 
                            echo '<br>';
                            if($d['na']=="want"){
                                echo '<span style="font-weight:bold; color:red;">'."NILTIK ASSURED WANTED".'</span>';
                            }else{
                                echo "";
                            }
                        
                        ?>
                    </td>
                    <td>
                    <?PHP echo $d['name']."<br>".$d['email']."<br>".$d['phone']."<br>".$d['address']; ?></td>
                    <td>
                    <?PHP
                        if($d['status']=="approved"){
                            echo '<a href="#" class="btn btn-primary btn-sm">'."Approved".'</a>';
                        }elseif($d['status']=="blocked"){
                            echo '<a href="#" class="btn btn-danger btn-sm">'."Blocked".'</a>';
                        }else{
                            echo '<a href="ads.php?approve='.$d['id'].'" class="btn btn-success btn-sm">'."Approve Now".'</a>';
                        }
                    ?>
&nbsp; <a href="ads.php?delete=<?PHP echo $d['id']; ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
&nbsp; <a href="ads.php?edit=<?PHP echo $d['id']; ?>" class="btn btn-warning btn-sm"><i class="fa fa-cog" aria-hidden="true"></i></a>
                    </td>                    
                </tr>
            <?PHP
                }
                }
            ?>
        </table>
        <hr>
          
           <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="ads.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="ads.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="ads.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
<?PHP 
        }
    }
    }
    include('mods/footer.php');
?>