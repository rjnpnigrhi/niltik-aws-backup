<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/functions.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');

        if(isset($_GET['update'])){
            //we will show a form and ask for an option
            $order_id = $_GET['update'];
?>
   <div class="col-md-3 hidden-xs">
       
   </div>
    <div class="col-md-6 col-xs-12">   
   
        <?PHP
            //let us get the order details first
            $order = get_order_by_id($order_id);
            
            echo '<h3>'."Order No : " . $order_id . '</h3>';
            echo $order['name'] . "<br>";
            echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>'. " : " . $order['email'] . "<br>";
            echo '<i class="fa fa-phone-square" aria-hidden="true"></i>'. " : " . $order['phone'] . "<br><br>";
                
            $timestamp = strtotime($order['posted_on']);
                   
            $p_day = date('d', $timestamp);
            $p_month = date('M', $timestamp);
            $p_year = date('Y', $timestamp);
            $p_hour = date('H', $timestamp);
            $p_minute = date('i', $timestamp);
            $p_second = date('s', $timestamp);
                
            echo '<i class="fa fa-calendar-check-o" aria-hidden="true"></i>'. " : " . $p_day ."-". $p_month ."-". $p_year . " - ";
            echo '<i class="fa fa-clock-o" aria-hidden="true"></i>'. " : " . $p_hour ."-". $p_minute ."-". $p_second."<br><br>";
            ?>
                Total Price : Rs. <?PHP echo number_format((float)$order['total_price'], 2, '.', ''); ?> <br>
                Coupon Code : <?PHP echo $order['coupon_code']; ?> <br>
                Discount : Rs. <?PHP echo number_format((float)$order['discount'], 2, '.', ''); ?> <br>
                Final Bill : Rs. <?PHP echo number_format((float)$order['final_bill'], 2, '.', ''); ?> <br><br>
            <?PHP
            
            if($order['status'] == 'pending'){
                //show options to update to processing
                
                echo "Click to Update status to Under Processing when Pickup of article is ready. <br><br>";
                
                echo '<a href="orders.php?order_process='.$order_id.'" class="btn btn-default">'."Order Under Processing".'</a>'."<br><br>";
                
                echo "After the processing of the order is over you can update to completion.<br><br>";
                
                echo '<a href="orders.php" class="btn btn-info">'."Back to Orders Page".'</a>';
            }elseif($order['status'] == 'processing'){
                //show option for updating to complete order
                
                echo "Click to Update status to Complete Order when order is delivered. <br><br>";
                echo '<a href="orders.php?order_complete='.$order_id.'" class="btn btn-default">'."Order Completed".'</a>'."<br><br>";
                
                echo "This will also send a thank you SMS to the user.<br><br>";
                                
                echo '<a href="orders.php" class="btn btn-info">'."Back to Orders Page".'</a>';
                
            }else{
                //say the order has been completed.
                
                echo "Hurray !!! We have successfully completed the Order. <br><br>";
                
                echo '<a href="orders.php" class="btn btn-info">'."Back to Orders Page".'</a>';
            }
            
        ?>
     
    </div> 
    <div class="col-md-3 hidden-xs">
        
    </div>
<?PHP
        }elseif(isset($_GET['order_process'])){
            echo '<div class="col-md-3 hidden-xs"></div>';
            echo '<div class="col-md-6 col-xs-12">';
            $order_id = $_GET['order_process'];
            
            process_order($order_id);
            
            echo "Order has been put on pickup and delivery process.<br><br>";
            echo '<a href="orders.php" class="btn btn-info">'."Back to Order Book".'</a>';
            echo '</div>';
            echo '<div class="col-md-3 hidden-xs"></div>';
        }elseif(isset($_GET['order_complete'])){
            echo '<div class="col-md-3 hidden-xs"></div>';
            echo '<div class="col-md-6 col-xs-12">';
            $order_id = $_GET['order_complete'];
            
            //let us update the table and put up the status as complete
            complete_order($order_id); 
                
            $order = get_order_by_id($order_id);
            
            //sms sending script here
                $tokens = explode(" ", $order['name']);
                $name_sms = $tokens[0];
                $phone = $order['phone'];
                //API Details
            
                //Create API URL
                $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$phone&msg=Dear%20$name_sms%2C%20Your%20$order_id%20has%20been%20completed.%20Thank%20you%20for%20availing%20our%20online%20Laundry%20Services.%20Admin&route=T";

                //Call API
                $ch = curl_init($fullapiurl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); 
                //echo $result ; // For Report or Code Check
                curl_close($ch);
                //email sending script here
                $from = "support@niltik.com";
                $to = $order['email'];
                $subject = "Your Online Laundry Order no : $order_no is now complete";
                $message = "Dear ".$order['name']. ", Thank you for availing our services. Your Online Laundry Order no : $order_id has been successfully completed. \r\n Operations Manager \r\n NILTIK.com";
                $headers = "From: support@niltik.com" . "\r\n" .
                    "CC: webmaster@niltik.com";

                mail($to,$subject,$message,$headers);
            
            echo "We have successfully completed an Order.<br><br>";
            echo '</div>';
            echo '<div class="col-md-3 hidden-xs"></div>';
            
        }elseif(isset($_GET['send_otp'])){
            echo '<div class="col-md-3 hidden-xs"></div>';
            echo '<div class="col-md-6 col-xs-12">';
            $order_id = $_GET['send_otp'];
            
            $order = get_order_by_id($order_id);
            
            echo '<h3>'."Order No : " . $order_id . '</h3>';
            echo $order['name'] . "<br>";
            echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>'. " : " . $order['email'] . "<br>";
            echo '<i class="fa fa-phone-square" aria-hidden="true"></i>'. " : " . $order['phone'] . "<br><br>";
                
            $timestamp = strtotime($order['posted_on']);
                   
            $p_day = date('d', $timestamp);
            $p_month = date('M', $timestamp);
            $p_year = date('Y', $timestamp);
            $p_hour = date('H', $timestamp);
            $p_minute = date('i', $timestamp);
            $p_second = date('s', $timestamp);
                
            echo '<i class="fa fa-calendar-check-o" aria-hidden="true"></i>'. " : " . $p_day ."-". $p_month ."-". $p_year . " - ";
            echo '<i class="fa fa-clock-o" aria-hidden="true"></i>'. " : " . $p_hour ."-". $p_minute ."-". $p_second."<br><br>";
            ?>
                Total Price : Rs. <?PHP echo number_format((float)$order['total_price'], 2, '.', ''); ?> <br>
                Coupon Code : <?PHP echo $order['coupon_code']; ?> <br>
                Discount : Rs. <?PHP echo number_format((float)$order['discount'], 2, '.', ''); ?> <br>
                Final Bill : Rs. <?PHP echo number_format((float)$order['final_bill'], 2, '.', ''); ?> <br><br>
            <?PHP
            echo "If there is a Delay in providing service you can send a SMS to the user.<br>";
            echo "Click on the Button Given below and we will send a SMS to the user intimating him of delay.<br>";
            
            echo "<br>";
            
            echo '<strong>'."This is what the user will receive.".'</strong>'."<br>";
            
            echo "Dear ".$order['name'].", Due to technical snag, pickup / delivery of your articles of Order No : ".$order_id." has been delayed. Inconvenience regretted. Admin <br><br>";
            
            echo '<a href="orders.php?send_sms='.$order_id.'" class="btn btn-default">'."Send SMS to User".'</a>';
            echo '</div>';
            echo '<div class="col-md-3 hidden-xs"></div>';
            
        }elseif(isset($_GET['send_sms'])){
            echo '<div class="col-md-3 hidden-xs"></div>';
            echo '<div class="col-md-6 col-xs-12">';
            $order_id = $_GET['send_sms'];
            
            $order = get_order_by_id($order_id);
            
            //sms sending script here
            $name = $order['name'];
            $tokens = explode(" ", $name);
            $name_sms = $tokens[0];
            $phone = $order['phone'];
            //API Details
            
            //Create API URL
            $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$phone&msg=Dear%20$name_sms%2C%20Due%20to%20technical%20snag%2C%20pickup%20%2F%20delivery%20of%20your%20articles%20of%20Order%20No%20%3A%20$order_id%20has%20been%20delayed.%20Inconvenience%20regretted.%20Admin&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);
            
            
            //let us send a email too
            $from = "support@niltik.com";
            $to = $order['email'];
            $subject = "Your Online Laundry Order no : $order_no Will be delayed";
            $message = "Dear ".$order['name']. "\r\n \r\n Thank you for availing our services. Due to some technical / environmental issues, Your Online Laundry Order no : $order_id has been delayed. \r\n \r\n We are sorry for the inconvenience caused. \r\n \r\n Operations Manager \r\n NILTIK.com";
            $headers = "From: support@niltik.com" . "\r\n" .
                    "CC: webmaster@niltik.com";

            mail($to,$subject,$message,$headers);
            
            echo "SMS sent successfully to User to inform about the delay.<br>";
            echo '<a href="orders.php" class="btn btn-info">'."Back to Order Book".'</a>';
            echo '</div>';
            echo '<div class="col-md-3 hidden-xs"></div>';
            
        }else{
            //let us get all orders here
            $q = "SELECT * FROM order_book ORDER BY id DESC";
            $r = $db->select($q);

            if(!$r){
                echo '<h3>'."No Orders Are there in Database".'</h3>';
            }else{
                while($ads = $r->fetch_array()){
                    $result[] = $ads;
                }

                //here we start paginating the data
                $numbers = $pagination->paginate($result, 12);

                //what are the data to be presented in these pages
                $data = $pagination->fetchresults();

                //let us get the current page number
                $pn = $pagination->page_num();

                //let us get all page numbers 
                $tp = count($numbers);
?>
    <h3>Orders Placed On Site for Laundry Services</h3>
     <table class="table table-hover">
         <tr>
             <th width="5%">ID</th>
             <th>Name</th>
             <th width="20%">User Info</th>
             <th width="20%">Address</th>
             <th width="20%">Invoice Info</th>
             <th width="15%">Options</th>
         </tr>
         <?PHP
            foreach($data as $d){      
        ?>
         <tr>
             <td><?PHP echo $d['id']; ?></td>
             <td><?PHP echo $d['name']; ?></td>
             <td>
             <?PHP 
               
                echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>'. " : " . $d['email'] . "<br>";
                echo '<i class="fa fa-phone-square" aria-hidden="true"></i>'. " : " . $d['phone'] . "<br>";
                
                $timestamp = strtotime($d['posted_on']);
                   
                $p_day = date('d', $timestamp);
                $p_month = date('M', $timestamp);
                $p_year = date('Y', $timestamp);
                $p_hour = date('H', $timestamp);
                $p_minute = date('i', $timestamp);
                $p_second = date('s', $timestamp);
                
                echo '<i class="fa fa-calendar-check-o" aria-hidden="true"></i>'. " : " . $p_day ."-". $p_month ."-". $p_year . "<br>";
                echo '<i class="fa fa-clock-o" aria-hidden="true"></i>'. " : " . $p_hour ."-". $p_minute ."-". $p_second;
             ?>
             
             </td>
             <td><?PHP echo $d['address']; ?></td>
             <td>
                Total Price : Rs. <?PHP echo number_format((float)$d['total_price'], 2, '.', ''); ?> <br>
                Coupon Code : <?PHP echo $d['coupon_code']; ?> <br>
                Discount : Rs. <?PHP echo number_format((float)$d['discount'], 2, '.', ''); ?> <br>
                Final Bill : Rs. <?PHP echo number_format((float)$d['final_bill'], 2, '.', ''); ?>
                 
             </td>
             <td>
                 <a href="orders.php?update=<?PHP echo $d['id']; ?>" class="btn btn-default" title="View and Update Status"><i class="fa fa-edit" aria-hidden="true"></i></a>
                 <a href="orders.php?send_otp=<?PHP echo $d['id']; ?>" class="btn btn-default" title="Send SMS to the User"><i class="fa fa-comment-o " aria-hidden="true"></i></a>
                 <a href="../order_details.php?id=<?PHP echo $d['id']; ?>" class="btn btn-default" title="View and Print Order" target="_blank"><i class="fa fa-print" aria-hidden="true"></i></a> <br><br>
                 <?PHP
                    if($d['status'] == 'pending'){
                 ?>
                 <a href="#VIEW" class="btn btn-danger" title="View and Print Order">Order <?PHP echo ucfirst($d['status']); ?></a>
                 <?PHP }elseif($d['status'] == 'processing'){ ?>
                 <a href="#VIEW" class="btn btn-primary" title="View and Print Order">Order <?PHP echo ucfirst($d['status']); ?></a>                 
                 <?PHP }else{ ?>
                 <a href="#VIEW" class="btn btn-success" title="View and Print Order">Order <?PHP echo ucfirst($d['status']); ?></a>                 
                 <?PHP } ?>
             </td>
         </tr>
         <?PHP 
            } 
            }
         ?>
     </table>  
     <hr>
     <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="orders.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="orders.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="orders.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
     
<?PHP
    }
    }
    include('mods/footer.php');
?>