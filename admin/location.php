<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
<a href="location.php" class="btn btn-default">List of Locations</a>
  <br><hr>
<?PHP
    if(isset($_GET['add'])){
        //add the location
        $location = $_POST['location'];
        $city_id = $_POST['city_id'];
        
        $ql = "SELECT * FROM location WHERE location_name = '$location' LIMIT 1";
        $rl = $db->select($ql);
        
        if($rl){
            echo "This location already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //insert the location into the table
            $qli = "INSERT INTO location (city_id, location_name) VALUES ('$city_id', '$location')";
            $rli = $db->insert($qli);
            
            echo "New Location has been added to the list on database.<br />";
            echo '<a href="location.php" class="btn btn-default btn-sm">'."Back to Location List View".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete the information from database
        $id = $_GET['delete'];
                
        $qld = "DELETE FROM location WHERE id = '$id'";
        $rld = $db->delete($qld);
        
        echo "The location has been deleted from our database.<br>";
        echo '<a href="location.php" class="btn btn-default btn-sm">'."Back to Location List View".'</a>';
    
    }else{
        //show all locations
?>
   <div class="col-md-6">
      <h3>Add a new Location (City / Town) </h3>
       <form action="location.php?add=true" method="post">
           <div class="form-group">
               <label for="location">Location</label>
               <input type="text" class="form-control" name="location" placeholder="Type a new location" required>
           </div>
           <div class="form-group">
               <label for="city_id">Parent City</label>
               <select name="city_id" id="city_id" class="form-control">
                   <option value="0">No Parent City</option>
                   <?PHP
                    $ql = "SELECT * FROM location WHERE city_id = 0";
                    $rl = $db->select($ql);

                    while($location = $rl->fetch_array()):
                   ?>
                   <option value="<?PHP echo $location['id']; ?>"><?PHP echo $location['location_name']; ?></option>
                   <?PHP endwhile; ?>
               </select>
           </div>
           <button class="btn btn-default" type="submit">Add New Location</button>
       </form>
   </div>
   <div class="col-md-6">
      <h3>All Locations We currently support</h3>
       <table class="table table-hover table-responsive" width="100%">
           <tr>
               <th width="5%">ID</th>
               <th>Name of Town</th>
               <th>Streets</th>
               <th width="15%">Action</th>
           </tr>
           <?PHP
            $ql = "SELECT * FROM location WHERE city_id = 0";
            $rl = $db->select($ql);
        
            while($location = $rl->fetch_array()):
           ?>
           <tr>
               <td><?PHP echo $location['id']; ?></td>
               <td><?PHP echo ucwords($location['location_name']); ?></td>
               <td></td>
               <td>
                   <?PHP
                        if($location['id']==1){
                            
                        }else{
                            echo '<a href="location.php?delete='.$street['id'].'" class="btn btn-info btn-xs">'."Delete".'</a>';
                        }
                    ?>
               </td>               
           </tr>
           
           
           <?PHP 
            $city_id = $location['id'];
            $ql1 = "SELECT * FROM location WHERE city_id = '$city_id'";
            $rl1 = $db->select($ql1);
            if($rl1){
            while($street = $rl1->fetch_array()):
        ?>
          <tr>
               <td><?PHP echo $street['id']; ?></td>
               <td>&nbsp; &nbsp; &nbsp; &nbsp; |____</td>
               <td><?PHP echo ucwords($street['location_name']); ?></td>
               <td><a href="location.php?delete=<?PHP echo $street['id']; ?>" class="btn btn-info btn-xs">Delete</a></td>               
           </tr>
           <?PHP
            endwhile; }
            endwhile; } ?>
       </table>
   </div>
<?PHP
    }
    include('mods/footer.php');
?>