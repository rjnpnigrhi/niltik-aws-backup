<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
<a href="companies.php" class="btn btn-default">List of Product Companies</a>
  <br><hr>
<?PHP
    if(isset($_GET['add'])){
        //add the location
        $company_name = $_POST['company_name'];
        $product_id = $_POST['product_id'];
        
        $ql = "SELECT * FROM product_company WHERE company_name = '$company_name' LIMIT 1";
        $rl = $db->select($ql);
        
        if($rl){
            echo "This company name already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //insert the location into the table
            $qli = "INSERT INTO product_company (company_name, product_id) VALUES ('$company_name', '$product_id')";
            $rli = $db->insert($qli);
            
            echo "New Company has been added to the list on database.<br />";
            echo '<a href="companies.php" class="btn btn-default btn-sm">'."Back to Company List View".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete the information from database
        $id = $_GET['delete'];
        
        $qld = "DELETE FROM product_company WHERE id = '$id'";
        $rld = $db->delete($qld);
        
        echo "The Company name has been deleted from our database.<br>";
        echo '<a href="companies.php" class="btn btn-default btn-sm">'."Back to Products Company List View".'</a>';
    
    }else{
        //show all locations
?>
   <div class="col-md-6">
      <h3>Add a new Company for Product </h3>
       <form action="companies.php?add=true" method="post">
           <div class="form-group">
               <label for="company">Company Name</label>
               <input type="text" class="form-control" name="company_name" placeholder="Type a new Company Name" required>
           </div>
           
           <div class="form-group">
               <label for="product_id">Under Which Product ?</label>
               <select name="product_id" id="product_id" class="form-control">
                  <?PHP
                    $ql = "SELECT * FROM products WHERE parent_id = 0";
                    $rl = $db->select($ql);

                    while($product = $rl->fetch_array()):
                  ?>
                   <option value="<?PHP echo $product['id']; ?>"><?PHP echo $product['product_name']; ?></option>
                   <?PHP endwhile; ?>
               </select>
           </div>
           <button class="btn btn-default" type="submit">Add New Company</button>
       </form>
   </div>
   <div class="col-md-6">
      <h3>List of Companies</h3>
       <table class="table table-hover table-responsive" width="100%">
           <tr>
               <th width="5%">ID</th>
               <th>Name</th>
               <th>Products</th>
               <th width="15%">Action</th>
           </tr>
           <?PHP
            $ql = "SELECT * FROM product_company ORDER BY product_id ASC";
            $rl = $db->select($ql);
        
            while($company = $rl->fetch_array()):
            //let us get the product name
            $product_id = $company['product_id'];
            $qpl = "SELECT * FROM products WHERE id = '$product_id'";
            $rpl = $db->select($qpl);
            if($rpl){
            $product = $rpl->fetch_array();
           ?>
           <tr>
               <td><?PHP echo $company['id']; ?></td>
               <td><?PHP echo ucwords($company['company_name']); ?></td>
               <td><?PHP echo ucwords($product['product_name']); ?></td>
               <td><a href="companies.php?delete=<?PHP echo $company['id']; ?>" class="btn btn-info btn-xs">Delete</a></td>               
           </tr>
           
           <?PHP
            }
                endwhile; } ?>
       </table>
   </div>
<?PHP
    }
    include('mods/footer.php');
?>