<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
<a href="products.php" class="btn btn-default">List of Products</a>
  <br><hr>
<?PHP
if(isset($_GET['delete'])){
$id = $_GET['delete'];

$q = "DELETE FROM products WHERE id = '$id'";
$r = $db->delete($q);

echo "The Product category has been successfully Deleted.<br>";
echo '<a href="products.php" class="btn btn-success btn-lg">'."Back to Product Page".'</a>';
}elseif(isset($_GET['add'])){
        //add the location
        $product_name = $_POST['product_name'];
        $parent_id = $_POST['parent_id'];
        
        $ql = "SELECT * FROM products WHERE product_name = '$product_name' LIMIT 1";
        $rl = $db->select($ql);
        
        if($rl){
            echo "This product already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //insert the location into the table
            $qli = "INSERT INTO products (product_name, parent_id) VALUES ('$product_name','$parent_id')";
            $rli = $db->insert($qli);
            
            echo "New product has been added to the list on database.<br />";
            echo '<a href="products.php" class="btn btn-default btn-sm">'."Back to Location List View".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete the information from database
        $id = $_GET['delete'];
        
        
        //before deleting check if child products are there ?
        $ql = "SELECT * FROM products WHERE parent_id = '$id'";
        $rl = $db->select($ql);
        
        if($rl){
            echo "There are child categories under this product.<br>";
            echo "Are you sure you want to delete this ?<br><br>";
            echo '<a href="products.php?delete_final='.$id.'" class="btn btn-warning">'."Yes, Delete this !".'</a>'." | ".'<a href="javascript:history.back()" class="btn btn-success">'."No, Do not delete this !".'</a>';
        }else{
        
        $qld = "DELETE FROM products WHERE id = '$id'";
        $rld = $db->delete($qld);
        
            //let us update the ads where product was this.
            $qu = "UPDATE classified SET product_id = 12 WHERE product_id = '$id'";
            $ru = $db->update($qu);
            
            //we need to delete the companies from our list too
            $qu1 = "DELETE FROM product_company WHERE product_id = '$id'";
            $ru1 = $db->delete($qu1);
            
            
        echo "The product has been deleted from our database.<br>";
        echo "The ads which were placed under this category has been shifted to OTHER category.<br>";
        echo '<a href="products.php" class="btn btn-default btn-sm">'."Back to Products List View".'</a>';
        }
    }elseif(isset($_GET['delete_final'])){
        $id = $_GET['delete_final']    ;
        
        $qu = "UPDATE products SET parent_id = 12 WHERE parent_id = '$id'";
        $ru = $db->update($qu);
        
        //let us update the ads where product was this.
        $qu1 = "UPDATE classified SET product_id = 12 WHERE product_id = '$id'";
        $ru1 = $db->update($qu1);
        
        $qld = "DELETE FROM products WHERE id = '$id'";
        $rld = $db->delete($qld);
        
        //we need to delete the companies from our list too
        $qu2 = "DELETE FROM product_company WHERE product_id = '$id'";
        $ru2 = $db->delete($qu2);
        
        echo "The product has been deleted from our database.<br>";
        echo "The sub-categories, placed under this category has been shifted to OTHER category.<br>";
        echo '<a href="products.php" class="btn btn-default btn-sm">'."Back to Products List View".'</a>';
    }else{
        //show all locations
?>
   <div class="col-md-6">
      <h3>Add a new Location (City / Town) </h3>
       <form action="products.php?add=true" method="post">
           <div class="form-group">
               <label for="product">Product Name</label>
               <input type="text" class="form-control" name="product_name" placeholder="Type a new Product Name" required>
           </div>
           
           <div class="form-group">
               <label for="parent_id">Parent Product</label>
               <select name="parent_id" id="parent_id" class="form-control">
                 <option value="0">No Parent Category</option>
                  <?PHP
                    $ql = "SELECT * FROM products WHERE parent_id = 0";
                    $rl = $db->select($ql);

                    while($product = $rl->fetch_array()):
                   ?>
                   <option value="<?PHP echo $product['id']; ?>"><?PHP echo $product['product_name']; ?></option>
                   <?PHP endwhile; ?>
               </select>
           </div>
           
           <button class="btn btn-default" type="submit">Add New Product</button>
       </form>
   </div>
   
   <div class="col-md-6">
      <h3>All Products We currently support</h3>
       <table class="table table-hover table-responsive" width="100%">
           <tr>
               <th width="5%">ID</th>
               <th>Product Name</th>
               <th>Sub-Product</th>
<th>Actions</th>
           </tr>
           <?PHP
            $ql = "SELECT * FROM products WHERE parent_id = 0";
            $rl = $db->select($ql);
        
            while($product = $rl->fetch_array()):
        
           ?>
           <tr>
               <td><?PHP echo $product['id']; ?></td>
               <td><strong style="color:red;"><?PHP echo ucwords($product['product_name']); ?></strong></td>
              
                <td></td>
                         <td></td> 
           </tr>
           
           <?PHP
            $parent_id = $product['id'];
            $qlc = "SELECT * FROM products WHERE parent_id = '$parent_id'";
            $rlc = $db->select($qlc);
        if($rlc){
            while($child = $rlc->fetch_array()):
        
        ?>
        <tr>
               <td><?PHP echo $child['id']; ?></td>
               <td> &nbsp; &nbsp; &nbsp; &nbsp;       |___ <?PHP echo ucwords($product['product_name']); ?> - ></td>
              
                <td><strong><i><?PHP echo ucwords($child['product_name']); ?></i></strong></td> 
<td><a href="products.php?delete=<?PHP echo $child['id']; ?>">DELETE</a></td>             
           </tr>
        <?PHP
        
            endwhile; }
            endwhile; } ?>
       </table>
   </div>
<?PHP
    }
    include('mods/footer.php');
?>