<?PHP
    session_start();
    if(!isset($_SESSION['admin'])){
        
        header('Location: login.php');

    }else{
    include('../inc/config.php');    
    include('../inc/db_conn.php');
    include('../inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination(); 
    
    include('mods/header.php');
?>
<a href="productcategories.php" class="btn btn-default">List of Sub - Products</a>
  <br><hr>
<?PHP
    if(isset($_GET['add'])){
        //add the location
        $product_category = $_POST['product_category'];
        $product_id = $_POST['product_id'];
        
        $ql = "SELECT * FROM product_category WHERE product_category = '$product_category' LIMIT 1";
        $rl = $db->select($ql);
        
        if($rl){
            echo "This product category already exists in the database.<br />";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            //insert the location into the table
            $qli = "INSERT INTO product_category (product_category, product_id) VALUES ('$product_category', '$product_id')";
            $rli = $db->insert($qli);
            
            echo "New product category has been added to the list on database.<br />";
            echo '<a href="productcategories.php" class="btn btn-default btn-sm">'."Back to product category List View".'</a>';
        }
    }elseif(isset($_GET['delete'])){
        //delete the information from database
        $id = $_GET['delete'];
        
        $qld = "DELETE FROM product_category WHERE id = '$id'";
        $rld = $db->delete($qld);
        
        echo "The product category has been deleted from our database.<br>";
        echo '<a href="productcategories.php" class="btn btn-default btn-sm">'."Back to Products Category List View".'</a>';
    
    }else{
        //show all locations
?>
   <div class="col-md-6">
      <h3>Add a new Product (Sub Category of Product) </h3>
       <form action="productcategories.php?add=true" method="post">
           <div class="form-group">
               <label for="product">Product Category Name</label>
               <input type="text" class="form-control" name="product_category" placeholder="Type a new Product Name" required>
           </div>
           
           <div class="form-group">
               <label for="product_id">Under Which Product ?</label>
               <select name="product_id" id="product_id" class="form-control">
                  <?PHP
                    $ql = "SELECT * FROM products";
                    $rl = $db->select($ql);

                    while($product = $rl->fetch_array()):
                  ?>
                   <option value="<?PHP echo $product['id']; ?>"><?PHP echo $product['product_name']; ?></option>
                   <?PHP endwhile; ?>
               </select>
           </div>
           <button class="btn btn-default" type="submit">Add New Product</button>
       </form>
   </div>
   <div class="col-md-6">
      <h3>All Products categories We currently support</h3>
       <table class="table table-hover table-responsive" width="100%">
           <tr>
               <th width="5%">ID</th>
               <th>Name</th>
               <th>Main Product</th>
               <th width="15%">Action</th>
           </tr>
           <?PHP
            $ql = "SELECT * FROM product_category order by product_id ASC";
            $rl = $db->select($ql);
        
            while($product = $rl->fetch_array()):
            //let us get the product name
            $product_id = $product['product_id'];
            $qpl = "SELECT * FROM products WHERE id = '$product_id'";
            $rpl = $db->select($qpl);
        
            $mainproduct = $rpl->fetch_array();
           ?>
           <tr>
               <td><?PHP echo $product['id']; ?></td>
               <td><?PHP echo ucwords($product['product_category']); ?></td>
               <td><?PHP echo ucwords($mainproduct['product_name']); ?></td>               
               <td><a href="productcategories.php?delete=<?PHP echo $product['id']; ?>" class="btn btn-info btn-xs">Delete</a></td>               
           </tr>
           <?PHP endwhile; } ?>
       </table>
   </div>
<?PHP
    }
    include('mods/footer.php');
?>