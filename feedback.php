<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row">
        <h1>User Feedback on Niltik.com</h1>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?PHP
               if(isset($_GET['add'])){
        //we will add the form here
            ?>
            <h1>Add Your Feedback Here</h1>
          <hr>
           <form method="post" action="feedback.php?add_final=true" enctype="multipart/form-data">
              <div class="form-group">
              <label>Your Name :</label>
              <input type="text" class="form-control" name="name" placeholder="Your Name Please" value="<?PHP if(isset($name)){ echo $name;} ?>"> 
              </div>
              
              <div class="form-group">
              <label>Your e-Mail ID :</label>
              <input type="text" class="form-control" name="email" placeholder="Type your valid e-Mail ID" value="<?PHP if(isset($email)){ echo $email;} ?>"> 
              </div>
              
              <div class="form-group">
              <label>Subject :</label>
              <input type="text" class="form-control" name="subject" placeholder="Type the subject of your feedback" value="<?PHP if(isset($subject)){ echo $subject;} ?>"> 
              </div>
              
              <div class="form-group">
              <label>Feedback :</label>
              <textarea class="form-control" name="feedback" id="textarea1" rows="15"><?PHP if(isset($feedback)){ echo $feedback;} ?></textarea>
              </div>
              
              <button type="submit" class="btn btn-default">Submit</button>
              <a href="feedback.php" class="btn btn-warning">Go Back !</a>
            </form>
   <!-- add a new article form ends -->
            
            <?PHP
                    echo '<a href="feedback.php" class="btn btn-success pull-right">'."Feedback Page".'</a>';
                   echo "<br><br>";
                }elseif(isset($_GET['add_final'])){
                   $name = $_POST['name'];
                   $email = $_POST['email'];
                   $subject = $_POST['subject'];
                   $feedback = $_POST['feedback'];
                   
                   if($name==''||$email==''||$subject==''||$feedback==''){
                       echo "All fields must be filled before submitting the form.<br />";
                       echo '<a href="javascript:history.back()">'."Go Back and Try Again".'</a>';
                   }else{
                       $q = "INSERT INTO feedback (name, email, subject, feedback) VALUES ('$name', '$email', '$subject', '$feedback')";
                       $r = $db->insert($q);
                       
                       if($r){
                           echo "Thank you for submitting your feedback.<br />";
                           echo "We will post your feedback on the site soon.<br />";
                           echo '<a href="feedback.php" class="btn btn-success">'."Feedback Page".'</a>';
                       }else{
                           echo "Your Feedback may have been posted already, wait a few hours before submitting again.<br />";
                           echo '<a href="feedback.php" class="btn btn-success pull-right">'."Feedback Page".'</a>';
                       }
                   }
               }else{
                   
               ?>
             <a href="feedback.php?add=true" class="btn btn-info pull-right">Add Your Feedback</a>
             <br><br>
              <?PHP
               
                $q = "SELECT * FROM feedback WHERE status = 'publish' ORDER BY id DESC";
                $r = $db->select($q);

                if(!$r){

                }else{

                while($rows = $r->fetch_array()){
                    $result[] = $rows;
                }

                //this is where we ask for the pagination
                //how many pages are there in the result set 
                $numbers = $pagination->paginate($result, 12);

                //what are the data to be presented in these pages
                $data = $pagination->fetchresults();

                //let us get the current page number
                $pn = $pagination->page_num();

                //let us get all page numbers
                $tp = count($numbers);
               //let us get the data for the page
                    foreach ($data as $f){ 
               ?>
               <div class="row thumbnail">
                   <h2 style="color:RED;"><?PHP echo ucfirst($f['subject']); ?></h2>
                   <p class="small">Posted by <?PHP echo ucfirst($f['name']); ?>, 
                   on 25, May 2016</p>
                   <hr>
                   <p><?PHP echo ucfirst(nl2br($f['feedback'])); ?></p>
               </div>
               <?PHP } ?>        

               <nav>
              <ul class="pagination">
                <?PHP
               
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="feedback.php?page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="feedback.php?page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="feedback.php?page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                }
               }
               ?>
              </ul>
            </nav>
              <div class="clearfix"></div>
           </div>
           <div class="col-md-4">
               Here is an Ad SPace
           </div>
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>