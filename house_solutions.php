<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row text-center">
        <h1>Niltik.com House for Rent Solutions </h1>
        <h2>Coming up soon</h2>
    </div>
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>