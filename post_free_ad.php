<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
    echo '<div class="pull-right"><span class="francois blink_me" title="Niltik Assured is Coming Up Soon !!!">'."NILTIK Assured".'</span></div>';

ini_set('memory_limit', -1);

class ImgResizer {
  private $originalFile = '';
  public function __construct($originalFile = '') {
      $this -> originalFile = $originalFile;
  }
  public function resize($newWidth, $targetFile) {
      if (empty($newWidth) || empty($targetFile)) {
          return false;
      }
      $src = imagecreatefromjpeg($this -> originalFile);
      list($width, $height) = getimagesize($this -> originalFile);
      $newHeight = ($height / $width) * $newWidth;
      $tmp = imagecreatetruecolor($newWidth, $newHeight);
      imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
      if (file_exists($targetFile)) {
          unlink($targetFile);
      }
      imagejpeg($tmp, $targetFile, 85); // 85 is my choice, make it between 0 – 100 for output image quality with 100 being the most luxurious
  }
}

    if(isset($_GET['add_new'])){
        $ad_title = $_POST['ad_title'];
        $product_id = $_POST['product_id'];
        
        if(!isset($_POST['sub_product_id'])){
            $sub_product_id = '';
        }else{
            $sub_product_id = $_POST['sub_product_id'];
        }
        
        if(!isset($_POST['product_company_id'])){
            $product_company_id = '';
        }else{
            $product_company_id = $_POST['product_company_id'];
        }
        $ad_details = $_POST['ad_details'];
        $price = $_POST['price'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
        $location_id = $_POST['location_id'];
        date_default_timezone_set('Asia/Kolkata');

        // Then call the date functions
        $posted_on = date('Y-m-d H:i:s');

        if(!isset($_POST['locality_id'])){
            $locality_id = '';
        }else{
            $locality_id = $_POST['locality_id'];
        }
        
        $address = $_POST['address'];
        
        if(isset($_POST['na']) && $_POST['na'] == 'YES'){
            $na = "want";
        }else{
            $na = "";
        }

        $otp = mt_rand(100000, 999999);
        $_SESSION['otp'] = $otp;
        $otp2 = mt_rand(10000000, 99999999);

        //are there images uploaded with this form
                if(isset($_FILES['images']['name'])){

	define("MAX_SIZE", "500000");
	
	for($i=0; $i<count($_FILES['images']['tmp_name']); $i++){
		$size = filesize($_FILES['images']['tmp_name'][$i]);
		
		if($size == 0){
			//if there is no image the size will be zero 
			//we will do nothing in such case
		}elseif($size < (MAX_SIZE*1024)){
			$path = "uploads/";
			$imagename = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			
			list($txt, $ext) = explode(".", $imagename);
			
			date_default_timezone_set ("Asia/Kolkata");
			$currentdate = date("d M Y");
			
			$file = time().substr(str_replace("", "_", $txt), 0);
			$info = pathinfo($file);
			$filename = $file.".".$ext;
			
			if(move_uploaded_file($_FILES['images']['tmp_name'][$i], $path.$filename)){
$work = new ImgResizer($path.$filename); 
$work -> resize(960, $path.$filename);
				$qm = "INSERT INTO uploads (image, otp, otp2, posted_on) VALUES ('$filename', '$otp', '$otp2', '$currentdate')";
				$rm = $db->insert($qm);
				
				if($rm){
					echo "Files Uploaded Successfully.<br>";
				}
			}else{
				echo "Files could not be inserted into the folder.<br>";
			}
		}else{
			echo "The size of the file is bigger than allowed.<br>";
		}
	}

}

  $tokens = explode(" ", $name);
  $name_sms = $tokens[0];
//API Details
            
            //Create API URL
            $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$mobile&msg=Dear%20$name_sms%2C%20thanks%20for%20posting%20an%20Ad%20on%20our%20site.%20Your%20OTP%20for%20Confirmation%20is%20$otp.%20Admin.&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);

if($sub_product_id ==''){
            $prod_id = $product_id;
        }else{
            $prod_id = $sub_product_id;
        }
        
        if($locality_id ==''){
            $loc_id = $location_id;
        }else{
            $loc_id = $locality_id;
        }

        if($ad_title ==''){
            echo "There is some error and Your ad May not have been updated on our server. Please try again.<br>";
            echo '<a href="post_free_ad.php" class="btn btn-primary btn-sm">'."Go Back to Ad Posting Page".'</a>';
            exit;
        }
        //let us insert this into our database
        $q = "INSERT INTO classifieds (ad_title, product_id, product_company_id, ad_details, price, name, email, phone, location_id, address, otp, otp2, na, posted_on) VALUES ('$ad_title', '$prod_id', '$product_company_id', '$ad_details', '$price', '$name', '$email', '$mobile', '$loc_id', '$address','$otp','$otp2', '$na', '$posted_on')";
        $r = $db->insert($q);
                
        //we will send the OTP to both the email ID and also to the mobile
        $from = "support@niltik.com";
        $to = $email;
        $subject = "Authentication code for Ad on NILTIK.com";
        $message = "Dear ".$name. ", please quote this OTP : ". $otp ." on site to approve your ad. NILTIK.com";
        $headers = "From: webmaster@niltik.com" . "\r\n" .
            "CC: support@niltik.com";

            mail($to,$subject,$message,$headers);
        
        echo '<p>We have sent a OTP to your registered Mobile Phone to authenticate your ad and also to your email inbox.</p>';
        
?>
    <h3>Your authentication code</h3>
    <p>Please check the mobile number you submitted. We have send a ONE TIME PASSWORD to that. Type that OTP to confirm your registration.</p>
    <form action="post_free_ad.php?confirmation=true" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="otp">Authentication Code</label>
            <input type="text" name="otp" placeholder="6 digit OTP code" required>
            <input type="hidden" name="otp2" value="<?PHP echo $otp2; ?>">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Confirm</button>&nbsp; &nbsp; <a href="post_free_ad.php?send_otp=<?PHP echo $otp2; ?>&user_mobile=<?PHP echo $mobile; ?>" class="btn btn-default">Click Here to Resend OTP</a>
        </div>
        <p class="help-block">If you did not receive the OTP on your mobile, please check your mail id for a mail from support@niltik.com</p>
    </form>
<?PHP
    }elseif(isset($_GET['authenticate'])){
?>
    <h3>Your authentication code</h3>
    <p>Please check the mobile number you submitted.<br> We have send a ONE TIME PASSWORD to that. <br> Type that OTP to confirm your registration.</p>
    <form action="post_free_ad.php?confirmation=true" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="otp">Authentication Code</label>
            <input type="text" name="otp" placeholder="6 digit OTP code" required>
            <input type="hidden" name="otp2" value="<?PHP echo $_GET['authenticate']; ?>">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Confirm</button>&nbsp; &nbsp; <a href="post_free_ad.php?send_otp=<?PHP echo $otp2; ?>&user_mobile=<?PHP echo $mobile; ?>" class="btn btn-default">Click Here to Resend OTP</a>
        </div>
        <p class="help-block">If you still did not receive the OTP on your mobile, please check your mail id for a mail from webmaster@niltik.com</p>
    </form>
<?PHP
     }elseif(isset($_GET['send_otp'])){
            $user_mobile = $_GET['user_mobile'];
            $otp2 = $_GET['send_otp'];

            $otp = $_SESSION['otp'];

            $tokens = explode(" ", $user_name);
            $name = $tokens[0];
            //API Details
            
            //Create API URL
$fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$mobile&msg=Dear%20$name%2C%20thanks%20for%20posting%20an%20Ad%20on%20our%20site.%20Your%20OTP%20for%20Confirmation%20is%20$otp.%20Admin.&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);

            echo "Your OTP has been sent to ".$user_mobile."<br> Please allow sometime for the SMS to reach your phone. <br>";

            echo '<a href="post_free_ad.php?authenticate='.$otp2.'" class="btn btn-default">'."Click here to Authenticate".'</a>';

    }elseif(isset($_GET['confirmation'])){
        $otp = $_POST['otp'];
        $otp2 = $_POST['otp2'];
        
        $q = "SELECT * FROM classifieds WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
            $r = $db->select($q);
            
            if($r){
                //let us confirm the profile
                $q1 = "UPDATE classifieds SET status = 'approved' WHERE otp2 = '$otp2'";
                $r1 = $db->update($q1);
                
                echo "Thank you for validating your advertisement placement.<br>";
                echo "You can now view the ad on our website.<br>";
        
        $res = $r->fetch_array();
        $name = $res['name'];
        $email = $res['email'];
        $tokens = explode(" ", $user_name);
        $name_sms = $tokens[0];
        $phone = $res['phone'];
        $id = $res['id'];
            
        $tokens = explode(" ", $name);
            $name_sms = $tokens[0];
        //let us send a sms to the user
        //Create API URL
        $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$phone&msg=Dear%20$name_sms%2C%20your%20ad%20$id%20is%20now%20Live!%20View%20Ad%20http%3A%2F%2Fwww.niltik.com%2Fview_product.php%3Fid%3D$id%2C%20Edit%20or%20Delete%20from%20your%20panel.%20Login%20%2F%20Register%20now!&route=T";

        //Call API
        $ch = curl_init($fullapiurl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch); 
        //echo $result ; // For Report or Code Check
        curl_close($ch);
        //let us send an email to the user
        $from = "support@niltik.com";
        $to = $email;
        $subject = "Your Ad is now online on NILTIK.com";
        $message = "Dear ".$name. ", It gives us pleasure to intimate you that your advertisement is now online on Niltik.com. \r\n View Your Ad Here \r\n http://www.niltik.com/view_product.php?id=$id \r\n You can Edit, Block or Delete the Ad as per your wish only by logging in to your member panel. \r\n Please join NILTIK.com if you have not joined yet. Or login to your member panel and update your ad.\r\n Operations Manager, \r\n NILTIK.com";
        $headers = "From: support@niltik.com" . "\r\n" .
            "CC: webmaster@niltik.com";

            mail($to,$subject,$message,$headers);        
                
            }

    }else{
?>
    <div class="row">
        <h1>Post a Free Ad </h1>
        <div class="row">
        <div class="col-sm-6">
           <form action="post_free_ad.php?add_new=true" id="post_free_ad" method="post" enctype="multipart/form-data"  onsubmit="ShowLoading()">
           <div class="form-group">
               <label for="ad_title">Ad Title <span style="color:red;">*</span></label>
               <input type="text" class="form-control" name="ad_title" placeholder="Type a Attractive Ad Title" required>
           </div>
           
       <div class="form-group">
           <label for="product_id">Product :</label>
           <select id="product_id1" name="product_id" class="form-control">
                <option value="">Select a Product</option>
                <?PHP
                    $q = "SELECT * FROM products WHERE parent_id = 0";
                    $r = $db->select($q);
                
                    if(!$r){
                        
                    }else{
                        while($d = $r->fetch_array()):
                ?>
                <option value="<?PHP echo $d['id']; ?>"><?PHP echo $d['product_name']; ?></option>
                <?PHP endwhile; } ?>
            </select>
            </div>

<div class="form-group">
        <label for="sub_product">Category under Product :</label>
            <select name="sub_product_id" id="sub_product" class="form-control">
                <option value="">Chose Category</option>
            </select>
        </div>  
                        
       <div class="form-group">
       <label for="product_company">Brand or Manufacturer</label>
       <select name="product_company_id" id="brand" class="form-control">
       <option value="">Chose a Brand Name</option>
       </select>
       </div>    
                        
       <div class="form-group">
       <label for="description">Description / Details of the Ad <span style="color:red;">*</span></label>
       <textarea name="ad_details" id="ad_details" class="form-control" cols="30" rows="10" required></textarea>
       </div>
                        
       <div class="form-group">
       <label for="price">Price <span style="color:red;">*</span></label>
       <input type="text" name="price" class="form-control" placeholder="Only Numbers i.e. 2000" required>
<p class="help-block">Please type only Numbers, Do not type Negotiable or other words.</p>
       </div>
                        
       <div class="form-group">
       <label for="name">Name <span style="color:red;">*</span></label>
       <input type="text" name="name" class="form-control" placeholder="Your Complete Name" required value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['name']; } ?>">
       </div>
                        
       <div class="form-group">
       <label for="email">email <span style="color:red;">*</span></label>
       <input type="text" name="email" class="form-control" placeholder="Your Complete email" required value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['email']; } ?>">
       </div>
                        
       <div class="form-group">
       <label for="mobile">Mobile No <span style="color:red;">*</span></label>
       <input type="text" name="mobile" class="form-control" placeholder="Your Mobile Number" required value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['phone']; } ?>">
       </div>
                        
       <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                                    
       <div class="form-group">
       <label for="Address">Address <span style="color:red;">*</span></label>
       <textarea name="address" id="address" class="form-control" cols="30" rows="10"></textarea>
       </div>
	<p class="help-block">Please Have patience, the images may take sometime to UPLOAD. If you are posting images directly from your mobile camera, it may take upto few minutes to upload. So Have patience and do not refresh the page.</p>   
	   <!--let us try to put some image uploaders here--> 
       <div class="form-group">
           <label for="image upload">Image 1</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_1').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_1" alt="your image" width="100" height="100" />
       </div>
       
         <div class="form-group">
           <label for="image upload">Image 2</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_2').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_2" alt="your image" width="100" height="100" />
       </div>
         
       <div class="form-group">
           <label for="image upload">Image 3</label> 
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_3').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_3" alt="your image" width="100" height="100" />
       </div>
          
          <div class="form-group">
           <label for="image upload">Image 4</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_4').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_4" alt="your image" width="100" height="100" />
       </div>
         
          <div class="form-group">
           <label for="image upload">Image 5</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_5').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_5" alt="your image" width="100" height="100" />
       </div>

<div class="checkbox">
        <label>
            <input type="checkbox" name="na" value="YES"> Are You Interested in <a data-toggle="modal" data-target="#myModal" title="NILTIK Assured" target="_blank">NILTIK Assured</a> ???
        </label>
        </div>
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">NILTIK Assured</h4>
	</div>
	<div class="modal-body text-left">
	
	<ul>
	<li>We Ensure sale of Your Products (* For a small FEE).</li>
	<li>Let our experts evaluate your product and fix a price.</li>
    <li>Evaluation can be done in our office or at your place.</li>
    <li>Large or Immovable products can be evaluated on spot.</li>
	<li>We will sell your product within a fixed duration.</li>
    <li>Do not wait for someone to click and buy your product.</li>
	<li>If we can not sale, we will refund your fee.</li>
	<li>Currently we assure Bikes, Vehicles, Mobiles, Computers only.</li>
	<li>We will add more categories soon...!!!</li>
	</ul>
	</div>

	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
	</div>
	</div>
	</div>
      <!--MODAL ENDS HERE-->

       <p class="help-block">You will receive an ONE TIME PASSWORD (OTP) on your mobile and in email when you submit the form. That OTP will be required to confirm your ad submission.</p>
                        
       <button class="btn btn-default" type="submit" id="submit_form">Submit Ad</button>
                        
       </form>
       </div>
       <div class="col-sm-6">
           <a href="post_house_ad.php"><img src="img/sqad_02.gif" class="img-responsive" /></a>
       </div>
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript">
        function ShowLoading(e) {
            var div = document.createElement('div');
            var img = document.createElement('img');
            img.src = 'img/loading-3.gif';
            div.innerHTML = "Uploading Ad...<br />";
            div.style.cssText = 'position: fixed; top: 5%; left: 10%; z-index: 5000; width: 240px; text-align: center; background: #ffcc00; border: 1px solid #CCCCCC; line-height:2.3em; padding:20px;';
            div.appendChild(img);
            document.body.appendChild(div);
            return true;
            // These 2 lines cancel form submission, so only use if needed.
            //window.event.cancelBubble = true;
            //e.stopPropagation();
        }
    </script>
    
<?PHP
    }

ini_restore('memory_limit');

    include("mods/trending_ads.php");

    include("mods/footer.php");
?>