<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    if(isset($_GET['submit'])){
        $otp = mt_rand(100000, 999999);
        $otp2 = mt_rand(10000000, 99999999);
        
        define("MAX_SIZE", "500000");
        
        if(isset($_FILES['images']['name'])){
            $count = count($_FILES['images']['name']);
            
            for($i = 0; $i<$count; $i++){
                $name = $_FILES['images']['name'][$i];
                $size = $_FILES['images']['size'][$i];
                
                if($size == 0){
                    
                }elseif($size < (MAX_SIZE*1024)){
                    
                
                $ext = end(explode(".", $name));
                $allowed_ext = array("jpg", "jpeg", "png");
                    
                date_default_timezone_set ("Asia/Kolkata");
			    $currentdate = date("d M Y");
                    
                if(in_array($ext, $allowed_ext)){
                        $new_image= '';
                        $new_name = md5(rand()) . '.' . $ext;
                        $path = 'uploads/' . $new_name;
                        list($width, $height) = getimagesize($_FILES['images']['tmp_name'][$i]);
                        
                        if($ext == 'png'){
                            $new_image = imagecreatefrompng($_FILES['images']['tmp_name'][$i]);
                        }
                            
                        if($ext == 'jpg' || $ext == 'jpeg'){
                            $new_image = imagecreatefromjpeg($_FILES['images']['tmp_name'][$i]);
                        }
                            
                        $new_width = 480;
                        $new_height = ($height/$width) * 480;
                        
                        $tmp_image = imagecreatetruecolor($new_width, $new_height);
                        imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                            
                        imagejpeg($tmp_image, $path, 60);
                        imagedestroy($new_image);
                        imagedestroy($tmp_image);
                            
                        //we are left with $new_name we include in database
                        $qm = "INSERT INTO uploads (image, otp, otp2, posted_on) VALUES ('$new_name', '$otp', '$otp2', '$currentdate')";
				        $rm = $db->insert($qm);
                    
                        }else{
                            echo "It is not a valid Image File.<br>";
                        }
                        
                    }else{
                        echo "The file size is too big for uploading.<br>";
                        
                        
                    }
                }
            }
        
    }else{
?>
    <div class="row">
        <h1>About Niltik.com </h1>
        <form action="img_upload.php?submit=true" method="post" enctype="multipart/form-data">
            <!--let us try to put some image uploaders here--> 
        <div class="form-group">
           <label for="image upload">Image 1</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_1').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_1" alt="your image" width="100" height="100" />
       </div>
       
         <div class="form-group">
           <label for="image upload">Image 2</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_2').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_2" alt="your image" width="100" height="100" />
       </div>
         
       <div class="form-group">
           <label for="image upload">Image 3</label> 
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_3').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_3" alt="your image" width="100" height="100" />
       </div>
          
          <div class="form-group">
           <label for="image upload">Image 4</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_4').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_4" alt="your image" width="100" height="100" />
       </div>
         
          <div class="form-group">
           <label for="image upload">Image 5</label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_5').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_5" alt="your image" width="100" height="100" />
       </div>
       
       <button class="btn btn-default" type="submit">Submit Ad</button>
        </form>
        
    </div>
    
<?PHP
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>