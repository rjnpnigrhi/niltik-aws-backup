<?PHP
    session_start();
if(isset($_SESSION['id'])){
        
        header('Location: user.php');

    }else{
    
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    
        
        if(isset($_GET['register'])){
            //show registration form
?>
   <div class="row">
    <div class="col-md-6">
    <h2>Please sign up here : </h2>               
        <form action="login.php?add_new=true" method="post">
        <div class="form-group">
            <label for="email">Your Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
        </div>
        <div class="form-group">
            <label for="email">Mobile No.</label>
            <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" required>
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password1" id="password1" placeholder="Password" required>
        </div>
        <div class="form-group">
            <label for="password">Confirm Password</label>
            <input type="password" class="form-control" name="password2" id="password2" placeholder="Password" required>
        </div>
                      
        <div class="checkbox">
        <label>
            <input type="checkbox" required> Agree to <a href="tos.php" title="terms and conditions of niltik.com" target="_blank">Terms &amp; Conditions</a> of Niltik.com
        </label>
        </div>
            <button type="submit" class="btn btn-default">Create Account</button> &nbsp; &nbsp;
            <a href="login.php" class="btn btn-default">Log In</a>
        </form>
    </div>
   <div class="col-md-6">
       <a href="http://www.smartvisionplus.in" target="_blank" title="Smart Vision Plus"><img src="img/svp_sqad.jpg" alt="Smart Vision Plus"></a>
   </div>
<?PHP
        }elseif(isset($_GET['forgotpass'])){
            //show forgot password form
?>
    <div class="row">
     <div class="col-md-6">
    <h2>Type your email ID : </h2>               
       <p>Please type the same email ID that you used during registration.</p>
        <form action="login.php?sendpass=true" method="post">
        
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" name="user_email" id="email" placeholder="Email" required>
        </div>
        
            <button type="submit" class="btn btn-default">Send Password</button>&nbsp; &nbsp;
            <a href="login.php" class="btn btn-default">Log In</a>
        </form>
    </div>
   <div class="col-md-6">
       <a href="http://www.smartvisionplus.in" target="_blank" title="Smart Vision Plus"><img src="img/svp_sqad.jpg" alt="Smart Vision Plus"></a>
   </div>  
<?PHP
        }elseif(isset($_GET['add_new'])){
            $user_name = $_POST['name'];
            $user_email = $_POST['email'];
            $user_mobile = $_POST['mobile'];
            $user_pass1 = $_POST['password1'];
            $user_pass2 = $_POST['password2'];
            
            if($user_pass1 !== $user_pass2){
                echo "Both passwords do not match.<br>";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
            }
            
            $q1 = "SELECT * FROM users WHERE user_mobile = '$user_mobile' LIMIT 1";
            $r1 = $db->select($q1);
            
            if($r1){
                echo "This mobile Number is already in our users list.<br>";
                echo "If you have forgotten login details visit login page and request for a password.<br>";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';  
            }else{
                
            $otp = mt_rand(100000, 999999);
            $_SESSION['otp'] = $otp;

  $tokens = explode(" ", $user_name);
  $name = $tokens[0];
//API Details
            
            //Create API URL
            $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$user_mobile&msg=Dear%20$name%2C%20thanks%20for%20Registering%20on%20our%20site.%20Your%20OTP%20for%20Confirmation%20is%20$otp.%20Check%20email%20too.%20Admin.&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);
            
            $q = "INSERT INTO users (user_name, user_mobile, user_email, user_pass, otp) VALUES ('$user_name', '$user_mobile', '$user_email', '$user_pass1', '$otp')";
            $r = $db->insert($q);           

            $to = $user_email;
            $subject = "Your confirmation code for NILTIK.com";
            $message = "Dear ".$user_name."\r\n Please enter this six digit code in the confirmation page on NILTIK.com to confirm and authenticate your registration.\r\n \r\n OTP Code : ".$otp."\r\n \r\n If you could not confirm or wish to confirm later, please visit this link and we would do that for you. \r\n http://www.niltik.com/login.php?authenticate=".$user_mobile." \r\n WebMaster \r\n NILTIK.com";
            $headers = "From: webmaster@niltik.com" . "\r\n" .
            "CC: support@niltik.com";

            mail($to,$subject,$message,$headers);            
            
            }
?>
    <h3>Your authentication code</h3>
    <p>Please check the mobile number you submitted.<br> We have send a ONE TIME PASSWORD to that.<br> Type that OTP to confirm your registration.</p>
    <form action="login.php?confirmation=true" method="post">
        <div class="form-group">
            <label for="otp">Authentication Code</label>
            <input type="text" name="otp" placeholder="6 digit OTP code" required>
            <input type="hidden" name="user_mobile" value="<?PHP echo $user_mobile; ?>">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Confirm</button> &nbsp; &nbsp; <a href="login.php?send_otp=<?PHP echo $user_mobile; ?>&user_name=<?PHP echo $user_name; ?>" class="btn btn-default">Click Here to Resend OTP</a>
        </div>
        <p class="help-block">If you did not receive the OTP on your mobile, please check your mail id for a mail from support@niltik.com</p>
<p>If You did not receive the OTP in our MOBILE, please contact us, Do not hit the back button.</p>
    </form>
<?PHP                
         }elseif(isset($_GET['send_otp'])){
            $user_name = $_GET['user_name'];
            $user_mobile = $_GET['send_otp'];

            $otp = $_SESSION['otp'];

            $tokens = explode(" ", $user_name);
            $name = $tokens[0];
            //API Details
            
            //Create API URL
            $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$user_mobile&msg=Dear%20$name%2C%20thanks%20for%20Registering%20on%20our%20site.%20Your%20OTP%20for%20Confirmation%20is%20$otp.%20Check%20email%20too.%20Admin.&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);

            echo "Your OTP has been sent to ".$user_mobile."<br> Please allow sometime for the SMS to reach your phone. <br>";

            echo '<a href="login.php?authenticate='.$user_mobile.'" class="btn btn-default">'."Click here to Authenticate".'</a>';
              
        }elseif(isset($_GET['authenticate'])){
?>
       <h2>Authenticate Your Registration</h2>
        <form action="login.php?confirmation=true" method="post">
        <div class="form-group">
            <label for="otp">Authentication Code</label>
            <input type="text" name="otp" placeholder="6 digit OTP code" required>
            <input type="hidden" name="user_mobile" value="<?PHP echo $_GET['authenticate']; ?>">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Confirm</button>
        </div>
        <p class="help-block">If you did not receive the OTP on your mobile, please check your mail id for a mail from support@niltik.com</p>
    </form>  
<?PHP
        }elseif(isset($_GET['confirmation'])){
            $otp = $_POST['otp'];
            $user_mobile = $_POST['user_mobile'];
            
            $q = "SELECT * FROM users WHERE otp = '$otp' AND user_mobile = '$user_mobile' LIMIT 1";
            $r = $db->select($q);
            
            if($r){
                //let us confirm the profile
                $q1 = "UPDATE users SET status = 'approved' WHERE user_mobile = '$user_mobile'";
                $r1 = $db->update($q1);
                
                echo "Thank you for authenticating your user profile.<br>";
                echo "You can now log in to your panel.<br>";
                
                echo '<a href="login.php" class="btn btn-default">'."Log In Here".'</a>';
                
            }else{
echo "Sorry, the authentication code (OTP) is not valid.<br>";
echo "Please contact us if you find any difficulty in logging in.<br>";

}
            
            
        }elseif(isset($_GET['sendpass'])){
            $user_email = $_POST['user_email'];
            
            $q = "SELECT * FROM users WHERE user_email = '$user_email'";
            $r = $db->select($q);
            
            if(!$r){
                echo "The e-Mail ID you typed is not in our database.<br> ";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
            }else{
                $d = $r->fetch_array();
                $user_pass = $d['user_pass'];
                $user_name = $d['user_name'];
                
                $to = $user_email;
                $subject = "Your Login credentials for NILTIK.com";
                $message = "Dear ".$user_name."\r\n Here is your password for logging in to NILTIK.com website.\r\n Please do not share this with anyone.\r\n \r\n Password : ".$user_pass."\r\n \r\n Thank You<br> Web Master,\r\n NILTIK.com";
                $headers = "From: webmaster@niltik.com" . "\r\n" .
                "CC: support@niltik.com";

                mail($to,$subject,$message,$headers); 
                
                echo "Your password has been sent to your email ID.<br>";
                echo "Check your mail inbox and also junk folder or spam folder for a mail from webmaster@niltik.com<br>";
            }
            
        }elseif(isset($_GET['login'])){
            //login the user
$email = $db->real_escape_string($_POST['email']);
            $pass = $db->real_escape_string($_POST['password']);
            
            $q = "SELECT * FROM users WHERE user_email = '$email' AND user_pass = '$pass' LIMIT 1";
            $r = $db->select($q);
            
            if(!$r){
                echo "Your Log in credentials are incorrect.<br>";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
            }else{
            
                $user = $r->fetch_array();
                $status = $user['status'];
                
                if($status !== "approved"){
                    echo "Your account has not been approved.<br>";
                    echo "Please contact support@niltik.com for help.<br>";
                }else{
                
                $_SESSION['id'] = $user['id'];
                $_SESSION['name'] = $user['user_name'];
                $_SESSION['email'] = $user['user_email'];
                $_SESSION['address'] = $user['user_address'];
                $_SESSION['phone'] = $user['user_mobile'];
		$_SESSION['user'] = $user['id'];
                $_SESSION['logged_in'] = TRUE;
                
                echo "You have been successfully logged in.<br>";
                echo '<a href="user.php" class="btn btn-default btn-sm">'."User Dashboard".'</a>';
                }
            }
        
        }else{
        
?>
   <div class="row">
    <div class="col-md-6">
    <h2>Please login here : </h2>               
        <form class="form-horizontal" method="post" action="login.php?login=true">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
            </div>                      
                      
            <div class="form-group">
                <button type="submit" class="btn btn-default">Sign in</button>
            </div>
                      
            <div class="form-group">                        
                <p class="help-block">
                <a href="login.php?register=true">Create New Account</a> &nbsp; | &nbsp; <a href="login.php?forgotpass=true">Forgot Password ?</a>                              
                </p>
            </div>
            </form> 
</div>
   <div class="col-md-6">
       <a href="http://www.smartvisionplus.in" target="_blank" title="Smart Vision Plus"><img src="img/svp_sqad.jpg" alt="Smart Vision Plus"></a>
   </div>
    
<?PHP
    }
    }

    include("mods/trending_ads.php");

    include("mods/footer.php");
?>