<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

?>
    <div class="row">
      
        <h1>Contact Niltik.com </h1>
        <hr>
 <div class="col-md-6">
<?PHP
if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'NILTIK.com Contact Us From'; 
		$to = 'support@niltik.com'; 
		$subject = 'Message from Contact us on NILTIK.com';
		
		$body = "From: $name\n E-Mail: $email\n Message:\n $message";
 
		// Check if email has been entered and is valid
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
            echo "The email ID you entered is invalid. <br>";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
		}
		
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			echo "The confirmation code result is not correct.<br>";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
		}else{
            
            $q = "INSERT INTO contacts (name, email, message) VALUES ('$name', '$email', '$message')";
            $r = $db->insert($q);
            
            echo "<p>We have received your message, and we will contact you soon.<br>";
            echo "If you are in a hurry do give us a call at 8908264076.</p>";
            
            $headers = "From: webmaster@niltik.com" . "\r\n" .
            "CC: support@niltik.com";

            mail($to,$subject,$message,$headers);             
            
        }

	}else{        
?>
        <form class="form-horizontal" role="form" method="post" action="contact_us.php">
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label">Name</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" required value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['name']; } ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-10">
			<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" required value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['email']; } ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="message" class="col-sm-2 control-label">Message</label>
		<div class="col-sm-10">
			<textarea class="form-control" rows="4" name="message" required></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer" required>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
		</div>
	</div>
</form> 
      <?PHP } ?>
      </div>
       <div class="col-md-6">
<p>Our Contact Address : </p>
           <h2>Niltik.com</h2>
<p>
Head Office : <br>
Arabinda Nagar, 1st Lane<br>
Berhampur, 760001<br>
Ganjam, Odisha<br><br>

Phone : 8908264076 / 7008584789 <br>
e-Mail ID : support@niltik.com
</p>
       </div>
    
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>