<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

ini_set('memory_limit', -1);

class ImgResizer {
  private $originalFile = '';
  public function __construct($originalFile = '') {
      $this -> originalFile = $originalFile;
  }
  public function resize($newWidth, $targetFile) {
      if (empty($newWidth) || empty($targetFile)) {
          return false;
      }
      $src = imagecreatefromjpeg($this -> originalFile);
      list($width, $height) = getimagesize($this -> originalFile);
      $newHeight = ($height / $width) * $newWidth;
      $tmp = imagecreatetruecolor($newWidth, $newHeight);
      imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
      if (file_exists($targetFile)) {
          unlink($targetFile);
      }
      imagejpeg($tmp, $targetFile, 85); // 85 is my choice, make it between 0 – 100 for output image quality with 100 being the most luxurious
  }
}

    $num = '';
    if(isset($_GET['add_new'])){
        $otp = $_POST['otp'];
        $otp2 = $_POST['otp2'];
                
        if(isset($_FILES['images']['name'])){

	define("MAX_SIZE", "500000");
	
	for($i=0; $i<count($_FILES['images']['tmp_name']); $i++){
		$size = filesize($_FILES['images']['tmp_name'][$i]);
		
		if($size == 0){
			//if there is no image the size will be zero 
			//we will do nothing in such case
		}elseif($size < (MAX_SIZE*1024)){
			$path = "uploads/";
			$imagename = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			
			list($txt, $ext) = explode(".", $imagename);
			
			date_default_timezone_set ("Asia/Kolkata");
			$currentdate = date("d M Y");
			
			$file = time().substr(str_replace("", "_", $txt), 0);
			$info = pathinfo($file);
			$filename = $file.".".$ext;
			
			if(move_uploaded_file($_FILES['images']['tmp_name'][$i], $path.$filename)){
$work = new ImgResizer($path.$filename); 
$work -> resize(960, $path.$filename); 
				$qm = "INSERT INTO uploads (image, otp, otp2, posted_on) VALUES ('$filename', '$otp', '$otp2', '$currentdate')";
				$rm = $db->insert($qm);
				
				if($rm){
					echo "Files Uploaded Successfully.<br>";
				}
			}else{
				echo "Files could not be inserted into the folder.<br>";
			}
		}else{
			echo "The size of the file is bigger than allowed.<br>";
		}
	}

}
        echo "Thank You for uploading the Images.<br>";
        echo '<a href="user.php?housead=true" class="btn btn-default">'."Ad Management Page".'</a>';
    }elseif(isset($_GET['delete'])){
        $id = $_GET['delete'];
        
        //let us get the image details and then delete
        $q = "SELECT * FROM uploads WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $img = $r->fetch_array();
        
        $image_name = $img['image'];
        
        //let us remove the image and delete it from database
        $q1 = "DELETE FROM uploads WHERE id = '$id'";
        $r1 = $db->delete($q1);
        
        //now we will remove the image from server
        unlink("uploads/$image_name");
        
        echo "You have deleted one Image.<br>";
        echo '<a href="javascript:history.back()">'."Go Back to Previous Page".'</a>';
        
    }else{
        $id = $_GET['id'];
        
        $q = "SELECT * FROM house_ad WHERE id = '$id'";
        $r = $db->select($q);

        $ad = $r->fetch_array();
        $otp = $ad['otp'];
        $otp2 = $ad['otp2'];
        
        //let us see how many images are there in the database
        $q1 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
        $r1 = $db->select($q1);
                                  
        if(!$r1){
            echo "NO Images Uploaded.<br>";
        }else{
            $num = mysqli_num_rows($r1);
            while($img = $r1->fetch_array()):                                                                  
        ?>
        <div class="five_img">                        
        <a href="uploads/<?PHP echo $img['image']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?PHP echo $ad['ad_title']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" class="img-responsive"></a>
        <a href="upload_house_ad_img.php?delete=<?PHP echo $img['id']; ?>" class="btn btn-xs btn-default"><i class="fa fa-trash fa-fw" aria-hidden="true"></i>&nbsp; Delete this</a>
        </div>   

        <?PHP 
            endwhile;
            echo '<div class="clearfix"></div>';
            echo "<br><br><br>";
            
            if($num == 5){
            echo "You have five Images for the ad.<br>";
            echo "Remove an image to add another.<br>";
            }
        }
        ?>
   <div class="clearfix"></div>
   <hr>
    <div class="row">
    <form action="upload_house_ad_img.php?add_new=true" method="post" enctype="multipart/form-data">
    <!--let us try to put some image uploaders here--> 
      <?PHP
        $required = 5 - $num; 
        
        echo '<h3>'."You can add ". $required ." more Image.".'</h3>';
        for($i=1; $i<=$required; $i++){
    ?>
       <div class="form-group">
           <label for="image upload">Image <?PHP echo $i; ?></label>
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_<?PHP echo $i; ?>').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_<?PHP echo $i; ?>" alt="your image" width="100" height="100" />
       </div>
     <?PHP } ?>
       <p class="help-block">Post small Images and if you are posting from mobile please wait sometime for the images to upload.</p>
        <input type="hidden" name="otp" value="<?PHP echo $ad['otp']; ?>">
        <input type="hidden" name="otp2" value="<?PHP echo $ad['otp2']; ?>">
       <button class="btn btn-default" type="submit">Submit Image</button>
    </form>
    
<?PHP
    }
ini_restore('memory_limit');
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>