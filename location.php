<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row">
        <h1>Location Map </h1>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d120492.95454520966!2d84.73821649662648!3d19.308231791042473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a3d500ef1cb60ad%3A0x5b75778874294ff!2sBrahmapur%2C+Odisha+760001!5e0!3m2!1sen!2sin!4v1467575446092" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>