<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    <div class="row">
        <h1>Featured Ads </h1><hr>
<a href="post_free_ad.php"><img src="img/sqad_01.gif" alt="Post a Free ad for your business" class="text-right img-responsive" style="float:right; margin:0 0 20px 20px;"></a>
        <p>
            We have the provision of posting your classified ad on the front page of our website for a small fee. <br>
            This will ensure your ad is seen by many people and gets maximum attention. <br>
            The visitors usually notice the ads posted on the front page of the website. <br>
        </p>
        <h3>Why Featured Ad ?</h3>
        <p>
            Because it will give you more views and create awareness about your product or company to the maximum. <br>
            As a simple visitor one will sure like to click on your ad more often then go and search for the same in the website. <br>
            And presence on the front page is the key to be noticed.
        </p>
        
        <h3>How do I feature my ad ?</h3>
        <p>
            If you wish to post your ad in the featured ad section, all you need is to intimate us. Send us a mail at support@niltik.com or call us at +91 - 00000000000. Our executives will help you in getting your ads featured on the site.
        </p>
        
        <h3>What options are there for the ads ?</h3>
        <p>
            We have many different options for your ads on our website. <br>
            The size and price of the ads can be had by requesting for a quotation. <br>
        </p>
    
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>