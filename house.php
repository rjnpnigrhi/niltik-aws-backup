<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    if(isset($_GET['find'])){
        if(isset($_POST['location_id'])){
            $location_id = $_POST['location_id'];
        }else{
            $location_id = $_GET['location_id'];
        }
        
        if(isset($_POST['locality_id'])){
            $locality_id = $_POST['locality_id'];
        }else{
            $locality_id = $_GET['locality_id'];
        }
        
        if(isset($_POST['category_id'])){
            $category_id = $_POST['category_id'];
        }else{
            $category_id = $_GET['category_id'];
        }
        
        if(isset($_POST['price'])){
            $price = $_POST['price'];
        }else{
            $price = $_GET['price'];
        }
        
        if(isset($_POST['filter'])){
            $filter = $_POST['filter'];
        }else{
            $filter = $_GET['filter'];
        }
        
        $q = "SELECT * FROM house_ad WHERE status = 'approved'";
        
        if($location_id == ''){
            
        }else{
            $q .= "AND FIND_IN_SET('$location_id', location_id)";
        }
        
        if($locality_id == ''){
            
        }else{
            $q .= "AND location_id = '$locality_id'";
        }
        
        if($category_id == ''){
            
        }else{
            $q .= "AND category_id = '$category_id'";
        }
        
        if($price == ''){
            
        }elseif($price == 1){
            $q .= "AND price < 2000";
        }elseif($price == 2){
            $q .= "AND price BETWEEN 2000 AND 3000";
        }elseif($price == 3){
            $q .= "AND price BETWEEN 3000 AND 4000";
        }elseif($price == 4){
            $q .= "AND price BETWEEN 4000 AND 6000";
        }elseif($price == 5){
            $q .= "AND price > 6000";
        }
        
        if($filter == ''){
            $q .= "ORDER BY id DESC";
        }elseif($filter == 1){
            $q .= "ORDER BY price ASC";
        }elseif($filter == 2){
            $q .= "ORDER BY price DESC";
        }
        


        $r = $db->select($q);
        
        if(!$r){
            echo "Sorry, We could not find a home as per your demand.<br>";
            echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
        }else{
            echo "We found matching ads for you.<br>";
            echo '<h1>Available Options for your search !</h1>';
            while($ads = $r->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 4);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers); 
?>
     <div class="row">
        <div class="col-md-6">      
<?PHP           
            foreach($data as $ad){        
        
?>
        
        <a href="view_house.php?id=<?PHP echo $ad['id']; ?>"><h3 style="padding-left:20px;"><?PHP echo ucfirst($ad['ad_title']); ?></h3></a>
        <div class="col-sm-4">
                <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                ?>
                <a href="view_house.php?id=<?PHP echo $ad['id']; ?>"><img src="img/no-img.jpg" alt="<?PHP echo ucfirst($ad['ad_title']); ?>" class="img-responsive"></a>
                <?PHP
                    }else{
                       $img = $r3->fetch_array();
                ?>
                    <a href="view_house.php?id=<?PHP echo $ad['id']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" alt="<?PHP echo $ad['ad_title']; ?>" class="img-responsive"></a>
                <?PHP } ?>
                           </div>
                           <div class="col-sm-8">                               
                               <h2>Rs. <?PHP echo $ad['price']; ?>.00</h2>
                               <a href="view_house.php?id=<?PHP echo $ad['id']; ?>" class="btn btn-default btn-right">View Details</a>
                           </div>
                    
                    <div class="clearfix"></div>
                    <hr>
                     <?PHP 
                        }
                     ?>   
                        
          <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="house.php?find=true&location_id='.$location_id.'&locality_id='.$locality_id.'&category_id='.$category_id.'&price='.$price.'&filter='.$filter.'&page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="house.php?find=true&location_id='.$location_id.'&locality_id='.$locality_id.'&category_id='.$category_id.'&price='.$price.'&filter='.$filter.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="house.php?find=true&location_id='.$location_id.'&locality_id='.$locality_id.'&category_id='.$category_id.'&price='.$price.'&filter='.$filter.'&page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
                
               ?>
              </ul>
            </nav>
</div>
<div class="col-md-6">
            <a href="post_house_ad.php"><img src="img/sqad_02.gif" alt="Post a Free House Ad" class="img-responsive"></a>

            </div>

<div class="clearfix"></div>
<a href="javascript:history.back()" class="btn btn-default">Go Back to Previous Page</a>

<?PHP          
            
            }

    }else{
?>
    <div class="row">
        <h1>Niltik.com House for Rent Solutions</h1>
        <a href="house.php?find=true" class="btn btn-success btn-lg">Find all Ads</a>
         <a href="post_house_ad.php" class= "btn btn-primary btn-lg"><i class="fa fa-plus"></i> <i class="fa fa-home"></i> add a house </a>
        <hr>
        <div class="col-sm-6">
           <h3>Chose Your Options for a House</h3>
           <hr>
            <form action="house.php?find=true" method="post">
                        <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                        
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="category_id" id="category" class="form-control">
                                <option value="">All Categories </option>
                                <?PHP
                                $qh = "SELECT * FROM house_category";
                                $rh = $db->select($qh);

                                while($hc = $rh->fetch_array()):
                                ?>       
                                <option value="<?PHP echo $hc['id']; ?>"><?PHP echo ucfirst($hc['category']); ?></option>
                                <?PHP endwhile; ?>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="price">Price Range</label>
                            <select name="price" id="price_range" class="form-control">
                                <option value="">Any Range</option>
                                <option value="1">Below 2000</option>
                                <option value="2">2000-3000</option>
                                <option value="3">3000-4000</option>
                                <option value="4">4000-6000</option>
                                <option value="5">above 6000</option>
                            </select>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label for="filter">Filter by Price</label>
                            <select name="filter" id="filter" class="form-control">
                                <option value="">Show All</option>
                                <option value="1">Price Low to High</option>
                                <option value="2">Price High to Low</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Find a House</button>
                        </div>
                    </form>
        </div>
        <div class="col-sm-6">
<a href="http://www.smartvisionplus.in" target="_blank" title="Smart Vision Plus"><img src="img/svp_sqad.jpg" alt="Smart Vision Plus"></a>
<br><br>
            <a href="post_house_ad.php"><img src="img/sqad_02.gif" alt="Post a Free House Ad" class="img-responsive"></a>
</div>
        
    
    
<?PHP
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>