<?PHP
	session_start();
	include("inc/functions.php");
	
	error_reporting(0);

    //let us initiate a new connection
    $db = new connection();
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Niltik Mobile Solutions : Mobile repair, Sale and Buying</title>
    
    <link rel="icon" href="img/favicon.png" type="img/png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome css--->	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--- odia font css--->
    <link rel="stylesheet" href="css/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container top_nav">
		<strong>Niltik Mobile Care</strong>
		<a href="#" class="btn btn-xs btn-default disabled pull-right"><i class="fa fa-phone-square" aria-hidden="true"></i>
 : 7008584789</a>
	</div>
	
	<div class="container">
	<?PHP
			if(isset($_GET['submit'])){
				//log in user
				$email = $db->safe_data($_POST['email']);
				$password = $db->safe_data($_POST['password']);
				
				$password = md5($password);
				
				$q = "SELECT * FROM mobile_admin WHERE email = '$email' LIMIT 1";
				$r = $db->select($q);
				
				if(!$r){
					echo "Your Login Credentials are incorrect.<br>";
					echo '<a href="login.php" class="btn btn-warning">'."Go Back and Retry!".'</a>';
				}else{
					$row = $r->fetch_assoc();
					
					$db_password = $row['password'];
					
					if($db_password == $password){
						$_SESSION['logged_in'] = TRUE;
						$_SESSION['name'] = $row['name'];
						
						echo "You are successfully Logged in.<br>";
						echo '<a href="admin.php">'."Administrators Dashboard".'</a>';
					}else{
						echo "Your Login Credentials are incorrect.<br>";
						echo '<a href="login.php" class="btn btn-warning">'."Go Back and Retry!".'</a>';
					}
				}
			}else{
				//show form
		?>
	<h3 class="order">Login to Admin</h3>
	<form method="post" action="login.php?submit=true" class="form">
	  <div class="form-group">
		<label for="exampleInputEmail1">Email address</label>
		<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
	  </div>
	  <div class="form-group">
		<label for="exampleInputPassword1">Password</label>
		<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-default">Sign In</button>
	</form>
	<?PHP
		}
	?>
	<hr>
	<a href="index.php" class="btn btn-success pull-center">Mobile Care Home Page</a>
	</div>
	
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>