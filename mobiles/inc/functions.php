<?PHP
	include("config.php");
	include("db_conn.php");
	
    //here we add all functions of the site
    $db = new connection();
	
    date_default_timezone_set('Asia/Kolkata');

	function get_db_to_array($result){
		global $db;
		
		$res_array = array();
		
		for($count=0; $row=mysqli_fetch_array($result); $count++){
			$res_array[$count] = $row;
		}
		
		return $res_array;
	}

    //let us get all the values from any table with $name
    function get_values($name){
        global $db;
        
        $q = "SELECT * FROM $name ORDER BY id DESC";
        $r = $db->select($q);
        
        $value = get_db_to_array($r);
        return $value;
    }

    //we can get a single row from any table with a name and id
    function get_value($name, $id){
        global $db;
        
        $q = "SELECT * FROM $name WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

    //we can find if an element exists in a table 
    function value_exists($table, $name, $value){
        global $db;
        
        $q = "SELECT * FROM $table WHERE $name LIKE '%$value%' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }

    //we can delete a row from any table with an id 
    function delete_row($table, $id){
        global $db;
        
        $q = "DELETE FROM $table WHERE id = '$id'";
        $r = $db->delete($q);
    }


    function random_code($num){
        global $db;
        
        $random_code = '';
        
        $count = 0;
        
        while($count < $num){
            $random_digit = mt_rand(0, 9);
            
            $random_code .= $random_digit;
            $count++;
        }
        
        return $random_code;
    }
	
	function get_date($date){
        global $db;
        
        list($month, $day, $year, $hour, $minute, $am) = explode('/', $date);
        $m = strftime("%B",mktime(0,0,0,$month));
        echo '<i class="fa fa-calendar" aria-hidden="true"></i>'." : " . $m ." ". $day . ", ".  $year . " <br>";
		echo '<i class="fa fa-clock-o" aria-hidden="true"></i>'." : " .$hour . " : " . $minute ."". $am;
    }

?>