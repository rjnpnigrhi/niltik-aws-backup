<?PHP
	session_start();
	include("inc/functions.php");
	
	error_reporting(0);

    //let us initiate a new connection
    $db = new connection();

	$msg ='';
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Niltik Mobile Solutions : Mobile repair, Sale and Buying</title>
    
    <link rel="icon" href="img/favicon.png" type="img/png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome css--->	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--- odia font css--->
    <link rel="stylesheet" href="css/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container top_nav">
		<strong>Niltik Mobile Care</strong>
		<a href="#" class="btn btn-xs btn-default disabled pull-right"><i class="fa fa-phone-square" aria-hidden="true"></i>
 : 7008584789</a>
	</div>
	
	<div class="container header-box text-center">
		<img src="img/niltik_mobile.jpg" width="100%" class="img-responsive" />
	</div>
	
	<div class="container">
		<h3 class="order">
			Book a Mobile Repair
		</h3>
		<?PHP
			if(isset($_GET['submit'])){
			$name = $db->safe_data($_POST['name']);
			$mobile = $db->safe_data($_POST['mobile']);
			$booking_time = date("m/d/Y/h/i/A");
			$status = "PENDING";
			
			$q = "INSERT INTO mobile_tbl (name, mobile, booking_time, status) VALUES ('$name', '$mobile', '$booking_time', '$status')";		
			$r = $db->insert($q);
			
//we will send the OTP to both the email ID and also to the mobile
        $from = "niltikbam@gmail.com";
        $to = "niltikbam@gmail.com";
        $subject = "New Mobile Repair Query on Website";
        $message = "We have received a mobile repair Query from :  ".$name. ", Mobile Number : ". $mobile ." on site at NILTIK.com";
        $headers = "From: webmaster@niltik.com" . "\r\n" .
            "CC: support@niltik.com";

            mail($to,$subject,$message,$headers);


			echo '<p class="text-success text-center">'."Thank You for Contacting us. <BR> Our Executive will call you soon.<br><br> If you do not receive a call within next 1 Hour please call us at : 7008584789.".'</p>';
		}else{
		?>
		<p class="text-center">Fill the form and we will CONTACT you.</p>
		<form action="index.php?submit=true" method="post" class="form">
		  <div class="form-group">
			<label for="exampleInputName1">Your Name :</label>
			<input type="text" name="name" class="form-control" placeholder="Type your Name" required>
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">Mobile No.</label>
			<input type="text" name="mobile" class="form-control" placeholder="10 Digit Mobile No" required>
		  </div>
		  <button type="submit" name="submit" class="btn btn-success">Submit</button>
		</form>
		<?PHP } ?>
		<hr>
		<h3>How it Works :</h3>
		<p>You Fill up the form.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> Our Executive calls you and records all your problems.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> You will be given a quotation for the amount which may be required for the repair of your phone.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> On your confirmation, we will pick up your mobile from your home.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> We will repair your mobile at the earliest possible time.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> We will deliver your Mobile at your Doorstep in working condition.</p>
		<p><i class="fa fa-plus-square" aria-hidden="true"></i> You will pay the dues on delivery of your Phone.</p>
		<p class="text-danger">We will be sending you SMS messages at all stages for your confirmation and safety.</p>
		<hr>
		<p class="text-primary small text-center">If you have any queries you can call us at : 7008584789</p>
		<hr>
		<a href="admin.php" class="btn btn-success btn-xs pull-right">Executive Login</a>
	</div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>