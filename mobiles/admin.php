<?PHP
	session_start();
	include("inc/functions.php");
	
	error_reporting(0);

    //let us initiate a new connection
    $db = new connection();
	
	if(!isset($_SESSION['logged_in'])){
		header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Niltik Mobile Solutions : Mobile repair, Sale and Buying</title>
    
    <link rel="icon" href="img/favicon.png" type="img/png">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome css--->	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--- odia font css--->
    <link rel="stylesheet" href="css/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container top_nav">
		<strong><a href="admin.php">Niltik Mobile Care</a></strong>
		<a href="logout.php" class="btn btn-xs btn-warning pull-right"><i class="fa fa-sign-out" aria-hidden="true"></i>
 Sign Out</a>
	</div>
	
	<div class="container">
		<p class="text-center">Order List</p>
		<?PHP
			$q = "SELECT * FROM mobile_tbl ORDER BY id DESC";
			$r = $db->select($q);
			
			if(!$r){
				echo "There are no Pending Orders.<br>";
			}else{
		?>
			<table class="table table-striped">
				<tr>
					<th>ID</th>
					<th>Details</th>
					<th>Status</th>
				</tr>
				<?PHP
					while($row = $r->fetch_array()){
				?>
				<tr>
					<td><?PHP echo $row['id']; ?></td>
					<td>
					<i class="fa fa-user" aria-hidden="true"></i> : <?PHP echo ucfirst($row['name']); ?><br>
					<i class="fa fa-phone" aria-hidden="true"></i> : <?PHP echo ucfirst($row['mobile']); ?><br>
					<?PHP echo get_date($row['booking_time']); ?>
					
					</td>
					<td>
					<?PHP
						if($row['status'] == "PENDING"){
							echo '<a href="book.php?id='.$row['id'].'" class="btn btn-sm btn-success"><i class="fa fa-gears" aria-hidden="true"></i></a>';
						}else{
							echo '<a href="info.php?id='.$row['id'].'" class="btn btn-sm btn-primary"><i class="fa fa-search" aria-hidden="true"></i></a>';
						}
					?>
					</td>
				</tr>
				<?PHP
					}
				?>	
			</table>
		<?PHP
			}
		?>
	</div>
	
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>