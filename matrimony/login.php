<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(isset($_SESSION['user_id'])){
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    
<body>
    <div class="container">
	<br>
      <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
	<br>
        </div>
       <br>
       
    <div class="clearfix"></div>
        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 info-box">
            <?PHP
                if(isset($_GET['submit'])){
                    $username = $db->safe_data($_POST['username']);
                    $password = $db->safe_data($_POST['password']);
                    $password = md5($password);
                    
                    $q = "SELECT * FROM users WHERE email = '$username' LIMIT 1";
                    $r = $db->select($q);
                    
                    if(!$r){
                        echo "<br>";
                        echo "The e-Mail ID you entered is not registerd.<br>";
                        echo '<a href="login.php" class="btn btn-xs btn-primary">'."Try Again".'</a>';
                        echo "<br>";
                        echo "<br>";
                    }else{
                        $row = $r->fetch_assoc();
                        
                        if($password !== $row['password']){
                            echo "<br>";
                            echo "The Password you entered is not correct.<br>";
                            echo '<a href="login.php" class="btn btn-xs btn-primary">'."Try Again".'</a>';
                            echo "<br>";
                            echo "<br>";
                        }else{
                            $_SESSION['logged_in'] = TRUE;
                            $_SESSION['user_id'] = $row['id'];
                            $_SESSION['name'] = $row['name'];
                            $_SESSION['email'] = $row['email'];
                            $_SESSION['mobile'] = $row['mobile'];
                            $_SESSION['last_login'] = $row['login_time'];

                            $time = date('F d, Y h:i A', time());
                            $id = $row['id'];

                            $q1 = "UPDATE users SET login_time = '$time' WHERE id = '$id'";
                            $r1 = $db->update($q1);
                            
                            echo "<br>";
                            echo "You are successfully logged in. <br>";
                            echo "<br>";
                            if($row['status']!=="approved"){
                                echo "You must complete your profile and get it approved to view more profiles.<br>";
                                echo '<a href="step1.php" class="btn btn-xs btn-success">'."Create Profile".'</a>';
                                header("Location: step1.php");
                            }else{
                                echo '<a href="index.php" class="btn btn-xs btn-success">'."View Profiles".'</a>';
                                header("Location: index.php");
                            }
                            echo "<br>";
                            echo "<br>";
                        }
                    }
                }else{
            ?>
            <h2>Sign in!</h2>
            <p class="small">You must be logged in to visit the profiles!</p>
            <hr>
            <form action="login.php?submit=true" method="post" class="form">
                <div class="form-group">
                    <input type="email" name="username" placeholder="Type your eMail ID" required class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Type your Password" required class="form-control">
                </div>
                <div class="form-group">
                    <button class="btn btn-md btn-primary btn-block" type="submit">Sign in!</button>
                </div>
                <div class="form-group">
                    <a href="signup.php" class="btn btn-block btn-success btn-md">New User! Sign Up!</a>
                </div>
                <hr>
                
                <p class="small">Forgot Password ? <a href="forgotpass.php" class="btn btn-xs btn-warning">Click here!</a></p>
            </form>
            <?PHP } ?>
        </div>
        <div class="clearfix"></div>
        <hr>
        <p class="text-center info-text">For any Queries please contact : <strong><em>7008584789</em></strong></p>
    </div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>