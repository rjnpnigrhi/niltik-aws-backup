<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if(value_exists('profile4', 'user_id', $user_id)>0){
        header("Location: step5.php");
    }

    //here we collect information about desires and requirements
?>
     <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <!-- image croping js-->
    <script type="text/javascript" src="js/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>
    
    <div class="container">
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    
    <div class="container main-box">   
        <div class="action-box">
           <h1 class="text-center text-success">Fill up your Requirement details below.</h1>
           <h4 class="text-center">Basic Profile &amp; Family &amp; Career Details Completed</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
            <?PHP
                if(isset($_GET['insert'])){
                    //insert into profile2 table
                    $minage = $db->safe_data($_POST['minage']);
                    $maxage = $db->safe_data($_POST['maxage']);
                    $caste = $db->safe_data($_POST['caste']);
                    $sub_caste = $db->safe_data($_POST['sub_caste']);
                    $place = $db->safe_data($_POST['place']);
                    $profession = $db->safe_data($_POST['profession']);
                    $abt_partner = $db->safe_data($_POST['abt_partner']);
                                        
                    $q = "INSERT INTO profile4 (user_id, minage, maxage, caste, sub_caste, place, profession, abt_partner) VALUES ('$user_id', '$minage', '$maxage', '$caste', '$sub_caste', '$place', '$profession', '$abt_partner')";
                    $r = $db->insert($q);
                            
                    if(!$r){
                        //do nothing
                    }else{
                        header("Location: step5.php");                                
                        echo '<a href="step5.php" class="btn btn-sm btn-primary">'."Proceed to next Step".'</a>';
                    }                        
                    
                }else{
                    //show the form here
            ?>
            <form action="step4.php?insert=true" method="post" class="form">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="minage">Minimum Age</label>
                            <input type="number" name="minage" class="form-control" required value="18">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="maxage">Maximum Age</label>
                            <input type="number" name="maxage" class="form-control" required value="99">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                          <label for="other">Caste Preference</label>
                          <select name="caste" class="form-control" id="" required>
                            <?PHP
                                $q = "SELECT * FROM caste ORDER BY caste_name ASC";
                                $r = $db->select($q);
                        
                                while($caste = $r->fetch_array()){
                                    echo '<option value="'.$caste['id'].'">'.$caste['caste_name'].'</option>';
                                }
                            ?>
                          </select>                  
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="religion">Sub-Caste (Optional)</label>
                            <input type="text" name="sub_caste" class="form-control" placeholder="sub caste">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="place">Place</label>
                            <select name="place" class="form-control" id="" required>
                                <?PHP
                            $qs = "SELECT * FROM state ORDER BY id ASC";
                            $rs = $db->select($qs);
                            while($state = $rs->fetch_array()){
                            ?>
                            <option value="<?PHP echo $state['id']; ?>" ><?PHP echo $state['name']; ?></option>
                            <?PHP
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="profession">Profession</label>
                             <select name="profession" class="form-control" id="profession" required>
                    <?PHP
                        $q = "SELECT * FROM jobs ORDER BY job_title ASC";
                        $r = $db->select($q);
                    
                        while($jobs = $r->fetch_array()){
                            echo '<option value="'.$jobs['id'].'">'.$jobs['job_title'].'</option>';
                        }
                    ?>
                    </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abtjob">I am Looking for</label>
                    <textarea name="abt_partner" id="abt" class="form-control" rows="10" required></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="logout.php" class="btn btn-sm btn-danger">Skip and Log Out</a>
                </div>
            </form>
            <?PHP
                }
            ?>
           </div>
        </div>
    </div>
    <!--container ends here-->
    
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>