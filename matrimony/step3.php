<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if(value_exists('profile3', 'user_id', $user_id)>0){
        header("Location: step4.php");
    }

    //here we collect information about profession and career
?>
     <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <!-- image croping js-->
    <script type="text/javascript" src="js/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>
    
    <div class="container">
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    
    <div class="container main-box">   
        <div class="action-box">
           <h1 class="text-center text-success">Fill up your Career and Job details below.</h1>
           <h4 class="text-center">Basic Profile &amp; Family Details Completed</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
            <?PHP
                if(isset($_GET['insert'])){
                    //insert into profile2 table
                    $qualification = $db->safe_data($_POST['qualification']);
                    $job = $db->safe_data($_POST['profession']);
                    $job_type = $db->safe_data($_POST['job_type']);
                    $place = $db->safe_data($_POST['place']);
                    $salary = $db->safe_data($_POST['salary']);
                    $abt_job = $db->safe_data($_POST['abt_job']);
                    
                    
                    $q = "INSERT INTO profile3 (user_id, qualification, job, job_type, place, salary, abt_job) VALUES ('$user_id', '$qualification', '$job', '$job_type', '$place', '$salary', '$abt_job')";
                    $r = $db->insert($q);
                            
                    if(!$r){
                        //do nothing
                    }else{
                        $q1 = "UPDATE profile1 SET profession = '$job' WHERE user_id = '$user_id'";
                        $r1 = $db->update($q1);
                        
                        header("Location: step4.php");                                
                        echo '<a href="step4.php" class="btn btn-sm btn-primary">'."Proceed to next Step".'</a>';
                    }                        
                    
                }else{
                    //show the form here
            ?>
            <form action="step3.php?insert=true" method="post" class="form">
                <div class="form-group">
                    <label for="qualification">Highest Qualification</label>
                    <input type="text" name="qualification" class="form-control" required placeholder="Highest Qualification">
                </div>
                <div class="form-group">
                    <label for="profession">Current Profession</label>
                    <select name="profession" class="form-control" id="profession" required>
                    <?PHP
                        $q = "SELECT * FROM jobs ORDER BY job_title ASC";
                        $r = $db->select($q);
                    
                        while($jobs = $r->fetch_array()){
                            echo '<option value="'.$jobs['id'].'">'.$jobs['job_title'].'</option>';
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Type of Job</label>
                    <select name="job_type" id="job_type" class="form-control" required>
                        <option value="fulltime">Fulltime / Permanent</option>
                        <option value="parttime">Parttime</option>
                        <option value="govt">Government Service</option>
                        <option value="business">Own Business</option>
                        <option value="office">Office Job</option>
                        <option value="looking">Looking for Job</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                  
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="profession">Place of Posting</label>
                            <input type="text" name="place" class="form-control" required placeholder="Place of Posting">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="salary">Annual Salary</label>
                            <input type="text" name="salary" class="form-control" placeholder="in Lakhs/Annum" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abtjob">About Job</label>
                    <textarea name="abt_job" id="abt_job" class="form-control" rows="10" required></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="logout.php" class="btn btn-sm btn-danger">Skip and Log Out</a>
                </div>
            </form>
            <?PHP
                }
            ?>
           </div>
        </div>
    </div>
    <!--container ends here-->
    
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>