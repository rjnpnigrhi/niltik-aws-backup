<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");

    if(isset($_GET['update'])){
        $old_pass = $db->safe_data($_POST['old_pass']);
        $new_pass = $db->safe_data($_POST['new_pass']);
        $new_pass_copy = $db->safe_data($_POST['new_pass_copy']);
        $old_pass_db = md5($old_pass);
        $pass = $user['password'];
        
        if($new_pass !==$new_pass_copy){
            echo "The new passwords do not match.<br>";
            echo "Hit back and Try Again.<br>";
        }elseif($old_pass_db !==$pass){
            echo "You have typed incorrect current password.<br>";
            echo "Hit back and Try Again.<br>";
        }else{
            $pass_db = md5($new_pass);
            
            $q = "UPDATE users SET password = '$pass_db' WHERE id = '$user_id'";
            $r = $db->update($q);
            
            session_destroy();
			echo "<script>
            alert('Your Password has been successfully updated.');
            window.location.href='login.php'; 
            </script>";
        }
    }else{
?>
  <h3>Update Password</h3>
  <hr>
  <div class="col-md-6 col-md-offset-3 col-xs-12">
      <form action="privasy.php?update=true" method="post" class="form">
          <div class="form-group">
              <label for="old_pass">Current Password :</label>
              <input type="password" name="old_pass" class="form-control" placeholder="Current Password" required>
          </div>
          <div class="form-group">
              <label for="new_pass">New Password :</label>
              <input type="password" name="new_pass" class="form-control" placeholder="New Password" required>
          </div>
          <div class="form-group">
              <label for="new_pass_copy">Retype New Password :</label>
              <input type="password" name="new_pass_copy" class="form-control" placeholder="Retype New Password" required>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-md btn-default">Update Password</button>
          </div>
      </form>
  </div>
<?PHP
    }
    include("mods/footer.php");
?>
    