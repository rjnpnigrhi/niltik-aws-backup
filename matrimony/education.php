<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-default btn-sm">Family</a>
   <a href="photos.php" class="btn btn-default btn-sm">Photos</a>
   <a href="address.php" class="btn btn-default btn-sm">Contact</a>
   <a href="education.php" class="btn btn-primary btn-sm">Education</a>
   <a href="career.php" class="btn btn-default btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-default btn-sm">Requirements</a>
   </div>
<?PHP
    //address.php
    $portfolio_id = $user['profile_id'];
    
    if(isset($_GET['insert'])){
        //insertinfo
        $highschool = $db->safe_data($_POST['highschool']);
        $intermediate = $db->safe_data($_POST['intermediate']);
        $graduation = $db->safe_data($_POST['graduation']);
        $post_graduation = $db->safe_data($_POST['post_graduation']);
        $qualification = $db->safe_data($_POST['qualification']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "INSERT INTO education (highschool, intermediate, graduation, post_graduation, qualification, about, portfolio_id) VALUES ('$highschool', '$intermediate', '$graduation', '$post_graduation', '$qualification', '$about', '$portfolio_id')";
        $r = $db->insert($q);
        
        $q1 = "UPDATE users SET profile_status = profile_status+10 WHERE profile_id = '$portfolio_id'";
        $r1 = $db->update($q1);
        
        inform_user($portfolio_id);
        
        echo "Thank You for updating your education profile.<br>";
        echo "Please complete all parts of the portfolio to get approved.<br>";
        echo '<a href="education.php" class="btn btn-sm btn-primary">'."View Education Profile".'</a>';
    }elseif(isset($_GET['update'])){
        //update info
        $highschool = $db->safe_data($_POST['highschool']);
        $intermediate = $db->safe_data($_POST['intermediate']);
        $graduation = $db->safe_data($_POST['graduation']);
        $post_graduation = $db->safe_data($_POST['post_graduation']);
        $qualification = $db->safe_data($_POST['qualification']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "UPDATE education SET highschool = '$highschool', intermediate = '$intermediate', graduation = '$graduation', post_graduation = '$post_graduation', qualification = '$qualification', about = '$about' WHERE portfolio_id = '$portfolio_id'";
        $r = $db->update($q);
        
        echo "Thank You for updating your education profile.<br>";
        echo '<a href="education.php" class="btn btn-sm btn-primary">'."View Education Profile".'</a>';
    }else{

    if(value_exists('education', 'portfolio_id', $portfolio_id)>0){
        //show the data for updation
        $q = "SELECT * FROM education WHERE portfolio_id = '$portfolio_id' LIMIT 1";
        $r = $db->select($q);
        
        $row = $r->fetch_assoc();
?>
   <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>Education Profile (10%)</h2>
   <form action="education.php?update=true" method="post" class="form">
       <div class="form-group">
           <label for="school">Schooling :</label>
           <input type="text" name="highschool" class="form-control" value="<?PHP echo $row['highschool']; ?>" required>
       </div>
       <div class="form-group">
           <label for="school">College (+2) :</label>
           <input type="text" name="intermediate" class="form-control" value="<?PHP echo $row['intermediate']; ?>" required>
       </div>
       <div class="form-group">
           <label for="school">Graduation :</label>
           <input type="text" name="graduation" class="form-control" value="<?PHP echo $row['graduation']; ?>" required>
       </div>
       <div class="form-group">
           <label for="school">Post-Graduation :</label>
           <input type="text" name="post_graduation" class="form-control" value="<?PHP echo $row['post_graduation']; ?>" required>
       </div>
       <div class="form-group">
           <label for="school">Highest Qualification :</label>
           <input type="text" name="qualification" class="form-control" value="<?PHP echo $row['qualification']; ?>" required>
       </div>
       <div class="form-group">
           <label for="school">About Education :</label>
           <textarea name="about" id="about" class="form-control" rows="10" required><?PHP echo $row['about']; ?></textarea>
       </div>
       <div class="form-group">
           <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">    
           <button class="btn btn-md btn-default" type="submit">Submit</button>
           <a href="career.php" class="btn btn-md btn-primary">Proceed to Career Profile</a>
       </div>
   </form>
   </div>
<?PHP
    }else{
?>
  <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>Education Profile (10%)</h2>
   <form action="education.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="school">Schooling :</label>
           <input type="text" name="highschool" class="form-control" placeholder="Highschool" required>
       </div>
       <div class="form-group">
           <label for="school">College (+2) :</label>
           <input type="text" name="intermediate" class="form-control" placeholder="+2 Class" required>
       </div>
       <div class="form-group">
           <label for="school">Graduation :</label>
           <input type="text" name="graduation" class="form-control" placeholder="Graduation" required>
       </div>
       <div class="form-group">
           <label for="school">Post-Graduation :</label>
           <input type="text" name="post_graduation" class="form-control" placeholder="Post Graduation" required>
       </div>
       <div class="form-group">
           <label for="school">Highest Qualification :</label>
           <input type="text" name="qualification" class="form-control" placeholder="Highest Qualification" required>
       </div>
       <div class="form-group">
           <label for="school">About Education :</label>
           <textarea name="about" id="about" class="form-control" rows="10" required></textarea>
       </div>
       <div class="form-group">
           <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">
           <button class="btn btn-md btn-default" type="submit">Submit</button>
       </div>
   </form>
   </div>
<?PHP
    }
    }
    include("mods/footer.php");
?>