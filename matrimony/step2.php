<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if(value_exists('profile2', 'user_id', $user_id)>0){
        header("Location: step3.php");
    }

    //here we collect information about family
?>
     <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <!-- image croping js-->
    <script type="text/javascript" src="js/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>
    
    <div class="container">
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    
    <div class="container main-box">   
        <div class="action-box">
           <h1 class="text-center text-success">Fill up your family details below.</h1>
           <h4 class="text-center">Basic Profile Complete > Family Details</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
            <?PHP
                if(isset($_GET['insert'])){
                    //insert into profile2 table
                    $father = $db->safe_data($_POST['father']);
                    $mother = $db->safe_data($_POST['mother']);
                    $father_job = $db->safe_data($_POST['father_job']);
                    $mother_job = $db->safe_data($_POST['mother_job']);
                    $bros = $db->safe_data($_POST['bros']);
                    $bros_married = $db->safe_data($_POST['bros_married']);
                    $sis = $db->safe_data($_POST['sis']);
                    $sis_married = $db->safe_data($_POST['sis_married']);
                    $abt_family = $db->safe_data($_POST['abt_family']);
                    
                    $q = "INSERT INTO profile2 (user_id, father, mother, father_job, mother_job, bros, bros_married, sis, sis_married, abt_family) VALUES ('$user_id', '$father', '$mother', '$father_job', '$mother_job', '$bros', '$bros_married', '$sis', '$sis_married', '$abt_family')";
                    $r = $db->insert($q);
                            
                    if(!$r){
                        //do nothing
                    }else{
                        header("Location: step3.php");                                
                        echo '<a href="step3.php" class="btn btn-sm btn-primary">'."Proceed to next Step".'</a>';
                    }                        
                    
                }else{
                    //show the form here
            ?>
            <form action="step2.php?insert=true" method="post" class="form">
                <div class="form-group">
                    <label for="father">Father</label>
                    <input type="text" name="father" class="form-control" required placeholder="Father's Name">
                </div>
                <div class="form-group">
                    <label for="mother">Mother</label>
                    <input type="text" name="mother" class="form-control" required placeholder="Mother's Name">
                </div>
                   <label for="jobs">Father &amp; Mother's Job</label>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <input type="text" name="father_job" class="form-control" placeholder="Father's Job" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <input type="text" name="mother_job" class="form-control" placeholder="Mother's Job" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers">Brothers</label>
                            <input type="number" name="bros" class="form-control" value="0" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers_married">Married</label>
                            <input type="number" name="bros_married" class="form-control" value="0" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers">Sisters</label>
                            <input type="number" name="sis" class="form-control" value="0" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="sisters_married">Married</label>
                            <input type="number" name="sis_married" class="form-control" value="0" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="about">About Family</label>
                    <textarea name="abt_family" id="abt_family" class="form-control" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="logout.php" class="btn btn-sm btn-danger">Skip and Log Out</a>
                </div>
            </form>
            <?PHP
                }
            ?>
           </div>
        </div>
    </div>
    <!--container ends here-->
    
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>