<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>     
        <div class="row">
           <h4 class="text-center">Update Your Family Profile</h4>
           <hr>  
           <div class="col-md-4 col-md-offset-4 col-xs-12">             
            <?PHP
                if(isset($_GET['update'])){
                    //insert into profile2 table
                    $father = $db->safe_data($_POST['father']);
                    $mother = $db->safe_data($_POST['mother']);
                    $father_job = $db->safe_data($_POST['father_job']);
                    $mother_job = $db->safe_data($_POST['mother_job']);
                    $bros = $db->safe_data($_POST['bros']);
                    $bros_married = $db->safe_data($_POST['bros_married']);
                    $sis = $db->safe_data($_POST['sis']);
                    $sis_married = $db->safe_data($_POST['sis_married']);
                    $abt_family = $db->safe_data($_POST['abt_family']);
                    
                    $q = "UPDATE profile2 SET father = '$father', mother = '$mother', father_job = '$father_job', mother_job = '$mother_job', bros = '$bros', sis = '$sis', bros_married = '$bros_married', sis_married = '$sis_married', abt_family = '$abt_family' WHERE user_id = '$user_id'";                   
                    
                }else{
                    //show the form here
                    $q1 = "SELECT * FROM profile2 WHERE user_id = '$user_id' LIMIT 1";
                    $r1 = $db->select($q1);

                    if(!$r1){        
                        header("Location: step2.php");
                    }else{
                        $family = $r1->fetch_assoc(); 
            ?>
            <form action="step2edit.php?update=true" method="post" class="form">
                <div class="form-group">
                    <label for="father">Father</label>
                    <input type="text" name="father" class="form-control" required value="<?PHP echo $family['father']; ?>">
                </div>
                <div class="form-group">
                    <label for="mother">Mother</label>
                    <input type="text" name="mother" class="form-control" required value="<?PHP echo $family['mother']; ?>">
                </div>
                   <label for="jobs">Father &amp; Mother's Job</label>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <input type="text" name="father_job" class="form-control" value="<?PHP echo $family['father_job']; ?>" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <input type="text" name="mother_job" class="form-control" value="<?PHP echo $family['mother_job']; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers">Brothers</label>
                            <input type="number" name="bros" class="form-control" value="<?PHP echo $family['bros']; ?>" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers_married">Married</label>
                            <input type="number" name="bros_married" class="form-control" value="<?PHP echo $family['bros_married']; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="brothers">Sisters</label>
                            <input type="number" name="sis" class="form-control" value="<?PHP echo $family['sis']; ?>" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                           <label for="sisters_married">Married</label>
                            <input type="number" name="sis_married" class="form-control" value="<?PHP echo $family['sis_married']; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="about">About Family</label>
                    <textarea name="abt_family" id="abt_family" class="form-control" rows="10"><?PHP echo nl2br($family['abt_family']); ?></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="profile.php" class="btn btn-sm btn-primary">Cancel</a>
                </div>
            </form>
            <?PHP
                }
                }
            ?>
           </div>
</div>
<?PHP include("mods/footer.php"); ?>