<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <h1>My Portfolio Page</h1>
      <!-- Nav tabs -->
<ul class="nav nav-pills">
  <li role="presentation" class="active"><a href="#">Basic Profile</a></li>
  <li role="presentation"><a href="#">Photos</a></li>
  <li role="presentation"><a href="#">Address</a></li>
  <li role="presentation"><a href="#">Family </a></li>
  <li role="presentation"><a href="#">Education</a></li>
  <li role="presentation"><a href="#">Career</a></li>
  <li role="presentation"><a href="#">Requirements</a></li>
</ul>
 <hr>
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#profile" aria-controls="home" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos</a></li>
    <li role="presentation"><a href="#address" aria-controls="address" role="tab" data-toggle="tab">Address</a></li>
    <li role="presentation"><a href="#family" aria-controls="family" role="tab" data-toggle="tab">Family</a></li>
    <li role="presentation"><a href="#education" aria-controls="education" role="tab" data-toggle="tab">Education</a></li>
    <li role="presentation"><a href="#career" aria-controls="career" role="tab" data-toggle="tab">Career</a></li>
    <li role="presentation"><a href="#requirements" aria-controls="requirements" role="tab" data-toggle="tab">Requirements</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="wide-box">
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="profile">
        <h3>Basic Profile</h3> 
        <?PHP
            include("mods/profile.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="photos">
        <h3>Photos</h3>
        <?PHP
            include("mods/photos.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="address">
        <?PHP
            include("mods/address.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="family">
        <h3>Family</h3>
        <?PHP
            include("mods/family.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="education">
        <h3>Education</h3>
        <?PHP
            include("mods/education.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="career">
        <h3>Career</h3>
        <?PHP
            include("mods/career.php");
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="requirements">
        <h3>Requirements</h3>
        <?PHP
            include("mods/requirements.php");
        ?>
    </div>
  </div>
  </div>
<?PHP
    include("mods/footer.php");
?>
    