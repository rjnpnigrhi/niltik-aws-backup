<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-primary btn-sm">Family</a>
   <a href="photos.php" class="btn btn-default btn-sm">Photos</a>
   <a href="address.php" class="btn btn-default btn-sm">Contact</a>
   <a href="education.php" class="btn btn-default btn-sm">Education</a>
   <a href="career.php" class="btn btn-default btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-default btn-sm">Requirements</a>
   </div>
<?PHP
    //address.php
    $portfolio_id = $user['profile_id'];
    if(isset($_GET['update'])){
        $father = $db->safe_data($_POST['father']);
        $mother = $db->safe_data($_POST['mother']);
        $father_job = $db->safe_data($_POST['father_job']);
        $mother_job = $db->safe_data($_POST['mother_job']);
        $brothers = $db->safe_data($_POST['brothers']);
        $sisters = $db->safe_data($_POST['sisters']);
        $brothers_married = $db->safe_data($_POST['brothers_married']);
        $sisters_married = $db->safe_data($_POST['sisters_married']);
        $family_type = $db->safe_data($_POST['family_type']);
        $family_status = $db->safe_data($_POST['family_status']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "UPDATE family SET father = '$father', mother = '$mother', father_job = '$father_job', mother_job = '$mother_job', brothers = '$brothers', sisters = '$sisters', brothers_married = '$brothers_married', sisters_married = '$sisters_married', family_type = '$family_type', family_status = '$family_status', about = '$about' WHERE portfolio_id = '$portfolio_id'";
        $r = $db->update($q);

    }elseif(isset($_GET['insert'])){
        $father = $db->safe_data($_POST['father']);
        $mother = $db->safe_data($_POST['mother']);
        $father_job = $db->safe_data($_POST['father_job']);
        $mother_job = $db->safe_data($_POST['mother_job']);
        $brothers = $db->safe_data($_POST['brothers']);
        $sisters = $db->safe_data($_POST['sisters']);
        $brothers_married = $db->safe_data($_POST['brothers_married']);
        $sisters_married = $db->safe_data($_POST['sisters_married']);
        $family_type = $db->safe_data($_POST['family_type']);
        $family_status = $db->safe_data($_POST['family_status']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "INSERT INTO family (father, mother, father_job, mother_job, brothers, sisters, brothers_married, sisters_married, family_type, family_status, about, portfolio_id) VALUES ('$father', '$mother', '$father_job', '$mother_job', '$brothers', '$sisters', '$brothers_married', '$sisters_married', '$family_type', '$family_status', '$about', '$portfolio_id')";
        $r = $db->insert($q);
        
        if($r){
            $q1 = "UPDATE users SET profile_status = profile_status+20 WHERE profile_id = '$portfolio_id'";
            $r1 = $db->update($q1);
        }
        
        inform_user($portfolio_id);
        
        echo "You have added your family details successfully.<br>";
        echo "Please complete all parts of the portfolio to get approved.<br>";
        echo '<a href="family.php" class="btn btn-sm btn-primary">'."View Family Details".'</a>';
    }

    if(value_exists('family', 'portfolio_id', $portfolio_id)<1){
?>
   <div class="col-md-6 col-md-offset-3 col-xs-12">
       <h2>Family Details (20%)</h2>
       <form action="family.php?insert=true" method="post" class="form">
           <div class="form-group">
               <label for="father">Father's Name :</label>
               <input type="text" name="father" class="form-control" required placeholder="Father's Name">
           </div>
           <div class="form-group">
               <label for="mother">Mother's Name :</label>
               <input type="text" name="mother" class="form-control" required placeholder="Mother's Name">
           </div>
           <div class="form-group">
               <label for="father_job">Father's Ocupation :</label>
               <input type="text" name="father_job" class="form-control" required placeholder="Father's Job">
           </div>
           <div class="form-group">
               <label for="mother_job">Mother's Ocupation :</label>
               <input type="text" name="mother_job" class="form-control" required placeholder="Mother's Job">
           </div>
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="brothers">No of Brothers :</label>
                       <input type="text" name="brothers" class="form-control" placeholder="0" required>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                      <label for="brothers_married">Married :</label>
                       <input type="text" name="brothers_married" class="form-control" placeholder="0" required>                       
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="sisters">No of Sisters :</label>
                       <input type="text" name="sisters" class="form-control" placeholder="0" required>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="sisters_married">Married :</label>
                       <input type="text" name="sisters_married" class="form-control" placeholder="0" required>
                   </div>
               </div>
           </div>
           
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="family_type">Family Type</label>
                       <select name="family_type" class="form-control" id="family_type" required>
                           <option value="nuclear">Nuclear </option>
                           <option value="joint">Joint </option>
                       </select>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="status">Family Status</label>
                       <select name="family_status" id="family_status" class="form-control" required>
                           <option value="rich">Rich class</option>
                           <option value="upper">Upper Middle class</option>
                           <option value="lower">Lower Middle class</option>
                           <option value="poor">Poor class</option>
                       </select>
                   </div>
               </div>
           </div>
           
           <div class="form-group">
               <label for="about">About Your Family :</label>
               <input type="hidden" name="portfolio_id" value="<?PHP echo $user['profile_id']; ?>">
               <textarea name="about" class="form-control" id="about" rows="10" required></textarea>
           </div>
           
           <div class="form-group">
               <button class="btn btn-md btn-default" type="submit">Submit</button>
           </div>
       </form>
   </div>
<?PHP
}else{
    //show the data for updation
    $q = "SELECT * FROM family WHERE portfolio_id = '$portfolio_id' LIMIT 1";
    $r = $db->select($q);
        
    $row = $r->fetch_assoc();
?>
   <div class="col-md-6 col-md-offset-3 col-xs-12">
       <h2>My Family Details (20%)</h2>
       <form action="family.php?update=true" method="post" class="form">
           <div class="form-group">
               <label for="father">Father's Name :</label>
               <input type="text" name="father" class="form-control" required value="<?PHP echo $row['father']; ?>">
           </div>
           <div class="form-group">
               <label for="mother">Mother's Name :</label>
               <input type="text" name="mother" class="form-control" required value="<?PHP echo $row['mother']; ?>">
           </div>
           <div class="form-group">
               <label for="father_job">Father's Ocupation :</label>
               <input type="text" name="father_job" class="form-control" required value="<?PHP echo $row['father_job']; ?>">
           </div>
           <div class="form-group">
               <label for="mother_job">Mother's Ocupation :</label>
               <input type="text" name="mother_job" class="form-control" required value="<?PHP echo $row['mother_job']; ?>">
           </div>
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="brothers">No of Brothers :</label>
                       <input type="text" name="brothers" class="form-control" value="<?PHP echo $row['brothers']; ?>" required>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                      <label for="brothers_married">Married :</label>
                       <input type="text" name="brothers_married" class="form-control" value="<?PHP echo $row['brothers_married']; ?>" required>                       
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="sisters">No of Sisters :</label>
                       <input type="text" name="sisters" class="form-control" value="<?PHP echo $row['sisters']; ?>" required>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="sisters_married">Married :</label>
                       <input type="text" name="sisters_married" class="form-control" value="<?PHP echo $row['sisters_married']; ?>" required>
                   </div>
               </div>
           </div>
           
           <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="family_type">Family Type</label>
                       <select name="family_type" class="form-control" id="family_type" required>
                          <option value="<?PHP echo $row['family_type']; ?>" selected><?PHP echo ucfirst($row['family_type']); ?></option>
                           <option value="nuclear">Nuclear </option>
                           <option value="joint">Joint </option>
                       </select>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="status">Family Status</label>
                       <select name="family_status" id="family_status" class="form-control" required>
                          <option value="<?PHP echo $row['family_status']; ?>" selected><?PHP echo $row['family_status']; ?></option>
                           <option value="rich">Rich class</option>
                           <option value="upper">Upper Middle class</option>
                           <option value="lower">Lower Middle class</option>
                           <option value="poor">Poor class</option>
                       </select>
                   </div>
               </div>
           </div>
           
           <div class="form-group">
               <label for="about">About Your Family :</label>
               <input type="hidden" name="portfolio_id" value="<?PHP echo $user['profile_id']; ?>">
               <textarea name="about" class="form-control" id="about" rows="10" required><?PHP echo $row['about']; ?></textarea>
           </div>
           
           <div class="form-group">
               <button class="btn btn-md btn-default" type="submit">Update Information</button>
               <a href="photos.php" class="btn btn-md btn-primary">Proceed to Photos >></a>
           </div>
       </form>
   </div>
<?PHP
    }
    include("mods/footer.php");
?>