<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if(value_exists('profile1', 'user_id', $user_id)>0){
        header("Location: step2.php");
    }

    //here we collect basic details
?>
     <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    
    <!-- image croping js-->
    <script type="text/javascript" src="js/croppie.js"></script>
    <link rel="stylesheet" href="css/croppie.css">
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>
    
    <div class="container">
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    
    <div class="container main-box">   
        <div class="action-box">
           <h1 class="text-center">You do not have a portfolio!</h1>
           <h4 class="text-center">Please click on the button below and create one.</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
                <?PHP
                    if(isset($_GET['insert_profile'])){
                        $user_id = $db->safe_data($_POST['user_id']);
                        $name = $db->safe_data($_POST['name']);
                        $dob = $db->safe_data($_POST['dob']);
                        $age = $db->safe_data($_POST['age']);
                        $sex = $db->safe_data($_POST['sex']);
                        $height = $db->safe_data($_POST['height']);
                        $weight = $db->safe_data($_POST['weight']);
                        $religion = $db->safe_data($_POST['religion']);
                        $caste = $db->safe_data($_POST['caste']);
                        $sub_caste = $db->safe_data($_POST['sub_caste']);
                        $rashi = $db->safe_data($_POST['rashi']);
                        $gothram = $db->safe_data($_POST['gothram']);
                        $marital_status = $db->safe_data($_POST['marital_status']);
                        $relationship = $db->safe_data($_POST['relationship']);
                        $email = $db->safe_data($_POST['email']);
                        $mobile = $db->safe_data($_POST['mobile']);
                        $address = $db->safe_data($_POST['address']);
                        $district = $db->safe_data($_POST['district']);
                        $state = $db->safe_data($_POST['state']);
                        $date = date("m/d/Y");                        
                        
                        if(value_exists('profile1', 'user_id', $user_id)>0){
                            echo "A portfolio already exists with your profile.<br>";
                            echo "Please contact admin for the Problem.<br>";
                        }else{                            
                        
                            $q = "INSERT INTO profile1 (user_id, name, dob, age, sex, height, weight, religion, caste, sub_caste, rashi, gothram, marital_status, relationship, email, mobile, address, district, state,  date) VALUES ('$user_id', '$name', '$dob', '$age', '$sex', '$height', '$weight', '$religion', '$caste', '$sub_caste', '$rashi', '$gothram', '$marital_status', '$relationship', '$email', '$mobile', '$address', '$district', '$state', '$date')";
                            $r = $db->insert($q);
                            $last_insert_id = $r->insert_id;
                            $_SESSION['insert_id'] = $last_insert_id;
                            if(!$r){
                                //do nothing
                            }else{
                                if($sex = "male"){
                                    $desire = "female";
                                }else{
                                    $desire = "male";
                                }
                                $q1 = "UPDATE users SET desire = '$desire' WHERE id = '$user_id'";
                                $r1 = $db->update($q1);
                                
                                header("Location: step2.php");                                
                                echo '<a href="step2.php?pid='.$last_insert_id.'" class="btn btn-sm btn-primary">'."Proceed to next Step".'</a>';
                            }                        
                        }
                    }else{
                ?>
               <form action="step1.php?insert_profile=true" method="post" class="form">
					<div class="form-group">
						<select class="form-control" name="relationship">
							<option value="">Creating Profile for</option>
							<option value="self">Myself</option>
							<option value="son">Son</option>
							<option value="daughter">Daughter</option>
							<option value="brother">Brother</option>
							<option value="sister">Sister</option>
							<option value="relative">Relative</option>
							<option value="friend">Friend</option>
						</select>
					</div>
                    <div class="form-group">
                       <label for="name">Name of Portfolio : </label>
                        <input type="text" name="name" class="form-control" placeholder="Type Name Here" required>
                    </div>
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Date of Birth : </label>
                       <div class="input-group date" data-provide="datepicker">
						<input type="text" name="dob" class="form-control">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
					<script type="text/javascript">
						$(function () {
							$('#datetimepicker1').datetimepicker();
						});
					</script>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Age in Years : </label>
                        <input type="text" name="age" class="form-control" placeholder="Age in Years" required>
                    </div>
						</div>
					</div>
                    
					
					<label for="sex">Sex : &nbsp; &nbsp;</label>
					<div class="row">                       
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio1" value="male"> Male
                        </label>
						</div>
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio2" value="female"> Female
                        </label>
						</div>
						<div class="clearfix"></div>
						<br>
					</div>
                    
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="height">Height in Feet</label>
                       <select name="height" id="height" class="form-control">
                           <?PHP
                            $qh = "SELECT * FROM height ORDER BY id ASC";
                            $rh = $db->select($qh);
                            while($ht = $rh->fetch_array()){
                            ?>
                            <option value="<?PHP echo $ht['ht_id']; ?>"><?PHP echo $ht['ht_name']; ?></option>
                            <?PHP
                            }
                            ?> 
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="weight">Weight in KG</label>
                       <input type="number" name="weight" class="form-control" value="50" required >
                    </div>
						</div>
					</div>
                    
					
                    <div class="form-group">
                       <label for="marital_status">Marital Status :</label>
                        <select name="marital_status" id="marital_status" class="form-control">
				           <?PHP
                            $qm = "SELECT * FROM marital_status ORDER BY id ASC";
                            $rm = $db->select($qm);
                            while($ms = $rm->fetch_array()){
                            ?>
                            <option value="<?PHP echo $ms['id']; ?>"><?PHP echo $ms['marital_status']; ?></option>
                            <?PHP
                            }
                            ?>                            
                        </select>
                    </div>
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Zodiac Sign (Rashi)</label>
                        <select name="rashi" id="zodiac_sign" class="form-control">
                          <?PHP
                            $qz = "SELECT * FROM zodiac_sign ORDER BY id ASC";
                            $rz = $db->select($qz);
                            while($zs = $rz->fetch_array()){
                            ?>
                            <option value="<?PHP echo $zs['id']; ?>" ><?PHP echo $zs['zodiac_sign']; ?></option>
                            <?PHP
                            }
                            ?>
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Gothram </label>
                        <input name="gothram" type="text" class="form-control" />
                    </div>
						</div>
					</div>
                    
					<div class="form-group">
						<label for="religion">Religion</label>
						<select class="form-control" name="religion">
							<?PHP
                            $qr = "SELECT * FROM religion ORDER BY id ASC";
                            $rr = $db->select($qr);
                            while($religion = $rr->fetch_array()){
                            ?>
                            <option value="<?PHP echo $religion['id']; ?>" ><?PHP echo $religion['religion']; ?></option>
                            <?PHP
                            }
                            ?>	
						</select>
					</div>
										
					<label for="caste">Caste and Sub-caste</label>
					<div class="row">
						<div class="col-xs-6">
							<select name="caste" class="form-control">
							<?PHP
                                $q = "SELECT * FROM caste ORDER BY caste_name ASC";
                                $r = $db->select($q);
                        
                                while($caste = $r->fetch_array()){
                                    echo '<option value="'.$caste['id'].'">'.$caste['caste_name'].'</option>';
                                }
                            ?>
							</select>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
							<input name="sub_caste" type="text" class="form-control" placeholder="Type Sub caste">
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="email">eMail ID</label>
                        <input name="email" type="email" class="form-control" value="<?PHP echo $_SESSION['email']; ?>" required />
                    </div>
					<div class="form-group">
                        <label for="mobile">Mobile</label>
                        <input name="mobile" type="text" class="form-control" value="<?PHP echo $_SESSION['mobile']; ?>" required />
                    </div>
					<div class="form-group">
                        <label for="address">Address</label>
						<textarea name="address" class="form-control" rows="3" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="district">District</label>
                        <input type="text" name="district" class="form-control" value="<?PHP echo $profile['district']; ?>">
                    </div>
                    <div class="form-group">
						<label for="religion">State</label>
						<select class="form-control" name="state">
						    <?PHP
                            $qs = "SELECT * FROM state ORDER BY id ASC";
                            $rs = $db->select($qs);
                            while($state = $rs->fetch_array()){
                            ?>
                            <option value="<?PHP echo $state['id']; ?>" ><?PHP echo $state['name']; ?></option>
                            <?PHP
                            }
                            ?>							
						</select>
					</div>/
                     <div class="form-group">
                        <input type="hidden" name="user_id" value="<?PHP echo $_SESSION['user_id']; ?>">
                        <button class="btn btn-md btn-default" type="submit">Submit</button>
                        <a href="logout.php" class="btn btn-md btn-danger">Log Out</a>
                    </div>
                </form>
			   <?PHP 
                    }
               ?>
           </div>
        </div>
    </div>
    <!--container ends here-->
    
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>