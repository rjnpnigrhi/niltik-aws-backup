<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
 <div class="container text-center">
     <h1>Requests You have Received</h1>
 </div>
 
 <div class="container top-20">
    <?PHP
        if($user['status']!=='approved'){
            echo "Your Profile is pending Approval.<br>";
        }else{
            $profile_id = $user['profile_id'];
            $profile = get_value('portfolios', $profile_id);
            $portfolio_id = $profile['portfolio_id'];
            
            $q = "SELECT * FROM requests WHERE portfolio_id = '$portfolio_id'";
            $r = $db->select($q);
            
            if(!$r){
                echo "No Records Found!<br>";
            }else{
                echo '<table id="users_tbl" class="table">';
                echo '<thead>';
                echo '<tr>';
                echo '<th width="30%">'."Image".'</th>';
                echo '<th>'."Name".'</th>';
                echo '<th>'."Date".'</th>';
                echo '<th>'."Status".'</th>';
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                while($row = $r->fetch_array()){
                    $pid = $row['portfolio_id'];
                    $q1 = "SELECT * FROM portfolios WHERE portfolio_id = '$pid' LIMIT 1";
                    $r1 = $db->select($q1);
                    $u1 = $r1->fetch_assoc();
                    $u_id = $u1['id'];
                    $photo = get_user('photos', $u_id);
                    echo '<td><img src="uploads/'.$photo['image'].'" width="100%" class="img-responsive"></td>';
                    echo '<td>'.$u1['name'].'</td>';
                    echo '<td>'.$row['date'].'</td>';
                    echo '<td><a href="admin_contact.php" class="btn btn-xs btn-primary">'."Contact Admin".'</a></td>';
                }
                echo '</tbody>';
                echo '</table>';
            }
            
        }
     
     ?>
     <br>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#users_tbl').DataTable();
        } );
        </script>
 </div>
<?PHP
    include("mods/footer.php");
?>