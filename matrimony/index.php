<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
       <div class="row">
        <div class="col-xs-12 top-20">
        <?PHP
            if($user['desire']=='female'){    
        ?>
           <a href="list.php">
            <img src="img/girl.jpg" width="100%" class="img-responsive img-rounded" alt="">
            </a>
        <?PHP
            }else{
        ?>
           <a href="list.php">
            <img src="img/boy.jpg" width="100%" class="img-responsive img-rounded" alt=""> 
            </a>
        <?PHP } ?>
        </div>
        <div class="col-xs-6 top-20">
          <a href="profile.php"><img src="img/profile.jpg" width="100%" class="img-responsive img-rounded" alt=""></a>         
        </div>
        <div class="col-xs-6 top-20">
          <a href="search.php"><img src="img/search.jpg" width="100%" class="img-responsive img-rounded" alt=""> </a>         
        </div>
    </div><!-- box with three images-->
    <hr>
    <div class="container">        
        <div class="row">
            <h4>Interests received ... <a href="#" class="btn btn-xs btn-primary pull-right">View All!</a></h4>
            <br>
            <div class="row">
            <div class="col-xs-6">
                <img src="img/sample2.jpg" class="img-responsive img-rounded" width="100%" alt="Name of the Girl">
            </div>
            <div class="col-xs-6">
               <p class="small">KM00213</p>
                <p class="text-primary"><strong>Name of the Girl</strong></p>
                <p>25 Years, 5'6" <br>
                Athagarh Anchal <br>
                Software Engineer <br>
                4.5 L/Annum</p>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="text-center">
            <a href="#" class="btn btn-sm btn-success">View Profile</a>
            <a href="#" class="btn btn-sm btn-warning">Reject</a>
            </div>
            </div>
            <hr>
        </div>      

        
        <div class="row">
        <h4>Profile Visitors... <a href="#" class="btn btn-xs btn-primary pull-right">View All!</a></h4>
        <div class="row">
        <div class="col-xs-6 top-20">
            <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
        </div>
        <div class="col-xs-6 top-20">
            <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
        </div>
        <div class="col-xs-6 top-20">
            <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
        </div>
        <div class="col-xs-6 top-20">
            <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
        </div>
        <div class="clearfix"></div>
        </div>
        <hr>
        </div>        

        <div class="row">
          <h4>Recommended Profiles ... <a href="#" class="btn btn-sm btn-primary pull-right">View All!</a></h4>
       <br>
           <div class="row">
            <div class="col-xs-6">
                <img src="img/sample2.jpg" class="img-responsive img-rounded" width="100%" alt="Name of the Girl">
            </div>
            <div class="col-xs-6">
               <p class="small">KM00213</p>
                <p class="text-primary"><strong>Name of the Girl</strong></p>
                <p>25 Years, 5'6" <br>
                Athagarh Anchal <br>
                Software Engineer <br>
                4.5 L/Annum</p>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="text-center">
            <a href="#" class="btn btn-sm btn-success">View Profile</a>
            <a href="#" class="btn btn-sm btn-warning">Reject</a>
            </div>
            </div>
            <hr>
        </div>   
        <div class="clearfix"></div>   
    </div><!-- main page box with three divs-->
    
    <div class="container">
        <div class="blackbox col-md-6 col-md-offset-3">
            If you require anytype of Assistance for your marriage organization, we can be of your help. Please consult with us for all kinds of necessary arrangements for Marriage including Kalyan Mandap, Band or DJ, Catering Service, Photographer, Videographer etc.
            <br>
            Call us at : <strong><em>7008584789</em></strong>
        </div>
<?PHP 
    include("mods/footer.php");
?>