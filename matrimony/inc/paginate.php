<?PHP 
	//this is pagination class
	
	class Pagination{
		var $data;
		
		public function paginate($values, $per_page){
			//let us get total values
			$total_values = count($values);
			
			//let us get total pages
			$total_pages = $this->total_pages($total_values, $per_page);
			
			//let us get page number
			$current_page = $this->page_num();
			
			//let us get first post of the page
			$begin_post = ($current_page - 1) * $per_page;
			
			//let us slice the values in diffent blocks
			$this->data = array_slice($values, $begin_post, $per_page);
			
			//let us create page numbers
			for($x=1; $x <= $total_pages; $x++){
				$numbers[] = $x;
			}
			//we get the page numbers here
			return $numbers;
			
		}
		
		public function fetchresults(){
			$resultValues = $this->data;
			
			//we get the page data here
			return $resultValues;
		}
		
		public function page_num(){
			if(isset($_GET['page'])){
				$current_page = $_GET['page'];
			}else{
				$current_page = 1;
			}
			
			return $current_page;
			
		}
		
		public function total_pages($total_values, $per_page){
			$total_pages = ceil($total_values/$per_page);
			
			return $total_pages;
		}
		
		public function next_page($n){
			$n = $n+1;
			
			return $n;
		}	
		
		public function prev_page($n){
			$n = $n-1;
			
			return $n;
		}	
	
	}
?>