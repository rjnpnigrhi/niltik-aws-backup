<?PHP
    //here we add all functions of the site
    $db = new connection();
    date_default_timezone_set('Asia/Kolkata');

	function get_db_to_array($result){
		global $db;
		
		$res_array = array();
		
		for($count=0; $row=mysqli_fetch_array($result); $count++){
			$res_array[$count] = $row;
		}
		
		return $res_array;
	}

    //let us get all the values from any table with $name
    function get_values($name){
        global $db;
        
        $q = "SELECT * FROM $name ORDER BY id DESC";
        $r = $db->select($q);
        
        $value = get_db_to_array($r);
        return $value;
    }

    //we can get a single row from any table with a name and id
    function get_value($name, $id){
        global $db;
        
        $q = "SELECT * FROM $name WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

    //we can find if an element exists in a table 
    function value_exists($table, $name, $value){
        global $db;
        
        $q = "SELECT * FROM $table WHERE $name = '$value' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }

    //we can delete a row from any table with an id 
    function delete_row($table, $id){
        global $db;
        
        $q = "DELETE FROM $table WHERE id = '$id'";
        $r = $db->delete($q);
    }

    function update_profile($user_id){
        global $db;
        
        $q = "SELECT * FROM portfolios WHERE user_id = '$user_id' LIMIT 1";
        $r = $db->select($q);
        
        $u = $r->fetch_array();
        $profile_id = $u['id'];
        
        $sex = $u['sex'];
        
        if($sex = 'male'){
            $desire = "female";
        }elseif($sex == 'female'){
            $desire = "male";
        }
        
        $profile_status = "20";
        
        $q1 = "UPDATE `users` SET `profile_id` = '$profile_id', `profile_status` = '$profile_status', `desire` = '$desire' WHERE `users`.`id` = '$user_id'";        
        $r1 = $db->update($q1);
        
    }

    function getExtension($str){
        $i = strrpos($str, ".");
        if(!$i){
            return "";
        }
        
        $l = strlen($str) - $i;
        $ext = substr($str, $i+1, $l);
        return $ext;
    }


    function random_code($num){
        global $db;
        
        $random_code = '';
        
        $count = 0;
        
        while($count < $num){
            $random_digit = mt_rand(0, 9);
            
            $random_code .= $random_digit;
            $count++;
        }
        
        return $random_code;
    }
	
    function get_date($date){
        global $db;
        
        list($month, $day, $year) = explode('/', $date);
        $m = strftime("%B",mktime(0,0,0,$month));
        echo $m ." ". $day . ", ".  $year;
    }

    function get_photo($portfolio_id, $image_name){
        global $db;
        $q = "SELECT * FROM photos WHERE portfolio_id = '$portfolio_id' AND image_name = '$image_name' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

    //for matching profiles this can be done
    function get_user($table, $id){
        global $db;
        
        $q = "SELECT * FROM $table WHERE portfolio_id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

    function inform_user($portfolio_id){
        global $db;
        
        $q2 = "SELECT * FROM users WHERE profile_id = '$portfolio_id'";
        $r2 = $db->select($q2);
        
        $u = $r2->fetch_assoc();
        
        if($u['profile_status'] == '100'){
            echo "<script>
            alert('Your Profile is now completed, Please pay your Membership Fee of Rs 1000 / One thousand Only by PayTM at 7008584789 or please contact our office for Payment and Verification!');
            window.location.href='admin_contact.php'; 
            </script>";
        }
    }

    function match_exists($pid, $vid){
        global $db;
        
        $q = "SELECT * FROM suggestions WHERE pid = '$pid' AND vid = '$vid' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }
    
    //we can get a single row from any table with a tablename and user_id
    function get_data($name, $id){
        global $db;
        
        $q = "SELECT * FROM $name WHERE user_id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $value = $r->fetch_array();
        
        return $value;
    }

	function compressImage($ext, $uploadedfile, $path, $actual_image_name, $newwidth){
        if($ext == "jpg" || $ext == "jpeg"){
            $src = imagecreatefromjpeg($uploadedfile);
        }elseif($ext == "png"){
            $src = imagecreatefrompng($uploadedfile);
        }elseif($ext == "gif"){
            $src = imagecreatefromgif($uploadedfile);
        }else{
            $src = imagecreatefromjpeg($uploadedfile);
        }
        
        list($width, $height) = getimagesize($uploadedfile);
        
        $newheight = ($height/$width)*$newwidth;
        
        $tmp = imagecreatetruecolor($newwidth, $newheight);
        
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        
        $filename = $path.$newwidth."_".$actual_image_name;
        
        imagejpeg($tmp, $filename, 100);
        
        imagedestroy($tmp);
        
        return $filename;
    }
    
    class ImgResizer {
      private $originalFile = '';
      public function __construct($originalFile = '') {
          $this -> originalFile = $originalFile;
      }
      public function resize($newWidth, $targetFile) {
          if (empty($newWidth) || empty($targetFile)) {
              return false;
          }
          $src = imagecreatefromjpeg($this -> originalFile);
          list($width, $height) = getimagesize($this -> originalFile);
          $newHeight = ($height / $width) * $newWidth;
          $tmp = imagecreatetruecolor($newWidth, $newHeight);
          imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
          if (file_exists($targetFile)) {
              unlink($targetFile);
          }
          imagejpeg($tmp, $targetFile, 85); // 85 is my choice, make it between 0 – 100 for output image quality with 100 being the most luxurious
      }
    }
?>