<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");
    
    error_reporting(0);

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-default btn-sm">Family</a>
   <a href="photos.php" class="btn btn-primary btn-sm">Photos</a>
   <a href="address.php" class="btn btn-default btn-sm">Contact</a>
   <a href="education.php" class="btn btn-default btn-sm">Education</a>
   <a href="career.php" class="btn btn-default btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-default btn-sm">Requirements</a>
   </div>
<?PHP
    $portfolio_id = $user['profile_id'];

    if(isset($_GET['delete'])){
        $id = $db->safe_data($_GET['delete']);
        
        $img = get_value('photos', $id);
        $old_img = $img['image'];
        
        delete_row('photos', $id);
        
        unlink("uploads/".$old_img);
        
        echo "You deleted an Image.<br>";
        echo '<a href="photos.php" class="btn btn-sm btn-primary">'."View Photos".'</a>';
    }elseif(isset($_GET['featured'])){
        $id = $db->safe_data($_GET['featured']);
        
        $q = "UPDATE photos SET featured = '' WHERE portfolio_id = '$portfolio_id'";
        $r = $db->update($q);
        
        $featured = "TRUE";
        $q1 = "UPDATE photos SET featured = '$featured' WHERE id = '$id'";
        $r1 = $db->update($q1);
        
        echo "Featured Image has been changed.<br>";
        echo '<a href="photos.php" class="btn btn-sm btn-primary">'."View Photos".'</a>';
    }elseif(isset($_GET['insert'])){
        $image_name = $db->safe_data($_POST['image_name']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        if($image_name=="img_1"){
            $featured = "TRUE";
        }else{
            $featured = "";
        }
        
        $image = $_FILES["image"]["name"];
        $path = "uploads/";
        $valid_formats = array("jpg", "png", "gif", "jpeg", "PNG", "JPG", "JPEG", "GIF");

        $icon_name = $_FILES["image"]["name"];
        $icon_size = $_FILES["image"]["size"];
        $icon_type = $_FILES["image"]["type"];

        //file extension
        $ext = pathinfo($image, PATHINFO_EXTENSION);

        if(in_array($ext, $valid_formats)==FALSE){
            $errors[] = "Extension not allowed. Please chose JPG, PNG or GIF only.";

        }

        if($icon_size > 2097152){
            $errors[] = "File size must not be bigger than 512 KB.";
        }

        $actual_icon_name = time().".".$ext;
        $uploadedfile = $_FILES["image"]["tmp_name"];

        if(empty($errors)== TRUE){
            
            if(move_uploaded_file($uploadedfile, $path.$actual_icon_name)){  
                $q = "SELECT * FROM photos WHERE portfolio_id = '$portfolio_id' AND image_name = '$image_name' LIMIT 1";
                $r = $db->select($q);

                $count = $r->num_rows;

                if($count<1){
                    
                    $q1 = "INSERT INTO photos (portfolio_id, image_name, image, featured) VALUES ('$portfolio_id', '$image_name', '$actual_icon_name', '$featured')";
                    $r1 = $db->insert($q1);
                    
                    if($image_name == "img_1"){
                        $q2 = "UPDATE users SET profile_status = profile_status+20 WHERE profile_id = '$portfolio_id'";
                        $r2= $db->update($q2);
                    }
                    
                    inform_user($portfolio_id);
                    
                    echo "New Image Updated.<br>";
                    echo '<a href="photos.php" class="btn btn-sm btn-primary">'."View Images".'</a>';
                }else{        
                    $img = $r->fetch_array();
                    $img_id = $img['id'];
                    $old_img = $img['image'];
                    //update code here
                    $q1 = "UPDATE photos SET image = '$actual_icon_name' WHERE id = '$img_id'";
                    $r1 = $db->update($q1);
                    
                    unlink("uploads/".$old_img);
                    
                    echo "New Image Updated.<br>";
                    echo '<a href="photos.php" class="btn btn-sm btn-primary">'."View Images".'</a>';
                }
            }else{
                echo "There is some error in your Image.<br>";
                echo '<a href="photos.php" class="btn btn-sm btn-primary">'."Try Again".'</a>';
            }
                
        }else{
            echo "You must add an Image.<br>";
            echo '<a href="products.php" class="btn btn-sm btn-primary">'."Try Again".'</a>';
        }
        
    }else{

?>
<div class="row">
  <h2>My Photos (20%)</h2>
   <?PHP
      for($i=1; $i<=4; $i++){
    ?>
     <div class="col-md-3 col-xs-12">
       <?PHP
            $image_name = "img_".$i;
            $q = "SELECT * FROM photos WHERE portfolio_id = '$portfolio_id' AND image_name = '$image_name' LIMIT 1";
            $r = $db->select($q);
            
            $count = $r->num_rows;
        
            if($count<1){
                echo "Add an Image Here!<br><br>";
            }else{        
                $img = $r->fetch_array();
                
                if($img['featured']=='TRUE'){
                    echo '<a href="#" class="btn btn-xs btn-success disabled">'."Featured".'</a>';
                }else{
                    echo '<a href="photos.php?featured='.$img['id'].'" class="btn btn-xs btn-primary">'."Make Featured".'</a>';
                    echo '<a href="photos.php?delete='.$img['id'].'" class="btn btn-xs btn-danger pull-right">'."Delete".'</a>';
                }                
                echo "<br><br>";
                echo '<img src="uploads/'.$img['image'].'" class="img-responsive" width="100%">';
            }
        ?>        
        <form action="photos.php?insert=true" method="post" enctype="multipart/form-data" class="form">
           <div class="form-group">
           <label for="image">Image <?PHP echo $i; ?></label>           
           <input type="hidden" name="image_name" value="<?PHP echo $image_name; ?>">
           <input type="file" name="image" accept="image/*" onchange="document.getElementById('<?PHP echo $image_name; ?>').src = window.URL.createObjectURL(this.files[0])">
           <img id="<?PHP echo $image_name; ?>" alt="" width="240px" />
           </div>
           <div class="form-group">
              <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">
               <button class="btn btn-sm btn-default" type="submit">Upload</button>
           </div>
        </form><hr>
    </div>
    
    <?PHP
      }  
    ?>
</div>
<hr>

<?PHP
    $q = "SELECT * FROM photos WHERE portfolio_id = '$portfolio_id'";
    $r = $db->select($q);
        
    $count = $r->num_rows;
        
    if($count>0){
        echo '<a href="address.php" class="btn btn-md btn-primary">'."Proceed to Contact Details".'</a>';
    }
    }
    include("mods/footer.php");
?>