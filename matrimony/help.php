<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <h1>Procedure for Joining</h1>
  
  <p>
      Hello member,
      <br>
      Are you worried about creation of your Portfolio?  <br>
      Do you need Assistance? <br>
      We are here to help you create your profile. <br>
      <br>
      <img src="img/help_001.jpg" width="100%" class="img-responsive" alt="Help Screen shot">
      <br>
      <h4>Profile Creation : </h4>
      Please visit your profile page and there chose a selection for creation of portfolio. On clicking the button a form will be presented which you need to completely fill and then complete all pages of profile creation like Basic Profile, Family Profile, Photos, Educational Details, Career Information, Requirements and your contact details.
      <hr>
      <br>
      <h4>Verification : </h4>
      After you complete the procedure you will be intimated to do the <strong>verification</strong> of your profile, for which you must personally come to the office and get your documents verified and in case you are unable to come personally, you can seek help of your parents and or relatives to get that done.
      <hr><br>
      <h4>Self declaration : </h4>
      In rare incidence, if neither you nor your family can be present in our office for verification, you will need to submit a <em>Self Declaration form</em> along with the joining fee amount of Rs. 1000.00 (One thousand Only) by online mode on request. 
      <hr><br>
      For any assistance you can give us a call during working hours on any working day between 10:00 AM to 6:00 PM.
      <br><br>
      Our Helpline Numbers : <strong><em>8 4 8 0 6 6 0 7 8 9</em></strong>, <strong><em>7 0 0 8 5 8 4 7 8 9</em></strong>
  </p>
<?PHP
    include("mods/footer.php");
?>
    