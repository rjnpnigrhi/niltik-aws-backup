<?PHP
    session_start();

	include("../inc/config.php");
	include("../inc/db_conn.php");
	include("../inc/functions.php");
	include("../inc/paginate.php");
    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    //error_reporting(0);

    if(!isset($_SESSION['admin_id'])){
		header("Location: login.php");
		exit;
	}
    include("mods/header.php");
	
	if(isset($_GET['delete_final'])){
		$user_id = $_GET['delete_final'];
		$user = get_value('users', $user_id);
		$profile_id = $user['profile_id'];
		delete_row('users', $user_id);
		delete_row('portfolios', $profile_id);
		
		echo "User and Portfolio Deleted Successfully.<br>";
		
	}elseif(isset($_GET['delete'])){
		$user_id = $_GET['delete'];
		$user = get_value('users', $user_id);
		$profile_id = $user['profile_id'];
		
		if($profile_id ==''){
			//delete
			delete_row('users', $user_id);
			
			echo "User is deleted Successfully.";
		}else{
			echo "User has a profile associated.<br>";
			echo "Like to delete the profile and the portfolio too ?<br>";
			echo '<a href="users.php?delete_final='.$user_id.'" class="btn btn-danger btn-sm">'."Delete Confirm".'</a>';
			echo '<a href="users.php" class="btn btn-success btn-sm">'."Do not Delete".'</a>';
		}
	}elseif(isset($_GET['change_pass'])){
          //we will show form to change pass
          $id = $db->safe_data($_GET['change_pass']);
          $user = get_value('users', $id);
?>
    <h4>User : <?PHP echo $user['name']; ?></h4>
    <h4>eMail ID : <?PHP echo $user['email']; ?></h4>
     <form action="users.php?pass_change=true" method="post" class="form">
         <div class="form-group">
             <label for="password">New Password</label>
             <input type="hidden" name="id" value="<?PHP echo $id; ?>">
             <input type="text" name="password" placeholder="Type a New Password" class="form-control" required>
         </div>
         <div class="form-group">
             <button class="btn btn-sm btn-default" type="submit">Change Password</button>
         </div>
     </form>
<?PHP
	}elseif(isset($_GET['pass_change'])){
        //we will update the password
        $password = $db->safe_data($_POST['password']);
        $id = $db->safe_data($_POST['id']);
        $pass = md5($password);
          
        $q = "UPDATE users SET password = '$pass' WHERE id = '$id'";
        $r = $db->update($q);
          
        echo "<script>
        alert('You changed the password for a User!');
        window.location.href='users.php'; 
        </script>";
	}else{
?>
      <h1>Users Manager
        <a href="logout.php" class="btn btn-sm btn-danger pull-right">Log Out</a> </h1>
        <hr>
       <table id="users_tbl" class="table table-striped">
          <thead>
           <tr>
               <th>Action</th>
               <th>Name</th>
               <th>email</th>
               <th>Mobile</th>
               <th>Desires</th>
               <th>Date</th>
           </tr>
       </thead>
       <tbody>
        <?PHP
            $users = get_values("users");
        
            foreach($users as $user):
        ?>
            <tr>
                <td>
                <a href="users.php?delete=<?PHP echo $user['id']; ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                </td>
                <td><i class="fa fa-user"></i>  <strong><?PHP echo $user['name']; ?></strong></td>
                <td><?PHP echo $user['email']; ?><br><a href="users.php?change_pass=<?PHP echo $user['id']; ?>" class="btn btn-xs btn-warning">Update Password</a></td>
                <td><i class="fa fa-phone"></i> <?PHP echo $user['mobile']; ?></td>
                <td><i class="fa fa-<?PHP echo $user['desire']; ?>"></i> <?PHP echo $user['desire']; ?></td>
                <td><i class="fa fa-calendar"></i>  <?PHP echo get_date($user['date']); ?></td>
           </tr>
        <?PHP
            endforeach;
        ?>
        </tbody>
        </table>
        <br>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#users_tbl').DataTable();
        } );
        </script>
<?PHP
	}
	include("mods/footer.php"); 
?>