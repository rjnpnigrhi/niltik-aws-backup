<?PHP
    session_start();

	include("../inc/config.php");
	include("../inc/db_conn.php");
	include("../inc/functions.php");
	include("../inc/paginate.php");
    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    //error_reporting(0);

    if(!isset($_SESSION['admin_id'])){
		header("Location: login.php");
		exit;
	}
    include("mods/header.php");
    
    $user_id = $db->safe_data($_GET['id']);
  
    echo '<div class="col-sm-6 col-sm-offset-3">';
?>
    <h3>View Profile <input type="button" class="btn btn-warning btn-md pull-right" onclick="printDiv('printableArea')" value="Print Document!" /></h3>
    <hr>
    <div class="row" id="printableArea">
<?PHP
    $q = "SELECT * FROM profile1 WHERE user_id = '$user_id' LIMIT 1";
    $r = $db->select($q);

    if(!$r){
        
        echo "Incomplete Data.<br>";
    }else{
        $profile = $r->fetch_assoc();
?>
     <img src="../uploads/<?PHP echo $profile['image']; ?>" class="img-rounded" width="50%" alt="<?PHP echo $profile['name']; ?>"> 
     <br>
     <?PHP
        $img = $profile['image'];
        $qp = "SELECT * FROM profile5 WHERE image_name != '$img' AND user_id = '$user_id'";
        $rp = $db->select($qp);
        
        if(!$rp){
            //do nothing
            echo "No More Images.<br>";
        }else{
            echo '<div class="row">';
            while($pp = $rp->fetch_array()){
    ?>
         <div class="col-xs-4">
             <a href="../uploads/<?PHP echo $pp['image_name']; ?>" data-lightbox="../uploads/<?PHP echo $pp['image_name']; ?>" data-title="<?PHP echo $profile['name']; ?>"><img src="../uploads/<?PHP echo $pp['image_name']; ?>" class="img-responsive img-rounded top-20" alt="<?PHP echo $profile['name']; ?>"></a>
         </div>  
    <?PHP
            }
            echo '</div>';
        }
     ?>
     <br>
     <hr>
     <h4 class="text-success"><?PHP echo $profile['name']; ?></h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Date of birth :</td>
             <td width="60%"><?PHP echo get_date($profile['dob']); ?></td>
         </tr>
         <tr>
             <td>Age :</td>
             <td><?PHP echo $profile['age']; ?> years</td>
         </tr>
         <tr>
             <td>Address :</td>
             <td><?PHP echo nl2br($profile['address']); ?></td>
         </tr>
         <tr>
             <td>District :</td>
             <td><?PHP echo nl2br($profile['district']); ?></td>
         </tr>
         <tr>
             <td>State :</td>
             <td><?PHP 
                $state_id = $profile['state']; 
                $state = get_value('state', $state_id);
                echo $state['name'];
             ?></td>
         </tr>
         <tr>
             <td>Height :</td>
             <td><?PHP $ht_id = $profile['height']; 
                 $qh = "SELECT * FROM height WHERE ht_id = '$ht_id' LIMIT 1";
                    $rh = $db->select($qh);
                    $ht = $rh->fetch_assoc();
                    echo $ht['ht_name'];
                 ?></td>
         </tr>
         <tr>
             <td>Weight :</td>
             <td><?PHP echo $profile['weight']; ?> KG</td>
         </tr>
         <tr>
             <td>Religion :</td>
             <td><?PHP $rel_id = $profile['religion']; 
                 $religion = get_value('religion', $rel_id);
                echo $religion['religion'];
                 ?></td>
         </tr>
         <tr>
             <td>Caste :</td>
             <td><?PHP
                $caste_id = $profile['caste']; 
                $caste = get_value('caste', $caste_id);
                echo $caste['caste_name'];
             ?></td>
         </tr>
         <tr>
             <td>Sub-Caste :</td>
             <td><?PHP echo $profile['sub_caste']; ?></td>
         </tr>
         <tr>
             <td>Rashi :</td>
             <td><?PHP
                $rashi_id = $profile['rashi']; 
                $rashi = get_value('zodiac_sign', $rashi_id);
                echo $rashi['zodiac_sign'];
             ?></td>
         </tr>
         <tr>
             <td>Gothra :</td>
             <td><?PHP echo $profile['gothram']; ?></td>
         </tr>
         <tr>
             <td>Marital Status :</td>
             <td><?PHP
                $ms_id = $profile['marital_status']; 
                $ms = get_value('marital_status', $ms_id);
                echo $ms['marital_status'];
                 ?></td>
         </tr>
         <tr>
             <td>eMail :</td>
             <td><?PHP echo $profile['email']; ?></td>
         </tr>
         <tr>
             <td>Mobile :</td>
             <td><?PHP echo $profile['mobile']; ?></td>
         </tr>
     </table>
<?PHP
    }
    $q1 = "SELECT * FROM profile2 WHERE user_id = '$user_id' LIMIT 1";
    $r1 = $db->select($q1);

    if(!$r1){        
        echo "Incomplete Data.<br>";
    }else{
        $family = $r1->fetch_assoc();  
?>
     <h4 class="text-success">Family Details </h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Father's Name :</td>
             <td><?PHP echo $family['father']; ?></td>
         </tr>
         <tr>
             <td width="40%">Ocupation :</td>
             <td><?PHP echo $family['father_job']; ?></td>
         </tr>
         <tr>
             <td width="40%">Mother's Name :</td>
             <td><?PHP echo $family['mother']; ?></td>
         </tr>
         <tr>
             <td width="40%">Ocupation :</td>
             <td><?PHP echo $family['mother_job']; ?></td>
         </tr>
         <tr>
             <td width="40%">Brothers :</td>
             <td><?PHP echo $family['bros']; ?></td>
         </tr>
         <tr>
             <td width="40%">Married :</td>
             <td><?PHP echo $family['bros_married']; ?></td>
         </tr>
         <tr>
             <td width="40%">Sisters :</td>
             <td><?PHP echo $family['sis']; ?></td>
         </tr>
         <tr>
             <td width="40%">Married :</td>
             <td><?PHP echo $family['sis_married']; ?></td>
         </tr>
         <tr>
             <td width="40%">About Family :</td>
             <td><?PHP echo nl2br($family['abt_family']); ?></td>
         </tr>  
</table>
<?PHP
    }
    $q2 = "SELECT * FROM profile3 WHERE user_id = '$user_id' LIMIT 1";
    $r2 = $db->select($q2);

    if(!$r2){        
        echo "Incomplete Data.<br>";
    }else{
        $job = $r2->fetch_assoc();  
?>
     <h4 class="text-success">Career &amp; Job Profile </h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Qualification :</td>
             <td><?PHP echo $job['qualification']; ?></td>
         </tr>
         <tr>
             <td width="40%">Profession :</td>
             <td>
             <?PHP 
                $job_id = $job['job']; 
                $job_title = get_value('jobs', $job_id);
                echo $job_title['job_title'];
             ?></td>
         </tr>
         <tr>
             <td width="40%">Type of Job :</td>
             <td><?PHP echo $job['job_type']; ?></td>
         </tr>
         <tr>
             <td width="40%">Place of Job :</td>
             <td><?PHP echo $job['place']; ?></td>
         </tr>
         <tr>
             <td width="40%">Salary :</td>
             <td><?PHP echo $job['salary']; ?> Lakh/Annum</td>
         </tr>
         <tr>
             <td width="40%">About Career :</td>
             <td><?PHP echo nl2br($job['abt_job']); ?></td>
         </tr>
</table>
<?PHP
    }
    $q3 = "SELECT * FROM profile4 WHERE user_id = '$user_id' LIMIT 1";
    $r3 = $db->select($q3);

    if(!$r3){        
        echo "Incomplete Data.<br>";
    }else{
        $prefer = $r3->fetch_assoc();  
?>
     <h4 class="text-success">Profile Preferences </h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Minimum Age :</td>
             <td><?PHP echo $prefer['minage']; ?> Years</td>
         </tr>
         <tr>
             <td width="40%">Maximum Age :</td>
             <td><?PHP echo $prefer['maxage']; ?> Years</td>
         </tr>
         <tr>
             <td width="40%">Caste :</td>
             <td>
             <?PHP 
                if($prefer['caste']=='0'){
                    echo "Any Caste";
                }else{
                    $caste = get_value('caste', $prefer['caste']);
                    echo $caste['caste_name'];
                }
            ?>
             </td>
         </tr>
         <tr>
             <td width="40%">Sub-Caste :</td>
             <td>
             <?PHP 
                if($prefer['sub_caste']==''){
                    echo "Any Sub-Caste";
                }else{
                    echo $prefer['sub_caste']; 
                }
            ?>
             </td>
         </tr>
         <tr>
             <td width="40%">Profession :</td>
             <td><?PHP 
                $job = get_value('jobs', $prefer['profession']);
                echo $job['job_title'];                 
            ?></td>
         </tr>
         <tr>
             <td width="40%">What I Want :</td>
             <td><?PHP echo nl2br($prefer['abt_partner']); ?></td>
         </tr>
</table>

<?PHP
    }
    echo '</div>';
    echo "<hr>";
    echo '<a href="javascript:history.back()" class="btn btn-sm btn-success">'."Go Back".'</a>';
    echo "<br><br>";
    echo "</div>";
?>
   <script type="text/javascript">
     function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
    }
    </script>
<?PHP
    include("mods/footer.php");
?>    