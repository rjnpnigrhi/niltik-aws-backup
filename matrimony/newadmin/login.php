<?PHP
    session_start();

	include("../inc/config.php");
	include("../inc/db_conn.php");
	include("../inc/functions.php");
	include("../inc/paginate.php");

    if(isset($_SESSION['admin_id'])){
		header("Location: index.php");
		exit;
	}

?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Niltik Matrimony Online : Administrators Login Form</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
<div style="text-align:center;" >
<?PHP
    if(isset($_GET['login'])){
        $email = $db->safe_data($_POST['email']);
        $password = $db->safe_data($_POST['password']);
        $password = md5($password);
			
        $q = "SELECT * FROM admin WHERE email = '$email' LIMIT 1";
        $r = $db->select($q);

        if(!$r){
            echo "Your Login Credentials are incorrect.<br>";
            echo '<a href="login.php">'."Go Back and Retry!".'</a>';
        }else{
            $row = $r->fetch_assoc();
            $db_password = $row['password'];
            if($db_password == $password){
                $_SESSION['logged_in'] = TRUE;
                $_SESSION['admin_id'] = $row['id'];
                $_SESSION['name'] = $row['name'];
                $_SESSION['last_login'] = $row['login_time'];
               
                $time = date('F d, Y h:i A', time());
                $id = $row['id'];
                
                $q1 = "UPDATE admin SET login_time = '$time' WHERE id = '$id'";
                $r1 = $db->update($q1);
				echo "<br><br><br><br><br>";				
                echo "You are successfully Logged in.<br>";
                echo '<a href="index.php">'."Administrators Dashboard".'</a>'."<br>";
				echo '<a href="logout.php">'."Log Out".'</a>';
				echo "<br><br><br><br><br>";
            }else{
                echo "Your Login Credentials are incorrect.<br>";
                echo '<a href="login.php">'."Go Back and Retry!".'</a>';
            }
        }
    echo '</div>';
    }else{
?>
    <hgroup>
    <h1>Niltik Matrimony Administrator</h1>
    <h3>Login Here</h3>
    </hgroup>
<form method="post" action="login.php?login=true">
  <div class="group">
    <input type="email" name="email"><span class="highlight"></span><span class="bar"></span>
    <label>Email</label>
  </div>
  <div class="group">
    <input type="password" name="password"><span class="highlight"></span><span class="bar"></span>
    <label>Password</label>
  </div>
  <button type="submit" class="button buttonBlue">Log In
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>
<?PHP
    }
?>
<footer>
  <p>You Gotta Love <a href="http://www.odiaguru.com/" target="_blank">Odiaguru</a></p>
</footer>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>