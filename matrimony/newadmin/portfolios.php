<?PHP
    session_start();

	include("../inc/config.php");
	include("../inc/db_conn.php");
	include("../inc/functions.php");
	include("../inc/paginate.php");
    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    //error_reporting(0);

    if(!isset($_SESSION['admin_id'])){
		header("Location: login.php");
		exit;
	}
    include("mods/header.php");

    if(isset($_GET['delete'])){
        //we will show some infor and ask for confirmation
        $id = $db->safe_data($_GET['delete']);
        $user = get_value('profile1', $id);
        
        echo '<h2>'."Do you want to Delete, ".'</h2>';
        echo '<h4>'.$user['name'].'</h4>';
        echo '<p>'.$user['age'].", ".ucfirst($user['sex']).'</p>';
        echo '<p>'."Created on : ". get_date($user['date']).'</p>';
        echo "<hr>";
        echo "Are you sure you want to delete this portfolio?<br>";
        echo "<br>";
        echo '<a href="portfolios.php?delete_final='.$id.'" class="btn btn-xs btn-danger">'."Yes, Delete it.".'</a>'." | ".'<a href="portfolios.php" class="btn btn-xs btn-primary">'."Cancel".'</a>';
        
    }elseif(isset($_GET['delete_final'])){
		$id = $_GET['delete_final'];
        //we will finally delete the profile
        delete_row('profile1', $id);
        
        echo "You deleted a Profile from our Portfolios.<br>";
        echo '<a href="portfolios.php" class="btn btn-xs btn-success">'."Portfolios Manager".'</a>';
    }else{
?>
      <h1>Portfolio Manager
        <a href="logout.php" class="btn btn-sm btn-danger pull-right">Log Out</a> </h1> 
        <hr>
       <table id="users_tbl" class="table table-striped">
          <thead>
           <tr>
               <th>Action</th>
			   <th width="120">Image</th>
               <th>Name</th>
               <th>age</th>
               <th>Sex</th>
			   <th>Caste</th>
               <th>Created by</th>
               <th>Date</th>
           </tr>
       </thead>
       <tbody>
        <?PHP
            $users = get_values("profile1");
        
            foreach($users as $user):
        ?>
            <tr>
                <td>
                <a href="view_portfolio.php?id=<?PHP echo $user['user_id']; ?>" class="btn btn-xs btn-primary"><i class="fa fa-search"></i></a> 
                <a href="portfolios.php?delete=<?PHP echo $user['id']; ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>              
                </td>
                <td>
					<?PHP
						if($user['image']==''){
							echo "No Image";
						}else{
							echo '<img src="../uploads/'.$user['image'].'" class="img-responsive" width="120px">';
						}
					?>
				</td>
                <td>
                <?PHP 
                    echo '<strong>'.$user['name'].'</strong>'."<br>";
                    echo $user['mobile']."<br>";
					echo nl2br($user['address'])."<br>";
					echo $user['district']."<br>";
					$state = get_value('state', $user['state']);
					echo $state['name']."<br>";
                ?>                
                </td>
                <td><?PHP echo $user['age']; ?></td>
                <td><i class="fa fa-<?PHP echo $user['sex']; ?>"></i> <?PHP echo $user['sex']; ?></td>
				<td>
					<?PHP
						$caste_id = $user['caste'];
						$caste = get_value('caste', $caste_id);
						echo $caste['caste_name'];
					?>
				</td>
                <td>
                <?PHP 
                    $user_id = $user['user_id']; 
                    $p = get_value("users", $user_id);
                    echo '<strong>'.$p['name'].'</strong>';
                    echo "<br>";
                    echo $p['mobile'];
                ?>                
                </td>
                <td><i class="fa fa-calendar"></i>  <?PHP echo get_date($user['date']); ?></td>
           </tr>
        <?PHP
            endforeach;
        ?>
        </tbody>
        </table>
        <br>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#users_tbl').DataTable();
        } );
        </script>
<?PHP 
    }
    include("mods/footer.php"); 
?>