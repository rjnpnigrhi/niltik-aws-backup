<?PHP
    session_start();

	include("../inc/config.php");
	include("../inc/db_conn.php");
	include("../inc/functions.php");
	include("../inc/paginate.php");
    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    //error_reporting(0);

    if(!isset($_SESSION['admin_id'])){
		header("Location: login.php");
		exit;
	}
    include("mods/header.php");
?>
    <h1>Dashboard <a href="logout.php" class="btn btn-sm btn-danger pull-right">Log Out</a></h1>
    
    <div class="col-md-3 db-box">
        <h1 class="dbl text-center">Users</h1>
        <h2 class="dbll text-center">
            <?PHP
                $q = "SELECT * FROM users";
                $r = $db->select($q);
                $user = $r->num_rows;
                echo $user;
            ?>
        </h2>
    </div>
    
    <div class="col-md-3 db-box">
        <h1 class="dbl text-center">Portfolios</h1>
        <h2 class="dbll text-center">
            <?PHP
                $q1 = "SELECT * FROM profile1";
                $r1 = $db->select($q1);
                $portfolios = $r1->num_rows;
                echo $portfolios;
            ?>
        </h2>
    </div>
    
    <div class="col-md-3 db-box">
        <h1 class="dbl text-center">Security</h1>
        <h2 class="dbll text-center"><a href="password.php" class="btn btn-primary">Update Password</a></h2>
    </div>
    
    <div class="col-md-3 db-box">
        <h1 class="dbl text-center">Sign Out</h1>
        <h2 class="dbll text-center"><a href="logout.php" class="btn btn-danger">Sign Out</a></h2>
    </div>
    
    <div class="clearfix"></div>
<?PHP include("mods/footer.php"); ?>