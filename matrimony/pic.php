<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if($user['status']!=='approved'){
        header("Location: dashboard.php");
    }

    include("mods/header.php");
?>
  <script type="text/javascript">
$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 200,
        height: 200,
        type: 'circle'
    },
    boundary: {
        width: 300,
        height: 300
    }
});
</script>
   <div class="panel panel-default">
       <div class="panel-heading">
           Select an Image for Upload
       </div>
       <div class="panel-body" align="center">
           <a class="btn file-btn">
             <span>Upload</span>
             <input type="file" id="upload" value="Choose a file" accept="image/*" />
             </a>
             <button class="upload-result">Result</button>
           <br>
        <div class="upload-demo-wrap">
            <div id="upload-demo"></div>
        </div>
       </div>
   </div>	
<?PHP 
    include("mods/footer.php");
?>


<div id="uploadimageModal" class="modal" role="dialog">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Upload &amp; Crop Image</h4>
           </div>
           <div class="modal-body">
               <div class="row">
                   <div class="col-md-8 text-center">
                       <div id="image_demo" style="width:350px; margin-top:30px;"></div>
                   </div>
                   <div class="col-md-4" style="padding-top:30px;">
                       <br>
                       <br>
                       <br>
                       <button class="btn btn-success crop-image">Crop &amp; Upload Image</button>
                   </div>
               </div>
           </div>
           <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
       </div>
   </div>
    
</div>


