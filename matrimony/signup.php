<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(isset($_SESSION['user_id'])){
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    
<body>
    <div class="container">
	<br>
      <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
	<br>
        </div>
       <br>
       
    <div class="clearfix"></div>
        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 info-box">
           <?PHP
           if(isset($_GET['submit'])){
                $ur_otp = $db->safe_data($_POST['otp']);
                $password = $db->safe_data($_POST['password']);
                $password = md5($password);
                $name = $_SESSION['name'];
                $email = $_SESSION['email'];
                $mobile = $_SESSION['mobile'];
                $otp = $_SESSION['otp'];
                $date = date("m/d/Y");

                if($ur_otp != $otp){
                    echo "The OTP is incorrect. <br>Go Back and Try again.<br>";
                }else{
                    $q = "INSERT INTO users (id, name, email, mobile, password, date) VALUES (NULL, '$name', '$email', '$mobile', '$password', '$date')";
                    $r = $db->insert($q);

                    echo "Welcome to NILTIK MATRIMONY.<br>";
                    echo "Your Registratin is successful.<br>";
                    echo '<a href="login.php" class="btn btn-xs btn-success">'."Login Now".'</a>';
                }

           }elseif(isset($_GET['getotp'])){
           		$otp = rand(100000,999999);
				$_SESSION['otp'] = $otp;
				$name = $db->safe_data($_POST['name']);
	           	$email = $db->safe_data($_POST['email']);
	           	$mobile = $db->safe_data($_POST['mobile']);
	           	$_SESSION['name'] = $name;
	           	$_SESSION['email'] = $email;
	           	$_SESSION['mobile'] = $mobile;
	           		
	                if(value_exists('users', 'email', $email)>0){
                        echo "<br>";
                        echo "The e-Mail ID is already on our Database.<br>";
                        echo "Try Registration with a new email.<br>";
                        echo "If you have forgotten your Password, Click Below.<br>";
                        echo '<a href="forgotpass.php" class="btn btn-xs btn-default">'."Click Here".'</a>';
                        echo "<br><br>";
                    	}elseif(value_exists('users', 'mobile', $mobile)>0){
                        echo "<br>";
                        echo "The Mobile Number is already on our Database.<br>";
                        echo "Try Registration with a new Mobile No.<br>";
                        echo "If you have forgotten your Password, Click Below.<br>";
                        echo '<a href="forgotpass.php" class="btn btn-xs btn-default">'."Click Here".'</a>';
                        echo "<br><br>";
	               }else{
           		$msg = $otp." is your OTP for Registration on NILTIK MATRIMONY";
		        
			//start here
		        $encoded_msg= urlencode($msg);
		        //Create API URL
		        $fullapiurl="http://api.msg91.com/api/sendhttp.php?sender=KALNJI&route=4&mobiles=$mobile&authkey=215426ARSXDZ9i5af98d91&country=91&message=$encoded_msg";
		        //Call API
		        $ch = curl_init($fullapiurl);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        $result = curl_exec($ch);
		        //echo $result ; // For Report or Code Check
		        curl_close($ch);
			//end here
			}
		        ?>
           <h2>Sign up!</h2>
            <hr>
            <form action="signup.php?submit=true" method="post" class="form">
            <?PHP
            	if(isset($error)){
            		echo $error."<br>";
            	}
            ?>
                <div class="form-group">
                    <input type="text" name="otp" placeholder="Type your OTP" required class="form-control">
                </div>               
                <div class="form-group">
                    <input type="password" name="password" placeholder="Type a Password" required class="form-control">
                </div>
                <div class="form-group">                	
                    <button class="btn btn-md btn-success btn-block" type="submit">Sign Up!</button>
                </div>
                <hr>
                
                <p class="small"><a href="login.php" class="btn btn-xs btn-primary">Already Registered! Sign in!</a> <a href="forgotpass.php" class="btn btn-xs btn-warning">Forgot Password!</a></p>
            </form>
           <?PHP	
                }else{
            
            ?>
            <h2>Sign up!</h2>
            <hr>
            <form action="signup.php?getotp=true" method="post" class="form">
                <div class="form-group">
                    <input type="text" name="name" placeholder="Type your Name" required class="form-control">
                </div>
                <div class="form-group">
                    <input type="email" name="email" placeholder="Type your eMail ID" required class="form-control">
                </div>
                <div class="form-group">
                    <input type="text" name="mobile" placeholder="Type your Mobile No" required class="form-control">
                </div>
               <p class="help-block">You will shortly receive an OTP on your Mobile Number</p>
                <div class="form-group">
                    <button class="btn btn-md btn-success btn-block" type="submit">Get OTP!</button>
                </div>
                <hr>
                
                <p class="small"><a href="login.php" class="btn btn-xs btn-primary">Already Registered! Sign in!</a> <a href="forgotpass.php" class="btn btn-xs btn-warning">Forgot Password!</a></p>
            </form>
            <?PHP } ?>
        </div>
        <div class="clearfix"></div>
        <hr>
        <p class="text-center info-text">For any Queries please contact : <strong><em>7008584789</em></strong></p>
    </div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>