<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-default btn-sm">Family</a>
   <a href="photos.php" class="btn btn-default btn-sm">Photos</a>
   <a href="address.php" class="btn btn-default btn-sm">Contact</a>
   <a href="education.php" class="btn btn-default btn-sm">Education</a>
   <a href="career.php" class="btn btn-default btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-primary btn-sm">Requirements</a>
   </div>
<?PHP
    //address.php
    $portfolio_id = $user['profile_id'];
    if(isset($_GET['insert'])){
        //inserrt data
        $min_qualification = $db->safe_data($_POST['min_qualification']);
        $max_qualification = $db->safe_data($_POST['max_qualification']);
        $min_age = $db->safe_data($_POST['min_age']);
        $max_age = $db->safe_data($_POST['max_age']);
        $min_height = $db->safe_data($_POST['min_height']);
        $max_height = $db->safe_data($_POST['max_height']);
        $family_type = implode(",",$_POST['family_type']);
        $family_status = implode(",",$_POST['family_status']);
        $employment_type = implode(",",$_POST['employment_type']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "INSERT INTO requirements (min_qualification, max_qualification, min_age, max_age, min_height, max_height, family_type, family_status, employment_type, about, portfolio_id) VALUES ('$min_qualification', '$max_qualification', '$min_age', '$max_age', '$min_height', '$max_height', '$family_type', '$family_status', '$employment_type', '$about', '$portfolio_id')";
        $r = $db->insert($q);
        
        $q1 = "UPDATE users SET profile_status = profile_status+10 WHERE profile_id = '$portfolio_id'";
        $r1 = $db->update($q1);
        
        inform_user($portfolio_id);

        echo "You have added your requirement details successfully.<br>";
        echo "Please complete all parts of the portfolio to get approved.<br>";
        echo '<a href="requirements.php" class="btn btn-sm btn-primary">'."View Requirements".'</a>';
        
    }elseif(isset($_GET['update'])){
        //update data
        $min_qualification = $db->safe_data($_POST['min_qualification']);
        $max_qualification = $db->safe_data($_POST['max_qualification']);
        $min_age = $db->safe_data($_POST['min_age']);
        $max_age = $db->safe_data($_POST['max_age']);
        $min_height = $db->safe_data($_POST['min_height']);
        $max_height = $db->safe_data($_POST['max_height']);
        $family_type = implode(",",$_POST['family_type']);
        $family_status = implode(",",$_POST['family_status']);
        $employment_type = implode(",",$_POST['employment_type']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "UPDATE requirements SET min_qualification = '$min_qualification', max_qualification = '$max_qualification', min_age = '$min_age', max_age = '$max_age', min_height = '$min_height', max_height = '$max_height', family_type = '$family_type', family_status = '$family_status', employment_type = '$employment_type', about = '$about' WHERE portfolio_id = '$portfolio_id'";
        $r = $db->update($q);

        echo "You have updated your requirement details successfully.<br>";
        echo '<a href="requirements.php" class="btn btn-sm btn-primary">'."View Requirements".'</a>';
        
    }else{

    if(value_exists('requirements', 'portfolio_id', $portfolio_id)>0){
        //show the data for updation
        $q = "SELECT * FROM requirements WHERE portfolio_id = '$portfolio_id'";
        $r = $db->select($q);
        
        $row = $r->fetch_assoc();
?>
   <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>I am Looking for (10%)</h2>
   <br>
   <form action="requirements.php?update=true" method="post" class="form">
       <div class="form-group">
       <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Qualification</label>
               <select name="min_qualification" class="form-control" id="">
                  <option value="<?PHP echo $row['min_qualification']; ?>" selected><?PHP echo $row['min_qualification']; ?></option>
                   <option value="">Not Important</option>
                   <option value="highschool">Some Schooling</option>
                   <option value="graduation">Graduate</option>
                   <option value="post_graduation">Post-graduate</option>
                   <option value="qualification">Professional Degree</option>
               </select>               
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Qualification</label>
               <select name="max_qualification" class="form-control" id="">
                  <option value="<?PHP echo $row['max_qualification']; ?>" selected><?PHP echo $row['max_qualification']; ?></option>
                   <option value="">Not Important</option>
                   <option value="highschool">Some Schooling</option>
                   <option value="graduation">Graduate</option>
                   <option value="post_graduation">Post-graduate</option>
                   <option value="qualification">Professional Degree</option>
               </select>    
           </div>
        </div>
       </div>
       
       <div class="form-group">
        <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Age</label>
              <input type="text" name="min_age" class="form-control" value="<?PHP echo $row['min_age']; ?>" required>       
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Age</label>
              <input type="text" name="max_age" class="form-control" value="<?PHP echo $row['max_age']; ?>" required>         
           </div>
        </div>
       </div>
       
       <div class="form-group">
        <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Height</label><br>
              <select name="min_height" class="form-control" id="height">
               <option value="4.5">4 Feet 5 Inches</option>
               <option value="4.6">4 Feet 6 Inches</option>
               <option value="4.7">4 Feet 7 Inches</option>
               <option value="4.8">4 Feet 8 Inches</option>
               <option value="4.9">4 Feet 9 Inches</option>
               <option value="4.10">4 Feet 10 Inches</option>
               <option value="4.11">4 Feet 11 Inches</option>
               <option value="5.0">5 Feet </option>
               <option value="5.1">5 Feet 1 Inch</option>
               <option value="5.2">5 Feet 2 Inches</option>
               <option value="5.3">5 Feet 3 Inches</option>
               <option value="5.4">5 Feet 4 Inches</option>
               <option value="5.5">5 Feet 5 Inches</option>
               <option value="5.6">5 Feet 6 Inches</option>
               <option value="5.7">5 Feet 7 Inches</option>
               <option value="5.8">5 Feet 8 Inches</option>
               <option value="5.9">5 Feet 9 Inches</option>
               <option value="5.10">5 Feet 10 Inches</option>
               <option value="5.11">5 Feet 11 Inches</option>
               <option value="6.0">6 Feet</option>
               <option value="6.1">6 Feet 1 Inch</option>
               <option value="6.2">6 Feet 2 Inch</option>
               <option value="6.3">6 Feet 3 Inch</option>
               <option value="6.4">6 Feet 4 Inch</option>
               <option value="6.5">6 Feet 5 Inch</option>
               <option value="6.6">6 Feet 6 Inch</option>
            </select>    
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Height</label><br>
             <select name="max_height" class="form-control" id="height">
               <option value="4.5">4 Feet 5 Inches</option>
               <option value="4.6">4 Feet 6 Inches</option>
               <option value="4.7">4 Feet 7 Inches</option>
               <option value="4.8">4 Feet 8 Inches</option>
               <option value="4.9">4 Feet 9 Inches</option>
               <option value="4.10">4 Feet 10 Inches</option>
               <option value="4.11">4 Feet 11 Inches</option>
               <option value="5.0">5 Feet </option>
               <option value="5.1">5 Feet 1 Inch</option>
               <option value="5.2">5 Feet 2 Inches</option>
               <option value="5.3">5 Feet 3 Inches</option>
               <option value="5.4">5 Feet 4 Inches</option>
               <option value="5.5">5 Feet 5 Inches</option>
               <option value="5.6">5 Feet 6 Inches</option>
               <option value="5.7">5 Feet 7 Inches</option>
               <option value="5.8">5 Feet 8 Inches</option>
               <option value="5.9">5 Feet 9 Inches</option>
               <option value="5.10">5 Feet 10 Inches</option>
               <option value="5.11">5 Feet 11 Inches</option>
               <option value="6.0">6 Feet</option>
               <option value="6.1">6 Feet 1 Inch</option>
               <option value="6.2">6 Feet 2 Inch</option>
               <option value="6.3">6 Feet 3 Inch</option>
               <option value="6.4">6 Feet 4 Inch</option>
               <option value="6.5">6 Feet 5 Inch</option>
               <option value="6.6">6 Feet 6 Inch</option>
            </select> 
           </div>
        </div>
       </div>
       
       <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="family_type">Family Type</label><br>
                          <input type="checkbox" name="family_type[]" value="Nuclear"> Nuclear <br>
                          <input type="checkbox" name="family_type[]" value="Joint"> Joint <br>
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="status">Family Status</label><br>
                          <input type="checkbox" name="family_status[]" value="Rich Class"> Rich Class <br>
                          <input type="checkbox" name="family_status[]" value="Middle Class"> Middle Class <br>
                          <input type="checkbox" name="family_status[]" value="Poor Class"> Poor Class <br>
                   </div>
               </div>
           </div>
           
          <div class="form-group">
              <label for="occupation">Employment Type : </label><br>
              <input type="checkbox" name="employment_type[]" value="Full Time"> Full Time &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Part Time &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Self Employed &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Unemployed &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Student &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Other &nbsp;
          </div>
       
       <div class="form-group">
           <label for="occupation">About my Choice : </label>
           <textarea name="about" id="about" class="form-control" rows="10" required placeholder="Brief Information"><?PHP echo $row['about']; ?></textarea>
       </div>
       <div class="form-group">
          <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">
           <button type="submit" class="btn btn-md btn-default">Submit</button>
       </div>
   </form>
   </div>
<?PHP
    }else{
?>
  <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>My Requirements (10%)</h2>
   <form action="requirements.php?insert=true" method="post" class="form">
       <div class="form-group">
       <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Qualification</label>
               <select name="min_qualification" class="form-control" id="">
                   <option value="">Not Important</option>
                   <option value="highschool">Some Schooling</option>
                   <option value="graduation">Graduate</option>
                   <option value="post_graduation">Post-graduate</option>
                   <option value="qualification">Professional Degree</option>
               </select>               
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Qualification</label>
               <select name="max_qualification" class="form-control" id="">
                   <option value="">Not Important</option>
                   <option value="highschool">Some Schooling</option>
                   <option value="graduation">Graduate</option>
                   <option value="post_graduation">Post-graduate</option>
                   <option value="qualification">Professional Degree</option>
               </select>    
           </div>
        </div>
       </div>
       
       <div class="form-group">
        <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Age</label>
              <input type="text" name="min_age" class="form-control" placeholder="Age in Years" required>       
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Age</label>
              <input type="text" name="max_age" class="form-control" placeholder="Age in Years" required>         
           </div>
        </div>
       </div>
       
        <div class="form-group">
        <div class="row">
           <div class="col-xs-6">
              <label for="qualification">Minimum Height</label><br>
              <select name="min_height" class="form-control" id="height">
               <option value="4.5">4 Feet 5 Inches</option>
               <option value="4.6">4 Feet 6 Inches</option>
               <option value="4.7">4 Feet 7 Inches</option>
               <option value="4.8">4 Feet 8 Inches</option>
               <option value="4.9">4 Feet 9 Inches</option>
               <option value="4.10">4 Feet 10 Inches</option>
               <option value="4.11">4 Feet 11 Inches</option>
               <option value="5.0">5 Feet </option>
               <option value="5.1">5 Feet 1 Inch</option>
               <option value="5.2">5 Feet 2 Inches</option>
               <option value="5.3">5 Feet 3 Inches</option>
               <option value="5.4">5 Feet 4 Inches</option>
               <option value="5.5">5 Feet 5 Inches</option>
               <option value="5.6">5 Feet 6 Inches</option>
               <option value="5.7">5 Feet 7 Inches</option>
               <option value="5.8">5 Feet 8 Inches</option>
               <option value="5.9">5 Feet 9 Inches</option>
               <option value="5.10">5 Feet 10 Inches</option>
               <option value="5.11">5 Feet 11 Inches</option>
               <option value="6.0">6 Feet</option>
               <option value="6.1">6 Feet 1 Inch</option>
               <option value="6.2">6 Feet 2 Inch</option>
               <option value="6.3">6 Feet 3 Inch</option>
               <option value="6.4">6 Feet 4 Inch</option>
               <option value="6.5">6 Feet 5 Inch</option>
               <option value="6.6">6 Feet 6 Inch</option>
            </select>    
           </div>
           <div class="col-xs-6">
              <label for="qualification">Maximum Height</label><br>
             <select name="max_height" class="form-control" id="height">
               <option value="4.5">4 Feet 5 Inches</option>
               <option value="4.6">4 Feet 6 Inches</option>
               <option value="4.7">4 Feet 7 Inches</option>
               <option value="4.8">4 Feet 8 Inches</option>
               <option value="4.9">4 Feet 9 Inches</option>
               <option value="4.10">4 Feet 10 Inches</option>
               <option value="4.11">4 Feet 11 Inches</option>
               <option value="5.0">5 Feet </option>
               <option value="5.1">5 Feet 1 Inch</option>
               <option value="5.2">5 Feet 2 Inches</option>
               <option value="5.3">5 Feet 3 Inches</option>
               <option value="5.4">5 Feet 4 Inches</option>
               <option value="5.5">5 Feet 5 Inches</option>
               <option value="5.6">5 Feet 6 Inches</option>
               <option value="5.7">5 Feet 7 Inches</option>
               <option value="5.8">5 Feet 8 Inches</option>
               <option value="5.9">5 Feet 9 Inches</option>
               <option value="5.10">5 Feet 10 Inches</option>
               <option value="5.11">5 Feet 11 Inches</option>
               <option value="6.0">6 Feet</option>
               <option value="6.1">6 Feet 1 Inch</option>
               <option value="6.2">6 Feet 2 Inch</option>
               <option value="6.3">6 Feet 3 Inch</option>
               <option value="6.4">6 Feet 4 Inch</option>
               <option value="6.5">6 Feet 5 Inch</option>
               <option value="6.6">6 Feet 6 Inch</option>
            </select> 
           </div>
        </div>
       </div>
       
       <div class="row">
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="family_type">Family Type</label>
                          <input type="checkbox" name="family_type[]" value="Nuclear">
                          <input type="checkbox" name="family_type[]" value="Joint">
                   </div>
               </div>
               <div class="col-xs-6">
                   <div class="form-group">
                       <label for="status">Family Status</label>
                          <input type="checkbox" name="family_status[]" value="Rich Class">
                          <input type="checkbox" name="family_status[]" value="Middle Class">
                          <input type="checkbox" name="family_status[]" value="Poor Class">
                   </div>
               </div>
           </div>
       <div class="form-group">
              <label for="occupation">Employment Type : </label><br>
              <input type="checkbox" name="employment_type[]" value="Full Time"> Full Time &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Part Time &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Self Employed &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Unemployed &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Student &nbsp;
              <input type="checkbox" name="employment_type[]" value="Full Time"> Other &nbsp;
          </div>
       <div class="form-group">
           <label for="occupation">About my Choice : </label>
           <textarea name="about" id="about" class="form-control" rows="10" required placeholder="Brief Information"></textarea>
       </div>
       <div class="form-group">
          <input type="hidden" name="portfolio_id" value="<?PHP echo $user['profile_id']; ?>">
           <button type="submit" class="btn btn-md btn-default">Submit</button>
       </div>
   </form>
   </div>
<?PHP
    }
    }
    include("mods/footer.php");
?>