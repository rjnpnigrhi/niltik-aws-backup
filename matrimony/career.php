<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-default btn-sm">Family</a>
   <a href="photos.php" class="btn btn-default btn-sm">Photos</a>
   <a href="address.php" class="btn btn-default btn-sm">Contact</a>
   <a href="education.php" class="btn btn-default btn-sm">Education</a>
   <a href="career.php" class="btn btn-primary btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-default btn-sm">Requirements</a>
   </div>
<?PHP
    if(isset($_GET['insert'])){
        //insert into database
        $ocupation = $db->safe_data($_POST['ocupation']);
        $place = $db->safe_data($_POST['place']);
        $annual_income = $db->safe_data($_POST['annual_income']);
        $employment_type = $db->safe_data($_POST['employment_type']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "INSERT INTO career (ocupation, place, annual_income, employment_type, about, portfolio_id) VALUES ('$ocupation', '$place', '$annual_income', '$employment_type', '$about', '$portfolio_id')";
        $r = $db->insert($q);
        
        $q1 = "UPDATE users SET profile_status = profile_status+10 WHERE profile_id = '$portfolio_id'";
        $r1 = $db->update($q1);
        
        inform_user($portfolio_id);
        
        echo "Thank You for updating your Professional profile.<br>";
        echo "Please complete all parts of the portfolio to get approved.<br>";
        echo '<a href="career.php" class="btn btn-sm btn-primary">'."View Professional Profile".'</a>';
        
    }elseif(isset($_GET['update'])){
        $ocupation = $db->safe_data($_POST['ocupation']);
        $place = $db->safe_data($_POST['place']);
        $annual_income = $db->safe_data($_POST['annual_income']);
        $employment_type = $db->safe_data($_POST['employment_type']);
        $about = $db->safe_data($_POST['about']);
        $portfolio_id = $db->safe_data($_POST['portfolio_id']);
        
        $q = "UPDATE career SET ocupation = '$ocupation', place = '$place', annual_income = '$annual_income', employment_type = '$employment_type', about = '$about' WHERE portfolio_id = '$portfolio_id'";
        $r = $db->update($q);
        
        echo "Thank You for updating your Professional profile.<br>";
        echo '<a href="career.php" class="btn btn-sm btn-primary">'."View Professional Profile".'</a>';
        
    }else{
    //address.php
    $portfolio_id = $user['profile_id'];

    if(value_exists('career', 'portfolio_id', $portfolio_id)>0){
        //show the data for updation
        $q = "SELECT * FROM career WHERE portfolio_id = '$portfolio_id'";
        $r = $db->select($q);
        
        $row = $r->fetch_assoc();
        
?>
   <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>Career Profile (10%)</h2>
   <form action="career.php?update=true" method="post" class="form">
       <div class="form-group">
           <label for="occupation">Current Profession : </label>
           <input type="text" name="ocupation" class="form-control" value="<?PHP echo $row['ocupation']; ?>" required>
       </div>
       <div class="form-group">
           <label for="occupation">Place of Job : </label>
           <input type="text" name="place" class="form-control" value="<?PHP echo $row['place']; ?>" required>
       </div>
       <div class="row">
           <div class="col-xs-6">
               <div class="form-group">
                   <label for="occupation">Annual Salary : </label>
                   <input type="text" name="annual_income" class="form-control" value="<?PHP echo $row['annual_income']; ?>" required>
               </div>
           </div>
           <div class="col-xs-6">
               <div class="form-group">
                   <label for="occupation">Employment Type : </label>
                   <select name="employment_type" id="" class="form-control" required>
                      <option value="<?PHP echo $row['employment_type']; ?>" selected><?PHP echo ucfirst($row['employment_type']); ?></option>
                       <option value="full_time">Full Time</option>
                       <option value="part_time">Part Time</option>
                       <option value="self_employed">Self Employed</option>
                       <option value="unemployed">Unemployed</option>
                       <option value="student">Student</option>
                       <option value="other">Other</option>
                   </select>
               </div>
           </div>
       </div>
       <div class="form-group">
           <label for="occupation">About Profession : </label>
           <textarea name="about" id="about" class="form-control" rows="10" required><?PHP echo $row['about']; ?></textarea>
       </div>
       <div class="form-group">
          <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">
           <button type="submit" class="btn btn-md btn-default">Submit</button>
           <a href="requirements.php" class="btn btn-md btn-primary">Proceed to Requirements Profile</a>
       </div>
   </form>
   </div>
<?PHP
    }else{
?>
  <div class="col-md-6 col-md-offset-3 col-xs-12">
   <h2>Career Profile (10%)</h2>
   <form action="career.php?insert=true" method="post" class="form">
       <div class="form-group">
           <label for="occupation">Current Profession : </label>
           <input type="text" name="ocupation" class="form-control" placeholder="Occupation" required>
       </div>
       <div class="form-group">
           <label for="occupation">Place of Job : </label>
           <input type="text" name="place" class="form-control" placeholder="Place of Job" required>
       </div>
       <div class="row">
           <div class="col-xs-6">
               <div class="form-group">
                   <label for="occupation">Annual Salary : </label>
                   <input type="text" name="annual_income" class="form-control" placeholder="Annual Salary" required>
               </div>
           </div>
           <div class="col-xs-6">
               <div class="form-group">
                   <label for="occupation">Employment Type : </label>
                   <select name="employment_type" id="" class="form-control" required>
                       <option value="full_time">Full Time</option>
                       <option value="part_time">Part Time</option>
                       <option value="self_employed">Self Employed</option>
                       <option value="unemployed">Unemployed</option>
                       <option value="student">Student</option>
                       <option value="other">Other</option>
                   </select>
               </div>
           </div>
       </div>
       <div class="form-group">
           <label for="occupation">About Profession : </label>
           <textarea name="about" id="about" class="form-control" rows="10" required></textarea>
       </div>
       <div class="form-group">
          <input type="hidden" name="portfolio_id" value="<?PHP echo $portfolio_id; ?>">
           <button type="submit" class="btn btn-md btn-default">Submit</button>
       </div>
   </form>
   </div>
<?PHP
    }
    }
    include("mods/footer.php");
?>