<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>

        <?PHP
            if($user['profile_id']==''){
        ?>
        <div class="action-box">
           <h1 class="text-center">You do not have a portfolio!</h1>
           <h4 class="text-center">Please click on the button below and create one.</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">
                <?PHP
                    if(isset($_GET['add'])){
                        //show form to add new profile
                ?>
                   
                <?PHP
                    }elseif(isset($_GET['insert_profile'])){
                        $user_id = $db->safe_data($_POST['user_id']);
                        $name = $db->safe_data($_POST['name']);
                        $dob = $db->safe_data($_POST['dob']);
                        $age = $db->safe_data($_POST['age']);
                        $sex = $db->safe_data($_POST['sex']);
                        $height = $db->safe_data($_POST['height']);
                        $marital_status = $db->safe_data($_POST['marital_status']);
                        $zodiac_sign = $db->safe_data($_POST['zodiac_sign']);
                        $gothram = $db->safe_data($_POST['gothram']);
                        $relationship = $db->safe_data($_POST['relationship']);
                        $email = $db->safe_data($_POST['email']);
                        $mobile = $db->safe_data($_POST['mobile']);
                        $date = date("m/d/Y");                        
                        
                        if(value_exists('portfolios', 'user_id', $user_id)>0){
                            echo "A portfolio already exists with your profile.<br>";
                            echo "Please contact admin for the Problem.<br>";
                        }else{                            
                        
                        $q = "INSERT INTO portfolios (user_id, relationship, name, age, sex, height, marital_status, zodiac_sign, about, date) VALUES ('$user_id', '$relationship', '$name', '$age', '$sex', '$height', '$marital_status', '$zodiac_sign', '$about', '$date')";
                        $r = $db->insert($q);
                        
                        update_profile($user_id);
                        
                        echo "You have successfully created your Portfolio.<br>";
                        echo "Please complete all parts of the portfolio to get approved.<br>";
                        echo '<a href="profile.php" class="btn btn-sm btn-primary">'."Proceed to Portfolio".'</a>';
                        }
                    }else{
                ?>
               <form action="dashboard.php?insert_profile=true" method="post" class="form">
					<div class="form-group">
						<select class="form-control" name="relationship">
							<option value="">Creating Profile for</option>
							<option value="self">Myself</option>
							<option value="son">Son</option>
							<option value="daughter">Daughter</option>
							<option value="brother">Brother</option>
							<option value="sister">Sister</option>
							<option value="relative">Relative</option>
							<option value="friend">Friend</option>
						</select>
					</div>
                    <div class="form-group">
                       <label for="name">Name of Portfolio : </label>
                        <input type="text" name="name" class="form-control" placeholder="Type Name Here" required>
                    </div>
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Date of Birth : </label>
                       <div class="input-group date" data-provide="datepicker">
						<input type="text" class="form-control">
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
					<script type="text/javascript">
						$(function () {
							$('#datetimepicker1').datetimepicker();
						});
					</script>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Age in Years : </label>
                        <input type="text" name="age" class="form-control" placeholder="Age in Years" required>
                    </div>
						</div>
					</div>
                    
					
					<label for="sex">Sex : &nbsp; &nbsp;</label>
					<div class="row">                       
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio1" value="male"> Male
                        </label>
						</div>
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio2" value="female"> Female
                        </label>
						</div>
						<div class="clearfix"></div>
						<br>
					</div>
                    
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="height">Height in Feet</label>
                       <select name="height" id="height" class="form-control">
                           <option value="4.5">4 Feet 5 Inches</option>
                           <option value="4.6">4 Feet 6 Inches</option>
                           <option value="4.7">4 Feet 7 Inches</option>
                           <option value="4.8">4 Feet 8 Inches</option>
                           <option value="4.9">4 Feet 9 Inches</option>
                           <option value="4.10">4 Feet 10 Inches</option>
                           <option value="4.11">4 Feet 11 Inches</option>
                           <option value="5.0">5 Feet </option>
                           <option value="5.1">5 Feet 1 Inch</option>
                           <option value="5.2">5 Feet 2 Inches</option>
                           <option value="5.3">5 Feet 3 Inches</option>
                           <option value="5.4">5 Feet 4 Inches</option>
                           <option value="5.5">5 Feet 5 Inches</option>
                           <option value="5.6">5 Feet 6 Inches</option>
                           <option value="5.7">5 Feet 7 Inches</option>
                           <option value="5.8">5 Feet 8 Inches</option>
                           <option value="5.9">5 Feet 9 Inches</option>
                           <option value="5.10">5 Feet 10 Inches</option>
                           <option value="5.11">5 Feet 11 Inches</option>
                           <option value="6.0">6 Feet</option>
                           <option value="6.1">6 Feet 1 Inch</option>
                           <option value="6.2">6 Feet 2 Inch</option>
                           <option value="6.3">6 Feet 3 Inch</option>
                           <option value="6.4">6 Feet 4 Inch</option>
                           <option value="6.5">6 Feet 5 Inch</option>
                           <option value="6.6">6 Feet 6 Inch</option>
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="height">Weight in KG</label>
                       <input type="number" name="weight" class="form-control" value="50" required >
                    </div>
						</div>
					</div>
                    
					
                    <div class="form-group">
                       <label for="marital_status">Marital Status :</label>
                        <select name="marital_status" id="marital_status" class="form-control">
								<option value="">Select Status</option>
                            <?PHP
                                $status = get_values('marital_status');
                                foreach($status as $stat){
                            ?>
                                 <option value="<?PHP echo $stat['id']; ?>"><?PHP echo $stat['marital_status']; ?></option>  
                            <?PHP                    
                                }
                            ?>
                        </select>
                    </div>
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Zodiac Sign (Rashi)</label>
                        <select name="zodiac_sign" id="zodiac_sign" class="form-control">
                           <?PHP
                                $signs = get_values('zodiac_sign');
                                foreach($signs as $sign){
                            ?>
                                 <option value="<?PHP echo $sign['id']; ?>"><?PHP echo $sign['zodiac_sign']; ?></option>  
                            <?PHP                    
                                }
                            ?>
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Gothram </label>
                        <input name="gothram" type="text" class="form-control" />
                    </div>
						</div>
					</div>
                    
					<div class="form-group">
						<label for="religion">Religion</label>
						<select class="form-control">
							<option value="hindu">Hindu</option>
							<option value="muslim">Muslim</option>
							<option value="christian">Christian</option>
							<option value="sikh">Sikh</option>
							<option value="jain">Jain</option>
						</select>
					</div>
										
					<label for="caste">Caste and Sub-caste</label>
					<div class="row">
						<div class="col-xs-6">
							<select name="caste" class="form-control">
								<option value="">Select Caste</option>
								<option value="caste1">Caste 1</option>
								<option value="caste2">Caste 2</option>
								<option value="caste3">Caste 3</option>
								<option value="caste4">Caste 4</option>
								<option value="caste5">Caste 5</option>
							</select>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
							<input name="sub-caste" type="text" class="form-control" placeholder="Type Sub caste" required >
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="email">eMail ID</label>
                        <input name="email" type="email" class="form-control" required />
                    </div>
					<div class="form-group">
                        <label for="mobile">Mobile</label>
                        <input name="mobile" type="text" class="form-control" required />
                    </div>
					<div class="form-group">
                        <label for="address">Address</label>
						<textarea name="address" class="form-control" rows="3" required></textarea>
                    </div>
                     <div class="form-group">
                        <input type="hidden" name="user_id" value="<?PHP echo $_SESSION['user_id']; ?>">
                        <button class="btn btn-md btn-default" type="submit">Submit</button>
                    </div>
                </form>
			   <?PHP } ?>
           </div>
        </div>
        <?PHP
            }else{
        ?>
        <div class="action-box">
           <div class="col-md-6 col-md-offset-3 col-xs-12">              
           <h2>My Details : </h2>
            <table class="table table-striped">
                <tr>
                    <td>Name : </td>
                    <td><?PHP echo $user['name']; ?></td>
                </tr>
                <tr>
                    <td>e-Mail : </td>
                    <td><?PHP echo $user['email']; ?></td>
                </tr>
                <tr>
                    <td>Mobile no :</td>
                    <td><?PHP echo $user['mobile']; ?></td>
                </tr>
                <tr>
                    <td>Date of Joining :</td>
                    <td><?PHP echo get_date($user['date']); ?></td>
                </tr>
                <tr>
                    <td>Profile ID : </td>
                    <td><?PHP echo $user['profile_id']; ?></td>
                </tr>
                <tr>
                    <td>Portfolio ID : </td>
                    <td>
                    <?PHP 
                        $profile_id = $user['profile_id'];
                        $portfolio = get_value('portfolios', $profile_id);    
                        echo $portfolio['portfolio_id']; 
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Profile Complete :</td>
                    <td>
                    <?PHP
                        $profile_status = $user['profile_status'];
                
                        if($profile_status < "100" ){
                    ?>
                    <strong class="text-danger"><?PHP echo $user['profile_status']; ?> % </strong> &nbsp; &nbsp;
                    <a href="profile.php" class="btn btn-xs btn-primary">Complete Portfolio</a>
                    <?PHP
                        }else{
                    ?>
                    <strong class="text-success"><?PHP echo $user['profile_status']; ?></strong>
                    <?PHP
                            if($user['status']!=='approved'){
                                
                                echo '<p class="small text-danger">'."Visit our Office / Call us for Verification.".'</p>';
                            }
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Desires to View : </td>
                    <td>
                    <?PHP
                        if($user['status']!=='approved'){
                            echo '<button class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="right" title="You can not visit Profiles Now!">'.ucfirst($user['desire']). " Profiles".'</button>';
                        }else{
                            if($user['desire']=='male'){
                                echo '<a href="boys.php" class="btn btn-xs btn-default">'.ucfirst($user['desire'])." Profiles".'</button>';
                            }else{
                                echo '<a href="girls.php" class="btn btn-xs btn-default">'.ucfirst($user['desire'])." Profiles".'</button>';
                            }
           
                        }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td>Last Login Time : </td>
                    <td><?PHP echo $user['login_time']; ?></td>
                </tr>
            </table>
            <hr>
            <a href="profile.php" class="btn btn-md btn-success pull-right">My Portfolio</a>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <?PHP
            }

            include("mods/footer.php");
        ?>
    