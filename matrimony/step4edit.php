<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect information about desires and requirements
include("mods/header.php");
?>
        <div class="row">
           <h4 class="text-center">Update your Preferences</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
            <?PHP
                if(isset($_GET['update'])){
                    //insert into profile2 table
                    $minage = $db->safe_data($_POST['minage']);
                    $maxage = $db->safe_data($_POST['maxage']);
                    $caste = $db->safe_data($_POST['caste']);
                    $sub_caste = $db->safe_data($_POST['sub_caste']);
                    $place = $db->safe_data($_POST['place']);
                    $profession = $db->safe_data($_POST['profession']);
                    $abt_partner = $db->safe_data($_POST['abt_partner']);
                    
                    $q = "UPDATE profile4 SET minage = '$minage', maxage = '$maxage', caste = '$caste', sub_caste = '$sub_caste', place = '$place', profession = '$profession', abt_partner = '$abt_partner' WHERE user_id = '$user_id'";
                    $r = $db->update($q);
                    
                    header("Location: profile.php");                                
                    echo '<a href="profile.php" class="btn btn-sm btn-primary">'."View Profile".'</a>';                      
                    
                }else{
                    //show the form here
                    $q3 = "SELECT * FROM profile4 WHERE user_id = '$user_id' LIMIT 1";
                    $r3 = $db->select($q3);

                    if(!$r3){        
                        header("Location: step3.php");
                    }else{
                        $prefer = $r3->fetch_assoc();
            ?>
            <form action="step4edit.php?update=true" method="post" class="form">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="minage">Minimum Age</label>
                            <input type="number" name="minage" class="form-control" required value="<?PHP echo $prefer['minage']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="maxage">Maximum Age</label>
                            <input type="number" name="maxage" class="form-control" required value="<?PHP echo $prefer['minage']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                          <label for="other">Caste Preference</label>
                          <select name="caste" class="form-control" id="caste">
                           <option value="">Any Caste</option>
                            <?PHP
                                $q = "SELECT * FROM caste ORDER BY caste_name ASC";
                                $r = $db->select($q);

                                while($caste = $r->fetch_array()){
                                    ?>
                                <option value="<?PHP echo $caste['id']; ?>" <?PHP if($caste['id']==$prefer['caste']){ echo "selected"; } ?>><?PHP echo ucfirst($caste['caste_name']); ?></option>       
                            <?PHP
                                }
                            ?>
                          </select>                  
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="religion">Sub-Caste (Optional)</label>
                            <input type="text" name="sub_caste" class="form-control" value="<?PHP echo $prefer['sub_caste']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="place">Place</label>
                            <select class="form-control" name="place">
						    <?PHP
                            $qs = "SELECT * FROM state ORDER BY id ASC";
                            $rs = $db->select($qs);
                            while($state = $rs->fetch_array()){
                            ?>
                            <option value="<?PHP echo $state['id']; ?>" <?PHP if($state['id']==$prefer['place']){ echo "selected"; } ?> ><?PHP echo $state['name']; ?></option>
                            <?PHP
                            }
                            ?>							
						</select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="profession">Profession</label>
                            <select name="profession" class="form-control" id="job">
                               <option value="">Any Profession</option>
                                <?PHP
                                    $q = "SELECT * FROM jobs ORDER BY job_title ASC";
                                    $r = $db->select($q);

                                    while($jobs = $r->fetch_array()){
                                        ?>
                      <option value="<?PHP echo $jobs['id']; ?>" <?PHP if($jobs['id']==$prefer['profession']){ echo "selected"; } ?>><?PHP echo $jobs['job_title']; ?></option>     
                    <?PHP
                                    }
                                ?>
                              </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abtjob">I am Looking for</label>
                    <textarea name="abt_partner" id="abt" class="form-control" rows="10" required><?PHP echo nl2br($prefer['abt_partner']); ?></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="profile.php" class="btn btn-sm btn-primary">Cancel</a>
                </div>
            </form>
            <?PHP
                }
                }
            ?>
           </div>
        </div>
<?PHP include("mods/footer.php"); ?>