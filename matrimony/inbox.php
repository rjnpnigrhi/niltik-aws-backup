<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect Photo of the user 
    include("mods/header.php");
?> 
    <div class="rows">
    <h3>Messages Inbox</h3>
    <hr>
    </div>
        
<?PHP
    include("mods/footer.php");
?>