<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect Photo of the user 
    include("mods/header.php");
    $me = get_data('profile1', $user_id);
    $prefer = get_data('profile4', $user_id);
    $desire = $user['desire'];
    
    $religion = $me['religion'];
    $minage = $prefer['minage'];
    $maxage = $prefer['maxage'];
    $caste = $prefer['caste'];
    $sub_caste = $prefer['sub_caste'];
    $profession = $prefer['profession'];
    echo '<h3>'.ucfirst($desire)." Profiles".'</h3>';
    $q = "SELECT * FROM profile1 WHERE sex = '$desire'";
    $q .= "AND religion = '$religion'";
    $q .= "AND age BETWEEN '$minage' AND '$maxage'";
    if($caste !==''){
    $q .= "AND caste = '$caste'";
    }
    if($sub_caste !==''){
    $q .= "AND sub_caste = '$sub_caste'";
    }
    $q .= "ORDER BY id DESC";
    $r = $db->select($q);

    if(!$r){
        echo "There are no profiles Matching Your Preference.<br>";
        echo "Visit us Again.<br>";
    }else{
        while($diaries = $r->fetch_array()){
                        $result[] = $diaries;
                    }

                    //here we start paginating the data
                    $numbers = $pagination->paginate($result, 2);

                    //what are the data to be presented in these pages
                    $data = $pagination->fetchresults();

                    //let us get the current page number
                    $pn = $pagination->page_num();

                    //let us get all page numbers 
                    $tp = count($numbers);

                    foreach($data as $row):
            ?>
            <div class="col-xs-12">
                <img src="uploads/<?PHP echo $row['image']; ?>" class="img-responsive img-rounded" width="100%" alt="<?PHP echo $row['name']; ?>">
                <h4 class="text-primary"><?PHP echo $row['name']; ?> <a href="view.php?id=<?PHP echo $row['user_id']; ?>" class="btn btn-xs btn-success pull-right"><i class="fa fa-search"></i></a></h4>
            <?PHP
                $profile_id = $row['user_id'];
                $q1 = "SELECT * FROM profile3 WHERE user_id = '$profile_id' LIMIT 1";
                $r1 = $db->select($q1);
            
                if(!$r){
                    //do nothing
                }else{
                    $job = $r1->fetch_assoc();
                    echo '<p class="text-center">'.$row['age']." yrs, ".$job['job'].", ".$row['caste'].", ".$row['religion'].'</p>';
                }
            ?>
            </div>
            <div class="clearfix"></div>
            <hr>
            <?PHP            
                    endforeach;
            ?>
            <div class="clearfix"></div>
                <nav class="nav-sm">
                <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$pp.'" aria-label="Previous"><< </a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$np.'" aria-label="Next"> >></a></li>';
                }
               ?>
              </ul>
            </nav>
<?PHP
    }    
    include("mods/footer.php");
?>