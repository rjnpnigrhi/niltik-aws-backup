<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	
	<!-- lightbox dependency files-->
	<link rel="stylesheet" href="css/lightbox.min.css">
	<script type="text/javascript" src="js/lightbox.min.js"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
    
<body>
    
    <div class="container">
    <br>
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="NILTIK MATRIMONY | ONLINE">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    <br>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><i class="fa fa-home"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  
          <ul class="nav navbar-nav navbar-right">
            <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="search.php"><i class="fa fa-search"></i> Search</a></li>
            <li><a href="likes.php"><i class="fa fa-heart"></i> Likes</a></li>
            <li><a href="inbox.php"><i class="fa fa-envelope"></i> Inbox</a></li>
            <li><a href="privasy.php"><i class="fa fa-key"></i> Privasy</a></li>
            <li><a href="logout.php"><i class="fa fa-sign-out"></i> Sign Out</a></li>            
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    
    <div class="container main-box">