</div>
    <!--container ends here-->
   
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>