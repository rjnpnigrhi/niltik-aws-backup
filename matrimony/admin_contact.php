<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
 <div class="container text-center">
     <h1>Contact Administrator</h1>
     <p>You can contact us at our Office Addresses given below</p>
 </div>
 
 <div class="container top-20">
    <div class="col-md-4">
     <h3>Head Office</h3>
     <p>Bhaba Nagar Square</p>
     <p>New Busstand, Berhampur</p>
     <p>PIN : 760001, Odisha</p>
     </div>
     
     <div class="col-md-4">
     <h3>Branch Office</h3>
     <p>Badadanda Street</p>
     <p>Bhanjanagar</p>
     <p>PIN : 760001, Odisha</p>
     </div>
     
     <div class="col-md-4">
     <h3>Branch Office</h3>
     <p>Main Road</p>
     <p>Jeypore, Koraput</p>
     <p>PIN : 760001, Odisha</p>
     </div>
     
     <div class="clearfix"></div>
     <hr>
     <br>
     <p class="text-center">In case of Emergency please contact : <strong>7 0 0 8 5 8 4 7 8 9</strong></p>
 </div>
<?PHP
    include("mods/footer.php");
?>