<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if(value_exists('profile5', 'user_id', $user_id)>0){
        header("Location: index.php");
    }

    //here we collect Photo of the user 
?>
     <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    
    <!-- Datatable requirements cdn link-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        img {
            max-width: 100%; important!
        }
    </style>
</head>
    
<body>
    
    <div class="container">
     <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
        </div>
    </div><!-- header box with logo image-->
    
    <div class="container main-box">   
        <div class="action-box">
           <h1 class="text-center text-success">Profile is incomplete without a Photo</h1>
           <h4 class="text-center">Select a very good quality photo of yours.</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12"> 
               <?PHP
                function crop_image($file, $max_resolution){

                    if(file_exists($file)){
                        $original_image = imagecreatefromjpeg($file);

                        $original_width = imagesx($original_image);
                        $original_height = imagesy($original_image);

                        //try max width first
                        if($original_height > $original_width){                            
                            $ratio = $max_resolution/$original_width;
                            $new_width = $max_resolution;
                            $new_height = $original_height*$ratio;
                            
                            $diff = $new_height - $new_width;
                            $x = 0;
                            $y = $diff;
                        }else{
                            $ratio = $max_resolution/$original_height;
                            $new_height = $max_resolution;
                            $new_width = $original_width*$ratio;
                            
                            $diff = $new_width - $new_height;
                            $x = $diff;
                            $y = 0;
                        }

                        if($original_image){
                            $new_image = imagecreatetruecolor($new_width,$new_height);
                            imagecopyresampled($new_image,$original_image,0,0,0,0,$new_width,$new_height,$original_width, $original_height);
                            
                            $new_crop_image = imagecreatetruecolor($max_resolution,$max_resolution);
                            if($new_height > $new_width){
                                imagecopyresampled($new_crop_image, $new_image,0,0,0,0,$max_resolution,$max_resolution, $max_resolution,$max_resolution);
                            }else{
                                imagecopyresampled($new_crop_image, $new_image,0,0,$x,$y,$max_resolution,$max_resolution, $max_resolution,$max_resolution);
                            }

                            imagejpeg($new_crop_image,$file,90);
                        }
                    }else{
                        echo "FILE NOT FOUND";
                    }
                }
	if(isset($_GET['upload'])){
        if($_FILES['image']){
            require_once("inc/wm.php");
            
            $new_file = time().".jpg";            
            
            move_uploaded_file($_FILES['image']['tmp_name'], "uploads/".$new_file);

            //resize file
            crop_image("uploads/".$new_file,"360");  
            $magicianObj = new imageLib("uploads/".$new_file);
            $magicianObj->addWatermark("img/wm.png", 'r', 0);
            $magicianObj->saveImage("uploads/".$new_file);           
            
                echo '<img src="uploads/'.$new_file.'" style="width:100%;">';
                echo "<br><br>";    
                echo '<a href="step5.php?confirm_photo='.$new_file.'" class="btn btn-sm btn-success">'."Confirm Photo".'</a>'. " " .'<a href="step5.php?discard_photo='.$new_file.'" class="btn btn-sm btn-primary">'."Change Photo".'</a>';
            }else{
                echo "FILE DID NOT UPLOAD";
            }
    }elseif(isset($_GET['confirm_photo'])){
        $image_name = $db->safe_data($_GET['confirm_photo']);
        $q = "INSERT INTO profile5 (id, user_id, image_name, featured) VALUES (NULL, '$user_id', '$image_name', '1')";
        $r = $db->insert($q);
        
        //insert this as featured image in profile 1 table
        //everytime the featuredd image is changed this table will be updated.
        $q1 = "UPDATE profile1 SET image = '$image_name' WHERE user_id = '$user_id'";
        $r1 = $db->update($q1);
        
        if(!$r){
            //do nothing
            header("Location:step5.php");
        }else{
            echo "Profile Completed Successfully.<br>";
            echo "You can edit your Profile from your profile section.<br>";
            echo "Proceed to visit your Profile " .'<a href="profile.php" class="btn btn-sm btn-success">'."Click Here".'</a>';
        }
        
    }elseif(isset($_GET['discard_photo'])){
        $photo = $db->safe_data($_GET['discard_photo']);
        unlink("uploads/".$photo);
        
        header("Location: step5.php");
	}else{
?>
           <form action="step5.php?upload=true" enctype="multipart/form-data" method="post" class="form">
            <label for="image">Select Image</label>
            <input type="file" name="image" accept="image/*" onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])" required>
            <p class="help-block">You must chose only JPG, JPEG, GIF, PNG files.</p>                    
            <img alt="" width="100%" id="image" />                       
            <button class="btn btn-sm btn-success">Upload</button>
       </form>
       <?PHP } ?>
        </div>
   </div>
    </div>
    <!--container ends here-->
   
    <footer class="footer-box">
        <div class="container">
            &copy; NILTIK MATRIMONY, 2018 <a href="privacy_policy.php" class="btn btn-xs btn-warning">Privacy Policy</a>
            <a href="#" class="btn btn-default btn-xs pull-right disabled"><i class="fa fa-globe"></i> <i class="fa fa-phone"></i> : 7008567085</a>
        </div>
    </footer>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>