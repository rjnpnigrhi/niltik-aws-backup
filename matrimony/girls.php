<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    if($user['status']!=='approved'){
        header("Location: dashboard.php");
    }

    include("mods/header.php");

    $q = "SELECT * FROM portfolios WHERE sex = 'female' AND portfolio_id !='' ORDER BY id DESC";
    $r = $db->select($q);
    
    if(!$r){
        
        echo "No Profiles Available.<br>";
    }else{
        while($g = $r->fetch_array()){
        echo '<div class="col-md-3 col-xs-12">';
        $p_id = $g['id'];
        $photo = get_user('photos', $p_id);
?>

        <img src="uploads/<?PHP echo $photo['image']; ?>" alt="<?PHP echo ucfirst($g['name']); ?>" class="img-responsive img-circle" width="100%">
        <br>
        <strong><?PHP echo $g['name']; ?></strong>
        <?PHP
            $job = get_user('career', $p_id);    
        ?>
        <p><?PHP echo $g['age']." Years, ".$g['height']." Ft"; ?> <span class="pull-right"><?PHP echo $job['ocupation']; ?></span></p>
    </div>
<?PHP
        }
    }
?>            
    </div><!-- main page box with three divs-->
    
    <div class="container">
        <div class="blackbox col-md-6 col-md-offset-3">
            If you require anytype of Assistance for your marriage organization, we can be of your help. Please consult with us for all kinds of necessary arrangements for Marriage including Kalyan Mandap, Band or DJ, Catering Service, Photographer, Videographer etc.
            <br>
            Call us at : <strong><em>7008584789</em></strong>
        </div>
<?PHP 
    include("mods/footer.php");
?>