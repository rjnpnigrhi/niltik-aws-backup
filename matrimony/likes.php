<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect Photo of the user 
    include("mods/header.php");
?> 
    <div class="col-xs-12">
    <h4>Profiles You Visited... <a href="#" class="btn btn-xs btn-primary pull-right">View All!</a></h4>
    <div class="row">
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    
    <div class="clearfix"></div>
    </div>
    <hr>
    </div>
    
    <div class="col-xs-12">
    <h4>Profile Visitors... <a href="#" class="btn btn-xs btn-primary pull-right">View All!</a></h4>
    <div class="row">
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    <div class="col-xs-4 top-20">
        <img src="img/sample2.jpg" width="100%" class="img-responsive img-rounded" alt="">
    </div>
    
    <div class="clearfix"></div>
    </div>
    <hr>
    </div>
        
<?PHP
    include("mods/footer.php");
?>