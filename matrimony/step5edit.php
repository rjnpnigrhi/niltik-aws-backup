<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect Photo of the user 
    include("mods/header.php");
?>  
        <div class="row">
           <h4 class="text-center">Add More Photos</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12"> 
               <?PHP
                function crop_image($file, $max_resolution){

                    if(file_exists($file)){
                        $original_image = imagecreatefromjpeg($file);

                        $original_width = imagesx($original_image);
                        $original_height = imagesy($original_image);

                        //try max width first
                        if($original_height > $original_width){                            
                            $ratio = $max_resolution/$original_width;
                            $new_width = $max_resolution;
                            $new_height = $original_height*$ratio;
                            
                            $diff = $new_height - $new_width;
                            $x = 0;
                            $y = $diff;
                        }else{
                            $ratio = $max_resolution/$original_height;
                            $new_height = $max_resolution;
                            $new_width = $original_width*$ratio;
                            
                            $diff = $new_width - $new_height;
                            $x = $diff;
                            $y = 0;
                        }

                        if($original_image){
                            $new_image = imagecreatetruecolor($new_width,$new_height);
                            imagecopyresampled($new_image,$original_image,0,0,0,0,$new_width,$new_height,$original_width, $original_height);
                            
                            $new_crop_image = imagecreatetruecolor($max_resolution,$max_resolution);
                            if($new_height > $new_width){
                                imagecopyresampled($new_crop_image, $new_image,0,0,0,0,$max_resolution,$max_resolution, $max_resolution,$max_resolution);
                            }else{
                                imagecopyresampled($new_crop_image, $new_image,0,0,$x,$y,$max_resolution,$max_resolution, $max_resolution,$max_resolution);
                            }

                            imagejpeg($new_crop_image,$file,90);
                        }
                    }else{
                        echo "FILE NOT FOUND";
                    }
                }
	if(isset($_GET['upload'])){
        if($_FILES['image']&&$_FILES['image']['type']=='image/jpeg'){
            require_once("inc/wm.php");
            
            $new_file = time().".jpg";            
            
            move_uploaded_file($_FILES['image']['tmp_name'], "uploads/".$new_file);

            //resize file
            crop_image("uploads/".$new_file,"360");  
            $magicianObj = new imageLib("uploads/".$new_file);
            $magicianObj->addWatermark("img/wm.png", 'r', 0);
            $magicianObj->saveImage("uploads/".$new_file);           
            
                echo '<img src="uploads/'.$new_file.'" style="width:100%;">';
                echo "<br><br>";    
                echo '<a href="step5edit.php?confirm_photo='.$new_file.'" class="btn btn-sm btn-success">'."Confirm Photo".'</a>'. " " .'<a href="step5edit.php?discard_photo='.$new_file.'" class="btn btn-sm btn-primary">'."Change Photo".'</a>';
            }else{
                echo "FILE DID NOT UPLOAD";
            }
    }elseif(isset($_GET['confirm_photo'])){
        $image_name = $db->safe_data($_GET['confirm_photo']);
        $q = "INSERT INTO profile5 (id, user_id, image_name) VALUES (NULL, '$user_id', '$image_name')";
        $r = $db->insert($q);
        
        if(!$r){
            //do nothing
            header("Location:profile.php");
        }else{
            header("Location:profile.php");
            echo "Proceed to visit your Profile " .'<a href="profile.php" class="btn btn-sm btn-success">'."Click Here".'</a>';
        }
        
    }elseif(isset($_GET['discard_photo'])){
        $photo = $db->safe_data($_GET['discard_photo']);
        unlink("uploads/".$photo);
        
        header("Location: step5edit.php");
	}else{
?>
           <form action="step5edit.php?upload=true" enctype="multipart/form-data" method="post" class="form">
            <label for="image">Select Image</label>
            <input type="file" name="image" accept="image/*" onchange="document.getElementById('image').src = window.URL.createObjectURL(this.files[0])" required>
            <p class="help-block">You must chose only JPG, JPEG, GIF, PNG files.</p>                    
            <img alt="" width="100%" id="image" />                       
            <button class="btn btn-sm btn-success">Upload</button>
       </form>
       <?PHP } ?>
        </div>
   </div>
<?PHP include("mods/footer.php"); ?>