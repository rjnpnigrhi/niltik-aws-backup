<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect information about profession and career
    include("mods/header.php");
?>
   
        <div class="row">
           <h4 class="text-center">Update Career and Profession Details</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
            <?PHP
                if(isset($_GET['update'])){
                    //insert into profile2 table
                    $qualification = $db->safe_data($_POST['qualification']);
                    $job = $db->safe_data($_POST['profession']);
                    $job_type = $db->safe_data($_POST['job_type']);
                    $place = $db->safe_data($_POST['place']);
                    $salary = $db->safe_data($_POST['salary']);
                    $abt_job = $db->safe_data($_POST['abt_job']);
                    
                    $q = "UPDATE profile3 SET qualification = '$qualification', job = '$job', job_type = '$job_type', place = '$place', salary = '$salary', abt_job = '$abt_job' WHERE user_id = '$user_id'";
                    $r = $db->update($q);
                    
                    $q1 = "UPDATE profile1 SET profession = '$job' WHERE user_id = '$user_id'";
                    $r1 = $db->update($q1);
                    
                    header("Location: profile.php");                                
                    echo '<a href="profile.php" class="btn btn-sm btn-primary">'."View Profile".'</a>';   
                    
                }else{
                    //show the form here
                    $q2 = "SELECT * FROM profile3 WHERE user_id = '$user_id' LIMIT 1";
                    $r2 = $db->select($q2);

                    if(!$r2){        
                        header("Location: step3.php");
                    }else{
                        $job = $r2->fetch_assoc();
            ?>
            <form action="step3edit.php?update=true" method="post" class="form">
                <div class="form-group">
                    <label for="qualification">Highest Qualification</label>
                    <input type="text" name="qualification" class="form-control" required value="<?PHP echo $job['qualification']; ?>">
                </div>
                <div class="form-group">
                    <label for="profession">Current Profession</label>
                    <select name="profession" class="form-control" id="profession" required>
                    <?PHP
                        $job_id = $job['job'];
                        $q = "SELECT * FROM jobs ORDER BY job_title ASC";
                        $r = $db->select($q);
                    
                        while($jobs = $r->fetch_array()){
                    ?>
                      <option value="<?PHP echo $jobs['id']; ?>" <?PHP if($jobs['id']==$job_id){ echo "selected"; } ?>><?PHP echo $jobs['job_title']; ?></option>     
                    <?PHP
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="type">Type of Job</label>
                    <select name="job_type" id="job_type" class="form-control" required>
                        <option value="fulltime" <?PHP if($job['job_type']=='fulltime'){ echo "selected"; } ?>>Fulltime / Permanent</option>
                        <option value="parttime" <?PHP if($job['job_type']=='parttime'){ echo "selected"; } ?>>Parttime</option>
                        <option value="govt" <?PHP if($job['job_type']=='govt'){ echo "selected"; } ?>>Government Service</option>
                        <option value="business" <?PHP if($job['job_type']=='business'){ echo "selected"; } ?>>Own Business</option>
                        <option value="office" <?PHP if($job['job_type']=='office'){ echo "selected"; } ?>>Office Job</option>
                        <option value="looking" <?PHP if($job['job_type']=='looking'){ echo "selected"; } ?>>Looking for Job</option>
                        <option value="other" <?PHP if($job['job_type']=='other'){ echo "selected"; } ?>>Other</option>
                    </select>
                </div>
                  
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="profession">Place of Posting</label>
                            <input type="text" name="place" class="form-control" required value="<?PHP echo $job['place']; ?>">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="salary">Annual Salary (Lakh)</label>
                            <input type="text" name="salary" class="form-control" value="<?PHP echo $job['salary']; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abtjob">About Job</label>
                    <textarea name="abt_job" id="abt_job" class="form-control" rows="10" required><?PHP echo nl2br($job['abt_job']); ?></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-default" type="submit">Submit</button>
                    <a href="profile.php" class="btn btn-sm btn-primary">Cancel</a>
                </div>
            </form>
            <?PHP
                }
                }
            ?>
           </div>
        </div>
<?PHP include("mods/footer.php"); ?>