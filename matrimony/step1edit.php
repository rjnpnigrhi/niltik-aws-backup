<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    //here we collect basic details
    include("mods/header.php");
?>  
        <div class="row">
           <h4 class="text-center">Update Basic Profile Details.</h4>
           <hr>
           <div class="col-md-4 col-md-offset-4 col-xs-12">                
                <?PHP
                    if(isset($_GET['update_profile'])){
                        $user_id = $db->safe_data($_POST['user_id']);
                        $name = $db->safe_data($_POST['name']);
                        $dob = $db->safe_data($_POST['dob']);
                        $age = $db->safe_data($_POST['age']);
                        $sex = $db->safe_data($_POST['sex']);
                        $height = $db->safe_data($_POST['height']);
                        $weight = $db->safe_data($_POST['weight']);
                        $religion = $db->safe_data($_POST['religion']);
                        $caste = $db->safe_data($_POST['caste']);
                        $sub_caste = $db->safe_data($_POST['sub_caste']);
                        $rashi = $db->safe_data($_POST['rashi']);
                        $gothram = $db->safe_data($_POST['gothram']);
                        $marital_status = $db->safe_data($_POST['marital_status']);
                        $address = $db->safe_data($_POST['address']);
                        $district = $db->safe_data($_POST['district']);
                        $state = $db->safe_data($_POST['state']);
                        
                        $q = "UPDATE profile1 SET name = '$name', dob = '$dob', age = '$age', sex = '$sex', height = '$height', weight = '$weight', religion = '$religion', caste = '$caste', sub_caste = '$sub_caste', rashi = '$rashi', gothram = '$gothram', marital_status = '$marital_status', address = '$address', district = '$district', state = '$state' WHERE user_id = '$user_id'";
                        $r = $db->update($q);
                        
        
                        if($sex = "male"){
                            $desire = "female";
                        }else{
                            $desire = "male";
                        }
                        $q1 = "UPDATE users SET desire = '$desire' WHERE id = '$user_id'";
                        $r1 = $db->update($q1);
                        
                        header("Location: profile.php");                                
                        echo '<a href="profile.php" class="btn btn-sm btn-primary">'."View Profile".'</a>';
                        
                    }else{
                        $q = "SELECT * FROM profile1 WHERE user_id = '$user_id' LIMIT 1";
                        $r = $db->select($q);

                        if(!$r){

                            header("Location: step1.php");
                        }else{
                            $profile = $r->fetch_assoc();
                ?>
               <form action="step1edit.php?update_profile=true" method="post" class="form">
                    <div class="form-group">
                       <label for="name">Name of Portfolio : </label>
                        <input type="text" name="name" class="form-control" value="<?PHP echo $profile['name']; ?>" required>
                    </div>
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Date of Birth : </label>
                       <div class="input-group date" data-provide="datepicker">
						<input type="text" name="dob" class="form-control" value="<?PHP echo $profile['dob']; ?>" >
						<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
						</div>
					</div>
					<script type="text/javascript">
						$(function () {
							$('#datetimepicker1').datetimepicker();
						});
					</script>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="age">Age in Years : </label>
                        <input type="text" name="age" class="form-control" value="<?PHP echo $profile['age']; ?>" required>
                    </div>
						</div>
					</div>
                    
					
					<label for="sex">Sex : &nbsp; &nbsp;</label>
					<div class="row">                       
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio1" value="male" <?PHP if($profile['sex']=='male'){ echo "checked"; } ?>> Male
                        </label>
						</div>
					   <div class="col-xs-6">
                        <label class="radio-inline">
                          <input type="radio" name="sex" id="inlineRadio2" value="female" <?PHP if($profile['sex']=='female'){ echo "checked"; } ?>> Female
                        </label>
						</div>
						<div class="clearfix"></div>
						<br>
					</div>
                    
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="height">Height in Feet</label>
                       <select name="height" id="height" class="form-control">
                          <?PHP
                            $qh = "SELECT * FROM height ORDER BY id ASC";
                            $rh = $db->select($qh);
                            while($ht = $rh->fetch_array()){
                            ?>
                            <option value="<?PHP echo $ht['ht_id']; ?>" <?PHP if($ht['ht_id']==$profile['height']){ echo "selected"; } ?> ><?PHP echo $ht['ht_name']; ?></option>
                            <?PHP
                            }
                            ?>                          
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                       <label for="weight">Weight in KG</label>
                       <input type="number" name="weight" class="form-control" value="<?PHP echo $profile['weight']; ?>" required >
                    </div>
						</div>
					</div>
                    
					
                    <div class="form-group">
                       <label for="marital_status">Marital Status :</label>
                        <select name="marital_status" id="marital_status" class="form-control">
                            <?PHP
                            $qm = "SELECT * FROM marital_status ORDER BY id ASC";
                            $rm = $db->select($qm);
                            while($ms = $rm->fetch_array()){
                            ?>
                            <option value="<?PHP echo $ms['id']; ?>" <?PHP if($profile['marital_status']==$ms['id']){ echo "selected"; } ?>><?PHP echo $ms['marital_status']; ?></option>
                            <?PHP
                            }
                            ?>
				                                      
                        </select>
                    </div>
					
					<div class="row">
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Zodiac Sign (Rashi)</label>
                        <select name="rashi" id="zodiac_sign" class="form-control">
                         <?PHP
                            $qz = "SELECT * FROM zodiac_sign ORDER BY id ASC";
                            $rz = $db->select($qz);
                            while($zs = $rz->fetch_array()){
                            ?>
                            <option value="<?PHP echo $zs['id']; ?>" <?PHP if($zs['id']==$profile['rashi']){ echo "selected"; } ?> ><?PHP echo $zs['zodiac_sign']; ?></option>
                            <?PHP
                            }
                            ?>
                        </select>
                    </div>
						</div>
						<div class="col-xs-6">
						<div class="form-group">
                        <label for="zodiac">Gothram </label>
                        <input name="gothram" type="text" value="<?PHP echo $profile['gothram']; ?>" class="form-control" />
                    </div>
						</div>
					</div>
                    
					<div class="form-group">
						<label for="religion">Religion</label>
						<select class="form-control" name="religion">
						    <?PHP
                            $qr = "SELECT * FROM religion ORDER BY id ASC";
                            $rr = $db->select($qr);
                            while($religion = $rr->fetch_array()){
                            ?>
                            <option value="<?PHP echo $religion['id']; ?>" <?PHP if($religion['id']==$profile['religion']){ echo "selected"; } ?> ><?PHP echo $religion['religion']; ?></option>
                            <?PHP
                            }
                            ?>							
						</select>
					</div>
										
					<label for="caste">Caste and Sub-caste</label>
					<div class="row">
						<div class="col-xs-6">
							<select name="caste" class="form-control">
								<?PHP
                                $q = "SELECT * FROM caste ORDER BY caste_name ASC";
                                $r = $db->select($q);
                        
                                while($caste = $r->fetch_array()){
                            ?>
                                <option value="<?PHP echo $caste['id']; ?>" <?PHP if($caste['id']==$profile['caste']){ echo "selected"; } ?>><?PHP echo ucfirst($caste['caste_name']); ?></option>       
                            <?PHP
                                }
                            ?>
							</select>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
							<input name="sub_caste" type="text" class="form-control" value="<?PHP echo $profile['sub_caste']; ?>">
							</div>
						</div>
					</div>
					<div class="form-group">
                        <label for="address">Address</label>
						<textarea name="address" class="form-control" rows="3" required><?PHP echo $profile['address']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="district">District</label>
                        <input type="text" name="district" class="form-control" value="<?PHP echo $profile['district']; ?>">
                    </div>
                    <div class="form-group">
						<label for="religion">State</label>
						<select class="form-control" name="state">
						    <?PHP
                            $qs = "SELECT * FROM state ORDER BY id ASC";
                            $rs = $db->select($qs);
                            while($state = $rs->fetch_array()){
                            ?>
                            <option value="<?PHP echo $state['id']; ?>" <?PHP if($state['id']==$profile['state']){ echo "selected"; } ?> ><?PHP echo $state['name']; ?></option>
                            <?PHP
                            }
                            ?>							
						</select>
					</div>
                     <div class="form-group">
                        <input type="hidden" name="user_id" value="<?PHP echo $_SESSION['user_id']; ?>">
                        <button class="btn btn-md btn-default" type="submit">Submit</button>
                        <a href="profile.php" class="btn btn-sm btn-primary">Cancel</a>
                    </div>
                </form>
			   <?PHP 
                    }
                    }
               ?>
           </div>
        </div>
<?PHP include("mods/footer.php"); ?>