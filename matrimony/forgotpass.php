<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="NILTIK MATRIMONY | ONLINE">
    <meta name="author" content="niltik.com">
    <link rel="icon" href="../../../../favicon.ico">

    <title>NILTIK MATRIMONY | ONLINE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

    
<body>
    <div class="container">
	<br>
      <div class="col-md-4 col-md-offset-4 col-xs-12 text-center">
            <img src="img/logo.png" class="img-responsive" width="100%" alt="">
    <div class="clearfix"></div>
	<br>
        </div>
       <br>
       
    <div class="clearfix"></div>
        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 info-box">
            <?PHP
				if(isset($_GET['changepass'])){
					$myotp = $_SESSION['otp'];
					$yourotp = $_POST['otp'];
					$password = $db->safe_data($_POST['password']);
					$password = md5($password);
					$mobile = $_SESSION['mobile'];
					
					if($myotp != $yourotp){
						$error = "OTP is not the same. Try Again!"; 
					}else{
						$q = "UPDATE users SET password = '$password' WHERE mobile = '$mobile'";
						$r = $db->update($q);
							   
						echo "<script>
							alert('You have successfully changed your Password. Proceed to Log in!');
							window.location.href='login.php'; 
							</script>";         
					}    
				}elseif(isset($_GET['get_otp'])){
                    $mobile = $db->safe_data($_POST['mobile']);
                    
                    $q = "SELECT * FROM users WHERE mobile = '$mobile' LIMIT 1";
                    $r = $db->select($q);
                    
                    if(!$r){
                        echo "<br>";
                        echo "The Mobile Number you entered is not registerd.<br>";
                        echo '<a href="forgotpass.php" class="btn btn-xs btn-primary">'."Try Again".'</a>';
                        echo "<br>";
                        echo "<br>";
                    }else{
                        //let us send an OTP 
						$otp = rand(100000,999999);
						$_SESSION['otp'] = $otp;
						$_SESSION['mobile'] = $mobile;
						$msg = $otp." is your OTP for Password Change on NILTIK MATRIMONY";
		        
						//start here
							$encoded_msg= urlencode($msg);
							//Create API URL
							$fullapiurl="http://api.msg91.com/api/sendhttp.php?sender=NILTIK&route=4&mobiles=$mobile&authkey=215426ARSXDZ9i5af98d91&country=91&message=$encoded_msg";
							//Call API
							$ch = curl_init($fullapiurl);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$result = curl_exec($ch);
							//echo $result ; // For Report or Code Check
							curl_close($ch);
						//end here
                        
                        echo "We will send password to mobile";
                    }
			?>
			<h2>Password Recovery!</h2>
            <p class="small">Type OTP and a New Password</p>
            <hr>
            <form action="forgotpass.php?changepass=true" method="post" class="form">
            <?PHP
            	if(isset($error)){
            		echo $error."<br>";
            	}
            ?>
                <div class="form-group">
                    <input type="text" name="otp" placeholder="Type your OTP" required class="form-control">
                </div>               
                <div class="form-group">
                    <input type="password" name="password" placeholder="Type a Password" required class="form-control">
                </div>
                <div class="form-group">                	
                    <button class="btn btn-md btn-success btn-block" type="submit">Update Password!</button>
                </div>
                <hr>                
                <p class="small"><a href="login.php" class="btn btn-xs btn-primary">Already Registered! Sign in!</a> <a href="forgotpass.php" class="btn btn-xs btn-warning">Forgot Password!</a></p>
            </form>
			<?PHP
                }else{
            ?>
            <h2>Password Recovery!</h2>
            <p class="small">Type your registered Mobile No Here</p>
            <hr>
            <form action="forgotpass.php?get_otp=true" method="post" class="form">
                
                <div class="form-group">
                    <input type="text" name="mobile" placeholder="Type your Mobile No" required class="form-control">
                </div>
                
                <div class="form-group">
                    <button class="btn btn-md btn-success btn-block" type="submit">Send me OTP!</button>
                </div>
                <hr>
                
                <p class="small"><a href="login.php" class="btn btn-xs btn-primary">Have Credentials, Sign in!</a> <a href="signup.php" class="btn btn-xs btn-warning">New User, Sign Up!</a></p>
            </form>
            <?PHP } ?>
        </div>
        <div class="clearfix"></div>
        <hr>
        <p class="text-center info-text">For any Queries please contact : <strong><em>7008584789</em></strong></p>
    </div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>

    
</body>
</html>