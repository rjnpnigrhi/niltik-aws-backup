<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);
    $desire = $user['desire'];

    //here we collect Photo of the user 
    include("mods/header.php");
    
    if(isset($_GET['search'])){
        if(isset($_POST['minage'])){
            $minage = $_POST['minage'];
        }else{
            $minage = $_GET['minage'];
        }
        if(isset($_POST['maxage'])){
            $maxage = $_POST['maxage'];
        }else{
            $maxage = $_GET['maxage'];
        }
        if(isset($_POST['religion'])){
            $religion = $_POST['religion'];
        }else{
            $religion = $_GET['religion'];
        }
        if(isset($_POST['caste'])){
            $caste = $_POST['caste'];
        }else{
            $caste = $_GET['caste'];
        }
        if(isset($_POST['state'])){
            $state = $_POST['state'];
        }else{
            $state = $_GET['state'];
        }
        if(isset($_POST['job'])){
            $job = $_POST['job'];
        }else{
            $job = $_GET['job'];
        }
        
        $q = "SELECT * FROM profile1 WHERE sex != '$desire' AND age BETWEEN '$minage' AND '$maxage'";
        if($religion !==''){
            $q .= "AND religion = '$religion'";
        }
        if($caste !==''){
            $q .= "AND caste = '$caste'";
        }
        if($state !==''){
            $q .= "AND state = '$state'";
        }
        if($job !==''){
            $q .= "AND profession = '$job'";
        }
        $q .= "ORDER BY id DESC";
        $r = $db->select($q);
        
        if(!$r){
            echo "We are sorry! <br> There are no profiles available now!<br><br>";
            echo '<a href="search.php" class="btn btn-sm btn-primary">'."Search Again".'</a>';
        }else{
            while($diaries = $r->fetch_array()){
                        $result[] = $diaries;
                    }

                    //here we start paginating the data
                    $numbers = $pagination->paginate($result, 2);

                    //what are the data to be presented in these pages
                    $data = $pagination->fetchresults();

                    //let us get the current page number
                    $pn = $pagination->page_num();

                    //let us get all page numbers 
                    $tp = count($numbers);

                    foreach($data as $row):
            ?>
            <div class="col-xs-12">
                <img src="uploads/<?PHP echo $row['image']; ?>" class="img-responsive img-rounded" width="100%" alt="<?PHP echo $row['name']; ?>">
                <h4 class="text-primary"><?PHP echo $row['name']; ?> <a href="view.php?id=<?PHP echo $row['user_id']; ?>" class="btn btn-xs btn-success pull-right"><i class="fa fa-search"></i></a></h4>
            <?PHP
                $job = get_value('jobs', $row['profession']);
                $caste = get_value('caste', $row['caste']);
                $religion = get_value('religion', $row['religion']);
                echo '<p class="text-center">'.$row['age']." yrs, ".$job['job_title'].", ".$caste['caste_name'].", ".$religion['religion'].'</p>';
            ?>
            </div>
            <div class="clearfix"></div>
            <hr>
            <?PHP            
                    endforeach;
            ?>
            <div class="clearfix"></div>
                <nav class="nav-sm">
                <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$pp.'" aria-label="Previous"><< </a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="search.php?search=true&minage='.$minage.'&maxage='.$maxage.'&caste='.$caste.'&religion='.$religion.'&page='.$np.'" aria-label="Next"> >></a></li>';
                }
               ?>
              </ul>
            </nav>
    <?PHP
            }
    }elseif(isset($_GET['view'])){
        $id = $_GET['view'];
    if(!$id){
        echo "Something is seriously not working.<br>";
        echo "We are fixing it for you.";
    }else{
    $q = "SELECT * FROM profile1 WHERE user_id = '$id' LIMIT 1";
    $r = $db->select($q);

    if(!$r){
        
        header("Location: step1.php");
    }else{
        $profile = $r->fetch_assoc();
?>
     <img src="uploads/<?PHP echo $profile['image']; ?>" class="img-rounded" width="100%" alt="<?PHP echo $profile['name']; ?>"> 
     <br>
     <?PHP
        $img = $profile['image'];
        $qp = "SELECT * FROM profile5 WHERE image_name != '$img' AND user_id = '$id'";
        $rp = $db->select($qp);
        
        if(!$rp){
            //do nothing
        }else{
            echo '<div class="row">';
            while($pp = $rp->fetch_array()){
    ?>
         <div class="col-xs-4">
             <img src="uploads/<?PHP echo $pp['image_name']; ?>" class="img-responsive img-rounded top-20" alt="<?PHP echo $profile['name']; ?>">
         </div>  
    <?PHP
            }
            echo '</div>';
        }
     ?> 
     <br>
     <hr>
     <h4 class="text-success"><?PHP echo $profile['name']; ?></h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Date of birth :</td>
             <td width="60%"><?PHP echo get_date($profile['dob']); ?></td>
         </tr>
         <tr>
             <td>Age :</td>
             <td><?PHP echo $profile['age']; ?> years</td>
         </tr>
         <tr>
             <td>Address :</td>
             <td><?PHP echo nl2br($profile['address']); ?></td>
         </tr>
         <tr>
             <td>Height :</td>
             <td><?PHP echo $profile['height']; ?> Ft</td>
         </tr>
         <tr>
             <td>Weight :</td>
             <td><?PHP echo $profile['weight']; ?> KG</td>
         </tr>
         <tr>
             <td>Religion :</td>
             <td><?PHP echo $profile['religion']; ?></td>
         </tr>
         <tr>
             <td>Caste :</td>
             <td><?PHP echo $profile['caste']; ?></td>
         </tr>
         <tr>
             <td>Sub-Caste :</td>
             <td><?PHP echo $profile['sub_caste']; ?></td>
         </tr>
         <tr>
             <td>Rashi :</td>
             <td><?PHP echo $profile['rashi']; ?></td>
         </tr>
         <tr>
             <td>Gothra :</td>
             <td><?PHP echo $profile['gothram']; ?></td>
         </tr>
         <tr>
             <td>Marital Status :</td>
             <td><?PHP echo $profile['marital_status']; ?></td>
         </tr>
         <tr>
             <td>eMail :</td>
             <td>********************</td>
         </tr>
         <tr>
             <td>Mobile :</td>
             <td>**********</td>
         </tr>
     </table>
<?PHP
    }
    $q1 = "SELECT * FROM profile2 WHERE user_id = '$id' LIMIT 1";
    $r1 = $db->select($q1);

    if(!$r1){        
        header("Location: step2.php");
    }else{
        $family = $r1->fetch_assoc();  
?>
     <h4 class="text-success">Family Details</h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Father's Name :</td>
             <td>******************</td>
         </tr>
         <tr>
             <td width="40%">Ocupation :</td>
             <td><?PHP echo $family['father_job']; ?></td>
         </tr>
         <tr>
             <td width="40%">Mother's Name :</td>
             <td>************************</td>
         </tr>
         <tr>
             <td width="40%">Ocupation :</td>
             <td><?PHP echo $family['mother_job']; ?></td>
         </tr>
         <tr>
             <td width="40%">Brothers :</td>
             <td><?PHP echo $family['bros']; ?></td>
         </tr>
         <tr>
             <td width="40%">Married :</td>
             <td><?PHP echo $family['bros_married']; ?></td>
         </tr>
         <tr>
             <td width="40%">Sisters :</td>
             <td><?PHP echo $family['sis']; ?></td>
         </tr>
         <tr>
             <td width="40%">Married :</td>
             <td><?PHP echo $family['sis_married']; ?></td>
         </tr>
         <tr>
             <td width="40%">About Family :</td>
             <td><?PHP echo nl2br($family['abt_family']); ?></td>
         </tr>  
</table>
<?PHP
    }
    $q2 = "SELECT * FROM profile3 WHERE user_id = '$id' LIMIT 1";
    $r2 = $db->select($q2);

    if(!$r2){        
        header("Location: step3.php");
    }else{
        $job = $r2->fetch_assoc();  
?>
     <h4 class="text-success">Career &amp; Job Profile </h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Qualification :</td>
             <td><?PHP echo $job['qualification']; ?></td>
         </tr>
         <tr>
             <td width="40%">Profession :</td>
             <td><?PHP echo $job['job']; ?></td>
         </tr>
         <tr>
             <td width="40%">Type of Job :</td>
             <td><?PHP echo $job['job_type']; ?></td>
         </tr>
         <tr>
             <td width="40%">Place of Job :</td>
             <td><?PHP echo $job['place']; ?></td>
         </tr>
         <tr>
             <td width="40%">Salary :</td>
             <td><?PHP echo $job['salary']; ?> Lakh/Annum</td>
         </tr>
         <tr>
             <td width="40%">About Career :</td>
             <td><?PHP echo nl2br($job['abt_job']); ?></td>
         </tr>
</table>
<?PHP
    }
    $q3 = "SELECT * FROM profile4 WHERE user_id = '$id' LIMIT 1";
    $r3 = $db->select($q3);

    if(!$r3){        
        header("Location: step3.php");
    }else{
        $prefer = $r3->fetch_assoc();  
?>
     <h4 class="text-success">Profile Preferences</h4>
     <table class="table table-striped">
         <tr>
             <td width="40%">Minimum Age :</td>
             <td><?PHP echo $prefer['minage']; ?> Years</td>
         </tr>
         <tr>
             <td width="40%">Maximum Age :</td>
             <td><?PHP echo $prefer['maxage']; ?> Years</td>
         </tr>
         <tr>
             <td width="40%">Caste :</td>
             <td>
             <?PHP 
                if($prefer['caste']=='0'){
                    echo "Any Caste";
                }else{
                    echo $prefer['caste']; 
                }
            ?>
             </td>
         </tr>
         <tr>
             <td width="40%">Sub-Caste :</td>
             <td>
             <?PHP 
                if($prefer['sub_caste']==''){
                    echo "Any Sub-Caste";
                }else{
                    echo $prefer['sub_caste']; 
                }
            ?>
             </td>
         </tr>
         <tr>
             <td width="40%">Profession :</td>
             <td><?PHP echo $prefer['profession']; ?></td>
         </tr>
         <tr>
             <td width="40%">What I Want :</td>
             <td><?PHP echo nl2br($prefer['abt_partner']); ?></td>
         </tr>
</table>
<?PHP
    }
        echo '<a href="javascript:history.back()" class="btn btn-xs btn-primary">'."Go Back!".'</a>';
    }
    }else{
?> 
   <div class="row">       
    <h3 class="text-center">Search Profiles</h3>
    <hr>
   </div>
    <form action="search.php?search=true" method="post" class="form">
       <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <label for="minage">Minimum Age</label>
                <input type="number" name="minage" class="form-control" required value="18">
            </div>
        </div>
        <div class="col-xs-6">
            <div class="form-group">
                <label for="maxage">Maximum Age</label>
                <input type="number" name="maxage" class="form-control" required value="99">
            </div>
        </div>
        
        <div class="col-xs-6">
            <label for="religion">Religion</label>
            <select class="form-control" name="religion">
               <option value="">Any Religion</option>
                <?PHP
                $qr = "SELECT * FROM religion ORDER BY id ASC";
                $rr = $db->select($qr);
                while($religion = $rr->fetch_array()){
                ?>
                <option value="<?PHP echo $religion['id']; ?>" ><?PHP echo $religion['religion']; ?></option>
                <?PHP
                }
                ?>
            </select>
        </div>
        
        <div class="col-xs-6">
            <div class="form-group">
              <label for="other">Caste</label>
              <select name="caste" class="form-control" id="caste">
               <option value="">Any Caste</option>
                <?PHP
                    $q = "SELECT * FROM caste ORDER BY caste_name ASC";
                    $r = $db->select($q);

                    while($caste = $r->fetch_array()){
                        echo '<option value="'.$caste['id'].'">'.$caste['caste_name'].'</option>';
                    }
                ?>
              </select>                  
            </div>
        </div>
        
        <div class="col-xs-6">
            <div class="form-group">
              <label for="other">Place</label>
              <select name="state" class="form-control" id="state">
               <option value="">Any State</option>
                <?PHP
                $qs = "SELECT * FROM state ORDER BY id ASC";
                $rs = $db->select($qs);
                while($state = $rs->fetch_array()){
                ?>
                <option value="<?PHP echo $state['id']; ?>" ><?PHP echo $state['name']; ?></option>
                <?PHP
                }
                ?>
              </select>                  
            </div>
        </div>
        
        <div class="col-xs-6">
            <div class="form-group">
              <label for="other">Profession</label>
              <select name="job" class="form-control" id="job">
               <option value="">Any Profession</option>
                <?PHP
                    $q = "SELECT * FROM jobs ORDER BY job_title ASC";
                    $r = $db->select($q);

                    while($jobs = $r->fetch_array()){
                        echo '<option value="'.$jobs['id'].'">'.$jobs['job_title'].'</option>';
                    }
                ?>
              </select>                  
            </div>
        </div>
        </div>
        <hr>
        <div class="form-group">
            <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i> Search</button>
            <a href="index.php" class="btn btn-sm btn-primary">Cancel</a>
        </div>
    </form>
<?PHP
    }
    include("mods/footer.php");
?>