<?PHP
    session_start();

    include("inc/config.php");
    include("inc/db_conn.php");
    include("inc/functions.php");
    include("inc/paginate.php");

    //let us initiate a new connection
    $db = new connection();

    //let us initiate a new pagination class
	$pagination = new pagination();

    if(!isset($_SESSION['user_id'])){
        header("Location: login.php");
    }
    
    $user_id = $_SESSION['user_id'];
    $user = get_value('users', $user_id);

    include("mods/header.php");
?>
  <div class="container text-center">
   <a href="profile.php" class="btn btn-default btn-sm">Basic Profile</a>
   <a href="family.php" class="btn btn-default btn-sm">Family</a>
   <a href="photos.php" class="btn btn-default btn-sm">Photos</a>
   <a href="education.php" class="btn btn-default btn-sm">Education</a>
   <a href="career.php" class="btn btn-default btn-sm">Career</a>
   <a href="requirements.php" class="btn btn-default btn-sm">Requirements</a>
   </div>
<?PHP
    //address.php
    $portfolio_id = $user['profile_id'];

    if(value_exists('address', 'portfolio_id', $portfolio_id)>0){
        //show the data for updation
        //$portfolio_id = $user['profile_id'];
        $q = "SELECT * FROM address WHERE portfolio_id = '$portfolio_id' LIMIT 1";
        $r = $db->select($q);
        
        $row = $r->fetch_assoc();
?>
    <div class="col-md-6 col-md-offset-3 col-xs 12">
    <h2>My Address Details : (10%)</h2>
     <form action="address.php?update=true" method="post" class="form">
          <div class="form-group">
           <label for="street">Present Address</label>
           <textarea name="present_address" id="" class="form-control" rows="10" required><?PHP echo $row['present_address']; ?></textarea>
       </div>
       <div class="form-group">
           <label for="nearest_town">Nearest City/Town</label>
           <input type="text" name="nearest_city" class="form-control" value="<?PHP echo $row['nearest_city']; ?>" required>
       </div>
        
        <div class="form-group">
           <label for="street">Permanent Address</label>
           <textarea name="permanent_address" id="" class="form-control" rows="10" required><?PHP echo $row['permanent_address']; ?></textarea>
       </div>
       <div class="form-group">
           <label for="ps">Belongs to Region (Anchala)?</label>
               <select name="anchal" class="form-control" id="">
                   <?PHP
                     $ancs = get_values("anchal");
                     foreach($ancs as $anc){
                     ?>
                      <option value="<?PHP echo $anc['id']; ?>" <?PHP if($row['anchal']==$anc['id']){ echo "selected"; } ?>><?PHP echo $anc['name']; ?></option>
                    <?PHP
                    }
                  ?>
               </select>
        </div>        
       
       <div class="form-group">
           <input type="hidden" name="portfolio_id" value="<?PHP echo $row['id']; ?>">
           <button class="btn btn-md btn-default" type="submit">Submit</button>
           <a href="education.php" class="btn btn-md btn-primary">Proceed to Education Page</a>
       </div>
      </div>
   </form>
     </div>
<?PHP
        
    }elseif(isset($_GET['insert_address'])){
            //we will insert the address
            $email = $db->safe_data($_POST['email']);
            $mobile = $db->safe_data($_POST['mobile']);
            $present_address = $db->safe_data($_POST['present_address']);
            $nearest_city = $db->safe_data($_POST['nearest_city']);
            $permanent_address = $db->safe_data($_POST['permanent_address']);
            $anchal = $db->safe_data($_POST['anchal']);
            $portfolio_id = $db->safe_data($_POST['portfolio_id']);
            
            $q = "INSERT INTO address (portfolio_id, email, mobile, present_address, nearest_city, permanent_address, anchal) VALUES ('$portfolio_id', '$email', '$mobile', '$present_address', '$nearest_city', '$permanent_address', '$anchal')";
            $r = $db->insert($q);
            
            $q1 = "UPDATE users SET profile_status = profile_status+10 WHERE profile_id = '$portfolio_id'";
            $r1 = $db->update($q1);
        
            inform_user($portfolio_id);
            
            echo "You have added your contact details successfully.<br>";
            echo "Please complete all parts of the portfolio to get approved.<br>";
            echo '<a href="address.php" class="btn btn-sm btn-primary">'."View Address".'</a>';
        }elseif(isset($_GET['update'])){
            //we will insert the address
            $email = $db->safe_data($_POST['email']);
            $mobile = $db->safe_data($_POST['mobile']);
            $present_address = $db->safe_data($_POST['present_address']);
            $nearest_city = $db->safe_data($_POST['nearest_city']);
            $permanent_address = $db->safe_data($_POST['permanent_address']);
            $anchal = $db->safe_data($_POST['anchal']);
            $portfolio_id = $db->safe_data($_POST['portfolio_id']);
            
            $q = "UPDATE address SET email = '$email', mobile = '$mobile', present_address = '$present_address', nearest_city = '$nearest_city', permanent_address = '$permanent_address', anchal = '$anchal' WHERE id = '$portfolio_id'";
            
            $r = $db->update($q);

            echo "You have updated your contact details successfully.<br>";
            echo "Please complete all parts of the portfolio to get approved.<br>";
            echo '<a href="address.php" class="btn btn-sm btn-primary">'."View Address".'</a>';
        
        }else{
?>
    <div class="col-md-6 col-md-offset-3 col-xs-12">

   <h1>Add Your Contact Details : (10%)</h1>
   <form action="address.php?insert_address=true" method="post" class="form">    
       <div class="form-group">
           <label for="street">Present Address</label>
           <textarea name="present_address" id="" class="form-control" rows="10" required></textarea>
       </div>
       <div class="form-group">
           <label for="nearest_town">Nearest City/Town</label>
           <input type="text" name="nearest_city" class="form-control" placeholder="Nearest city/town" required>
       </div>
        
        <div class="form-group">
           <label for="street">Permanent Address</label>
           <textarea name="permanent_address" id="" class="form-control" rows="10" required></textarea>
       </div>       
             
       <div class="form-group">
           <input type="hidden" name="portfolio_id" value="<?PHP echo $user['profile_id']; ?>">
           <button class="btn btn-md btn-default" type="submit">Submit</button>
       </div>
   </form>
           
    </div>
<?PHP
    }
    include("mods/footer.php");
?>