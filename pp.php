<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
?>
    

<style>
<!--
 /* Font Definitions */
 @font-face
    {font-family:Wingdings;
    panose-1:5 0 0 0 0 0 0 0 0 0;
    mso-font-charset:2;
    mso-generic-font-family:decorative;
    mso-font-pitch:variable;
    mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
    {font-family:"Cambria Math";
    panose-1:2 4 5 3 5 4 6 3 2 4;
    mso-font-charset:0;
    mso-generic-font-family:roman;
    mso-font-pitch:variable;
    mso-font-signature:3 0 0 0 1 0;}
@font-face
    {font-family:Calibri;
    panose-1:2 15 5 2 2 2 4 3 2 4;
    mso-font-charset:0;
    mso-generic-font-family:swiss;
    mso-font-pitch:variable;
    mso-font-signature:-536859905 -1073697537 9 0 511 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-unhide:no;
    mso-style-qformat:yes;
    mso-style-parent:"";
    margin-top:0cm;
    margin-right:0cm;
    margin-bottom:8.0pt;
    margin-left:0cm;
    line-height:107%;
    mso-pagination:widow-orphan;
    font-size:11.0pt;
    font-family:"Calibri",sans-serif;
    mso-ascii-font-family:Calibri;
    mso-ascii-theme-font:minor-latin;
    mso-fareast-font-family:Calibri;
    mso-fareast-theme-font:minor-latin;
    mso-hansi-font-family:Calibri;
    mso-hansi-theme-font:minor-latin;
    mso-bidi-font-family:"Times New Roman";
    mso-bidi-theme-font:minor-bidi;
    mso-fareast-language:EN-US;}
h1
    {mso-style-priority:9;
    mso-style-unhide:no;
    mso-style-qformat:yes;
    mso-style-link:"Heading 1 Char";
    mso-margin-top-alt:auto;
    margin-right:0cm;
    mso-margin-bottom-alt:auto;
    margin-left:0cm;
    mso-pagination:widow-orphan;
    mso-outline-level:1;
    font-size:24.0pt;
    font-family:"Times New Roman",serif;
    mso-fareast-font-family:"Times New Roman";
    mso-fareast-language:EN-IN;
    font-weight:bold;}
p
    {mso-style-noshow:yes;
    mso-style-priority:99;
    mso-margin-top-alt:auto;
    margin-right:0cm;
    mso-margin-bottom-alt:auto;
    margin-left:0cm;
    mso-pagination:widow-orphan;
    font-size:12.0pt;
    font-family:"Times New Roman",serif;
    mso-fareast-font-family:"Times New Roman";
    mso-fareast-language:EN-IN;}
span.Heading1Char
    {mso-style-name:"Heading 1 Char";
    mso-style-priority:9;
    mso-style-unhide:no;
    mso-style-locked:yes;
    mso-style-link:"Heading 1";
    mso-ansi-font-size:24.0pt;
    mso-bidi-font-size:24.0pt;
    font-family:"Times New Roman",serif;
    mso-ascii-font-family:"Times New Roman";
    mso-fareast-font-family:"Times New Roman";
    mso-hansi-font-family:"Times New Roman";
    mso-bidi-font-family:"Times New Roman";
    mso-font-kerning:18.0pt;
    mso-fareast-language:EN-IN;
    font-weight:bold;}
span.header-title
    {mso-style-name:header-title;
    mso-style-unhide:no;}
span.number
    {mso-style-name:number;
    mso-style-unhide:no;}
.MsoChpDefault
    {mso-style-type:export-only;
    mso-default-props:yes;
    font-size:11.0pt;
    mso-ansi-font-size:11.0pt;
    mso-bidi-font-size:11.0pt;
    font-family:"Calibri",sans-serif;
    mso-ascii-font-family:Calibri;
    mso-ascii-theme-font:minor-latin;
    mso-fareast-font-family:Calibri;
    mso-fareast-theme-font:minor-latin;
    mso-hansi-font-family:Calibri;
    mso-hansi-theme-font:minor-latin;
    mso-bidi-font-family:"Times New Roman";
    mso-bidi-theme-font:minor-bidi;}
.MsoPapDefault
    {mso-style-type:export-only;
    margin-bottom:8.0pt;
    line-height:107%;}
@page WordSection1
    {size:595.3pt 841.9pt;
    margin:72.0pt 72.0pt 72.0pt 72.0pt;
    mso-header-margin:35.4pt;
    mso-footer-margin:35.4pt;
    mso-paper-source:0;}
div.WordSection1
    {page:WordSection1;}
 /* List Definitions */
 @list l0
    {mso-list-id:308286270;
    mso-list-template-ids:-1435484510;}
@list l0:level1
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:36.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Symbol;}
@list l0:level2
    {mso-level-number-format:bullet;
    mso-level-text:o;
    mso-level-tab-stop:72.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:"Courier New";
    mso-bidi-font-family:"Times New Roman";}
@list l0:level3
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:108.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level4
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:144.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level5
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:180.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level6
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:216.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level7
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:252.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level8
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:288.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l0:level9
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:324.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1
    {mso-list-id:326787928;
    mso-list-template-ids:-163836158;}
@list l1:level1
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:36.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Symbol;}
@list l1:level2
    {mso-level-number-format:bullet;
    mso-level-text:o;
    mso-level-tab-stop:72.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:"Courier New";
    mso-bidi-font-family:"Times New Roman";}
@list l1:level3
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:108.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level4
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:144.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level5
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:180.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level6
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:216.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level7
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:252.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level8
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:288.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l1:level9
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:324.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2
    {mso-list-id:509874842;
    mso-list-template-ids:-1522137970;}
@list l2:level1
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:36.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Symbol;}
@list l2:level2
    {mso-level-number-format:bullet;
    mso-level-text:o;
    mso-level-tab-stop:72.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:"Courier New";
    mso-bidi-font-family:"Times New Roman";}
@list l2:level3
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:108.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level4
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:144.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level5
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:180.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level6
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:216.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level7
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:252.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level8
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:288.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l2:level9
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:324.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3
    {mso-list-id:606698992;
    mso-list-template-ids:1270359136;}
@list l3:level1
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:36.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Symbol;}
@list l3:level2
    {mso-level-number-format:bullet;
    mso-level-text:o;
    mso-level-tab-stop:72.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:"Courier New";
    mso-bidi-font-family:"Times New Roman";}
@list l3:level3
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:108.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level4
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:144.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level5
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:180.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level6
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:216.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level7
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:252.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level8
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:288.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l3:level9
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:324.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4
    {mso-list-id:2022858294;
    mso-list-template-ids:409909578;}
@list l4:level1
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:36.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Symbol;}
@list l4:level2
    {mso-level-number-format:bullet;
    mso-level-text:o;
    mso-level-tab-stop:72.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:"Courier New";
    mso-bidi-font-family:"Times New Roman";}
@list l4:level3
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:108.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level4
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:144.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level5
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:180.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level6
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:216.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level7
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:252.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level8
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:288.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
@list l4:level9
    {mso-level-number-format:bullet;
    mso-level-text:;
    mso-level-tab-stop:324.0pt;
    mso-level-number-position:left;
    text-indent:-18.0pt;
    mso-ansi-font-size:10.0pt;
    font-family:Wingdings;}
ol
    {margin-bottom:0cm;}
ul
    {margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
    {mso-style-name:"Table Normal";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    mso-style-noshow:yes;
    mso-style-priority:99;
    mso-style-parent:"";
    mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
    mso-para-margin-top:0cm;
    mso-para-margin-right:0cm;
    mso-para-margin-bottom:8.0pt;
    mso-para-margin-left:0cm;
    line-height:107%;
    mso-pagination:widow-orphan;
    font-size:11.0pt;
    font-family:"Calibri",sans-serif;
    mso-ascii-font-family:Calibri;
    mso-ascii-theme-font:minor-latin;
    mso-hansi-font-family:Calibri;
    mso-hansi-theme-font:minor-latin;
    mso-bidi-font-family:"Times New Roman";
    mso-bidi-theme-font:minor-bidi;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=EN-IN style='tab-interval:36.0pt'>

<div class=WordSection1>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal'><span style='font-size:12.0pt;font-family:"Times New Roman",serif;
mso-fareast-font-family:"Times New Roman";mso-fareast-language:EN-IN'>&nbsp;</span><span
style='font-size:15.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:EN-IN'>Privacy Policy - India</span><span
style='font-size:12.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
"Times New Roman";mso-fareast-language:EN-IN'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>1.GENERAL<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>Niltik.com has
copyright over this Privacy Policy. Use by third parties, even by way of
extract, for any purposes is not allowed. Infringements may be subject to legal
action.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>1.1NILTIK values the trust you place in us and that is why we insist
upon the highest standards for driver / operator information privacy. Please
read the following statement to learn about NILTIK's information gathering and
dissemination practices.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>1.2This privacy policy explains how NILTIK (<b>&quot;Our&quot;,
&quot;Us&quot;, &quot;We&quot;, &quot;NILTIK&quot;</b>) collects and uses Your
(<b>&quot;You&quot;, or &quot;Your&quot;</b>) personal and non-personal
information shared by You either while registering as a driver partner /
operator or browsing on the NILTIK Driver Partner mobile application /NILTIK
Operator mobile application / the website Cabs.niltik.com (collectively “Site”)
and Your access / usage of Your account at Our Site (<b>“Account”</b>). For
purposes of this Privacy Policy,&nbsp;<b>“You”</b>&nbsp;shall either mean an
“Operator” who has listed himself /itself and his / its fleet of vehicles on
the Site and /or has the Account to provide transport services through the
drivers employed by the Operator; or (ii) “Driver Partner” who has listed
himself and his vehicle on the Site and / or has the Account to directly
provide transport services. For an Operator, “You” shall also include the
drivers employed by the Operator.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>1.3 By using Our Site, You impliedly agree that We can collect, disclose
and use Your information to provide transport service, in accordance with the
terms of this privacy policy (&quot;Privacy Policy&quot;). You represent and
warrant that the drivers employed by You to provide transport service have
agreed for collection, disclosure and use of their information by NILTIK. By
visiting /using this Site, You agree to be bound by the terms and conditions of
this Privacy Policy.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>2.INFORMATION COLLECTED<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1 We may collect
certain Personal Information from You as well as drivers employed by You to
provide transport services. ‘Personal Information' is data that can be used to
identify a person (&quot;<b>Personal Information</b>&quot;) including:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.1.Your full
name, address, email address, telephone number, date of birth and any proof of
Your identity and/or address that We may request;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.2.Details of
any bank account (including but not limited to, account holder, account name,
account number, available balance and transaction history of Your bank account)
or any other income documents as necessary by Us;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.3.Surveys that
You complete through the Site or based on Our request;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.4.Information
collected through “Cookies”; Please refer to section 3.2 and 3.3 for more
information on Cookies.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level1 lfo1;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.Other details
as set out below-<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.1.Your visits
to Our Site;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.2.Messages –
read your sent and received text messages;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.3Phone
directory – directly call phone numbers from your directory and read your phone
status and identity;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.4Photos /
media / files – read the content of USB storage and modify and delete content
from your USB storage;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.5Storage -
read the content of USB storage and modify and delete content from your USB
storage;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.6Contact
Directory;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.7Camera –
take pictures and videos;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.8Device ID
&amp; Location – read phone status and identity;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.9View network
connections;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.10Full
network access;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:64.5pt;text-indent:-18.0pt;line-height:15.75pt;mso-list:l0 level2 lfo1;
tab-stops:list 72.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:10.5pt;font-family:"Courier New";mso-fareast-font-family:
"Courier New";color:#818487;mso-fareast-language:EN-IN'><span style='mso-list:
Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.1.5.11Location
data - approximate as well as precise location;;<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:64.5pt;margin-bottom:.0001pt;text-indent:-18.0pt;line-height:15.75pt;
mso-list:l0 level2 lfo1;tab-stops:list 72.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:"Courier New";
mso-fareast-font-family:"Courier New";color:#818487;mso-fareast-language:EN-IN'><span
style='mso-list:Ignore'>o<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span></span><![endif]><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>2.1.5.12Web logs and other communication data, whether this is required
for Our own purposes or otherwise and the resources that You access whilst
visiting Our Site.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>2.2 In addition to the Personal Information, certain information may be
collected from You at the time You visit the Site. This data may include:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l3 level1 lfo2;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.2.1.IP address of
Your server from where the Site is being accessed, the type of browser
(Internet Explorer, Firefox, Opera, Google Chrome, Safari etc.), the operating
system of Your system and the Site You last visited before visiting to Our
Site, referring source which may have sent You to the Site;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l3 level1 lfo2;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.2.2.&nbsp;The
duration of Your stay on Our Site is also stored in the session along with the
date and time of Your access;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l3 level1 lfo2;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.2.3.&nbsp;The
transactions that You either conduct with Us or with any third party through
Our Site; And<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l3 level1 lfo2;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.2.4.Other
information associated with the interaction of Your Site.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>2.3 When you use the Site, some third party servers (which may be hosted
by a third party service provider) may collect information indirectly and
automatically about Your activities on the Site; for instance by way of
cookies, web beacons or web analytics. This anonymous information is maintained
distinctly and is not linked to the Personal Information You submit to the
third party and shall not be used to identify Your Account.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l4 level1 lfo3;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.3.1.&nbsp;Web
beacons are graphic image files embedded in a web page that provides
information form the user’s browser. This allows us to monitor and ascertain
the number of users using the Site and selected sponsors, advertiser website
and for what purposes.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l4 level1 lfo3;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>2.3.2.&nbsp;Web
analytics are services provided by third parties in connection with a website.
We may use such services to find usability problems in order to make the Site
easier to use. These services do not collect information that has not been
voluntarily provided by you. Accordingly, these services do not track your
browsing habits. The information shared through these services will not
identify you or your Account.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>3. USE OF THE INFORMATION<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1 The information
collected from You is used for the following purposes only:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.1.&nbsp;to
facilitate Your use of the Site;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.2.&nbsp;to
enable You to provide transport services to the customers;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.3.to respond to
Your inquiries or fulfil Your requests for information about the various
services;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.4.to provide
You with information about NILTIK’s products/services and to send You
information, materials, and offers from NILTIK;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.5.to send You
important information regarding the Site, changes to NILTIK's terms,
conditions, and policies and/or other administrative information;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.6.&nbsp;to send
You surveys and marketing communications that NILTIK believes may be of
interest to You;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.7.&nbsp;to help
You address Your problems with the Site including addressing any technical
problems;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.8.if You
purchase any content or avail of any service from the Site, to complete and
fulfil Your purchase, for example, to have Your payments processed, communicate
with You regarding Your purchase and provide You with related customer service;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.9.&nbsp;for
proper administration of the Site;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.10.&nbsp;to
conduct internal reviews and data analysis for the Site (e.g., to determine the
number of visitors to specific pages within the Site);<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.11.to provide
it for scrutiny to any statutory, regulatory or any other governmental
authority or any other agency as advised by government or any judicial body;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.12.&nbsp;to
resolve disputes;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.13.detect and
protect Us against error, fraud and other criminal activity; enforce Our terms
and conditions;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.14.&nbsp;We
identify and use Your IP address to help diagnose problems with our server, and
to administer Our Site. Your IP address is also used to help identify You and
to gather broad demographic information;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.15.to improve
Our internal customer training;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.16.&nbsp;to
comply with financial services regulation including retention of financial
information and transaction;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.17.to conduct
financial and identity checks; fraud prevention checks; anti money laundering
and credit checks;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.18.collect /
share your Personal Information for Your background verification, either by Us
or through a third party, including but not limited to court record
verification and police verification;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.19.to enhance
the security of Our Site; and<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l1 level1 lfo4;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.1.20&nbsp;sharing
the information with third parties in order to facilitate certain value-added
services provided by third parties to you or provide certain value-added
services to You by Us.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>3.2A &quot;cookie&quot; is a small piece of information stored by a web
server on a web browser so it can be later read back from that browser. Cookies
are useful for enabling the browser to remember information specific to a given
user. We place both permanent and temporary cookies in your computer's hard
drive. These cookies do not contain any of your personally identifiable
information. You may choose to disable the cookies using the settings in your
computer system. However, if you do so, You may not be able to use the full
functionality of the Site.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>3.3Cookies may be used for the following purposes:<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.3.1. Customize
Your experience on Our Site;<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.3.2. For
obtaining information regarding use of the Site and assess which pages do You
visit the most in order to provide You with goods/services that may be of
interest to You.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.3.3.&nbsp;To
obtain information regarding Your IP address, location and other demographic
information such as age range and gender in order to ascertain users internet
behaviour.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.3.4. Assist in
complying with regulatory obligations such as anti-fraud and anti-money
laundering obligations.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;margin-left:42.0pt;
text-indent:-18.0pt;line-height:15.75pt;mso-list:l2 level1 lfo5;tab-stops:list 36.0pt'><![if !supportLists]><span
style='font-size:10.0pt;mso-bidi-font-size:10.5pt;font-family:Symbol;
mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#818487;
mso-fareast-language:EN-IN'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>3.3.5. For allowing
us to provide You with personalized services by remembering the choices made by
You with respect to language You prefer, geographical location where You are
etc.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>3.4 When You visit third party websites from Our Site, cookies embedded
in such third party content may be downloaded onto Your device. We do not control
such third party cookies and the same are governed by the terms and conditions
of such third parties. Please read the terms and conditions governing such
third party websites for more information on the same.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>4. RETENTION OF DATA.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>4.1 To the extent legally
permissible, after termination of Your Account with Us, We, shall retain all
the data collected from you<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>4.2 Please note that we will not require you to disclose Your personal
or security detail vide email or other communication. If you receive any email/
phone call/ SMS or other communication requesting you to share Your
personal/security/financial information, please do not respond to such requests
and delete such requests immediately.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>4.3&nbsp;&quot;Phishing&quot; is an attempt to steal personal details
and financial account details from a website user. Such “Phishers” use fake
emails to lead users to counterfeit websites where the user is tricked into
entering their personal details such as credit card numbers, passwords, PIN
etc. We shall not be held responsible for any loss, damage caused by responding
to such emails and sharing information with third parties specified under
Sections 4.2 and 4.3.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>5.&nbsp;DISCLOSURE OF INFORMATION<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>5.1 By using the
Site, You consent to the collection, transfer, use, storage and disclosure of
Your information as described in this Privacy Policy, including to the transfer
of Your information outside of Your country of residence. You hereby expressly
authorized us to share Your information. We may disclose Your information to
Our employees, consultants, affiliates, agents, contractors, business partners,
associates, subsidiaries, investors, registered merchants with Us and other
persons with whom We are required to share such information as per applicable
laws and on a need to know basis. We may share information with third parties
where you have expressed an interest in receiving information about their goods/services.
We may combine your information with information We collect from other
companies and use it to improve and personalize the Services we provide.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>5.2 However we may disclose the personal data or any other information
collected form You to any statutory, lawful enforcement body, regulatory body
or court if We are under a duty to do so or required by law to disclose or
share Your personal data or any other information collected from You.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>5.3&nbsp;We may further use Your information in an aggregate form to
ascertain the usage of the Site, for development of marketing and strategic
business plans etc. Please note, that in such use of information, no individual
user shall be identified separately.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>5.4&nbsp;In order to prevent money laundering activities and comply with
applicable regulations, We may report suspicious transactions to law enforcement
agencies. You hereby grant us the right to share Your information with such law
enforcement agencies in the event of any activity that may seem suspicious in
Our sole discretion. In the event We notice any suspicious activity on the
Site, We may, without prior intimation to You, (1) report such instance to the
law enforcement agency and share all relevant information that may be required
for investigation; (2) suspend Your Account during the period of investigation;
and (3) block Your use of our site under a cobranding arrangement with an
entity and withhold such funds.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>6. SECURITY<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>6.1&nbsp;We strive
to ensure the security, integrity and privacy of Your Personal Information and
to protect Your Personal Information against unauthorized access or
unauthorized alteration, disclosure or destruction. We adopt adequate measures
to prevent unauthorized access to Your Personal Information.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>6.2 We reserve the right to conduct a security review at any time to
verify your identity. You agree to provide us all the information that We request
for the security review. If you fail to comply with any security request, We
reserve the right to terminate Your Account with Us and prohibit Your access to
the Site.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>6.3 We are not liable and cannot be held liable for any breach of
security or for any actions of any third parties that receive Your Personal
Information.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>6.4 We are not responsible for the authenticity of the Personal
Information supplied by You.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>6.5 Notwithstanding anything contained in this Policy or elsewhere, We
shall not be held responsible for any loss, damage or misuse of Your Personal
Information, if such loss, damage or misuse is attributable to a Force Majeure
Event. A &quot;Force Majeure Event&quot; shall mean any event that is beyond
Our reasonable control and shall include, without limitation, sabotage, fire,
flood, explosion, acts of God, civil commotion, strikes or industrial action of
any kind, riots, insurrection, war, acts of government, computer hacking,
unauthorised access to computer data and storage device, computer crashes,
breach of security and encryption, etc.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>7. LINK TO OTHER SITES<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>7.1 Our Site may
provide links to other sites. These links are provided for Your convenience
only and the provision of these links does not mean that sites are related or
associated with Us. Please note that these Sites have their terms of use and
privacy policies. You should check their privacy policy before You submit Your
Personal Information or any other data with them. We don’t guarantee the
content, reliability, trustworthiness and/or the security of those sites.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>7.2 We may have certain features on Our Site which may be hosted by
third parties, Your interaction with such features shall be governed by the
privacy policy of such third parties.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>7.3&nbsp;We shall not be responsible for any loss, damage, claim or
expense caused as a result of You accessing these third party sites and
features.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>8. USER DISCRETION<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>You can always
choose not to provide information and in such cases, if the information required
is classified as mandatory, You will not be able to avail of that part of the
Site, services, feature or content.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>9. COMMUNICATIONS<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>9.1We may contact
You via the e-mail address and phone number as provided by You to Us. You may
also receive system-generated transactional e-mails such as confirmations,
notification of transfer of payments, notification of password changes etc.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>9.2 You hereby expressly consent to receive communications from Us on
Your NILTIK Driver Partner mobile application / NILTIK Operator mobile
application or through Your registered phone number and/or e-mail id. You agree
that any communication so received by You from Us will not amount to spam,
unsolicited communication or a violation of Your registration on the national
do not call registry.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>9.3 You hereby expressly consent to receive communications from Us /
third parties offering value-added services to you through Your registered
phone number and/or e-mail id and/or the Site. You hereby consent to receiving
such information and You agree that you shall not hold Us responsible for such
communication received from third parties. You agree that any communication so
received by You from third parties will not amount to spam, unsolicited
communication or a violation of Your registration on the national do not call
registry.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>10. REVISION OF THE POLICY<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>10.1 This Privacy
Policy can change at any time to include / exclude clauses without prior notice
to You and NILTIK does not bear responsibility for updating Users on the same.
Any changes will be effective upon posting of the revisions on the Site.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:15.75pt'><span style='font-size:10.5pt;font-family:"Arial",sans-serif;
mso-fareast-font-family:"Times New Roman";color:#818487;mso-fareast-language:
EN-IN'>10.2&nbsp;Unless otherwise specified by Us, revised policy will take
effect automatically and be binding on and from the day they are posted on the
Site. You are advised to regularly check for any amendments or updates to the
Privacy Policy from time to time. By continuing to access, You will be deemed
to have agreed to accept and be bound by such revised policy. If You do not agree
to the revised policy, You should discontinue accessing Our Site. Please go
through the Privacy Policy from time to time to be informed about any changes
that may have been made.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>11. GRIEVANCES<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>If You have any
concern, question, grievance or complaints in relation to the Site or its
content, or this Privacy Policy, You can contact Us through the&nbsp;<b><u>“Support”</u></b>&nbsp;section
on the Site.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:7.5pt;line-height:normal;mso-outline-level:
1'><span style='font-size:12.0pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#4A5054;text-transform:uppercase;mso-font-kerning:18.0pt;
mso-fareast-language:EN-IN'>12.&nbsp;GOVERNING LAW AND DISPUTE RESOLUTION.<o:p></o:p></span></p>

<p class=MsoNormal style='mso-margin-bottom-alt:auto;line-height:15.75pt'><span
style='font-size:10.5pt;font-family:"Arial",sans-serif;mso-fareast-font-family:
"Times New Roman";color:#818487;mso-fareast-language:EN-IN'>This Privacy Policy
shall be governed by and construed in accordance with the laws of India. All
disputes in relation to these Terms will be adjudicated exclusively before a
competent court in Berhampur, Odisha, India only.<o:p></o:p></span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>