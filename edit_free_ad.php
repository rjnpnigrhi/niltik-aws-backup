<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    if(isset($_GET['update'])){
        $id = $_POST['id'];
      $ad_title = $_POST['ad_title'];
      $product_id = $_POST['product_id'];
        
      if(!isset($_POST['sub_product_id'])){
        $sub_product_id = '';
      }else{
        $sub_product_id = $_POST['sub_product_id'];
      }
        
      if(!isset($_POST['product_company_id'])){
        $product_company_id = '';
      }else{
        $product_company_id = $_POST['product_company_id'];
      }
      $ad_details = $_POST['ad_details'];
      $price = $_POST['price'];
      $name = $_POST['name'];
      $email = $_POST['email'];
      $mobile = $_POST['mobile'];
      $location_id = $_POST['location_id'];
        
      if(!isset($_POST['locality_id'])){
        $locality_id = '';
      }else{
        $locality_id = $_POST['locality_id'];
      }
        
      $address = $_POST['address'];
        
      if($sub_product_id ==''){
        $prod_id = $product_id;
      }else{
        $prod_id = $sub_product_id;
      }
        
      if($locality_id ==''){
        $loc_id = $location_id;
      }else{
        $loc_id = $locality_id;
      }
        
      $q = "UPDATE classifieds SET ad_title = '$ad_title', product_id = '$prod_id', product_company_id = '$product_company_id', ad_details = '$ad_details', price = '$price', name = '$name', email = '$email', phone = '$mobile', location_id = '$loc_id', address = '$address' WHERE id = '$id'";
      $r = $db->update($q);
        
        echo "Your advertisement has been updated with new information.<br>";
        echo '<a href="user.php?freead=true">'."Ad Management Page".'</a>';
        
    }else{

    $id = $_GET['id'];

    $q = "SELECT * FROM classifieds WHERE id = '$id'";
    $r = $db->select($q);

    $ad = $r->fetch_array();
?>
    <div class="row">
        
    <h1>Edit / Update Your Ad </h1>
        <div class="row">
        <div class="col-sm-6">
           <form action="edit_free_ad.php?update=true" method="post" enctype="multipart/form-data">
           <div class="form-group">
               <label for="ad_title">Ad Title <span style="color:red;">*</span></label>
               <input type="text" class="form-control" name="ad_title" placeholder="Type a Attractive Ad Title" required value="<?PHP echo $ad['ad_title']; ?>">
           </div>
           
           <div class="form-group">
           <label for="product_id">Product :</label>
           <select id="product_id" name="product_id" class="form-control">
                <option value="">Select a Product</option>
                <?PHP
                    $q = "SELECT * FROM products WHERE parent_id = 0";
                    $r = $db->select($q);
                
                    if(!$r){
                        
                    }else{
                        while($d = $r->fetch_array()):
                ?>
                <option value="<?PHP echo $d['id']; ?>"><?PHP echo $d['product_name']; ?></option>
                <?PHP
                  endwhile;
                }
                ?>
            </select>
            <br>
<label for="sub_product_id">Category under Product :</label>
            <select name="sub_product_id" id="sub_product"  class="form-control">
                <option value="">Chose Category</option>
            </select>
        </div>  
                        
       <div class="form-group">
       <label for="product_company_id">Brand or Manufaturer</label>
       <select name="product_company_id" id="brand" class="form-control">
       <option value="">Chose a Brand Name</option>
       </select>
       </div>
                        
       <div class="form-group">
       <label for="description">Description / Details of the Ad <span style="color:red;">*</span></label>
       <textarea name="ad_details" id="ad_details" class="form-control" cols="30" rows="10" required><?PHP echo $ad['ad_details']; ?></textarea>
       </div>
                        
       <div class="form-group">
       <label for="price">Price <span style="color:red;">*</span></label>
       <input type="text" name="price" class="form-control" placeholder="Type your Quoted Price" required value="<?PHP echo $ad['price']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="name">Name <span style="color:red;">*</span></label>
       <input type="text" name="name" class="form-control" placeholder="Your Complete Name" required value="<?PHP echo $ad['name']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="email">email <span style="color:red;">*</span></label>
       <input type="text" name="email" class="form-control" placeholder="Your Complete email" required value="<?PHP echo $ad['email']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="mobile">Mobile No <span style="color:red;">*</span></label>
       <input type="text" name="mobile" class="form-control" placeholder="Your Mobile Number" required value="<?PHP echo $ad['phone']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                                            
       <div class="form-group">
       <label for="Address">Address <span style="color:red;">*</span></label>
       <textarea name="address" id="address" class="form-control" cols="30" rows="10"><?PHP echo $ad['address']; ?></textarea>
       </div>
       <input type="hidden" name="id" value="<?PHP echo $id; ?>">                 
       <button class="btn btn-default" type="submit">Submit Ad</button>
                        
       </form>
            </div>
            <div class="col-sm-6">
                <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                        echo "NO Images Uploaded.<br>";
                        echo '<a href="upload_free_ad_img.php?id='.$id.'" class="btn btn-primary btn-sm">'."Upload Images".'</a>';
                    }else{
                        echo '<a href="upload_free_ad_img.php?id='.$id.'" class="btn btn-primary btn-sm btn-right">'."Add / Change Images".'</a>';
                        echo "<hr>";
                        while($img = $r3->fetch_array()):                                                                  
                    ?>
                                   
                    <a href="uploads/<?PHP echo $img['image']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?PHP echo $ad['ad_title']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" class="img-responsive"></a><br>

                    <?PHP 
                        endwhile; 
                        } 
                        
                    ?>
            </div>
        </div>
<?PHP
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>