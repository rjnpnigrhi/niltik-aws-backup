<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    if(isset($_GET['submit'])){
        $otp = mt_rand(100000, 999999);
        $otp2 = mt_rand(10000000, 99999999);

        include('upload_img.php');
    }else{
?>
    <div class="row">
        <h1>About Niltik.com </h1>
        <form action="photo_upload.php?submit=true" method="post" enctype="multipart/form-data">
            <!--let us try to put some image uploaders here--> 
       <div class="form-group">
           <label for="image upload">Image 1</label> 
           <input type="hidden" name="MAX_FILE_SIZE" value="512000" />          
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_1').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_1" alt="your image" width="100" height="100" />
       </div>
       
         <div class="form-group">
           <label for="image upload">Image 2</label>
           <input type="hidden" name="MAX_FILE_SIZE" value="512000" />           
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_2').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_2" alt="your image" width="100" height="100" />
       </div>
         
       <div class="form-group">
           <label for="image upload">Image 3</label> 
           <input type="hidden" name="MAX_FILE_SIZE" value="512000" />          
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_3').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_3" alt="your image" width="100" height="100" />
       </div>
          
          <div class="form-group">
           <label for="image upload">Image 4</label>     
           <input type="hidden" name="MAX_FILE_SIZE" value="512000" />      
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_4').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_4" alt="your image" width="100" height="100" />
       </div>
         
          <div class="form-group">
           <label for="image upload">Image 5</label>     
           <input type="hidden" name="MAX_FILE_SIZE" value="512000" />      
           <input type="file" name="images[]" accept="image/*" onchange="document.getElementById('ad_img_5').src = window.URL.createObjectURL(this.files[0])">
           <img id="ad_img_5" alt="your image" width="100" height="100" />
       </div>
       
       <button class="btn btn-default" type="submit">Submit Ad</button>
        </form>
    </div>
    
<?PHP
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>