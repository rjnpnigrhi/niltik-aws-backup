<?PHP 
    //here we add all functions of the site 
    $db = new connection();

    function safe_data($data){
		global $db;
		$data = mysqli_real_escape_string($data);
		
		return $data;
	}
	
	function get_db_to_array($result){
		global $db;
		
		$res_array = array();
		
		for($count=0; $row=mysqli_fetch_array($result); $count++){
			$res_array[$count] = $row;
		}
		
		return $res_array;
	}

    function item_exists($cloth, $category, $service){
        $q = "SELECT * FROM cloth_iron WHERE cloth = '$cloth' AND category = '$category' AND service = '$service'";
        $r = $db->select($q);
        
        $count = mysql_num_rows($r);
        return $count;
    }

    function add_to_cart($id, $qty){
        
        if(isset($_SESSION['cart'][$id])){  
                $old = $_SESSION['cart'][$id];
                $new = $old+$qty;
                $_SESSION['cart'][$id] = $new;
            } else {
                $_SESSION['cart'][$id] = $qty;
        }
        
        return false;
    }

    function total_items($cart){
        $num_items = 0;
        if(is_array($cart)){
            foreach($cart as $id => $qty){
                $num_items += $qty;
            }
        }
        return $num_items;
    }

    function clothes($service){
        global $db;
        $q = "SELECT * FROM cloth_iron WHERE service = '$service' ORDER BY category DESC";
        $r = $db->select($q);
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }
    }

    function clothes_iron(){
        global $db;
        $q = "SELECT * FROM cloth_iron WHERE service = 'ironing' ORDER BY category";
        $r = $db->select($q);
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }
    }

    function clothes_wash(){
        global $db;
        $q = "SELECT * FROM cloth_iron WHERE service = 'washing' ORDER BY category";
        $r = $db->select($q);
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }
    }

    function clothes_drywash(){
        global $db;
        $q = "SELECT * FROM cloth_iron WHERE service = 'drywashing' ORDER BY category";
        $r = $db->select($q);
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }
    }

    function exists_coupon($coupon_code){
        global $db;
        $q = "SELECT * FROM coupons WHERE coupon_code = '$coupon_code' LIMIT 1";
        $r = $db->select($q);
        
        if($r){
            $c = '1';
        }else{
            $c = '0';
        }
        
        return $c;
    }
    
    function get_coupons(){
        global $db;
        
        $q = "SELECT * FROM coupons ORDER BY id DESC";
        $r = $db->select($q);
        
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }else{
            return false;
        }
        
    }

    function get_coupon_code($coupon_code){
        global $db;
        $q = "SELECT * FROM coupons WHERE coupon_code = '$coupon_code' LIMIT 1";
        $r = $db->select($q);
        
        $coupon = $r->fetch_array();
        
        return $coupon;
    }

    function get_order_by_id($id){
        global $db;
        
        $q = "SELECT * FROM order_book WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $res = $r->fetch_array();
        
        return $res;
    }
    
    function process_order($id){
        global $db;
        
        $q = "UPDATE order_book SET status = 'processing' WHERE id = '$id'";
        $r = $db->update($q);
        
        if(!$r){
            return false;
        }else{
            echo "Order has been Put on Process of Pickup and delivery.<br>";
        }
    }

    function complete_order($id){
        global $db;
        
        $q = "UPDATE order_book SET status = 'complete' WHERE id = '$id'";
        $r = $db->update($q);
        
        if(!$r){
            return false;
        }else{
            echo "Order has been successfully Completed.<br>";
        }
    }

    function get_cloth_for_id($id){
        global $db;
        
        $q = "SELECT * FROM cloth_iron WHERE id = '$id' LIMIT 1";
        $r = $db->select($q);
        
        $res = $r->fetch_array();
        
        return $res;
    }

    function get_clothes($service, $category){
        global $db;
        
        $q = "SELECT * FROM cloth_iron WHERE service = '$service' AND category = '$category' ORDER BY id";
        $r = $db->select($q);
        if($r){
            $result = get_db_to_array($r);
		    return $result;
        }
    }

    function compress_image($source_url, $destination_url, $quality) {

		$info = getimagesize($source_url);

    		if ($info['mime'] == 'image/jpeg')
        			$image = imagecreatefromjpeg($source_url);

    		elseif ($info['mime'] == 'image/gif')
        			$image = imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
        			$image = imagecreatefrompng($source_url);

    		imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}

    function prev_button($id){
        global $db;
        
        $q = "SELECT * FROM classifieds WHERE id < '$id' AND status = 'approved' ORDER BY id DESC LIMIT 1 ";
        $r = $db->select($q);
        
        if($r){
            $c = $r->fetch_array();
            
            echo '<a href="view_product.php?id='.$c['id'].'" class="btn btn-default">'."<< Previous Ad".'</a>';
        }else{
            echo "No Previous Ads";
        }
    }

    function next_button($id){
        global $db;
        
        $q = "SELECT * FROM classifieds WHERE id > '$id' AND status = 'approved' ORDER BY id ASC LIMIT 1 ";
        $r = $db->select($q);
        
        if($r){
            $c = $r->fetch_array();
            
            echo '<a href="view_product.php?id='.$c['id'].'" class="btn btn-default">'."Next Ad >>".'</a>';
        }else{
            echo "No More Ads";
        }
    }

    function prev_house($id){
        global $db;
        
        $q = "SELECT * FROM house_ad WHERE id < '$id' AND status = 'approved' ORDER BY id DESC LIMIT 1 ";
        $r = $db->select($q);
        
        if($r){
            $c = $r->fetch_array();
            
            echo '<a href="view_house.php?id='.$c['id'].'" class="btn btn-default">'."<< Previous Ad".'</a>';
        }else{
            echo "No Previous Ads";
        }
    }

    function next_house($id){
        global $db;
        
        $q = "SELECT * FROM house_ad WHERE id > '$id' AND status = 'approved' ORDER BY id ASC LIMIT 1 ";
        $r = $db->select($q);
        
        if($r){
            $c = $r->fetch_array();
            
            echo '<a href="view_house.php?id='.$c['id'].'" class="btn btn-default">'."Next Ad >>".'</a>';
        }else{
            echo "No More Ads";
        }
    }

	function get_ad($id){
		global $db;
		
		$q = "SELECT * FROM classifieds WHERE id = '$id'";
		$r = $db->select($q);
		
		$c = $r->fetch_array();
		
		echo '<a href="view_product.php?id='.$id.'">';
        $otp = $c['otp'];
        $otp2 = $c['otp2'];
                
        $q2 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
        $r2 = $db->select($q2);
        
        if(!$r2){
            echo '<img src="img/no-img.jpg" class="img-responsive img-rounded" style="width:100%; height:92px;" alt="'.$c['ad_title'].'">';
        }else{
            $di = $r2->fetch_array();
            echo '<img src="uploads/'.$di['image'].'" class="img-responsive img-rounded" style="width:100%; height:92px;" alt="'.$c['ad_title'].'">';
        } 

	echo '<br>';	
	echo '<strong>'.$c['ad_title'].'</strong>';
	echo '<br>';
	echo '</a>';
	}
?>