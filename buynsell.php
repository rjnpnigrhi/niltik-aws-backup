<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');
    
?>
         
    <div class="row" style="padding:20px;">
        <?PHP
            if(isset($_GET['view_product'])){
                
                $product_id = $_GET['view_product'];
                //we will get the different category of the product

                $qc = "SELECT * FROM products WHERE parent_id = '$product_id' ORDER BY id DESC";
                $rc = $db->select($qc);
                
                if(!$rc){
                    
                }else{
                    
                        
                    echo '<div class="row">';
                    echo '<a href="index.php">'."Home".'</a>'.">>".'<a href="buynsell.php">'."Product Ads".'</a>'.">>".'<a href="#">'."Product Page".'</a><br><br>';
                    echo '<span style="font-size:1.3em;">'. "Search By Product Category ".'<a class="btn btn-success" role="button" type="submit" href="post_free_ad.php" style="float:right;">'."Post a Free Ad".'</a></span>';
echo '<div class="clearfix"></div>';
echo "<hr>";
                    while($c = $rc->fetch_array()){
                    //we can show the number of ads too

                    $prod_id = $c['id'];
                    $qn = "SELECT * FROM classifieds WHERE FIND_IN_SET('$prod_id', product_id) AND status = 'approved' ORDER BY id DESC";
                    $rn = $db->select($qn);
                    
                    if(!$rn){
                        $cn = 0;
                    }else{
                       $cn = mysqli_num_rows($rn); 
                    }
                    echo '<div class="col-sm-4"><a href="buynsell.php?view_product='.$c['id'].'">'.$c['product_name'].'</a>'. " (".$cn.") " . '</div>';
                }
                    echo '</div>';
                }
echo "<hr>";

            //let us get the product companies for the same
                $q5 = "SELECT * FROM product_company WHERE product_id = '$product_id' ORDER BY company_name";
                $r5 = $db->select($q5);
                
                if(!$r5){
                    
                }else{
                    echo '<div class="row">';                    
                    echo '<h3>'."Search By Brand".'</h3>';
                    echo '<form class="form-inline main-form" method="post" action="brand.php?view_brand=true">';
                    echo '<select name="brand_id" class="form-control">';
                    while($comp = $r5->fetch_array()){
                        $comp_id = $comp['id'];
                        $qcomp = "SELECT * FROM classifieds WHERE product_company_id = '$comp_id' AND status = 'approved' ORDER BY id DESC";
                        $rcomp = $db->select($qcomp);
                    
                    if(!$rcomp){
                        $ccomp = 0;
                    }else{
                       $ccomp = mysqli_num_rows($rcomp); 
                    }
                    echo '<option value="'.$comp['id'].'">'.$comp['company_name']."(".$ccomp.")".'</option>';                        
                    }
                    echo '</select>';
                    echo "&nbsp; &nbsp;".'<button class="btn btn-default btn-sm" type="submit">'." FIND ADS".'</button>';
                    echo '</form>';
                    echo '</div>';
                }
            //let us get products for this category
            $q1 = "SELECT * FROM classifieds WHERE FIND_IN_SET('$product_id', product_id) AND status = 'approved' ORDER BY id DESC";
            $r1 = $db->select($q1);
            if(!$r1){
                echo '<h1>'."There are no Ads in this category".'</h1>';
                echo '<p>'."Be the first to post an ad here.".'</p>';
            }else{
                
                echo "<hr>";
                 echo '<a href="index.php">'."Home".'</a>'." >> ".'<a href="buynsell.php">'."Product Ads".'</a>'." >> ".'<a href="buynsell.php?view_product='.$product_id.'">'."Products".'</a><br><br>';
            while($ads = $r1->fetch_array()){
                $result[] = $ads;
            }
            
            //here we start paginating the data
            $numbers = $pagination->paginate($result, 12);
            
            //what are the data to be presented in these pages
            $data = $pagination->fetchresults();
            
            //let us get the current page number
            $pn = $pagination->page_num();
            
            //let us get all page numbers 
            $tp = count($numbers); 
                
            foreach($data as $ad){
?>
           <div class="row" style="background-color:#ffffcc; border:1px solid #e5e5e5; padding-top:10px; padding-bottom:10px; margin-bottom:20px;">
               <div class="col-md-4 col-sm-12 col-xs-12">
                   <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2' LIMIT 1";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                ?>
                <a href="view_product.php?id=<?PHP echo $ad['id']; ?>" title="<?PHP echo $ad['ad_title']; ?>"><img src="img/no-img.jpg" style="width:100%; height:150px; float:left;" alt="<?PHP echo $ad['ad_title']; ?>" ></a>
                <?PHP
                    }else{
                       $img = $r3->fetch_array();
                ?>
                    <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" style="width:100%; float:left;" alt="<?PHP echo $ad['ad_title']; ?>" class="img-responsive"></a>
                <?PHP } ?>
               </div>
               
               <div class="col-md-8 col-sm-12 col-xs-12">
                   <a href="view_product.php?id=<?PHP echo $ad['id']; ?>"><span style="font-size:1.4em; color:black; text-shadow:1px 1px #ffffff;"><?PHP echo $ad['ad_title']; ?></span></a>
                                     <span style="float:right; font-size:1.4em; color:RED;">Rs. <?PHP echo $ad['price']; ?>.00</span>
                                     <br>
                                     <?PHP
                                    $product = $ad['product_id'];
                                    $product = explode(",", $product);

                                    $product_id = $product[0];
                                    $sub_product_id = $product[1];
                                    
                                    $qpro = "SELECT * FROM products WHERE id = '$product_id'";
                                    $rpro = $db->select($qpro);

                                    $prod = $rpro->fetch_array();
                if(!$sub_product_id){
                                        echo '<a href="buynsell.php?view_product='.$prod['id'].'">'.$prod['product_name'].'</a>';
                                    }else{
                                    $qspro = "SELECT * FROM products WHERE id = '$sub_product_id'";
                                    $rspro = $db->select($qspro);
                                    
                                    if(!$rspro){
                                    }else{
                                    $sprod = $rspro->fetch_array();
      echo '<a href="buynsell.php?view_product='.$prod['id'].'">'.$prod['product_name'].'</a>'.">>".'<a href="buynsell.php?view_product='.$sprod['id'].'">'.$sprod['product_name'].'</a>';
                                   }
                                    }
                
                                ?>
                                <br>
                                Place : 
                                <?PHP
                                    $location = $ad['location_id'];
                                    $location = explode(",", $location);

                                    $location_id = $location[0];
                                    
                                    $q1 = "SELECT * FROM location WHERE id = '$location_id'";
                                    $r1 = $db->select($q1);

                                    $loc = $r1->fetch_array();
                                ?>
                                      <strong><?PHP echo $loc['location_name']; ?></strong>
                                      <br>
                                      <br class="hidden-xs">
                                      <br class="hidden-xs">                                                                          
                                <?PHP
                                    $timestamp = strtotime($ad['posted_on']);
                   
                                    $p_day = date('d', $timestamp);
                                    $p_month = date('M', $timestamp);
                                    $p_year = date('Y', $timestamp);
                                    echo "Posted on : ". "$p_day $p_month, $p_year";
                                    echo "&nbsp; &nbsp; &nbsp; &nbsp;";
                                    echo '<br class="hidden-lg hidden-md hidden-sm">';
                                    echo "( ".$ad['hits']. " Views )";
                                    echo '<br class="hidden-lg hidden-md hidden-sm">';
                                ?>
                                      
                                  <a href="view_product.php?id=<?PHP echo $ad['id']; ?>" class="btn btn-info btn-right" style="float:right">View Details</a>  
               </div>
                       </div>
                     <?PHP 
                        }
                     ?>   
                        
           <nav>
              <ul class="pagination">
                <?PHP
                
                //let us do some more pagination tricks here
                //this is the previous page
                if($pn == 1){

                }else{
                    $pp = $pn-1;
                    echo '<li><a href="buynsell.php?view_product='.$product_id.'&page='.$pp.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
                }

                //let us get the page numbers here
                foreach ($numbers as $n){	
                    echo '<li class="'.($pn == $n ? 'active' : '').'"><a href="buynsell.php?view_product='.$product_id.'&page='.$n.'">'.$n.'</a></li>';
                }

                //this is for the next page
                if($pn == $tp){

                }else{
                    $np = $pn+1;
                    echo '<li><a href="buynsell.php?view_product='.$product_id.'&page='.$np.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
                }
            }
               ?>
              </ul>
            </nav>

<div class="clearfix"></div>
<a href="javascript:history.back()" class="btn btn-default">Go Back to Previous Page</a>
<?PHP          
            
            }else{
            echo '<a href="index.php">'."Home".'</a>'.">>".'<a href="buynsell.php">'."Product Ads".'</a>';
                echo '<br><br>';
                
            $qc1 = "SELECT * FROM classifieds WHERE status = 'approved'";
                $rc1 = $db->select($qc1);
                
                if(!$rc1){
                    
                    echo '<h2>'."No Ads on our site".'</h2>';
                    
                }else{
                    
                    $c8 = mysqli_num_rows($rc1);
                    echo '<span style="font-size:1.3em;">'. "Total Ads : " . $c8 .'<a class="btn btn-success" role="button" type="submit" href="post_free_ad.php" style="float:right; position:relative; margin-left:10px;">'."Post a Free Ad".'</a>'." ".'<a class="btn btn-primary" role="button" type="submit" href="archieves.php" style="float:right; margin-left:10px;">'."View All Ads".'</a></span>';
                }
                
                echo "<hr>";
                
                
                echo '<div class="clearfix"></div>';
				
                
            $qp = "SELECT * FROM products WHERE parent_id = 0 AND id < 7 ORDER BY id ASC";
            $rp = $db->select($qp);
            
			echo '<h3 style="color:WHITE; background-color:BLACK;">'."Buy USED Products".'</h3>';
            while($product=$rp->fetch_array()){
                
                
        ?>
            <div class="col-sm-2 col-xs-4 thumb_6">
                <a href="buynsell.php?view_product=<?PHP echo $product['id']; ?>" title="<?PHP echo $product['product_name']; ?>">
                <img src="img/<?PHP echo $product['product_icon']; ?>" alt="<?PHP echo $product['product_name']; ?>" width="60%"><br>
                <?PHP
                
                //let us get the number of ads from classifieds table for this category
                $product_id = $product['id'];
                
                $qc = "SELECT * FROM classifieds WHERE FIND_IN_SET('$product_id', product_id) AND status = 'approved'";
                $rc = $db->select($qc);
                
                if(!$rc){
                    
                    $c9 = 0;
                    
                }else{
                    
                    $c9 = mysqli_num_rows($rc);
                }
                echo $product['product_name']; ?> (<?PHP echo $c9; ?>) 
                
                </a>
            </div>
        <?PHP
            }
		echo "<hr>";
                
                
                echo '<div class="clearfix"></div>';
	
			$qp2 = "SELECT * FROM products WHERE parent_id = 0 AND id > 6 ORDER BY id ASC";
            $rp2 = $db->select($qp2);
            
			echo '<h3 style="color:WHITE; background-color:BLACK;">'."Advertisements".'</h3>';
			
            while($product2=$rp2->fetch_array()){
                
                
        ?>
            <div class="col-sm-2 col-xs-4 thumb_6">
                <a href="buynsell.php?view_product=<?PHP echo $product2['id']; ?>" title="<?PHP echo $product2['product_name']; ?>">
                <img src="img/<?PHP echo $product2['product_icon']; ?>" alt="<?PHP echo $product2['product_name']; ?>" width="60%"><br>
                <?PHP
                
                //let us get the number of ads from classifieds table for this category
                $product_id2 = $product2['id'];
                
                $qc2 = "SELECT * FROM classifieds WHERE FIND_IN_SET('$product_id2', product_id) AND status = 'approved'";
                $rc2 = $db->select($qc2);
                
                if(!$rc2){
                    
                    $cx = 0;
                    
                }else{
                    
                    $cx = mysqli_num_rows($rc2);
                }
                echo $product2['product_name']; ?> (<?PHP echo $cx; ?>) 
                
                </a>
            </div>
        <?PHP
            }
			
			
            }
        ?>
                    
            <div class="clearfix"></div> 
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>