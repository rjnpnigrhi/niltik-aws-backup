<h3>Image Resizing on Niltik.com</h3>
<p>This script may take some time to execute, do not worry.</p>
<?php
ini_set('memory_limit', -1);

$source = "uploads";

$directory = opendir($source);

//Scan through the folder one file at a time

while(($file = readdir($directory)) != false)
{

       echo "<br>".$file;

       //Run each file through the image resize function

       imageResize($file, $source.'/', 960, 960);

}

function imageResize($file, $path, $width, $height)
{

       $target = 'uploads/';

       $handle = opendir($path);

       if($file != "." && $file != ".." && !is_dir($path.$file))
       {

              $thumb = $path.$file;

              $imageDetails = getimagesize($thumb);

              $originalWidth = $imageDetails[0];

              $originalHeight = $imageDetails[1];

        

                     $thumbWidth = $width;

                     $thumbHeight = ($originalHeight/($originalWidth/$thumbWidth));

        
              $originalImage = ImageCreateFromJPEG($thumb);

              $thumbImage = ImageCreateTrueColor($thumbWidth, $thumbHeight);

              ImageCopyResized($thumbImage, $originalImage, 0, 0, 0, 0, $thumbWidth,
              $thumbHeight, $originalWidth, $originalHeight);

              $filename = $file;

              imagejpeg($thumbImage, $target.$filename, 85);

       }

       closedir($handle);

}

ini_restore('memory_limit');
?>