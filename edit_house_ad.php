<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    if(isset($_GET['update'])){
        $id = $_POST['id'];
       $ad_title = $_POST['ad_title'];
        $category_id = $_POST['category_id'];
        $details = $_POST['details'];
        $price = $_POST['price'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $location_id = $_POST['location_id'];
        
        if(!isset($_POST['locality_id'])){
            $locality_id = '';
        }else{
            $locality_id = $_POST['locality_id'];
        }
        $address = $_POST['address'];
        
      if($locality_id ==''){
        $loc_id = $location_id;
      }else{
        $loc_id = $locality_id;
      }
        
      $q = "UPDATE house_ad SET ad_title = '$ad_title', category_id = '$category_id', details = '$details', price = '$price', name = '$name', email = '$email', phone = '$phone', location_id = '$loc_id', address = '$address' WHERE id = '$id'";
      $r = $db->update($q);
        
        echo "Your advertisement has been updated with new information.<br>";
        echo '<a href="user.php?housead=true">'."Ad Management Page".'</a>';
        
    }else{

    $id = $_GET['id'];

    $q = "SELECT * FROM house_ad WHERE id = '$id'";
    $r = $db->select($q);

    $ad = $r->fetch_array();
?>
    <div class="row">
        
    <h1>Edit / Update Your Ad </h1>
        <div class="row">
        <div class="col-sm-6">
           <form action="edit_house_ad.php?update=true" method="post" enctype="multipart/form-data">
           <div class="form-group">
               <label for="ad_title">Ad Title <span style="color:red;">*</span></label>
               <input type="text" class="form-control" name="ad_title" placeholder="Type a Attractive Ad Title" required value="<?PHP echo $ad['ad_title']; ?>" >
           </div>
           
           <div class="form-group">
           <label for="product_id">Category :</label>
           <select id="category_id" name="category_id" class="form-control">
                <option value="">Select a Category</option>
                <?PHP
                    $q = "SELECT * FROM house_category";
                    $r = $db->select($q);
                
                    if(!$r){
                        
                    }else{
                        while($d = $r->fetch_array()):
                ?>
                <option value="<?PHP echo $d['id']; ?>"><?PHP echo ucfirst($d['category']); ?></option>
                <?PHP
                  endwhile;
                }
                ?>
            </select>
        </div>  
                        
       <div class="form-group">
       <label for="description">Description / Details of the Ad <span style="color:red;">*</span></label>
       <textarea name="details" id="details" class="form-control" cols="30" rows="10" required><?PHP echo $ad['details']; ?></textarea>
       </div>
                        
       <div class="form-group">
       <label for="price">Price <span style="color:red;">*</span></label>
       <input type="text" name="price" class="form-control" placeholder="Type your Quoted Price" required value="<?PHP echo $ad['price']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="name">Name <span style="color:red;">*</span></label>
       <input type="text" name="name" class="form-control" placeholder="Your Complete Name" required value="<?PHP echo $ad['name']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="email">email <span style="color:red;">*</span></label>
       <input type="email" name="email" class="form-control" placeholder="Your Complete email" required value="<?PHP echo $ad['email']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="mobile">Mobile No <span style="color:red;">*</span></label>
       <input type="text" name="phone" class="form-control" placeholder="Your Mobile Number" required value="<?PHP echo $ad['phone']; ?>">
       </div>
                        
       <div class="form-group">
       <label for="location">Location</label>
       <select name="location_id" id="city_id" class="form-control">
       <option value="">Select Your City/Town</option>
       <?PHP
        $q3 = "SELECT * FROM location WHERE city_id = 0";
        $r3 = $db->select($q3);
        
        while($loc = $r3->fetch_array()):
        ?>       
        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
        <?PHP endwhile; ?>
       </select>                            
       </div>
       
       <div class="form-group">
       <label for="locality_id">Nearest Area / Locality</label>
       <select name="locality_id" class="form-control" id="street">
           <option value="">Chose nearest Location</option>
       </select>
       </div>
                                            
       <div class="form-group">
       <label for="Address">Address <span style="color:red;">*</span></label>
       <textarea name="address" id="address" class="form-control" cols="30" rows="10"><?PHP echo $ad['address']; ?></textarea>
       </div>
	   
	   <input type="hidden" name="id" value="<?PHP echo $id; ?>">        
                        
       <button class="btn btn-default" type="submit">Submit Ad</button>
                        
       </form>
            </div>
            <div class="col-sm-6">
                <?PHP
                    $otp = $ad['otp'];
                    $otp2 = $ad['otp2'];
                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
                    $r3 = $db->select($q3);
                                  
                    if(!$r3){
                        echo "NO Images Uploaded.<br>";
                        echo '<a href="upload_house_ad_img.php?id='.$id.'" class="btn btn-primary btn-sm">'."Upload Images".'</a>';
                    }else{
                        echo '<a href="upload_house_ad_img.php?id='.$id.'" class="btn btn-primary btn-sm btn-right">'."Add / Change Images".'</a>';
                        echo "<hr>";
                        while($img = $r3->fetch_array()):                                                                  
                    ?>
                                   
                    <a href="uploads/<?PHP echo $img['image']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?PHP echo $ad['ad_title']; ?>"><img src="uploads/<?PHP echo $img['image']; ?>" class="img-responsive"></a><br>

                    <?PHP endwhile; } ?>
            </div>
        </div>
<?PHP
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>