<?PHP
    session_start();
    if(!isset($_SESSION['user'])){
        
        header('Location: login.php');

    }else{

    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    
        
?>
    <div class="row"> 
      <h1><?PHP echo $_SESSION['name']; ?></h1>
           <h3><a href="logout.php">Log Out</a></h3>
            <?PHP 
            if(isset($_GET['profile'])){
                $user_id = $_SESSION['id'];
                
                $q = "SELECT * FROM users WHERE id = '$user_id' LIMIT 1";
                $r = $db->select($q);
                
                $d = $r->fetch_array();
        ?>
          <h3>My Profile</h3>
                    <table width="100%" class="table table-striped">
                        <tr>
                            <td><strong>Name :</strong></td>
                            <td><?PHP echo $d['user_name']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>e-Mail ID :</strong></td>
                            <td><?PHP echo $d['user_email']; ?></td>
                        </tr>
                        
                        <tr>
                            <td><strong>Mobile No. :</strong></td>
                            <td><?PHP echo $d['user_mobile']; ?></td>
                        </tr>
                        
                        <tr>
                            <td><strong>Address :</strong></td>
                            <td>
                                <?PHP echo nl2br($d['user_address']); ?>
                            </td>
                        </tr>
                        <?PHP 
                        $location = $d['user_location_id'];
                        $location = explode(",", $location);

                        $location_id = $location[0];
                        $locality_id = $location[1];
                                    
                        $q1 = "SELECT * FROM location WHERE id = '$location_id'";
                        $r1 = $db->select($q1);
                        
                        if(!$r1){
                            
                        }else{

                        $loc = $r1->fetch_array();
                        
                        ?>
                        <tr>
                            <td><strong>Location :</strong></td>
                            <td><?PHP echo $loc['location_name']; } ?></td>
                        </tr>
                        <?PHP 
                        if($locality_id == ''){
                        echo "Unknown / Other";
                        }else{ 
                        $q2 = "SELECT * FROM location WHERE id = '$locality_id'";
                        $r2 = $db->select($q2);
                        if(!$r2){
                            
                        }else{
                        $loc2 = $r2->fetch_array();
                        }
                        ?>
                        <tr>
                            <td><strong>Location :</strong></td>
                            <td><?PHP echo $loc2['location_name']; } ?></td>
                        </tr>
                    </table>           
        <?PHP
            }elseif(isset($_GET['edit'])){
                $user_id = $_SESSION['id'];
                
                $q = "SELECT * FROM users WHERE id = '$user_id' LIMIT 1";
                $r = $db->select($q);
                
                $d = $r->fetch_array();
        ?>
           <h3>Edit Your Profile</h3>
                   
                    <form method="post" action="user.php?update=true">
                      <div class="form-group">
                        <label for="name">Your Name</label>
                        <input type="text" class="form-control" name="user_name" value="<?PHP echo $d['user_name']; ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control" name="user_address" cols="30" rows="10" required><?PHP echo $d['user_address']; ?></textarea>
                      </div>
                      
                      <div class="form-group">
                       <label for="location">Location</label>
                       <select name="location_id" id="city_id" class="form-control">
                       <option value="">Select Your City/Town</option>
                       <?PHP
                        $q3 = "SELECT * FROM location WHERE city_id = 0";
                        $r3 = $db->select($q3);

                        while($loc = $r3->fetch_array()):
                        ?>       
                        <option value="<?PHP echo $loc['id']; ?>"><?PHP echo $loc['location_name']; ?></option>
                        <?PHP endwhile; ?>
                       </select>                            
                       </div>

                       <div class="form-group">
                       <label for="locality_id">Nearest Area / Locality</label>
                       <select name="locality_id" class="form-control" id="street">
                           <option value="">Chose nearest Location</option>
                       </select>
                       </div>
                      <input type="hidden" name="id" value="<?PHP echo $user_id; ?>">
                      <button type="submit" class="btn btn-default">Update Profile</button>
                    </form>
        <?PHP
            }elseif(isset($_GET['change_pass'])){
        ?>
           <h3>Change Password</h3>
                    <form method="post" action="user.php?pass_change=true">
                      <div class="form-group">
                        <label for="password">Old Password</label>
                        <input type="password" name="password_old" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="password">New Password</label>
                        <input type="password" name="password_new" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="password">Confirm Password</label>
                        <input type="password" name="password_new2" class="form-control" required>
                      </div>
                      <button type="submit" class="btn btn-default">Update Password</button>
                    </form>
        <?PHP
            }elseif(isset($_GET['pass_change'])){
                $user_id = $_SESSION['id'];
                $password_old = $_POST['password_old'];
                $password_new = $_POST['password_new'];
                $password_new2 = $_POST['password_new2'];
                
                if($password_new !== $password_new2){
                echo "Both new passwords do not match.<br>";
                echo '<a href="javascript:history.back()" class="btn btn-info">'."Go Back and Retry".'</a>';
                }else{
                    $q = "UPDATE users SET user_pass = '$password_new' WHERE id = '$user_id' AND user_pass = '$password_old'";
                    $r = $db->update($q);
                    
                    if($r){                   
                        
                        echo "Your Password has been changed.<br>";
                        echo '<a href="login.php">'."Click Here to Log In Again".'</a>';
                        
                        session_destroy();
                    }
                }
                
            }elseif(isset($_GET['update'])){
                $user_name = $_POST['user_name'];
                $user_address = $_POST['user_address'];
                $location_id = $_POST['location_id'];
                $locality_id = $_POST['locality_id'];
                $id = $_POST['id'];
                
                if($locality_id ==''){
                    $loc_id = $location_id;
                  }else{
                    $loc_id = $locality_id;
                  }
                
                $q = "UPDATE users SET user_name = '$user_name', user_address = '$user_address', user_location_id = '$loc_id' WHERE id = '$id'";
                $r = $db->update($q);
                
                echo "Your Profile Information has been updated.<br>";
                echo '<a href="user.php?profile=true">'."View Your Profile".'</a>';
              
}elseif(isset($_GET['iron'])){
        
                echo '<h3>'."Iron Services begins on the 15th of August".'</h3>';
        
            }elseif(isset($_GET['block_free_ad'])){
                $id = $_GET['block_free_ad'];
                $q = "UPDATE classifieds SET status = 'blocked' WHERE id = '$id'";
                $r = $db->update($q);
        
                echo "Classified Ad has been blocked.<br>";
                echo '<a href="user.php?freead=true" class="btn btn-info">'."Ads Management".'</a>';
            
            }elseif(isset($_GET['block_house_ad'])){
                $id = $_GET['block_free_ad'];
                $q = "UPDATE house_ad SET status = 'blocked' WHERE id = '$id'";
                $r = $db->update($q);
        
                echo "House Rent Ad has been blocked.<br>";
                echo '<a href="user.php?housead=true" class="btn btn-info">'."House Ads Management".'</a>';
                
            }elseif(isset($_GET['unblock_free_ad'])){
                $id = $_GET['unblock_free_ad'];
                $q = "UPDATE classifieds SET status = 'approved' WHERE id = '$id'";
                $r = $db->update($q);
        
                echo "Classified Ad has been unblocked.<br>";
                echo '<a href="user.php?freead=true" class="btn btn-info">'."Ads Management".'</a>';
            
            }elseif(isset($_GET['unblock_house_ad'])){
                $id = $_GET['unblock_free_ad'];
                $q = "UPDATE house_ad SET status = 'approved' WHERE id = '$id'";
                $r = $db->update($q);
        
                echo "House Rent Ad has been unblocked.<br>";
                echo '<a href="user.php?housead=true" class="btn btn-info">'."House Ads Management".'</a>';
                
            }elseif(isset($_GET['housead'])){
                $user_mobile = $_SESSION['phone'];
                $q = "SELECT * FROM house_ad WHERE phone = '$user_mobile' ORDER BY id DESC";
                $r = $db->select($q);
                
                if(!$r){
                    echo '<h3>'."You Have not posted any Ads.".'</h3>';
                }else{
                    
                
        ?>
           <h3>My House Rent Ads</h3>
              <table width="100%" class="table table-striped">
                  <tr>
                      <th width="15%">Date</th>
                      <th>Title</th>
                      <th width="10%">Status</th>
                      <th width="10%">Block Ad</th>
                  </tr>
                  <?PHP while($d = $r->fetch_array()): 
                    
                    $timestamp = strtotime($d['posted_on']);
                   
                   $p_day = date('d', $timestamp);
                   $p_month = date('M', $timestamp);
                   $p_year = date('Y', $timestamp);
                  ?>
                  <tr>
                      <td><?PHP echo "$p_day $p_month, $p_year"; ?></td>
                      <td><a href="edit_house_ad.php?id=<?PHP echo $d['id']; ?>"><?PHP echo $d['ad_title']; ?></a></td>
                      <td><?PHP echo ucfirst($d['status']); ?></td>
                      <td>
                      <?PHP
                        if($d['status']=='blocked'){
                        ?>
                       <a href="user.php?unblock_house_ad=<?PHP echo $d['id']; ?>" class="btn btn-success btn-sm">Unblock</a>     
                       <?PHP
                        }else{
                        ?>
                      <a href="user.php?block_house_ad=<?PHP echo $d['id']; ?>" class="btn btn-success btn-sm">Block</a>
                      <?PHP } ?>
                      </td>
                  </tr>
                  
                <?PHP endwhile; ?>
              </table>
        <?PHP
                }
            }elseif(isset($_GET['freead'])){
                $user_mobile = $_SESSION['phone'];
                $q = "SELECT * FROM classifieds WHERE phone = '$user_mobile' ORDER BY id DESC";
                $r = $db->select($q);
                
                if(!$r){
                    echo '<h3>'."You Have not posted any Ads.".'</h3>';
                }else{
                    
        ?>
           <h3>My Buy and Sell Ads</h3>
              <table width="100%" class="table table-striped">
                  <tr>
                      <th width="15%">Date</th>
                      <th>Title</th>
                      <th width="10%">Status</th>
                      <th width="10%">Action</th>
                  </tr>
                  <?PHP while($d = $r->fetch_array()): 
                  
                    $timestamp = strtotime($d['posted_on']);
                   
                   $p_day = date('d', $timestamp);
                   $p_month = date('M', $timestamp);
                   $p_year = date('Y', $timestamp);
                  ?>
                  <tr>
                      <td><?PHP echo "$p_day $p_month, $p_year"; ?></td>
                      <td><a href="edit_free_ad.php?id=<?PHP echo $d['id']; ?>"><?PHP echo $d['ad_title']; ?></a></td>
                      <td><?PHP echo ucfirst($d['status']); ?></td>
                      <td>
                      <?PHP
                        if($d['status']=='blocked'){
                        ?>
                      <a href="user.php?unblock_free_ad=<?PHP echo $d['id']; ?>" class="btn btn-success btn-sm">Unblock</a>      
                       <?PHP
                        }else{
                        ?>
                      <a href="user.php?block_free_ad=<?PHP echo $d['id']; ?>" class="btn btn-success btn-sm">Block</a>
                      <?PHP } ?>
                      </td>
                  </tr>
                  <?PHP endwhile; ?>
                  
              </table>
        <?PHP
                }
            }elseif(isset($_GET['inbox'])){
        ?>
           <h3>Your Inbox is empty.</h3>
        <?PHP
            }else{
                echo "Welcome ".$_SESSION['name']."<br>";
                echo '<a href="logout.php" class="btn btn-danger btn-xs">'."Log Out".'</a>';
            }
        
    }

    include("mods/trending_ads.php");

    include("mods/footer.php");
?>