<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
    include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    $total_price = ''; 
    $msg = '';
    $less = '';

    if(isset($_GET['unset'])){
        unset($_SESSION['cart']);
        unset($_SESSION['coupon_code']);
        unset($_SESSION['amount']);
        unset($_SESSION['discount']);
        
        echo '<h3>'."Uh, Oh ... It seems you Emptied Your Basket.".'</h3>';
        echo "Your Basket is Empty.<br><br>";
        echo '<a href="laundry.php#articles" class="btn btn-default">'."Add Articles in the Basket".'</a>';
        
    }else{
    
?>
    <div class="row">
       <?PHP
        if(isset($_GET['place_order'])){
            
            $name = $_POST['name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $address = $_POST['address'];
            
            $final_bill = $_POST['final_price'];
            $discount = $_POST['discount'];
            $coupon_code = $_POST['coupon_code'];
            $total_price = $final_bill + $discount;
            
            $otp = mt_rand(100000, 999999);
            $_SESSION['otp'] = $otp;

            $tokens = explode(" ", $name);
            $firstname = $tokens[0];
            //let us create an order first
            $q1 = "INSERT INTO order_book (name, email, phone, address, otp, total_price, coupon_code, discount, final_bill) VALUES ('$name', '$email', '$phone', '$address', '$otp', '$total_price', '$coupon_code', '$discount', '$final_bill')";
            $r1 = $db->insert($q1);
            
            $order_id = mysqli_insert_id($db->link);
            $_SESSION['order_id'] = $order_id;
            //we include an OTP sending script here
            
            
            $tokens = explode(" ", $name);
            $name_sms = $tokens[0];
            
            //Create API URL
            $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$phone&msg=Dear%20$name_sms%2C%20otp%20for%20confirmation%20of%20Laundry%20Booking%20on%20oursite%20is%20$otp.%20Admin&route=T";

            //Call API
            $ch = curl_init($fullapiurl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch); 
            //echo $result ; // For Report or Code Check
            curl_close($ch);
       
            
            //we send a mail to the user about the order
            $to = $email;
            $subject = "Your order confirmation code for NILTIK.com";
            $message = "Dear ".$name."\r\n \r\n Please enter this six digit code in the confirmation page on NILTIK.com to confirm and authenticate your Laundry Service Order No: ".$order_id.".\r\n \r\n 
            OTP Code : ".$otp."\r\n \r\n 
            If you could not confirm or wish to confirm later, please visit this link and we would do that for you. \r\n 
            http://www.niltik.com/iron_order.php?authenticate=".$order_id." \r\n 
            Operations Manager \r\n 
            NILTIK.com";
            $headers = "From: webmaster@niltik.com" . "\r\n" .
            "CC: support@niltik.com";

            mail($to,$subject,$message,$headers);
            
            foreach($_SESSION['cart'] as $id => $qty):
            
            $q = "SELECT * FROM cloth_iron WHERE id = '$id' LIMIT 1";
            $r = $db->select($q);
            
            $prod = $r->fetch_array();
            $cloth_id = $prod['id'];
            $category = $prod['category'];
            $service = $prod['service'];
            $price = $prod['price'];
            
            $q = "INSERT INTO invoice_book (order_id, cloth_id, category, service, price, qty) VALUES ('$order_id', '$cloth_id', '$category', '$service', '$price', '$qty')";
            $r = $db->insert($q);
            
            endforeach;
            
            unset($_SESSION['cart']);
            unset($_SESSION['coupon_code']);
            unset($_SESSION['amount']);
            unset($_SESSION['discount']);
            
            echo "Thank You for Placing your Order.<br>";
            echo "An OTP has been sent to your mobile phone and eMail ID now.<br>";
            echo "You must confirm your order by clicking on the order confirmation link below.<br>";
?>
    <form action="iron_order.php?confirm=true" method="post" class="form-horizontal">
        <div class="form-group">
            <label for="activation_code">Type OTP code : </label>
            <input type="text" name="otp" placeholder="OTP from your Mobile" class="form-control">
        </div>
        <div class="form-group">
           <input type="hidden" name="order_id" value="<?PHP echo $_SESSION['order_id']; ?>">
            <button type="submit" class="btn btn-default btn-sm">Submit</button>
        </div>
        <p class="help-block">If you did not receive the OTP on your mobile, please check your mail id for a mail from webmaster@niltik.com</p>
    </form>           
<?PHP
            
            //here we send OTP to Phone
            //here we sent email to email id
        }elseif(isset($_GET['confirm'])){
            $order_id = $_POST['order_id'];
            $otp = $_POST['otp'];
            
            $q = "SELECT * FROM order_book WHERE id = '$order_id' AND otp = '$otp'";
            $r = $db->select($q);
            
            if($r){
                $q1 = "UPDATE order_book SET otp_confirm = 'true' WHERE id = '$order_id'";
                $r1 = $db->update($q1);
                
                echo "Congratulations, Your Order has been confirmed.<br>";
                //let us send an SMS to the user
                $res = $r->fetch_array();
                $name = $res['name'];
                $email = $res['email'];
                $tokens = explode(" ", $name);
                $name_sms = $tokens[0];
                $phone = $res['phone'];
                $final_bill = $res['final_bill'];

                //let us send a sms to the user
                //Create API URL
                $fullapiurl="http://smsodisha.in/sendsms?uname=saicharan&pwd=password@12&senderid=NILTIK&to=$phone&msg=Dear%20$name_sms%2C%20thank%20you%20for%20Booking%20Online%20Laundry%20Order%20$order_id%20for%20Rs.%20$final_bill%20on%20our%20site.%20Admin&route=T";

                //Call API
                $ch = curl_init($fullapiurl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); 
                //echo $result ; // For Report or Code Check
                curl_close($ch);
                
                //let us send an email to the user
                $from = "support@niltik.com";
                $to = $email;
                $subject = "Online Laundry Service Order no. $order_id on NILTIK.com";
                $message = "Dear ".$name. " \r\n \r\n It gives us pleasure to intimate you that your order for online laundry Services has been confirmed on Niltik.com. \r\n \r\n View Your Order Details Here \r\n \r\n http://www.niltik.com/order_details.php?id=$order_id \r\n \r\n You can also view the details by logging in to your member panel. \r\n \r\n Please join NILTIK.com if you have not joined yet. Or login to your member panel and check status of your Order.\r\n \r\n Operations Manager, \r\n NILTIK.com";
                $headers = "From: support@niltik.com" . "\r\n" .
                    "CC: webmaster@niltik.com";

                    mail($to,$subject,$message,$headers);        
                
                echo "Our Executives will call you shortly.<br>";
                echo '<a href="order_details.php?id='.$order_id.'">'."My Order Details".'</a>';
            }else{
                echo "The OTP is not valid, try again.<br>";
                echo '<a href="javascript:history.back()">'."Go back and retry".'</a>';
            }
            }elseif(isset($_GET['authenticate'])){
        ?>
        <form action="iron_order.php?confirm=true" method="post" class="form-horizontal">
            <div class="form-group">
                <label for="activation_code">Type OTP code : </label>
                <input type="text" name="otp" placeholder="Type the OTP here" class="form-control">
            </div>
            <div class="form-group">
               <input type="hidden" name="order_id" value="<?PHP echo $_GET['authenticate']; ?>">
                <button type="submit" class="btn btn-default btn-sm">Submit</button>
            </div>
        </form>   
        <?PHP
            }else{
        ?>
        <h1>Your Order for Online Laundry Services</h1>
        <p style="text-align:right;"><a href="iron_order.php?unset=true" class="btn btn-danger btn-sm btn-right">Clear Contents of Cart</a></p>
        <table class="table table-striped" width="100%">
            <tr>
                <th width="10%">Service</th>
                <th width="72px">Icon</th>
                <th>Article</th>
                <th>Category</th>
                <th width="10%">Quantity</th>
                <th width="15%">Price/Item</th>
                <th width="12%">Sub-total</th>
                <th width="15%" align="center">Actions</th>
            </tr>
        <?PHP 
            foreach($_SESSION['cart'] as $id => $qty):
            
            $q = "SELECT * FROM cloth_iron WHERE id = '$id' LIMIT 1";
            $r = $db->select($q);
            
            $prod = $r->fetch_array();
            
            $price = $prod['price']*$qty;
            ?>
            <tr>
               <form action="laundry.php?update=true" method="post">
               <input type="hidden" name="id" value="<?PHP echo $prod['id']; ?>">
                <td><?PHP echo ucfirst($prod['service']); ?></td>
                <td><img src="iron/<?PHP echo $prod['cloth_icon']; ?>" alt="<?PHP echo ucfirst($prod['cloth']); ?>" title="<?PHP echo ucfirst($prod['cloth']); ?>" class="img-responsive"></td>
                <td><?PHP echo ucfirst($prod['cloth']); ?></td>
                <td><?PHP echo ucfirst($prod['category']); ?></td>
                <td><input type="text" name="qty" value="<?PHP echo $qty; ?>" size="4"></td>
                <td>Rs. <?PHP echo $prod['price']; ?>.00</td>
                <td>Rs. <?PHP echo number_format($price); ?>.00</td>
                <td align="middle">
                
                <button type="submit" class="btn btn-xs btn-success" title="Update Items"><i class="fa fa-refresh fa-fw" aria-hidden="true"></i></button> 
                
                <a href="laundry.php?delete=<?PHP echo $id; ?>" class="btn btn-danger btn-xs" title="Remove Item"><i class="fa fa-trash fa-fw" aria-hidden="true"></i></a>
                </td>
                </form>
            </tr>
        <?PHP 
            
            $total_price += $price;
            endforeach; 
        ?>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">Total Articles :</td>
            <td><?PHP echo $_SESSION['total_items']; ?></td>
            <td align="right">Total Price :</td>
            <td>Rs. <?PHP echo $total_price; ?>.00</td>
            <td></td>
        </tr>
        <?PHP
                $req = '';
                $pickup = '';
            if($total_price < 100){
                
                unset($_SESSION['coupon_code']);
                unset($_SESSION['amount']);
                unset($_SESSION['discount']);
                
                $req = 100 - $total_price;
                $pickup = 20;
               echo '<tr><td colspan="8" align="center"><p class="help_block">'."For FREE Pick up and Delivery and Use of Gift Coupons add more articles of ".'<span style="color:red; font-weight:bold;">'." Rs. ".$req.".00 ".'</span><a href="laundry.php" class="btn btn-default btn-sm">'."Go To Cart".'</a></p></td></tr>';
                echo '<tr><td colspan="2"></td><td colspan="4" align="right">'."Pick Up Charges :".'</td><td colspan="2">'."Rs. 20.00".'</td></tr>';

            }else{
                $pickup = 0;
            }            
            
            if($total_price < 100){
                
            }elseif(isset($_SESSION['coupon_code'])){
                $coupon_code = $_SESSION['coupon_code'];
                $amount = $_SESSION['amount'];
                $discount = $_SESSION['discount'];
                
                if($amount !== '0'){
                    $total_price = $total_price - $amount;
                    $less = $amount;
                   
                }elseif($discount !== '0'){
                    $less = $total_price * ($discount / 100);
                    $total_price = $total_price - $less;
                }
                
                echo '<tr>
                    <td colspan="8">';
                echo $coupon_code . " Coupon Successfully Applied.";
                echo '</td></tr>';
            }else
            //let us check the gift coupon thing here
            if(isset($_GET['gift_coupon'])){
                echo '<tr>
                    <td colspan="8">';
                $coupon_code = strtoupper($_POST['coupon_code']);
                
                //let us get the discount and apply here                    
                $q = "SELECT * FROM coupons WHERE coupon_code = '$coupon_code' LIMIT 1";
                $r = $db->select($q);
                    
                if($r){
                    
                $coupon = $r->fetch_array();
                $amount = $coupon['amount'];
                $discount = $coupon['discount'];
                $info = $coupon['info'];
                $valid_from = $coupon['valid_from'];
                $valid_upto = $coupon['valid_upto'];
                $valid_for = $coupon['valid_for'];
                $min_spend = $coupon['min_spend'];
                $today = date('d/m/Y');
                
                if($valid_upto > $today){
                    
                    if($valid_for == 'users'){
                        //check if the user is logged in 
                        if(isset($_SESSION['id'])){
                            if($amount!==0){
                                $total_price = $total_price - $amount;
                                $less = $amount;
                                
                                $_SESSION['coupon_code'] = $coupon_code;
                                $_SESSION['amount'] = $amount;
                                $_SESSION['discount'] = '0';
                                
                            }elseif($discount !==0){
                                $less = $total_price * ($discount / 100);
                                $total_price = $total_price - $less;
                                $_SESSION['coupon_code'] = $coupon_code;
                                $_SESSION['discount'] = $discount;
                                $_SESSION['amount'] = '0';
                            }
                            
                            echo $coupon_code . " Coupon Successfully Applied.";
                            
                        }else{
                            echo "Coupon valid for Registered Users Only.<br>";
                            echo "Please log in / Register to get this discount.<br>";
                        }
                    }else{
                        if($amount !== '0'){
                            $total_price = $total_price - $amount;
                            
                            $less = $amount;
                            
                            $_SESSION['coupon_code'] = $coupon_code;
                            $_SESSION['amount'] = $amount;
                            $_SESSION['discount'] = '0';
                            
                        }elseif($discount !== '0'){                            

                            $less = $total_price * ($discount / 100);
                            $total_price = $total_price - $less;
                            $_SESSION['coupon_code'] = $coupon_code;
                            $_SESSION['discount'] = $discount;
                            $_SESSION['amount'] = '0';
                        }
                         echo $coupon_code . " Coupon Successfully Applied.";
                    }
                }else{
                   echo "The Coupon Code is invalid. ";
                   echo '<a href="iron_order.php" class="btn btn-default">'."Try another Coupon".'</a>';
                }
                }else{
                    echo "The coupon You entered does not exist.<br>";
                    echo '<a href="iron_order.php" class="btn btn-default">'."Try another Coupon".'</a>';
                }
                
                echo '</td></tr>';
            
            }else{
                
            ?>
            <tr>                
            <td colspan="3"></td>
            <form action="iron_order.php?gift_coupon=true" method="post">
            <td colspan="2" align="right"><span style="color:red;">Do You Have a Gift Coupon : </span></td>
            <td colspan="3"><input type="text" placeholder="Gift Coupon Code" name="coupon_code" id="gift_coupon" size="20">&nbsp; &nbsp;<button type="submit" class="btn btn-success btn-sm">Apply</button></td>           
        </tr>
           </form>
            <?PHP
            }            
            ?>
        <tr>
            <td colspan="4">
                <?PHP
                    if($total_price > 100){
                        echo '<p>'.'<i class="fa fa-smile-o" aria-hidden="true"></i>' . " FREE Pickup and Drop For You".'</p>';
                    }
                ?>
            </td>
            <td colspan="2" align="right">Final Billing Amount :</td>           
            <td colspan="2">Rs. 
            <?PHP 
                $final_price = $total_price+$pickup;
            
                echo number_format((float)$final_price, 2, '.', ''); 

                if(isset($_SESSION['coupon_code'])){
                    echo '<span style="color:red;">'." [ Rs. ". number_format((float)$less, 2, '.', ''). " Less ]".'</span>';
                }
                ?>
            </td>
        </tr>
        </table>
        
        <h3>Fill Up Your Details : </h3>
        <form class="form-horizontal" action="iron_order.php?place_order=true" method="post">
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Your Name :</label>
                <div class="col-sm-9">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['name']; } ?>" required>
                </div>
            </div>
            
            <input type="hidden" name="total_price" value="<?PHP echo $total_price; ?>">
            <input type="hidden" name="final_price" value="<?PHP echo $final_price; ?>">
            <input type="hidden" name="coupon_code" value="<?PHP if(isset($_SESSION['coupon_code'])){ echo $_SESSION['coupon_code']; }else{ echo "";} ?>">
            <input type="hidden" name="discount" value="<?PHP if(isset($_SESSION['coupon_code'])){ echo $less; }else{ echo "0";} ?>">
            
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Your eMail ID :</label>
                <div class="col-sm-9">
                  <input type="email" name="email" class="form-control" id="email" placeholder="Your eMail ID" value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['email']; } ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label for="mobile" class="col-sm-3 control-label">Your Mobile No. :</label>
                <div class="col-sm-9">
                  <input type="text" name="phone" class="form-control" id="mobile" placeholder="Your Mobile No" value="<?PHP if(isset($_SESSION['id'])){ echo $_SESSION['phone']; } ?>" required>
                  <p class="help-block">You will receive an OTP on your Mobile to confirm your Order.</p>
                </div>
            </div>
            
            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Your Address :</label>
                <div class="col-sm-9">
                 <textarea name="address" id="address" cols="60" rows="5" placeholder="Type the address for Pickup" required></textarea>
                  <p class="help-block">We can not serve orders from outside Berhampur now.</p>
                </div>
            </div>
            
            <div class="form-group">
                <label for="Submit" class="col-sm-3 control-label">Submit</label>
                <div class="col-sm-9">
                 <button type="submit" class="btn btn-default" role="button">Submit</button>
                </div>
            </div>
        </form>
<?PHP
        }
    }
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>