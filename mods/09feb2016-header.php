<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<head>
    <title>NILTIK.com : Neither in Link, Together in Klick</title>
    
    <!--Bootstrap css here-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--Awesome fonts css here-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--Custom css here-->
    <link rel="stylesheet" href="css/styles.css">
    <!--Floating button css here-->
    <link rel="stylesheet" href="css/mfb.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400' rel='stylesheet' type='text/css'>
    <!--We can add a favicon here-->
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="stylesheet" type="text/css" href="slick/slick.css">
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
    <!-- for dynamic drop down list -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/dropdown.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
      <li class="mfb-component__wrap">
        <a href="#" class="mfb-component__button--main">
          <i class="mfb-component__main-icon--resting ion-plus-round"></i>
          <i class="mfb-component__main-icon--active ion-close-round"></i>
        </a>
        <ul class="mfb-component__list">
          <li>
            <a href="post_free_ad.php" data-mfb-label="Post a Free Ad" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-pencil fa-fw"></i>
            </a>
          </li>
          
          <li>
            <a href="house.php" data-mfb-label="Find a Home for You" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-home fa-fw" aria-hidden="true"></i>
            </a>
          </li>
          
          <li>
            <a href="laundry.php" data-mfb-label="Book Iron Solution" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-refresh fa-fw"></i>
            </a>
          </li>
        </ul>
      </li>
    </ul>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">
          <span class="col-xs-3"><img src="img/favicon.png" alt="Niltik.com" width="35px" style="margin-top:-10px;"></span><span class="col-xs-4 francois">Niltik.com</span>
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="aboutus.php"><i class="fa fa-book" aria-hidden="true"></i> About Us</a></li>
            
            <li><a href="buynsell.php"><i class="fa fa-wpforms" aria-hidden="true"></i>Buy Products</a></li>
            <li><a href="laundry.php"><i class="fa fa-cart-plus" aria-hidden="true"></i> <span class="blink_me">Book Laundry</span></a></li>
<li><a href="feedback.php"><i class="fa fa-comment fa-fw" aria-hidden="true"></i>User Feedback</a></li>
            <li><a href="contact_us.php"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contact Us</a></li>
            <?PHP
                if(isset($_SESSION['user'])){
                    echo '<li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a></li>';
                }  
            ?>        
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
<div class="clearfix"></div>
<br>
<?PHP
$url = basename($_SERVER['PHP_SELF']);
if($url == 'index.php'){
?>
    <div class="container">
        <div class="row">

            <div class="col-md-2 hidden-xs text-center">
                <a href="index.php"><img src="img/site_logo.png" alt="Niltik.com" width="72px"></a>
            </div>
            <div class="col-md-7 col-xs-12 text-center">

                 <form class="form-inline main_form" action="search.php?submit=true" method="post" style="margin-bottom:20px;">
               
                 <div class="form-group">
                     <input type="text" name="term" class="form-control" id="iconified" placeholder="&#xF002; Search Product" style="font-family:Arial, FontAwesome" required>
                 </div>
                  
                  <div class="form-group hidden-xs">
                    <label for="product_id"></label>
                    <select name="product_id" class="form-control">
                        <option value="">Select Product</option>
                        <?PHP
                        //let us get the locations from database dynamically
                        $q = "SELECT * FROM products WHERE parent_id = 0";
                        $r = $db->select($q);
                        
                        while($product = $r->fetch_array()):
                     ?>
                     <option value="<?PHP echo $product['id']; ?>"><?PHP echo $product['product_name']; ?></option>
                     <?PHP endwhile; ?>
                    </select>
                    
                  </div>
                  <div class="form-group hidden-xs">
                    <label for="location_id"></label>
                    <select class="form-control" name="location_id">
                    <option value="">Location</option>
                     <?PHP
                        //let us get the locations from database dynamically
                        $q = "SELECT * FROM location WHERE city_id = 0";
                        $r = $db->select($q);
                        
                        while($location = $r->fetch_array()):
                     ?>
                     <option value="<?PHP echo $location['id']; ?>"><?PHP echo $location['location_name']; ?></option>
                     <?PHP endwhile; ?>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-default hidden-xs"><i class="fa fa-search fa-fw" aria-hidden="true"></i> Search</button>
                </form>
  
  <div class="clearfix"></div>
  <div class="row">
            
            </div>
            <div class="col-md-3 col-xs-8">
<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal"><i class="fa fa-bolt" aria-hidden="true"></i> -- NILTIK Assured -- <i class="fa fa-bolt" aria-hidden="true"></i></button>
              <br><br>
              <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

	<!-- Modal content-->
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">NILTIK Assured</h4>
	</div>
	<div class="modal-body text-left">
	<ul>
	<li>We Ensure sale of Your Products (* For a small FEE).</li>
	<li>Let our experts evaluate your product and fix a price.</li>
    <li>Evaluation can be done in our office or at your place.</li>
    <li>Large or Immovable products can be evaluated on spot.</li>
	<li>We will sell your product within a fixed duration.</li>
    <li>Do not wait for someone to click and buy your product.</li>
	<li>If we can not sale, we will refund your fee.</li>
	<li>Currently we assure Bikes, Vehicles, Mobiles, Computers, home rent only.</li>
	<li>We will add more categories soon...!!!</li>

<a href="post_free_ad.php" data-mfb-label="Post a Free Ad" class="mfb-component__button--child">
              <i class="mfb-component__child-icon fa fa-pencil fa-fw"></i>
            </a>
          </li>
	</ul>
	</div>

	<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
	</div>
	</div>
	</div>
              <!--MODAL ENDS HERE-->
               <?PHP
                    if(!isset($_SESSION['user'])){
                ?>
                <a href="user.php" class="btn btn-info"><i class="fa fa-user" aria-hidden="true"></i> Login</a>
                <?PHP
                    }else{
                ?>
                 <a href="logout.php" class="btn btn-info"><i class="fa fa-user" aria-hidden="true"></i> Log out</a>
                 <?PHP
                    }
                ?>
                  &nbsp; &nbsp;
                <a href="post_free_ad.php" class="btn btn-success">Post a Free Ad</a>
 <div class="clearfix"></div><br>
                <a href="post_house_ad.php" class="btn btn-primary">Post FREE ad for House Rent</a>
            </div>
<!--showing only on mobile-->
<div class="col-xs-4 hidden-md hidden-sm hidden-lg">
	<a href="laundry.php" class="btn btn-warning btn-md" style="margin-top:15px;"><span class="francois blink_me" style="font-size:1.2em;">Book <br>Laundry</span></a>
</div>
<!--showing only on mobile-->
        </div>
    </div>
<?PHP
}else{
?>
    <div class="container">
        
    </div>
<?PHP
}
?> 
    
   <div class="container" style="margin-bottom:40px;">
        <div class="row">
            <div class="col-md-2 hidden-xs">

           <hr>
<h3>QR Code Join Us</h3>
<img src="img/qr_code_niltik.jpg" class="img-responsive" />
<hr>
 
<hr>
<h3>QR Code Join Us</h3>
<img src="img/qr_code_niltik.jpg" class="img-responsive" />
<hr>
            </div>
            <div class="col-md-8 col-xs-12">
            
            <?PHP
        if(isset($_SESSION['user'])){
        ?>
        <nav class="navbar navbar-inverse">
                  <div class="row">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>                      
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-center" id="bs-example-navbar-collapse-2">
                      <ul class="nav navbar-nav">
                        
                        <li><a href="user.php"> Dashboard</a></li>
                        <li><a href="laundry.php"> Book Laundry</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="user.php?profile=true">View Profile</a></li>
                            <li><a href="user.php?edit=true">Edit Details</a></li>
                            <li><a href="user.php?change_pass=true">Change Password</a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Services <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="user.php?iron=true">Iron Solutions</a></li>
                            <li><a href="user.php?housead=true">House Rent</a></li>
                            <li><a href="user.php?freead=true">Buy and Sell Ads</a></li>
                          </ul>
                        </li>
                        <li><a href="post_free_ad.php">Post a Free Ad</a></li>      
                        <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out </a></li>        
                      <!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <hr>
        <?PHP
        }
        ?>   