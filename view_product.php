<?PHP
    session_start();
    include('inc/config.php');    
    include('inc/db_conn.php');
include('inc/functions.php');
    include('inc/paginate.php');
	
	//let us initiate an instance of database connection
	$db = new connection();
	
	//let us initiate a new pagination class
	$pagination = new pagination();
    include('inc/meta.php');

    include('mods/header.php');

    $id = $_GET['id'];
    
    $qh = "UPDATE classifieds SET hits = hits+1 WHERE id = '$id'";
    $rh = $db->update($qh);

    $q = "SELECT * FROM classifieds WHERE id = '$id'";
    $r = $db->select($q);

    $ad = $r->fetch_array();
?>
    <div class="row">
         <h3><?PHP echo $ad['ad_title']; ?> &nbsp; &nbsp; &nbsp; &nbsp; [Product ID : <?PHP echo $ad['id']; ?> ]  <span style="float:right;color:red;">( # <?PHP echo $ad['hits']; ?> Views) </span> </h3>
                    
                        <table width="100%" class="table table-striped">
<tr>
                                <td colspan="2" align="center">
                                  <?PHP
                                    $otp = $ad['otp'];
                                    $otp2 = $ad['otp2'];
                                    $q3 = "SELECT * FROM uploads WHERE otp = '$otp' AND otp2 = '$otp2'";
                                    $r3 = $db->select($q3);
                                  
                                    if(!$r3){
                                        echo "NO Images Uploaded.<br>";
                                    }else{
                                        while($img = $r3->fetch_array()):
                                                                  
                                  ?>
                                   
                                    <a href="uploads/<?PHP echo $img['image']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?PHP echo $ad['ad_title']; ?>">
                                    <img src="uploads/<?PHP echo $img['image']; ?>" class="img-responsive house_img">
                                    </a>

                                   <?PHP endwhile; } ?>
                                </td>
                            </tr>
<tr>
                                <td>Price :</td>
                                <td><span style="font-size:1.6em; color:#ff6600;">Rs. <?PHP echo $ad['price']; ?>.00 (Negotiable)</span></td>
                            </tr>
                            <tr>
                                <td width="180px">Name :</td>
                                <td><?PHP echo ucfirst($ad['name']); ?></td>
                            </tr>
<tr>
                                <td>Contact :</td>
                                <td><?PHP echo $ad['phone']; ?> <span class="francois blink_me"> [ PHONE NUMBER VERIFIED ]</span>
                                <div class="text-right">
                                    <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Buyer Tips</button>
<br>
<p class="help-block">When You call, Do not forget to mention that you found this Ad on <strong>NILTIK.COM</strong></p>
                                <!-- Modal -->
                                <div id="myModal" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Tips for Safety</h4>
                                      </div>
                                      <div class="modal-body text-left">
                                        <p>
                                          <ul>
                                              <li>Call the Owner before Visiting.</li>
                                              <li>Visit at the right time as decided.</li>
                                              <li>Please ensure you have checked all details before paying for the article.</li>
                                              <li>Check for valid documents while purchasing electronic gadgets, bikes and cars.</li>
                                          </ul>
                                          </p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Address :</td>
                                <td><p><?PHP echo nl2br($ad['address']); ?></p></td>
                            </tr>
                            <tr>
                                <td>City / Town :</td>
                                <td><p>
                                <?PHP 
                                  $location = $ad['location_id'];
                                    $location = explode(",", $location);

                                    $location_id = $location[0];
                                    $locality_id = $location[1];
                                    
                                    $q1 = "SELECT * FROM location WHERE id = '$location_id'";
                                    $r1 = $db->select($q1);
					if(!$r1){
					}else{
                                    $loc = $r1->fetch_array();
                                  
                                  echo $loc['location_name'];
                                  }
                                ?>
                                </p></td>
                            </tr>
                            <tr>
                                <td>Nearest Street / Square :</td>
                                <td><p>
                                <?PHP 
                                  if($locality_id == ''){
                                      echo "Unknown / Other";
                                  }else{ 
                                  $q2 = "SELECT * FROM location WHERE id = '$locality_id'";
                                    $r2 = $db->select($q2);

                                    $loc2 = $r2->fetch_array();
                                  
                                  echo $loc2['location_name'];
                                  }
                                ?>
                                </p></td>
                            </tr>
                            
                            <tr>
                                <td>Details : </td>
                                <td><p><?PHP echo nl2br($ad['ad_details']); ?></p></td>
                            </tr>
                            
<?PHP
                                   $timestamp = strtotime($ad['posted_on']);
                   
                                   $p_day = date('d', $timestamp);
                                   $p_month = date('M', $timestamp);
                                   $p_year = date('Y', $timestamp);
                            ?>
                            <tr>
                                <td>Posted on :</td>
                                <td><?PHP echo "$p_day $p_month, $p_year"; ?></td>
                            </tr>
<tr>
<td colspan="2">
<iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=
<?PHP echo urlencode($loc2['location_name']).",".urlencode($loc['location_name']); ?>
&t=m&ll=,&z=12&iwloc=&output=embed"></iframe>
</td>
</tr>
                            
                        </table>
                        <hr>
<?PHP
    if($_SESSION['phone'] == $ad['phone']){
        echo '<a href="edit_free_ad.php?id='.$id.'" class="btn btn-info btn-sm">'."Edit this Ad".'</a>';
    }
?>

<div class="col-sm-6 col-xs-6">
    <?PHP prev_button($id); ?>
</div>
<div class="col-sm-6 col-xs-6 text-right">
    <?PHP next_button($id); ?>
</div>
<div class="clearfix"></div> 
<br><br>
<a href="javascript:history.back()" class="btn btn-primary btn-right" style="float:right;">Go Back to Previous Page</a>
    
<?PHP
    include("mods/trending_ads.php");

    include("mods/footer.php");
?>